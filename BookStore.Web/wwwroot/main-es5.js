(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./admin-panel/adminpanel.module": "./src/app/admin-panel/adminpanel.module.ts",
	"./authentication/authentication.module": "./src/app/authentication/authentication.module.ts",
	"./bookstore/bookstore.module": "./src/app/bookstore/bookstore.module.ts",
	"./menu/menu.module": "./src/app/menu/menu.module.ts"
};

function webpackAsyncContext(req) {
	return Promise.resolve().then(function() {
		if(!__webpack_require__.o(map, req)) {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		}

		var id = map[req];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/admin-page/admin-page.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/admin-page/admin-page.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"image\">\r\n  <app-mainpage></app-mainpage>\r\n  <div class=\"wind\">\r\n    <div class=\"cont\">\r\n      <a class=\"header\">Admin Page</a>\r\n      <div class=\"row\">\r\n        <div class=\"my-div1\">\r\n          <h1 class=\"text\">Editing Books</h1>\r\n          <button\r\n            mdbBtn\r\n            type=\"button\"\r\n            gradient=\"peach\"\r\n            rounded=\"true\"\r\n            mdbWavesEffect\r\n            [routerLink]=\"[{ outlets: { editing: ['book-editing'] } }]\"\r\n          >\r\n            Edit Books\r\n          </button>\r\n        </div>\r\n        <div class=\"my-div2\">\r\n          <h1 class=\"text\">Editing Magazines</h1>\r\n          <button\r\n            mdbBtn\r\n            type=\"button\"\r\n            gradient=\"peach\"\r\n            rounded=\"true\"\r\n            mdbWavesEffect\r\n            [routerLink]=\"[{ outlets: { editing: ['magazine-editing'] } }]\"\r\n          >\r\n            Edit Magazines\r\n          </button>\r\n          <div class=\"my-div3\">\r\n            <h1 class=\"text\">Editing Authors</h1>\r\n            <button\r\n              mdbBtn\r\n              type=\"button\"\r\n              gradient=\"peach\"\r\n              rounded=\"true\"\r\n              mdbWavesEffect\r\n              [routerLink]=\"[{ outlets: { editing: ['author-editing'] } }]\"\r\n            >\r\n              Edit Authors\r\n            </button>\r\n          </div>\r\n\r\n          <div class=\"my-div4\">\r\n            <h1 class=\"text\">Editing Genres</h1>\r\n            <button\r\n              mdbBtn\r\n              type=\"button\"\r\n              gradient=\"peach\"\r\n              rounded=\"true\"\r\n              mdbWavesEffect\r\n              [routerLink]=\"[{ outlets: { editing: ['genre-editing'] } }]\"\r\n            >\r\n              Edit Genres\r\n            </button>\r\n          </div>\r\n        </div>\r\n        <div class=\"my-div6\">\r\n          <h1 class=\"text\">Editing Users</h1>\r\n          <button\r\n            mdbBtn\r\n            type=\"button\"\r\n            gradient=\"peach\"\r\n            rounded=\"true\"\r\n            mdbWavesEffect\r\n            [routerLink]=\"[{ outlets: { editing: ['user-editing'] } }]\"\r\n          >\r\n            Edit Users\r\n          </button>\r\n        </div>\r\n        <div class=\"my-div5\">\r\n          <h1 class=\"text\">Editing Orders</h1>\r\n          <button\r\n            mdbBtn\r\n            type=\"button\"\r\n            gradient=\"peach\"\r\n            rounded=\"true\"\r\n            mdbWavesEffect\r\n            [routerLink]=\"[{ outlets: { editing: ['order-editing'] } }]\"\r\n          >\r\n            View Orders\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div><router-outlet name=\"editing\"></router-outlet></div>\r\n  </div>\r\n</body>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/author-editing/author-editing.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/author-editing/author-editing.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    <div class=\"myForm\">\r\n      <div class=\"create\">\r\n        <button (click)=\"openCreateAuthorDialog()\" class=\"btn btn-default\">\r\n          Create Author\r\n        </button>\r\n      </div>\r\n      <kendo-grid\r\n        [data]=\"gridData\"\r\n        [pageSize]=\"gridState.take\"\r\n        [skip]=\"gridState.skip\"\r\n        [sort]=\"gridState.sort\"\r\n        [filter]=\"gridState.filter\"\r\n        [sortable]=\"true\"\r\n        [pageable]=\"true\"\r\n        filterable=\"menu\"\r\n        (dataStateChange)=\"dataStateChange($event)\"\r\n        [height]=\"533\"\r\n        (edit)=\"editHandler($event)\"\r\n        (cancel)=\"cancelHandler($event)\"\r\n        (save)=\"saveHandler($event)\"\r\n        (remove)=\"removeHandler($event)\"\r\n        (add)=\"addHandler($event)\"\r\n        [navigable]=\"true\"\r\n      >\r\n        <kendo-grid-column field=\"name\" editor=\"string\" title=\"Name\">\r\n        </kendo-grid-column>\r\n        <kendo-grid-column title=\"command\" width=\"250\">\r\n          <ng-template\r\n            kendoGridCellTemplate\r\n            let-isNew=\"isNew\"\r\n            let-dataItem=\"datItem\"\r\n          >\r\n            <button kendoGridEditCommand [primary]=\"true\">Edit</button>\r\n            <button class=\"sad\" kendoGridRemoveCommand>Remove</button>\r\n            <button kendoGridSaveCommand>\r\n              {{ isNew ? \"Add\" : \"Update\" }}\r\n            </button>\r\n          </ng-template>\r\n        </kendo-grid-column>\r\n      </kendo-grid>\r\n    </div>\r\n  </mat-dialog-content>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/book-editing/book-editing.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/book-editing/book-editing.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    <div class=\"myForm\">\r\n      <div class=\"create\">\r\n        <button (click)=\"openCreateBookDialog()\" class=\"btn btn-default \">\r\n          Create Book\r\n        </button>\r\n      </div>\r\n      <kendo-grid\r\n        [data]=\"gridData\"\r\n        [pageSize]=\"gridState.take\"\r\n        [skip]=\"gridState.skip\"\r\n        [sort]=\"gridState.sort\"\r\n        [filter]=\"gridState.filter\"\r\n        [sortable]=\"true\"\r\n        [pageable]=\"true\"\r\n        filterable=\"menu\"\r\n        (dataStateChange)=\"dataStateChange($event)\"\r\n        [height]=\"533\"\r\n        (edit)=\"editHandler($event)\"\r\n        (cancel)=\"cancelHandler($event)\"\r\n        (save)=\"saveHandler($event)\"\r\n        (remove)=\"removeHandler($event)\"\r\n        (add)=\"addHandler($event)\"\r\n        [navigable]=\"true\"\r\n      >\r\n        <kendo-grid-column title=\"Image\" width=\"70\">\r\n          <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n            <div class=\"div-zoom\">\r\n              <img class=\"enlarge\" src=\"{{ dataItem.picture }}\" alt=\"image\" />\r\n            </div>\r\n          </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"title\" editor=\"string\" title=\"Title\">\r\n        </kendo-grid-column>\r\n        <kendo-grid-column\r\n          field=\"price\"\r\n          editor=\"numeric\"\r\n          title=\"Price\"\r\n          width=\"280\"\r\n          filter=\"numeric\"\r\n          format=\"{0:c}\"\r\n        >\r\n        </kendo-grid-column>\r\n        <kendo-grid-column title=\"Author\">\r\n          <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n            {{ getAuthorList(dataItem) }}\r\n          </ng-template>\r\n          <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n            <kendo-multiselect\r\n              name=\"dataItem.authorIdList\"\r\n              [data]=\"authorIdList.authorList\"\r\n              textField=\"name\"\r\n              valueField=\"id\"\r\n              [(ngModel)]=\"dataItem.authorIdList\"\r\n              (valueChange)=\"authorChange($event)\"\r\n            >\r\n            </kendo-multiselect>\r\n          </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column title=\"Genre\">\r\n          <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n            {{ getGenreList(dataItem) }}\r\n          </ng-template>\r\n          <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n            <kendo-multiselect\r\n              name=\"dataItem.genreIdList\"\r\n              [data]=\"genreIdList.genreList\"\r\n              textField=\"title\"\r\n              valueField=\"id\"\r\n              [(ngModel)]=\"dataItem.genreIdList\"\r\n              (valueChange)=\"genreChange($event)\"\r\n            >\r\n            </kendo-multiselect>\r\n          </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-command-column title=\"command\" width=\"250\">\r\n          <ng-template\r\n            kendoGridCellTemplate\r\n            let-isNew=\"isNew\"\r\n            let-dataItem=\"dataItem\"\r\n          >\r\n            <button kendoGridEditCommand [primary]=\"true\">Edit</button>\r\n            <button class=\"sad\" kendoGridRemoveCommand>Remove</button>\r\n            <button kendoGridSaveCommand>\r\n              {{ isNew ? \"Add\" : \"Update\" }}\r\n            </button>\r\n            <button kendoGridCancelCommand>\r\n              {{ isNew ? \"Discard changes\" : \"Cancel\" }}\r\n            </button>\r\n          </ng-template>\r\n        </kendo-grid-command-column>\r\n      </kendo-grid>\r\n    </div>\r\n  </mat-dialog-content>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/create-item-author/create-item-author.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/create-item-author/create-item-author.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    <form [formGroup]=\"formGroup\" class=\"credit-card\">\r\n      <h4>Create Author</h4>\r\n\r\n      <div class=\"from-body\">\r\n        <input\r\n          type=\"text\"\r\n          class=\"myForm\"\r\n          placeholder=\"Name\"\r\n          formControlName=\"name\"\r\n        />\r\n      </div>\r\n      <button (click)=\"onSubmit()\" type=\"submit\" class=\"paypal-btn\">\r\n        Create\r\n      </button>\r\n      <button mat-dialog-close>Cancel</button>\r\n    </form>\r\n  </mat-dialog-content>\r\n</body>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/create-item-book/create-item-book.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/create-item-book/create-item-book.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    <form [formGroup]=\"formGroup\" class=\"credit-card\">\r\n      <h4>Create new Book</h4>\r\n      <div class=\"from-body\">\r\n        <input\r\n          type=\"text\"\r\n          class=\"myForm\"\r\n          placeholder=\"Title\"\r\n          formControlName=\"title\"\r\n        />\r\n        <div class=\"myForm\">\r\n          <input type=\"text\" placeholder=\"Price\" formControlName=\"price\" />\r\n        </div>\r\n        <div class=\"myForm\">\r\n          <input\r\n            type=\"text\"\r\n            placeholder=\"Description\"\r\n            formControlName=\"description\"\r\n          />\r\n        </div>\r\n        <div class=\"from-body\">\r\n          <ng-select\r\n            class=\"inp\"\r\n            [multiple]=\"true\"\r\n            formControlName=\"genreIdList\"\r\n          >\r\n            <ng-option\r\n              *ngFor=\"let genre of genreIdList.genreList\"\r\n              [value]=\"genre.id\"\r\n              >{{ genre.title }}</ng-option\r\n            >\r\n          </ng-select>\r\n        </div>\r\n        <div class=\"from-body\">\r\n          <ng-select\r\n            class=\"inp\"\r\n            [multiple]=\"true\"\r\n            formControlName=\"authorIdList\"\r\n          >\r\n            <ng-option\r\n              *ngFor=\"let author of authorIdList.authorList\"\r\n              [value]=\"author.id\"\r\n              >{{ author.name }}</ng-option\r\n            >\r\n          </ng-select>\r\n        </div>\r\n        <div class=\"myForm\">\r\n          <input\r\n            class=\"inp2\"\r\n            type=\"file\"\r\n            (change)=\"onFileChanged($event)\"\r\n            formControlName=\"picture\"\r\n          />\r\n        </div>\r\n      </div>\r\n      <button (click)=\"onSubmit()\" type=\"submit\" class=\"paypal-btn\">\r\n        Create\r\n      </button>\r\n      <button mat-dialog-close>Cancel</button>\r\n    </form>\r\n  </mat-dialog-content>\r\n</body>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/create-item-genre/create-item-genre.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/create-item-genre/create-item-genre.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    <form [formGroup]=\"formGroup\" class=\"credit-card\">\r\n      <h4>Create Author</h4>\r\n\r\n      <div class=\"from-body\">\r\n        <input\r\n          type=\"text\"\r\n          class=\"myForm\"\r\n          placeholder=\"Name\"\r\n          formControlName=\"name\"\r\n        />\r\n      </div>\r\n      <button (click)=\"onSubmit()\" type=\"submit\" class=\"paypal-btn\">\r\n        Create\r\n      </button>\r\n      <button mat-dialog-close>Cancel</button>\r\n    </form>\r\n  </mat-dialog-content>\r\n</body>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/create-item-magazine/create-item-magazine.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/create-item-magazine/create-item-magazine.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    <form [formGroup]=\"formGroup\" class=\"credit-card\">\r\n      <h4>Create new Magazine</h4>\r\n      <div class=\"formBody\">\r\n        <input\r\n          type=\"text\"\r\n          class=\"myForm\"\r\n          placeholder=\"Title\"\r\n          formControlName=\"title\"\r\n        />\r\n        <div class=\"myForm\">\r\n          <input type=\"text\" placeholder=\"Price\" formControlName=\"price\" />\r\n        </div>\r\n        <div class=\"myForm\">\r\n          <input\r\n            type=\"text\"\r\n            placeholder=\"Description\"\r\n            formControlName=\"description\"\r\n          />\r\n        </div>\r\n      </div>\r\n      <div class=\"myForm\">\r\n        <input\r\n          class=\"inp2\"\r\n          type=\"file\"\r\n          (change)=\"onFileChanged($event)\"\r\n          formControlName=\"picture\"\r\n        />\r\n      </div>\r\n      <button (click)=\"onSubmit()\" type=\"submit\" class=\"paypal-btn\">\r\n        Create\r\n      </button>\r\n      <button mat-dialog-close>Cancel</button>\r\n    </form>\r\n  </mat-dialog-content>\r\n</body>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/create-user/create-user.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/create-user/create-user.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"overlay\">\n  <mat-dialog-content class=\"mat-typography\">\n    <form [formGroup]=\"formGroup\" class=\"credit-card\">\n      <h4>Create new User</h4>\n      <input\n        type=\"text\"\n        class=\"myForm\"\n        placeholder=\"Email\"\n        formControlName=\"email\"\n      />\n      <div class=\"myForm\">\n        <input placeholder=\"Password\" formControlName=\"password\" />\n      </div>\n      <div class=\"myForm\">\n        <select name=\"Role\" formControlName=\"role\" placeholder=\"choose role\">\n          <option value=\"user\">User</option>\n          <option value=\"admin\">Admin</option>\n        </select>\n      </div>\n      <button (click)=\"onSubmit()\" type=\"submit\" class=\"paypal-btn\">\n        Create\n      </button>\n      <button mat-dialog-close>Cancel</button>\n    </form>\n  </mat-dialog-content>\n</body>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/genre-editing/genre-editing.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/genre-editing/genre-editing.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    <div class=\"myForm\">\r\n      <div class=\"create\">\r\n        <button (click)=\"openCreateGenreDialog()\" class=\"btn btn-default\">\r\n          Create Genre\r\n        </button>\r\n      </div>\r\n      <kendo-grid\r\n        [data]=\"gridData\"\r\n        [pageSize]=\"gridState.take\"\r\n        [skip]=\"gridState.skip\"\r\n        [sort]=\"gridState.sort\"\r\n        [filter]=\"gridState.filter\"\r\n        [sortable]=\"true\"\r\n        [pageable]=\"true\"\r\n        filterable=\"menu\"\r\n        (dataStateChange)=\"dataStateChange($event)\"\r\n        [height]=\"533\"\r\n        (edit)=\"editHandler($event)\"\r\n        (cancel)=\"cancelHandler($event)\"\r\n        (remove)=\"removeHandler($event)\"\r\n        (add)=\"addHandler($event)\"\r\n        [navigable]=\"true\"\r\n      >\r\n        <kendo-grid-column field=\"title\" editor=\"string\" title=\"Title\">\r\n        </kendo-grid-column>\r\n        <kendo-grid-column title=\"command\" width=\"250\">\r\n          <ng-template\r\n            kendoGridCellTemplate\r\n            let-isNew=\"isNew\"\r\n            let-dataItem=\"datItem\"\r\n          >\r\n            <button class=\"sad\" kendoGridRemoveCommand>Remove</button>\r\n          </ng-template>\r\n        </kendo-grid-column>\r\n      </kendo-grid>\r\n    </div>\r\n  </mat-dialog-content>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/magazine-editing/magazine-editing.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/magazine-editing/magazine-editing.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    <div class=\"myForm\">\r\n      <div class=\"create\">\r\n        <button (click)=\"openCreateMagazineDialog()\" class=\"btn btn-default\">\r\n          Create Magazine\r\n        </button>\r\n      </div>\r\n      <kendo-grid\r\n        [data]=\"gridData\"\r\n        [pageSize]=\"gridState.take\"\r\n        [skip]=\"gridState.skip\"\r\n        [sort]=\"gridState.sort\"\r\n        [filter]=\"gridState.filter\"\r\n        [sortable]=\"true\"\r\n        [pageable]=\"true\"\r\n        filterable=\"menu\"\r\n        (dataStateChange)=\"dataStateChange($event)\"\r\n        [height]=\"533\"\r\n        (edit)=\"editHandler($event)\"\r\n        (cancel)=\"cancelHandler($event)\"\r\n        (save)=\"saveHandler($event)\"\r\n        (remove)=\"removeHandler($event)\"\r\n        (add)=\"addHandler($event)\"\r\n        [navigable]=\"true\"\r\n      >\r\n        <kendo-grid-column title=\"Image\" width=\"70\">\r\n          <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n            <div class=\"div-zoom\">\r\n              <img class=\"enlarge\" src=\"{{ dataItem.picture }}\" alt=\"image\" />\r\n            </div>\r\n          </ng-template>\r\n        </kendo-grid-column>\r\n        <kendo-grid-column field=\"title\" editor=\"string\" title=\"Title\">\r\n        </kendo-grid-column>\r\n        <kendo-grid-column\r\n          field=\"price\"\r\n          editor=\"numeric\"\r\n          title=\"Price\"\r\n          width=\"280\"\r\n          filter=\"numeric\"\r\n          format=\"{0:c}\"\r\n        >\r\n        </kendo-grid-column>\r\n        <kendo-grid-command-column title=\"command\" width=\"250\">\r\n          <ng-template\r\n            kendoGridCellTemplate\r\n            let-isNew=\"isNew\"\r\n            let-dataItem=\"dataItem\"\r\n          >\r\n            <button kendoGridEditCommand [primary]=\"true\">Edit</button>\r\n            <button class=\"sad\" kendoGridRemoveCommand>Remove</button>\r\n            <button kendoGridSaveCommand>\r\n              {{ isNew ? \"Add\" : \"Update\" }}\r\n            </button>\r\n            <button kendoGridCancelCommand>\r\n              {{ isNew ? \"Discard changes\" : \"Cancel\" }}\r\n            </button>\r\n          </ng-template>\r\n        </kendo-grid-command-column>\r\n      </kendo-grid>\r\n    </div>\r\n  </mat-dialog-content>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/order-editing/order-editing.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/order-editing/order-editing.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\">\n  <mat-dialog-content class=\"mat-typography\">\n    <div class=\"myForm\">\n      <kendo-grid\n        [data]=\"gridData\"\n        [pageSize]=\"gridState.take\"\n        [skip]=\"gridState.skip\"\n        [sort]=\"gridState.sort\"\n        [filter]=\"gridState.filter\"\n        [sortable]=\"true\"\n        [pageable]=\"true\"\n        filterable=\"menu\"\n        (dataStateChange)=\"dataStateChange($event)\"\n        [height]=\"533\"\n        (edit)=\"editHandler($event)\"\n        (cancel)=\"cancelHandler($event)\"\n        (add)=\"addHandler($event)\"\n        [navigable]=\"true\"\n      >\n        <kendo-grid-column field=\"email\" editor=\"string\" title=\"User Email\">\n        </kendo-grid-column>\n        <kendo-grid-column title=\"Ordered Book\">\n          <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\n            {{ getBookList(dataItem) }}\n          </ng-template>\n          <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\n            <kendo-multiselect\n              name=\"dataItem.books\"\n              [data]=\"books.books\"\n              textField=\"title\"\n              valueField=\"id\"\n              [(ngModel)]=\"dataItem.books\"\n            >\n            </kendo-multiselect>\n          </ng-template>\n        </kendo-grid-column>\n        <kendo-grid-column title=\"Ordered Magazine\">\n          <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\n            {{ getMagazineList(dataItem) }}\n          </ng-template>\n          <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\n            <kendo-multiselect\n              name=\"dataItem.magazines\"\n              [data]=\"magazines.magazines\"\n              textField=\"title\"\n              valueField=\"id\"\n              [(ngModel)]=\"dataItem.magazines\"\n            >\n            </kendo-multiselect>\n          </ng-template>\n        </kendo-grid-column>\n        <kendo-grid-column\n          field=\"amount\"\n          editor=\"numeric\"\n          title=\"Total Amount\"\n          width=\"280\"\n          filter=\"numeric\"\n          format=\"{0:c}\"\n        >\n        </kendo-grid-column>\n      </kendo-grid>\n    </div>\n  </mat-dialog-content>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-panel/user-editing/user-editing.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-panel/user-editing/user-editing.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\">\n  <mat-dialog-content class=\"mat-typography\">\n    <div class=\"myForm\">\n      <div class=\"create\">\n        <button (click)=\"openCreateUserDialog()\" class=\"btn btn-default \">\n          Create User\n        </button>\n      </div>\n      <kendo-grid\n        [data]=\"gridData\"\n        [pageSize]=\"gridState.take\"\n        [skip]=\"gridState.skip\"\n        [sort]=\"gridState.sort\"\n        [filter]=\"gridState.filter\"\n        [sortable]=\"true\"\n        [pageable]=\"true\"\n        filterable=\"menu\"\n        (dataStateChange)=\"dataStateChange($event)\"\n        [height]=\"533\"\n        (edit)=\"editHandler($event)\"\n        (remove)=\"removeHandler($event)\"\n        (cancel)=\"cancelHandler($event)\"\n        (add)=\"addHandler($event)\"\n        [navigable]=\"true\"\n      >\n        <kendo-grid-column field=\"email\" editor=\"string\" title=\"Email\">\n        </kendo-grid-column>\n        <kendo-grid-column field=\"userName\" editor=\"string\" title=\"User Name\">\n        </kendo-grid-column>\n        <kendo-grid-column field=\"lastName\" editor=\"string\" title=\"Last Name\">\n        </kendo-grid-column>\n        <kendo-grid-column field=\"firstName\" editor=\"string\" title=\"First Name\">\n        </kendo-grid-column>\n        <kendo-grid-column\n          field=\"phoneNumber\"\n          editor=\"string\"\n          title=\"Phone Number\"\n        >\n        </kendo-grid-column>\n        <kendo-grid-column field=\"role\" editor=\"string\" title=\"role\">\n        </kendo-grid-column>\n        <kendo-grid-command-column title=\"command\" width=\"250\">\n          <ng-template\n            kendoGridCellTemplate\n            let-isNew=\"isNew\"\n            let-dataItem=\"dataItem\"\n          >\n            <button kendoGridEditCommand [primary]=\"true\">Edit</button>\n            <button class=\"sad\" kendoGridRemoveCommand>Remove</button>\n            <button kendoGridSaveCommand>\n              {{ isNew ? \"Add\" : \"Update\" }}\n            </button>\n            <button kendoGridCancelCommand>\n              {{ isNew ? \"Discard changes\" : \"Cancel\" }}\n            </button>\n          </ng-template>\n        </kendo-grid-command-column>\n      </kendo-grid>\n    </div>\n  </mat-dialog-content>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/authentication/login/login.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/authentication/login/login.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"image\">\r\n  <form class=\"text-center\">\r\n    <form #f=\"ngForm\" novalidate (ngSubmit)=\"login(f)\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <div *ngIf=\"brandNew\" class=\"alert alert-success\" role=\"alert\">\r\n            <strong>All set!</strong> Please check your mail, and confirm\r\n            registration for continue registration.\r\n          </div>\r\n          <h2>Login</h2>\r\n        </div>\r\n      </div>\r\n      <div class=\"my-div1\">\r\n        <div class=\"form-group\">\r\n          <input\r\n            type=\"text\"\r\n            class=\"form-control\"\r\n            pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\"\r\n            id=\"email\"\r\n            name=\"email\"\r\n            ngModel\r\n            #emailref=\"ngModel\"\r\n            placeholder=\"Email\"\r\n          />\r\n          <div\r\n            *ngIf=\"emailref.errors && (emailref.touched || emailref.dirty)\"\r\n            class=\"aler alert-danger\"\r\n          >\r\n            <div class=\"hid\" [hidden]=\"!emailref.errors?.pattern\">\r\n              InvalidEmail\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"my-div2\">\r\n        <input\r\n          type=\"password\"\r\n          placeholder=\"Enter your Password\"\r\n          class=\"form-control\"\r\n          required\r\n          name=\"password\"\r\n          ngModel\r\n        />\r\n      </div>\r\n      <div class=\"my-div3\">\r\n        <button type=\"submit\" class=\"btn btn-link\">Login</button>\r\n      </div>\r\n      <div *ngIf=\"errors\" class=\"alert alert-danger\" role=\"alert\">\r\n        <strong>Ooops!</strong> {{ errors }}\r\n      </div>\r\n      <div class=\"my-div3\">\r\n        <a routerLink=\"/register\" class=\"btn btn-link\">Registragion</a>\r\n      </div>\r\n      <div class=\"my-div4\">\r\n        <button (click)=\"socialSignIn('google')\" class=\"my-but\">\r\n          <img\r\n            class=\"img\"\r\n            src=\"https://img.icons8.com/color/48/000000/google-logo.png\"\r\n          />\r\n        </button>\r\n      </div>\r\n    </form>\r\n  </form>\r\n</body>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/authentication/registration-completion/registration-completion.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/authentication/registration-completion/registration-completion.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"image\">\n  <form class=\"text-center\">\n    <div class=\"s\">\n      <h2>Welcome</h2>\n    </div>\n    <div class=\"a\">\n      <h2>Please, activate your account</h2>\n      <div class=\"my-div5\">\n        <button (click)=\"confirm()\" class=\"btn \">\n          Confirm\n        </button>\n      </div>\n    </div>\n  </form>\n</body>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/authentication/registration/registration.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/authentication/registration/registration.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"image\">\r\n  <form class=\"text-center\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <h2>Registration</h2>\r\n      </div>\r\n    </div>\r\n    <form #f=\"ngForm\" class=\"my-form\" novalidate (ngSubmit)=\"registerUser(f)\">\r\n      <div class=\"my-div\">\r\n        <input\r\n          class=\"form-control\"\r\n          type=\"text\"\r\n          id=\"fris-name\"\r\n          placeholder=\"Frist Name\"\r\n          name=\"firstName\"\r\n          ngModel\r\n        />\r\n      </div>\r\n      <div class=\"my-div2\">\r\n        <input\r\n          class=\"form-control\"\r\n          type=\"text\"\r\n          id=\"last-name\"\r\n          placeholder=\"Last Name\"\r\n          name=\"lastName\"\r\n          ngModel\r\n        />\r\n      </div>\r\n      <div class=\"my-div3\">\r\n        <div class=\"form-group\">\r\n          <input\r\n            type=\"text\"\r\n            class=\"form-control\"\r\n            pattern=\"^((\\+3|7|8)+([0-9]){10})$\"\r\n            id=\"phonenumber\"\r\n            name=\"phonenumber\"\r\n            ngModel\r\n            #phonenumber=\"ngModel\"\r\n            placeholder=\"Phone number +38091813381\"\r\n          />\r\n          <div\r\n            *ngIf=\"\r\n              phonenumber.errors && (phonenumber.touched || phonenumber.dirty)\r\n            \"\r\n            class=\"aler alert-danger\"\r\n          >\r\n            <div class=\"valid\" [hidden]=\"!phonenumber.errors?.pattern\">\r\n              PhoneNumber must be a valid email address\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"my-div3\">\r\n        <div class=\"form-group\">\r\n          <input\r\n            type=\"text\"\r\n            class=\"form-control\"\r\n            pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\"\r\n            id=\"email\"\r\n            name=\"email\"\r\n            ngModel\r\n            #emailref=\"ngModel\"\r\n            placeholder=\"Email\"\r\n          />\r\n          <div\r\n            *ngIf=\"emailref.errors && (emailref.touched || emailref.dirty)\"\r\n            class=\"aler alert-danger\"\r\n          >\r\n            <div class=\"valid\" [hidden]=\"!emailref.errors?.pattern\">\r\n              Email must be a valid email address\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"my-div4\">\r\n        <input\r\n          class=\"form-control\"\r\n          type=\"password\"\r\n          required\r\n          id=\"password\"\r\n          name=\"password\"\r\n          placeholder=\"Password\"\r\n          ngModel\r\n        />\r\n      </div>\r\n      <div class=\"my-div5\">\r\n        <button\r\n          type=\"submit\"\r\n          class=\"btn bnt-primary\"\r\n          [disabled]=\"f.invalid || isRequesting\"\r\n        >\r\n          Sing Up\r\n        </button>\r\n      </div>\r\n      <div class=\"my-div6\">\r\n        <a routerLink=\"/login\" class=\"btn btn-link\">Sing in</a>\r\n      </div>\r\n    </form>\r\n  </form>\r\n</body>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/bookstore/author/author.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/bookstore/author/author.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\r\n  <app-mainpage></app-mainpage>\r\n  <form class=\"my-form\">\r\n    <div class=\"row\">\r\n      <div class=\"col=xs=9 col-md-9\">\r\n        <p *ngFor=\"let author of authorList.authorList\">\r\n          {{ author.name }}\r\n        </p>\r\n      </div>\r\n    </div>\r\n    <button #anchor (click)=\"onToggle()\" class=\"k-button\">\r\n      {{ toggleText }}\r\n    </button>\r\n    <kendo-popup [anchor]=\"anchor\" *ngIf=\"show\">\r\n      <div class=\"inner-wrapper\">\r\n        <input\r\n          type=\"text\"\r\n          [(ngModel)]=\"author.name\"\r\n          [ngModelOptions]=\"{ standalone: true }\"\r\n        />\r\n        <button (click)=\"createAuthor()\">Create</button>\r\n      </div>\r\n    </kendo-popup>\r\n  </form>\r\n</nav>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/bookstore/book-description/book-description.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/bookstore/book-description/book-description.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    <h2 class=\"headline\">\r\n      <p>{{ bookDescription.title }}</p>\r\n    </h2>\r\n    <mat-card class=\"my-div2\">\r\n      <div class=\"wrapper\">\r\n        <div id=\"left\">\r\n          <img\r\n            class=\"enlarge\"\r\n            src=\"{{ bookDescription.picture }}\"\r\n            alt=\"image\"\r\n          />\r\n        </div>\r\n        <div id=\"right\">\r\n          About Book: {{ bookDescription.description }}\r\n          <div class=\"price\">Price: {{ bookDescription.price }}$</div>\r\n        </div>\r\n      </div>\r\n    </mat-card>\r\n    <mat-dialog-actions>\r\n      <button mdbBtn type=\"button\" color=\"info\" mdbWavesEffect mat-dialog-close>\r\n        Cancel\r\n      </button>\r\n      <button\r\n        (click)=\"onButtonClick(bookDescription.id)\"\r\n        mdbBtn\r\n        type=\"button\"\r\n        color=\"info\"\r\n        mdbWavesEffect\r\n      >\r\n        Add To Order\r\n      </button>\r\n    </mat-dialog-actions>\r\n  </mat-dialog-content>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/bookstore/book/book.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/bookstore/book/book.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"image\">\r\n  <app-mainpage></app-mainpage>\r\n  <div class=\"myForm\">\r\n    <kendo-grid\r\n      [data]=\"gridData\"\r\n      [pageSize]=\"gridState.take\"\r\n      [skip]=\"gridState.skip\"\r\n      [sort]=\"gridState.sort\"\r\n      [filter]=\"gridState.filter\"\r\n      [sortable]=\"true\"\r\n      [pageable]=\"true\"\r\n      filterable=\"menu\"\r\n      (dataStateChange)=\"dataStateChange($event)\"\r\n      [height]=\"810\"\r\n      (edit)=\"editHandler($event)\"\r\n      (cancel)=\"cancelHandler($event)\"\r\n      (save)=\"saveHandler($event)\"\r\n      (remove)=\"removeHandler($event)\"\r\n      (add)=\"addHandler($event)\"\r\n      [navigable]=\"true\"\r\n    >\r\n      <kendo-grid-column title=\"Image\" width=\"70\">\r\n        <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n          <div class=\"div-zoom\">\r\n            <img class=\"enlarge\" src=\"{{ dataItem.picture }}\" alt=\"image\" />\r\n          </div>\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-column field=\"title\" editor=\"string\" title=\"Book Title\">\r\n      </kendo-grid-column>\r\n      <kendo-grid-column\r\n        field=\"price\"\r\n        editor=\"numeric\"\r\n        title=\"Price\"\r\n        width=\"280\"\r\n        filter=\"numeric\"\r\n        format=\"{0:c}\"\r\n      >\r\n      </kendo-grid-column>\r\n      <kendo-grid-column width=\"180\" title=\"Author\">\r\n        <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n          {{ getAuthorList(dataItem) }}\r\n        </ng-template>\r\n        <ng-template kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n          <kendo-dropdownlist\r\n            name=\"dataItem.authorList\"\r\n            [data]=\"authorList.authorList\"\r\n            textField=\"name\"\r\n            valueField=\"id\"\r\n            [(ngModel)]=\"dataItem.author\"\r\n            (valueChange)=\"authorChange($event)\"\r\n          >\r\n          </kendo-dropdownlist>\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-column width=\"150\" title=\"Genre\">\r\n        <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n          {{ getGenreList(dataItem) }}\r\n        </ng-template>\r\n        <ng-template width=\"50\" kendoGridEditTemplate let-dataItem=\"dataItem\">\r\n          <kendo-dropdownlist\r\n            name=\"dataItem.genreList\"\r\n            [data]=\"genreList.genreList\"\r\n            textField=\"title\"\r\n            valueField=\"id\"\r\n            [(ngModel)]=\"dataItem.genre\"\r\n            (valueChange)=\"genreChange($event)\"\r\n          >\r\n          </kendo-dropdownlist>\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-command-column title=\"command\" width=\"250\">\r\n        <ng-template\r\n          kendoGridCellTemplate\r\n          let-isNew=\"isNew\"\r\n          let-dataItem=\"dataItem\"\r\n        >\r\n          <button\r\n            kendoButton\r\n            button\r\n            mdbBtn\r\n            type=\"button\"\r\n            gradient=\"blue\"\r\n            rounded=\"true\"\r\n            mdbWavesEffect\r\n            (click)=\"onButtonClick(dataItem.id, dataItem.price)\"\r\n          >\r\n            Add To Order\r\n          </button>\r\n          <button\r\n            button\r\n            mdbBtn\r\n            type=\"button\"\r\n            gradient=\"blue\"\r\n            rounded=\"true\"\r\n            mdbWavesEffect\r\n            kendoButton\r\n            (click)=\"openDescription(dataItem.id)\"\r\n          >\r\n            About\r\n          </button>\r\n        </ng-template>\r\n      </kendo-grid-command-column>\r\n    </kendo-grid>\r\n  </div>\r\n</body>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/bookstore/magazine-description/magazine-description.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/bookstore/magazine-description/magazine-description.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    <h2 class=\"headline\">\r\n      <p>{{ magazineDescription.title }}</p>\r\n    </h2>\r\n    <mat-card class=\"my-div2\">\r\n      <div class=\"wrapper\">\r\n        <div id=\"left\">\r\n          <img\r\n            class=\"enlarge\"\r\n            src=\"{{ magazineDescription.picture }}\"\r\n            alt=\"image\"\r\n          />\r\n        </div>\r\n        <div id=\"right\">\r\n          About Magazine: {{ magazineDescription.description }}\r\n          <div class=\"price\">Price: {{ magazineDescription.price }}$</div>\r\n        </div>\r\n      </div>\r\n    </mat-card>\r\n    <mat-dialog-actions>\r\n      <button mdbBtn type=\"button\" color=\"info\" mdbWavesEffect mat-dialog-close>\r\n        Cancel\r\n      </button>\r\n      <button\r\n        (click)=\"onButtonClick(magazineDescription.id)\"\r\n        mdbBtn\r\n        type=\"button\"\r\n        color=\"info\"\r\n        mdbWavesEffect\r\n      >\r\n        Add To Order\r\n      </button>\r\n    </mat-dialog-actions>\r\n  </mat-dialog-content>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/bookstore/magazine/magazine.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/bookstore/magazine/magazine.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"image\">\r\n<app-mainpage></app-mainpage>\r\n  <div class=\"myForm\">\r\n    <kendo-grid\r\n      [data]=\"gridData\"\r\n      [pageSize]=\"gridState.take\"\r\n      [skip]=\"gridState.skip\"\r\n      [sort]=\"gridState.sort\"\r\n      [filter]=\"gridState.filter\"\r\n      [sortable]=\"true\"\r\n      [pageable]=\"true\"\r\n      filterable=\"menu\"\r\n      (dataStateChange)=\"dataStateChange($event)\"\r\n      [height]=\"810\"\r\n      (edit)=\"editHandler($event)\"\r\n      (cancel)=\"cancelHandler($event)\"\r\n      (save)=\"saveHandler($event)\"\r\n      (add)=\"addHandler($event)\"\r\n      [navigable]=\"true\"\r\n    >\r\n      <kendo-grid-column title=\"Image\" width=\"40\">\r\n          <ng-template kendoGridCellTemplate let-dataItem=\"dataItem\">\r\n            <div class=\"div-zoom\">\r\n              <img class=\"enlarge\" src=\"{{dataItem .picture }}\" alt=\"image\" />\r\n            </div>\r\n          </ng-template>\r\n        </kendo-grid-column>\r\n      <kendo-grid-column width = \"650\" field=\"title\" editor=\"string\" title=\"Title\">\r\n      </kendo-grid-column>\r\n      <kendo-grid-column\r\n        field=\"price\"\r\n        editor=\"numeric\"\r\n        title=\"Price\"\r\n        width=\"150\"\r\n        filter=\"numeric\"\r\n        format=\"{0:c}\"\r\n      >\r\n      </kendo-grid-column>\r\n      <kendo-grid-command-column title=\"command\" width=\"140\">\r\n        <ng-template\r\n          kendoGridCellTemplate\r\n          let-isNew=\"isNew\"\r\n          let-dataItem=\"dataItem\"\r\n        >\r\n        <button button mdbBtn type=\"button\" gradient=\"blue\" rounded=\"true\" mdbWavesEffect (click)=\"onButtonClick(dataItem.id, dataItem.price)\">\r\n          Add To Order\r\n        </button>\r\n        <button button mdbBtn type=\"button\" gradient=\"blue\" rounded=\"true\" mdbWavesEffect (click)=\"openDescription(dataItem.id)\">About</button>\r\n        </ng-template>\r\n      </kendo-grid-command-column>\r\n    </kendo-grid>\r\n  </div>\r\n</body>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/menu/mainpage/mainpage.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/menu/mainpage/mainpage.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\r\n  <a class=\"navbar-brand\" href=\"#\">BookStore</a>\r\n  <button\r\n    class=\"navbar-toggler\"\r\n    type=\"button\"\r\n    data-toggle=\"collapse\"\r\n    data-target=\"#navbarSupportedContent\"\r\n    aria-controls=\"navbarSupportedContent\"\r\n    aria-expanded=\"false\"\r\n    aria-label=\"Toggle navigation\"\r\n  >\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n    <ul class=\"navbar-nav mr-auto\">\r\n      <li class=\"nav-item active\">\r\n        <a class=\"nav-link\" [routerLink]=\"['/magazine']\">\r\n          Magazine\r\n        </a>\r\n      </li>\r\n      <li class=\"nav-item dropdown\">\r\n        <a\r\n          class=\"nav-link dropdown-toggle\"\r\n          href=\"#\"\r\n          id=\"navbarDropdown\"\r\n          role=\"button\"\r\n          data-toggle=\"dropdown\"\r\n          aria-haspopup=\"true\"\r\n          aria-expanded=\"false\"\r\n        >\r\n          Menu\r\n        </a>\r\n\r\n        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n          <ng-container *ngIf=\"admin\">\r\n            <a\r\n              class=\"dropdown-menu\"\r\n              (click)=\"openAdminPage()\"\r\n              class=\"dropdown-item\"\r\n            >\r\n              Admin Page\r\n            </a>\r\n          </ng-container>\r\n          <a class=\"dropdown-item\" [routerLink]=\"['/app-shop-description']\"\r\n            >About Shop</a\r\n          >\r\n          <div class=\"dropdown-divider\"></div>\r\n        </div>\r\n      </li>\r\n      \r\n      <button\r\n        (click)=\"openDialog()\"\r\n        mdbBtn\r\n        type=\"button\"\r\n        gradient=\"aqua\"\r\n        rounded=\"true\"\r\n        mdbWavesEffect\r\n      >\r\n        <span class=\"z-index\" for=\"totalamount\">{{ orderCounter }}</span>\r\n        <link\r\n          rel=\"stylesheet\"\r\n          href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\"\r\n        />\r\n        <span class=\"glyphicon glyphicon-shopping-cart\"></span>\r\n      </button>\r\n    </ul>\r\n  </div>\r\n  <div class=\"userMail\">\r\n    <h1 class=\"txt\">Welcome</h1>\r\n    {{ model.Email }}\r\n  </div>\r\n  <form class=\"form-inline my-2 my-lg-0\">\r\n    <input\r\n      class=\"form-control mr-sm-2\"\r\n      type=\"search\"\r\n      placeholder=\"Search\"\r\n      aria-label=\"Search\"\r\n    />\r\n\r\n    <button\r\n      mdbBtn\r\n      type=\"button\"\r\n      gradient=\"purple\"\r\n      rounded=\"true\"\r\n      mdbWavesEffect\r\n    >\r\n      Search\r\n    </button>\r\n  </form>\r\n\r\n  <button\r\n    [routerLink]=\"['/#']\"\r\n    mdbBtn\r\n    type=\"button\"\r\n    gradient=\"purple\"\r\n    rounded=\"true\"\r\n    mdbWavesEffect\r\n    (click)=\"Logout()\"\r\n  >\r\n    <span class=\"glyphicon glyphicon-log-out\"></span>\r\n  </button>\r\n</nav>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/menu/order-basket/order-basket.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/menu/order-basket/order-basket.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\">\r\n  <mat-dialog-content class=\"mat-typography\">\r\n    \r\n    <div class=\"title\">Order Basket</div>\r\n    <div class=\"warning\" *ngIf=\"basketIsEmpty\">Your Basket is empty</div>\r\n\r\n    <mat-card class=\"my-div\" *ngFor=\"let book of booklistOrder\">\r\n      <div class=\"my-div1\">{{ book?.title }}</div>\r\n      <div id=\"left\">\r\n        <img class=\"enlarge\" src=\"{{ book?.picture }}\" alt=\"image\" />\r\n      </div>\r\n      <div class=\"my-div3\">Price: {{ book?.price }}$</div>\r\n    </mat-card>\r\n\r\n    <mat-card class=\"my-div\" *ngFor=\"let magazine of magazineListOrder\">\r\n      <div class=\"my-div1\">{{ magazine?.title }}</div>\r\n      <div id=\"left\">\r\n        <img class=\"enlarge\" src=\"{{ magazine?.picture }}\" alt=\"image\" />\r\n      </div>\r\n      <div class=\"my-div3\">Price: {{ magazine?.price }}$</div>\r\n    </mat-card>\r\n    <p class=\"total\">Total Price: {{ orderTotalPrice }}$</p>\r\n    <mat-dialog-actions>\r\n\r\n      <button\r\n        (click)=\"deleteItem()\"\r\n        mdbBtn\r\n        type=\"button\"\r\n        color=\"danger\"\r\n        mdbWavesEffec\r\n      >\r\n        Remove Order\r\n      </button>\r\n\r\n      <button\r\n        mat-dialog-close\r\n        mdbBtn\r\n        type=\"button\"\r\n        color=\"success\"\r\n        mdbWavesEffect\r\n      >\r\n        Cancel\r\n      </button>\r\n\r\n      <button\r\n        mat-dialog-close\r\n        (click)=\"openPayment()\"\r\n        mdbBtn\r\n        type=\"button\"\r\n        color=\"success\"\r\n        mdbWavesEffect\r\n      >\r\n        Order Now\r\n      </button>\r\n\r\n      <h4>{{orderCounter}}</h4>\r\n    </mat-dialog-actions>\r\n  </mat-dialog-content>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/menu/payment/payment.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/menu/payment/payment.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"image\">\r\n  <form [formGroup]=\"formGroup\" class=\"credit-card\">\r\n    <div class=\"form-header\">\r\n      <h4 class=\"title\">Credit card detail</h4>\r\n      <h4 for=\"totalamount\">Total amount: {{orderTotalPrice}}$</h4>\r\n    </div>\r\n    <div class=\"form-body\">\r\n      <input\r\n        type=\"text\"\r\n        class=\"card-number\"\r\n        placeholder=\"Card Number\"\r\n        formControlName=\"CardNumber\"\r\n      />\r\n      <div class=\"date-field\">\r\n        <div class=\"month\">\r\n          <select name=\"Month\" formControlName=\"CardExpMonth\">\r\n            <option value=\"1\">January</option>\r\n            <option value=\"2\">February</option>\r\n            <option value=\"3\">March</option>\r\n            <option value=\"4\">April</option>\r\n            <option value=\"5\">May</option>\r\n            <option value=\"6\">June</option>\r\n            <option value=\"7\">July</option>\r\n            <option value=\"8\">August</option>\r\n            <option value=\"9\">September</option>\r\n            <option value=\"10\">October</option>\r\n            <option value=\"11\">November</option>\r\n            <option value=\"12\">December</option>\r\n          </select>\r\n        </div>\r\n        <div class=\"year\">\r\n          <select name=\"Year\" formControlName=\"CardExpYear\">\r\n            <option value=\"2016\">2016</option>\r\n            <option value=\"2017\">2017</option>\r\n            <option value=\"2018\">2018</option>\r\n            <option value=\"2019\">2019</option>\r\n            <option value=\"2020\">2020</option>\r\n            <option value=\"2021\">2021</option>\r\n            <option value=\"2022\">2022</option>\r\n            <option value=\"2023\">2023</option>\r\n            <option value=\"2024\">2024</option>\r\n          </select>\r\n        </div>\r\n      </div>\r\n      <div class=\"card-verification\">\r\n        <div class=\"cvv-input\">\r\n          <input type=\"text\" placeholder=\"CVC\" formControlName=\"CardCvc\" />\r\n        </div>\r\n      </div>  \r\n      <button (click)=\"onSubmit()\" type=\"submit\" class=\"paypal-btn\">Pay</button>\r\n      <a class=\"nav-link\" href=\"/#\">Cancel</a>\r\n    </div>\r\n  </form>\r\n</body>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/menu/shop-description/shop-description.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/menu/shop-description/shop-description.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-mainpage></app-mainpage>\r\n\r\n<body class=\"image\">\r\n  <form class=\"myForm\">\r\n    <h1 class=\"head\">\r\n      Welcome\r\n    </h1>\r\n    <div class=\"div1\">\r\n      BOOK/SHOP is a design-minded, multi-disciplinary brand offering objects,\r\n      events, and experiences related to books and reading. We are based in the\r\n      Kharkiv Bay Area and Kharkiv obl. OUR PHILOSOPHY Some people like to read\r\n      on a screen. Other people need the variety and artistry, the sight, smell,\r\n      and feel of actual books.They love seeing them on their shelves; they love\r\n      having shelves for them. They love taking them along when they leave the\r\n      house, and stacking them by their bedsides. They love finding old letters\r\n      and bookmarks in them. They like remembering where they bought them or who\r\n      they received them from. They want to read in a way that offers a rich\r\n      experience, more than the words only: the full offering of a book. They\r\n      are particular about covers, they want to surround themselves with the\r\n      poetry of good design. They can't pass a bookstore without going in and\r\n      getting something, they keep a library card and use it. They are allergic\r\n      to cheap bestsellers; they delight in the out-of-the-way and the rare, the\r\n      well-made and the hard-to-accomplish. They take care of their books; they\r\n      know a book is only theirs until it passes on to someone else. They are\r\n      good stewards of a timeless object. These are the people we're working\r\n      for.\r\n    </div>\r\n    <div class=\"div2\">\r\n      CONTACT US: Email: info@shopbookshop.com Phone: 510-907-9649 Follow:\r\n      Instagram or Facebook PRESS: Book/Shop's products and stores have been\r\n      featured in many print & web publications, including\r\n    </div>\r\n  </form>\r\n</body>\r\n"

/***/ }),

/***/ "./src/app/admin-panel/admin-page/admin-page.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/admin-panel/admin-page/admin-page.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".image {\r\n  position: absolute;\r\n  overflow: auto;\r\n  height: 100vh;\r\n  width: 100%;\r\n  background-image: url(https://images.clipartlogo.com/files/istock/previews/7631/76316673-stack-of-books.jpg);\r\n}\r\n.myForm {\r\n  position: relative;\r\n  height: 92%;\r\n}\r\n.k-widget {\r\n  border: none;\r\n}\r\n.myForm2 {\r\n  height: 12%;\r\n  background-color: #226f8d;\r\n}\r\n.btn-sm {\r\n  margin-right: 0.5%;\r\n  border-radius: 7rem;\r\n  color: #ffffff;\r\n}\r\n.btn-lg {\r\n  margin-left: 2.5%;\r\n  border-radius: 10rem;\r\n  color: #ffffff;\r\n}\r\n.btn {\r\n  background-color:#923ebfd1;\r\n}\r\n.small-image {\r\n  background-image: url(http://sportkniga.com.ua/images/books228577dtg3.jpg);\r\n  height: 10vh;\r\n  width: 5%;\r\n}\r\n.k-button.k-primary,\r\n.k-slider .k-draghandle {\r\n  border-color: #747b1d;\r\n  color: #000000;\r\n  background-color: #d5b0f86e;\r\n}\r\n.sad {\r\n  border-color: #747b1d;\r\n  color: #000000;\r\n  background-color: #d5b0f86e;\r\n\r\n  background-image: none;\r\n  background-image: none;\r\n}\r\n.bg-light {\r\n  background-color: #5fa4b7 !important;\r\n}\r\n.but {\r\n  background-color: #d5b0f86e;\r\n  margin-right: 2%;\r\n  width: 35%;\r\n}\r\n.img {\r\n  width: 50%;\r\n}\r\n.enlarge:hover {\r\n  -webkit-transform: scale(3.5, 3.5);\r\n          transform: scale(3.5, 3.5);\r\n  -webkit-transform-origin: 0 0;\r\n          transform-origin: 0 0;\r\n  position: absolute;\r\n}\r\n.div-zoom {\r\n  width: 85px;\r\n  height: 55px;\r\n}\r\n.cont {\r\n  height: 100%; \r\n  width: 8%;\r\n  background: #ace2d5c9;\r\n  text-align: -webkit-center;\r\n}\r\n.h1,\r\nh1 {\r\n  font-size: 1.5rem;\r\n  color: navy;\r\n}\r\n.btn-sm {\r\n  display: block;\r\n  margin-top: 5%;\r\n}\r\n.row {\r\n  display: unset;\r\n}\r\n.header {\r\n  font-family: \"Times New Roman\", Times, serif;\r\n  font-size: 180%;\r\n  margin: auto;\r\n display: table;\r\n color: #58cce0;\r\n}\r\n.mat-dialog-content{\r\n  display: block;\r\n    margin: 95px 17px;\r\n    padding: 0px 16px;\r\n    max-height: 84vh;\r\n    overflow: auto;\r\n    margin-left: 8%;\r\n}\r\n.wind{\r\n  display: -webkit-box;\r\n  display: flex;\r\n}\r\n.text{    font-family: monospace;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC9hZG1pbi1wYWdlL2FkbWluLXBhZ2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsYUFBYTtFQUNiLFdBQVc7RUFDWCw0R0FBNEc7QUFDOUc7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0FBQ2I7QUFFQTtFQUNFLFlBQVk7QUFDZDtBQUNBO0VBQ0UsV0FBVztFQUNYLHlCQUF5QjtBQUMzQjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGNBQWM7QUFDaEI7QUFDQTtFQUNFLDBCQUEwQjtBQUM1QjtBQUNBO0VBQ0UsMEVBQTBFO0VBQzFFLFlBQVk7RUFDWixTQUFTO0FBQ1g7QUFDQTs7RUFFRSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLDJCQUEyQjtBQUM3QjtBQUNBO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7O0VBRTNCLHNCQUFzQjtFQUN0QixzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLG9DQUFvQztBQUN0QztBQUNBO0VBQ0UsMkJBQTJCO0VBQzNCLGdCQUFnQjtFQUNoQixVQUFVO0FBQ1o7QUFDQTtFQUNFLFVBQVU7QUFDWjtBQUNBO0VBQ0Usa0NBQTBCO1VBQTFCLDBCQUEwQjtFQUMxQiw2QkFBcUI7VUFBckIscUJBQXFCO0VBQ3JCLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUVBO0VBQ0UsWUFBWTtFQUNaLFNBQVM7RUFDVCxxQkFBcUI7RUFDckIsMEJBQTBCO0FBQzVCO0FBQ0E7O0VBRUUsaUJBQWlCO0VBQ2pCLFdBQVc7QUFDYjtBQUVBO0VBQ0UsY0FBYztFQUNkLGNBQWM7QUFDaEI7QUFDQTtFQUNFLGNBQWM7QUFDaEI7QUFFQTtFQUNFLDRDQUE0QztFQUM1QyxlQUFlO0VBQ2YsWUFBWTtDQUNiLGNBQWM7Q0FDZCxjQUFjO0FBQ2Y7QUFFQTtFQUNFLGNBQWM7SUFDWixpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsZUFBZTtBQUNuQjtBQUNBO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0FBQ2Y7QUFDQSxVQUFVLHNCQUFzQjtBQUNoQyIsImZpbGUiOiJhcHAvYWRtaW4tcGFuZWwvYWRtaW4tcGFnZS9hZG1pbi1wYWdlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW1hZ2Uge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBvdmVyZmxvdzogYXV0bztcclxuICBoZWlnaHQ6IDEwMHZoO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL2ltYWdlcy5jbGlwYXJ0bG9nby5jb20vZmlsZXMvaXN0b2NrL3ByZXZpZXdzLzc2MzEvNzYzMTY2NzMtc3RhY2stb2YtYm9va3MuanBnKTtcclxufVxyXG4ubXlGb3JtIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgaGVpZ2h0OiA5MiU7XHJcbn1cclxuXHJcbi5rLXdpZGdldCB7XHJcbiAgYm9yZGVyOiBub25lO1xyXG59XHJcbi5teUZvcm0yIHtcclxuICBoZWlnaHQ6IDEyJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjI2ZjhkO1xyXG59XHJcbi5idG4tc20ge1xyXG4gIG1hcmdpbi1yaWdodDogMC41JTtcclxuICBib3JkZXItcmFkaXVzOiA3cmVtO1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG59XHJcbi5idG4tbGcge1xyXG4gIG1hcmdpbi1sZWZ0OiAyLjUlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcmVtO1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG59XHJcbi5idG4ge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IzkyM2ViZmQxO1xyXG59XHJcbi5zbWFsbC1pbWFnZSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHA6Ly9zcG9ydGtuaWdhLmNvbS51YS9pbWFnZXMvYm9va3MyMjg1NzdkdGczLmpwZyk7XHJcbiAgaGVpZ2h0OiAxMHZoO1xyXG4gIHdpZHRoOiA1JTtcclxufVxyXG4uay1idXR0b24uay1wcmltYXJ5LFxyXG4uay1zbGlkZXIgLmstZHJhZ2hhbmRsZSB7XHJcbiAgYm9yZGVyLWNvbG9yOiAjNzQ3YjFkO1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkNWIwZjg2ZTtcclxufVxyXG4uc2FkIHtcclxuICBib3JkZXItY29sb3I6ICM3NDdiMWQ7XHJcbiAgY29sb3I6ICMwMDAwMDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q1YjBmODZlO1xyXG5cclxuICBiYWNrZ3JvdW5kLWltYWdlOiBub25lO1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IG5vbmU7XHJcbn1cclxuLmJnLWxpZ2h0IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWZhNGI3ICFpbXBvcnRhbnQ7XHJcbn1cclxuLmJ1dCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q1YjBmODZlO1xyXG4gIG1hcmdpbi1yaWdodDogMiU7XHJcbiAgd2lkdGg6IDM1JTtcclxufVxyXG4uaW1nIHtcclxuICB3aWR0aDogNTAlO1xyXG59XHJcbi5lbmxhcmdlOmhvdmVyIHtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDMuNSwgMy41KTtcclxuICB0cmFuc2Zvcm0tb3JpZ2luOiAwIDA7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi5kaXYtem9vbSB7XHJcbiAgd2lkdGg6IDg1cHg7XHJcbiAgaGVpZ2h0OiA1NXB4O1xyXG59XHJcblxyXG4uY29udCB7XHJcbiAgaGVpZ2h0OiAxMDAlOyBcclxuICB3aWR0aDogOCU7XHJcbiAgYmFja2dyb3VuZDogI2FjZTJkNWM5O1xyXG4gIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xyXG59XHJcbi5oMSxcclxuaDEge1xyXG4gIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gIGNvbG9yOiBuYXZ5O1xyXG59ICBcclxuXHJcbi5idG4tc20ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi10b3A6IDUlO1xyXG59ICBcclxuLnJvdyB7XHJcbiAgZGlzcGxheTogdW5zZXQ7XHJcbn1cclxuXHJcbi5oZWFkZXIge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlRpbWVzIE5ldyBSb21hblwiLCBUaW1lcywgc2VyaWY7XHJcbiAgZm9udC1zaXplOiAxODAlO1xyXG4gIG1hcmdpbjogYXV0bztcclxuIGRpc3BsYXk6IHRhYmxlO1xyXG4gY29sb3I6ICM1OGNjZTA7XHJcbn1cclxuXHJcbi5tYXQtZGlhbG9nLWNvbnRlbnR7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXJnaW46IDk1cHggMTdweDtcclxuICAgIHBhZGRpbmc6IDBweCAxNnB4O1xyXG4gICAgbWF4LWhlaWdodDogODR2aDtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDglO1xyXG59XHJcbi53aW5ke1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuLnRleHR7ICAgIGZvbnQtZmFtaWx5OiBtb25vc3BhY2U7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/admin-panel/admin-page/admin-page.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/admin-panel/admin-page/admin-page.component.ts ***!
  \****************************************************************/
/*! exports provided: AdminPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminPageComponent", function() { return AdminPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_author_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/author.service */ "./src/app/services/author.service.ts");



var AdminPageComponent = /** @class */ (function () {
    function AdminPageComponent(authService) {
        this.authService = authService;
    }
    AdminPageComponent.prototype.ngOnInit = function () { };
    AdminPageComponent.ctorParameters = function () { return [
        { type: src_app_services_author_service__WEBPACK_IMPORTED_MODULE_2__["AuthorService"] }
    ]; };
    AdminPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-admin-page",
            template: __webpack_require__(/*! raw-loader!./admin-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/admin-page/admin-page.component.html"),
            styles: [__webpack_require__(/*! ./admin-page.component.css */ "./src/app/admin-panel/admin-page/admin-page.component.css")]
        })
    ], AdminPageComponent);
    return AdminPageComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/adminpanel.module.ts":
/*!**************************************************!*\
  !*** ./src/app/admin-panel/adminpanel.module.ts ***!
  \**************************************************/
/*! exports provided: AdminPanelModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminPanelModule", function() { return AdminPanelModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _author_editing_author_editing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./author-editing/author-editing.component */ "./src/app/admin-panel/author-editing/author-editing.component.ts");
/* harmony import */ var _book_editing_book_editing_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./book-editing/book-editing.component */ "./src/app/admin-panel/book-editing/book-editing.component.ts");
/* harmony import */ var _magazine_editing_magazine_editing_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./magazine-editing/magazine-editing.component */ "./src/app/admin-panel/magazine-editing/magazine-editing.component.ts");
/* harmony import */ var _genre_editing_genre_editing_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./genre-editing/genre-editing.component */ "./src/app/admin-panel/genre-editing/genre-editing.component.ts");
/* harmony import */ var _create_item_author_create_item_author_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./create-item-author/create-item-author.component */ "./src/app/admin-panel/create-item-author/create-item-author.component.ts");
/* harmony import */ var _create_item_book_create_item_book_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./create-item-book/create-item-book.component */ "./src/app/admin-panel/create-item-book/create-item-book.component.ts");
/* harmony import */ var _create_item_genre_create_item_genre_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./create-item-genre/create-item-genre.component */ "./src/app/admin-panel/create-item-genre/create-item-genre.component.ts");
/* harmony import */ var _create_item_magazine_create_item_magazine_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./create-item-magazine/create-item-magazine.component */ "./src/app/admin-panel/create-item-magazine/create-item-magazine.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @progress/kendo-angular-dropdowns */ "./node_modules/@progress/kendo-angular-dropdowns/dist/fesm5/index.js");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/fesm5/angular-bootstrap-md.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/fesm5/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select-ng-select.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/esm5/table.es5.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _ng_select_ng_option_highlight__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @ng-select/ng-option-highlight */ "./node_modules/@ng-select/ng-option-highlight/fesm5/ng-select-ng-option-highlight.js");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/fesm5/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _authentication_authentication_module__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../authentication/authentication.module */ "./src/app/authentication/authentication.module.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _admin_page_admin_page_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./admin-page/admin-page.component */ "./src/app/admin-panel/admin-page/admin-page.component.ts");
/* harmony import */ var _menu_menu_module__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ../menu/menu.module */ "./src/app/menu/menu.module.ts");
/* harmony import */ var _shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ../shared/helpers/auth-guard */ "./src/app/shared/helpers/auth-guard.ts");
/* harmony import */ var _order_editing_order_editing_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./order-editing/order-editing.component */ "./src/app/admin-panel/order-editing/order-editing.component.ts");
/* harmony import */ var _user_editing_user_editing_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./user-editing/user-editing.component */ "./src/app/admin-panel/user-editing/user-editing.component.ts");
/* harmony import */ var _create_user_create_user_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./create-user/create-user.component */ "./src/app/admin-panel/create-user/create-user.component.ts");



































var AdminPanelModule = /** @class */ (function () {
    function AdminPanelModule() {
    }
    AdminPanelModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_11__["RouterModule"].forChild([
                    {
                        path: "app-admin-page",
                        component: _admin_page_admin_page_component__WEBPACK_IMPORTED_MODULE_29__["AdminPageComponent"],
                        canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                        children: [
                            {
                                path: "author-editing",
                                component: _author_editing_author_editing_component__WEBPACK_IMPORTED_MODULE_3__["AuthorEditingComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            },
                            {
                                path: "book-editing",
                                component: _book_editing_book_editing_component__WEBPACK_IMPORTED_MODULE_4__["BookEditingComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            },
                            {
                                path: "magazine-editing",
                                component: _magazine_editing_magazine_editing_component__WEBPACK_IMPORTED_MODULE_5__["MagazineEditingComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            },
                            {
                                path: "genre-editing",
                                component: _genre_editing_genre_editing_component__WEBPACK_IMPORTED_MODULE_6__["GenreEditingComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            },
                            {
                                path: "order-editing",
                                component: _order_editing_order_editing_component__WEBPACK_IMPORTED_MODULE_32__["OrderEditingComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            },
                            {
                                path: "create-item-author",
                                component: _create_item_author_create_item_author_component__WEBPACK_IMPORTED_MODULE_7__["CreateItemAuthorComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            },
                            {
                                path: "create-item-book",
                                component: _create_item_book_create_item_book_component__WEBPACK_IMPORTED_MODULE_8__["CreateItemBookComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            },
                            {
                                path: "user-editing",
                                component: _user_editing_user_editing_component__WEBPACK_IMPORTED_MODULE_33__["UserEditingComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            },
                            {
                                path: "create-item-genre",
                                component: _create_item_genre_create_item_genre_component__WEBPACK_IMPORTED_MODULE_9__["CreateItemGenreComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            },
                            {
                                path: "create-item-magazine",
                                component: _create_item_magazine_create_item_magazine_component__WEBPACK_IMPORTED_MODULE_10__["CreateItemMagazineComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            },
                            {
                                path: "create-user",
                                component: _create_user_create_user_component__WEBPACK_IMPORTED_MODULE_34__["CreateUserComponent"],
                                canActivate: [_shared_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_31__["AuthGuard"]],
                                outlet: "editing"
                            }
                        ]
                    }
                ]),
                _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_12__["DropDownsModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_13__["TooltipModule"].forRoot(),
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_13__["ModalModule"].forRoot(),
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_14__["GridModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatDialogModule"],
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_16__["NgSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatCardModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_17__["ToastrModule"].forRoot({
                    timeOut: 1000,
                    positionClass: "toast-bottom-right"
                }),
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatMenuModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_18__["NgbAlertModule"],
                _angular_cdk_table__WEBPACK_IMPORTED_MODULE_19__["CdkTableModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_18__["NgbModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatButtonModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_13__["ButtonsModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_20__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_21__["AppRoutingModule"],
                _ng_select_ng_option_highlight__WEBPACK_IMPORTED_MODULE_22__["NgOptionHighlightModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_13__["ButtonsModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_13__["CollapseModule"],
                _menu_menu_module__WEBPACK_IMPORTED_MODULE_30__["MenuModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_13__["WavesModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatSidenavModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_23__["NgMultiSelectDropDownModule"].forRoot(),
                _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_24__["PopupModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_25__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_26__["HttpClientModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _authentication_authentication_module__WEBPACK_IMPORTED_MODULE_27__["AuthenticationModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatToolbarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_25__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_28__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatSelectModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_28__["NoopAnimationsModule"]
            ],
            declarations: [
                _admin_page_admin_page_component__WEBPACK_IMPORTED_MODULE_29__["AdminPageComponent"],
                _author_editing_author_editing_component__WEBPACK_IMPORTED_MODULE_3__["AuthorEditingComponent"],
                _book_editing_book_editing_component__WEBPACK_IMPORTED_MODULE_4__["BookEditingComponent"],
                _magazine_editing_magazine_editing_component__WEBPACK_IMPORTED_MODULE_5__["MagazineEditingComponent"],
                _user_editing_user_editing_component__WEBPACK_IMPORTED_MODULE_33__["UserEditingComponent"],
                _create_user_create_user_component__WEBPACK_IMPORTED_MODULE_34__["CreateUserComponent"],
                _genre_editing_genre_editing_component__WEBPACK_IMPORTED_MODULE_6__["GenreEditingComponent"],
                _order_editing_order_editing_component__WEBPACK_IMPORTED_MODULE_32__["OrderEditingComponent"],
                _create_item_author_create_item_author_component__WEBPACK_IMPORTED_MODULE_7__["CreateItemAuthorComponent"],
                _create_item_book_create_item_book_component__WEBPACK_IMPORTED_MODULE_8__["CreateItemBookComponent"],
                _create_item_genre_create_item_genre_component__WEBPACK_IMPORTED_MODULE_9__["CreateItemGenreComponent"],
                _create_item_magazine_create_item_magazine_component__WEBPACK_IMPORTED_MODULE_10__["CreateItemMagazineComponent"]
            ]
        })
    ], AdminPanelModule);
    return AdminPanelModule;
}());



/***/ }),

/***/ "./src/app/admin-panel/author-editing/author-editing.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/admin-panel/author-editing/author-editing.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".k-grid {\r\n  background-clip: unset;\r\n}\r\n.enlarge {\r\n  width: 50%;\r\n}\r\n.btn {\r\n  color: #ffffff;\r\n  border-radius: 10rem;\r\n  background-color: #926dbe;\r\n}\r\n.k-grid {\r\n  background-clip: unset;\r\n}\r\nimg {\r\n  width: 50px;\r\n}\r\n.div-zoom {\r\n  width: 85px;\r\n  height: 55px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC9hdXRob3ItZWRpdGluZy9hdXRob3ItZWRpdGluZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLGNBQWM7RUFDZCxvQkFBb0I7RUFDcEIseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLFdBQVc7QUFDYjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7QUFDZCIsImZpbGUiOiJhcHAvYWRtaW4tcGFuZWwvYXV0aG9yLWVkaXRpbmcvYXV0aG9yLWVkaXRpbmcuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5rLWdyaWQge1xyXG4gIGJhY2tncm91bmQtY2xpcDogdW5zZXQ7XHJcbn1cclxuLmVubGFyZ2Uge1xyXG4gIHdpZHRoOiA1MCU7XHJcbn1cclxuLmJ0biB7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogMTByZW07XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzkyNmRiZTtcclxufVxyXG5cclxuLmstZ3JpZCB7XHJcbiAgYmFja2dyb3VuZC1jbGlwOiB1bnNldDtcclxufVxyXG5pbWcge1xyXG4gIHdpZHRoOiA1MHB4O1xyXG59XHJcbi5kaXYtem9vbSB7XHJcbiAgd2lkdGg6IDg1cHg7XHJcbiAgaGVpZ2h0OiA1NXB4O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/admin-panel/author-editing/author-editing.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/admin-panel/author-editing/author-editing.component.ts ***!
  \************************************************************************/
/*! exports provided: AuthorEditingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorEditingComponent", function() { return AuthorEditingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/author/get.author.list.view */ "./src/app/models/author/get.author.list.view.ts");
/* harmony import */ var _progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @progress/kendo-data-query */ "./node_modules/@progress/kendo-data-query/dist/es/main.js");
/* harmony import */ var _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-pwa/local-storage */ "./node_modules/@ngx-pwa/local-storage/fesm5/ngx-pwa-local-storage.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_services_author_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/author.service */ "./src/app/services/author.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _create_item_author_create_item_author_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../create-item-author/create-item-author.component */ "./src/app/admin-panel/create-item-author/create-item-author.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");












var AuthorEditingComponent = /** @class */ (function () {
    function AuthorEditingComponent(localStorage, dialog, authorService, toastr, dialogRef, router) {
        this.localStorage = localStorage;
        this.dialog = dialog;
        this.authorService = authorService;
        this.toastr = toastr;
        this.dialogRef = dialogRef;
        this.router = router;
        this.notifyParent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.authorList = new src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_3__["GetAuthorView"]();
    }
    AuthorEditingComponent_1 = AuthorEditingComponent;
    AuthorEditingComponent.prototype.ngOnInit = function () {
        this.loadData();
        this.notifyParent.emit(AuthorEditingComponent_1);
    };
    AuthorEditingComponent.prototype.loadData = function () {
        var _this = this;
        this.authorService.get().subscribe(function (data) {
            _this.authorList = data;
            _this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__["process"])(_this.authorList.authorList, _this.gridState);
        });
    };
    AuthorEditingComponent.prototype.addHandler = function (_a) {
        var sender = _a.sender;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](0),
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
        });
        sender.addRow(this.formGroup);
    };
    AuthorEditingComponent.prototype.editHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.id),
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.name)
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
        this.toastr.success("Edited");
    };
    AuthorEditingComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    AuthorEditingComponent.prototype.saveHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, formGroup = _a.formGroup, isNew = _a.isNew;
        if (isNew) {
            var product = formGroup.value;
            this.authorService.create(product).subscribe(function () { return _this.loadData(); });
        }
        if (!isNew) {
            var product = formGroup.value;
            this.authorService.update(product).subscribe(function () { return _this.loadData(); });
        }
        sender.closeRow(rowIndex);
    };
    AuthorEditingComponent.prototype.removeHandler = function (_a) {
        var _this = this;
        var dataItem = _a.dataItem;
        this.authorService.delete(dataItem.id).subscribe(function () { return _this.loadData(); });
        this.toastr.success("Deleted");
    };
    AuthorEditingComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    AuthorEditingComponent.prototype.Logout = function () {
        localStorage.removeItem("data");
        window.location.reload();
    };
    AuthorEditingComponent.prototype.dataStateChange = function (state) {
        this.gridState = state;
        this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__["process"])(this.authorList.authorList, this.gridState);
    };
    AuthorEditingComponent.prototype.cloasing = function () {
        this.router.navigate([{ outlets: { editing: null } }]);
    };
    AuthorEditingComponent.prototype.openCreateAuthorDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_create_item_author_create_item_author_component__WEBPACK_IMPORTED_MODULE_9__["CreateItemAuthorComponent"]);
        dialogRef.afterClosed().subscribe(function (result) {
            _this.loadData();
        });
    };
    var AuthorEditingComponent_1;
    AuthorEditingComponent.ctorParameters = function () { return [
        { type: _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_5__["LocalStorage"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialog"] },
        { type: src_app_services_author_service__WEBPACK_IMPORTED_MODULE_7__["AuthorService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_10__["ToastrService"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialogRef"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], AuthorEditingComponent.prototype, "notifyParent", void 0);
    AuthorEditingComponent = AuthorEditingComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "author-editing",
            template: __webpack_require__(/*! raw-loader!./author-editing.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/author-editing/author-editing.component.html"),
            styles: [__webpack_require__(/*! ./author-editing.component.css */ "./src/app/admin-panel/author-editing/author-editing.component.css")]
        })
    ], AuthorEditingComponent);
    return AuthorEditingComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/book-editing/book-editing.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/admin-panel/book-editing/book-editing.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".enlarge {\r\n  width: 50%;\r\n}\r\n.btn {\r\n  color: #ffffff;\r\n  border-radius: 10rem;\r\n  background-color: #926dbe;\r\n}\r\n.k-grid {\r\n  background-clip: unset;\r\n}\r\nimg {\r\n  width: 50px;\r\n}\r\n.div-zoom {\r\n  width: 85px;\r\n  height: 55px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC9ib29rLWVkaXRpbmcvYm9vay1lZGl0aW5nLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLGNBQWM7RUFDZCxvQkFBb0I7RUFDcEIseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLFdBQVc7QUFDYjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7QUFDZCIsImZpbGUiOiJhcHAvYWRtaW4tcGFuZWwvYm9vay1lZGl0aW5nL2Jvb2stZWRpdGluZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVubGFyZ2Uge1xyXG4gIHdpZHRoOiA1MCU7XHJcbn1cclxuLmJ0biB7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogMTByZW07XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzkyNmRiZTtcclxufVxyXG5cclxuLmstZ3JpZCB7XHJcbiAgYmFja2dyb3VuZC1jbGlwOiB1bnNldDtcclxufVxyXG5pbWcge1xyXG4gIHdpZHRoOiA1MHB4O1xyXG59XHJcbi5kaXYtem9vbSB7XHJcbiAgd2lkdGg6IDg1cHg7XHJcbiAgaGVpZ2h0OiA1NXB4O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/admin-panel/book-editing/book-editing.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/admin-panel/book-editing/book-editing.component.ts ***!
  \********************************************************************/
/*! exports provided: BookEditingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookEditingComponent", function() { return BookEditingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/author/get.author.list.view */ "./src/app/models/author/get.author.list.view.ts");
/* harmony import */ var _progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @progress/kendo-data-query */ "./node_modules/@progress/kendo-data-query/dist/es/main.js");
/* harmony import */ var _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-pwa/local-storage */ "./node_modules/@ngx-pwa/local-storage/fesm5/ngx-pwa-local-storage.js");
/* harmony import */ var src_app_services_author_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/author.service */ "./src/app/services/author.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var src_app_services_book_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/book.service */ "./src/app/services/book.service.ts");
/* harmony import */ var src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/authentication/user.service */ "./src/app/services/authentication/user.service.ts");
/* harmony import */ var _create_item_book_create_item_book_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../create-item-book/create-item-book.component */ "./src/app/admin-panel/create-item-book/create-item-book.component.ts");
/* harmony import */ var src_app_models_genre_get_genre_view__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/models/genre/get.genre.view */ "./src/app/models/genre/get.genre.view.ts");
/* harmony import */ var src_app_services_genre_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/services/genre.service */ "./src/app/services/genre.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var src_app_models_book_get_book_list_view__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! src/app/models/book/get.book.list.view */ "./src/app/models/book/get.book.list.view.ts");















var BookEditingComponent = /** @class */ (function () {
    function BookEditingComponent(genreService, localStorage, dialog, authorService, bookService, toastr, userService) {
        this.genreService = genreService;
        this.localStorage = localStorage;
        this.dialog = dialog;
        this.authorService = authorService;
        this.bookService = bookService;
        this.toastr = toastr;
        this.userService = userService;
        this.isOpen = false;
        this.isClose = true;
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.books = new src_app_models_book_get_book_list_view__WEBPACK_IMPORTED_MODULE_14__["GetBookListView"]();
        this.authorIdList = new src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_3__["GetAuthorView"]();
        this.genreIdList = new src_app_models_genre_get_genre_view__WEBPACK_IMPORTED_MODULE_11__["GetAllGenreView"]();
    }
    BookEditingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadData();
        this.authorService.get().subscribe(function (data) {
            _this.authorIdList = data;
        });
        this.genreService.get().subscribe(function (data) {
            _this.genreIdList = data;
        });
        this.admin = this.userService.getIsAdmin();
    };
    BookEditingComponent.prototype.loadData = function () {
        var _this = this;
        this.bookService.getAll().subscribe(function (data) {
            _this.books = data;
            _this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__["process"])(_this.books.books, _this.gridState);
        });
    };
    BookEditingComponent.prototype.addHandler = function (_a) {
        var sender = _a.sender;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](0),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            price: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](0),
            picture: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            authorIdList: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            genreIdList: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("")
        });
        sender.addRow(this.formGroup);
    };
    BookEditingComponent.prototype.editHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.id),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.title, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            price: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.price),
            picture: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.picture),
            authorIdList: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]([]),
            genreIdList: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]([])
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    BookEditingComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    BookEditingComponent.prototype.saveHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, formGroup = _a.formGroup, isNew = _a.isNew;
        if (isNew) {
            var product = formGroup.value;
            this.bookService.create(product).subscribe(function () { return _this.loadData(); });
        }
        if (!isNew) {
            var product = formGroup.value;
            this.bookService.update(product).subscribe(function () { return _this.loadData(); });
        }
        sender.closeRow(rowIndex);
    };
    BookEditingComponent.prototype.removeHandler = function (_a) {
        var _this = this;
        var dataItem = _a.dataItem;
        this.bookService.delete(dataItem.id).subscribe(function () { return _this.loadData(); });
        this.toastr.success("removed");
    };
    BookEditingComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    BookEditingComponent.prototype.Logout = function () {
        localStorage.removeItem("data");
        window.location.reload();
    };
    BookEditingComponent.prototype.authorChange = function (value) {
        var ValuesId = new Array();
        for (var i = 0; i < value.length; i++) {
            var res = value[i].id;
            ValuesId.push(res);
        }
        this.formGroup.get("authorIdList").setValue(ValuesId);
    };
    BookEditingComponent.prototype.genreChange = function (value) {
        var idValues = new Array();
        for (var i = 0; i < value.length; i++) {
            var res = value[i].id;
            idValues.push(res);
        }
        this.formGroup.get("genreIdList").setValue(idValues);
    };
    BookEditingComponent.prototype.getAuthorList = function (book) {
        var authorString = "";
        for (var i = 0; i < book.authorList.length; i++) {
            authorString += book.authorList[i].name + " ";
        }
        return authorString;
    };
    BookEditingComponent.prototype.getGenreList = function (book) {
        var genreString = "";
        for (var i = 0; i < book.genreList.length; i++) {
            genreString += book.genreList[i].title + "";
        }
        return genreString;
    };
    BookEditingComponent.prototype.dataStateChange = function (state) {
        this.gridState = state;
        this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__["process"])(this.books.books, this.gridState);
    };
    BookEditingComponent.prototype.openCreateBookDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_create_item_book_create_item_book_component__WEBPACK_IMPORTED_MODULE_10__["CreateItemBookComponent"]);
        {
        }
        dialogRef.afterClosed().subscribe(function (result) {
            _this.loadData();
        });
    };
    BookEditingComponent.ctorParameters = function () { return [
        { type: src_app_services_genre_service__WEBPACK_IMPORTED_MODULE_12__["GenreService"] },
        { type: _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_5__["LocalStorage"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialog"] },
        { type: src_app_services_author_service__WEBPACK_IMPORTED_MODULE_6__["AuthorService"] },
        { type: src_app_services_book_service__WEBPACK_IMPORTED_MODULE_8__["BookService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_13__["ToastrService"] },
        { type: src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_9__["UserService"] }
    ]; };
    BookEditingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "book-editing",
            template: __webpack_require__(/*! raw-loader!./book-editing.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/book-editing/book-editing.component.html"),
            styles: [__webpack_require__(/*! ./book-editing.component.css */ "./src/app/admin-panel/book-editing/book-editing.component.css")]
        })
    ], BookEditingComponent);
    return BookEditingComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/create-item-author/create-item-author.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/admin-panel/create-item-author/create-item-author.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-typography {\r\n    position: absolute;\r\n    background: none;\r\n    background: rgba(208, 252, 255, 0.88);\r\n    padding: 5px;\r\n    width: 15%;\r\n    height: 235px;\r\n    margin-left: 40%;\r\n    border: 3px #cccccc solid;\r\n    border-radius: 24px;\r\n    padding: 24px 45px;\r\n    text-align: center;\r\n  }\r\n  .headline {\r\n    text-align: center;\r\n    color: rgb(128, 0, 191);\r\n    font-size: 45px;\r\n    font-weight: 1000;\r\n  }\r\n  .overlay {\r\n    content: \"\";\r\n    display: block;\r\n    position: fixed;\r\n    top: 0;\r\n    left: 0;\r\n    height: 100%;\r\n    width: 100%;\r\n    background: rgba(0, 0, 0, 0.5);\r\n    z-index: 100;\r\n    padding-top: 10%;\r\n  }\r\n  .col-sm{\r\n    flex-basis: 2;\r\n  }\r\n  .credit-card{\r\n    margin-top: 25%;\r\n  }\r\n  .toast-container .toast {\r\n    position: relative;\r\n    overflow: hidden;\r\n    margin: 0 0 6px;\r\n    padding: 15px 15px 15px 50px;\r\n    width: 300px;\r\n    border-radius: 3px 3px 3px 3px;\r\n    background-position: 15px center;\r\n    background-repeat: no-repeat;\r\n    background-size: 24px;\r\n    box-shadow: 0 0 12px #999999;\r\n    color: #FFFFFF;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC9jcmVhdGUtaXRlbS1hdXRob3IvY3JlYXRlLWl0ZW0tYXV0aG9yLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLHFDQUFxQztJQUNyQyxZQUFZO0lBQ1osVUFBVTtJQUNWLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsa0JBQWtCO0VBQ3BCO0VBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsdUJBQXVCO0lBQ3ZCLGVBQWU7SUFDZixpQkFBaUI7RUFDbkI7RUFDQTtJQUNFLFdBQVc7SUFDWCxjQUFjO0lBQ2QsZUFBZTtJQUNmLE1BQU07SUFDTixPQUFPO0lBQ1AsWUFBWTtJQUNaLFdBQVc7SUFDWCw4QkFBOEI7SUFDOUIsWUFBWTtJQUNaLGdCQUFnQjtFQUNsQjtFQUNBO0lBQ0UsYUFBYTtFQUNmO0VBQ0E7SUFDRSxlQUFlO0VBQ2pCO0VBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZiw0QkFBNEI7SUFDNUIsWUFBWTtJQUNaLDhCQUE4QjtJQUM5QixnQ0FBZ0M7SUFDaEMsNEJBQTRCO0lBQzVCLHFCQUFxQjtJQUNyQiw0QkFBNEI7SUFDNUIsY0FBYztFQUNoQiIsImZpbGUiOiJhcHAvYWRtaW4tcGFuZWwvY3JlYXRlLWl0ZW0tYXV0aG9yL2NyZWF0ZS1pdGVtLWF1dGhvci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC10eXBvZ3JhcGh5IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDIwOCwgMjUyLCAyNTUsIDAuODgpO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgd2lkdGg6IDE1JTtcclxuICAgIGhlaWdodDogMjM1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogNDAlO1xyXG4gICAgYm9yZGVyOiAzcHggI2NjY2NjYyBzb2xpZDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBwYWRkaW5nOiAyNHB4IDQ1cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5oZWFkbGluZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBjb2xvcjogcmdiKDEyOCwgMCwgMTkxKTtcclxuICAgIGZvbnQtc2l6ZTogNDVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiAxMDAwO1xyXG4gIH1cclxuICAub3ZlcmxheSB7XHJcbiAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNSk7XHJcbiAgICB6LWluZGV4OiAxMDA7XHJcbiAgICBwYWRkaW5nLXRvcDogMTAlO1xyXG4gIH1cclxuICAuY29sLXNte1xyXG4gICAgZmxleC1iYXNpczogMjtcclxuICB9XHJcbiAgLmNyZWRpdC1jYXJke1xyXG4gICAgbWFyZ2luLXRvcDogMjUlO1xyXG4gIH1cclxuICAudG9hc3QtY29udGFpbmVyIC50b2FzdCB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgbWFyZ2luOiAwIDAgNnB4O1xyXG4gICAgcGFkZGluZzogMTVweCAxNXB4IDE1cHggNTBweDtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweCAzcHggM3B4IDNweDtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IDE1cHggY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogMjRweDtcclxuICAgIGJveC1zaGFkb3c6IDAgMCAxMnB4ICM5OTk5OTk7XHJcbiAgICBjb2xvcjogI0ZGRkZGRjtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/admin-panel/create-item-author/create-item-author.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/admin-panel/create-item-author/create-item-author.component.ts ***!
  \********************************************************************************/
/*! exports provided: CreateItemAuthorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateItemAuthorComponent", function() { return CreateItemAuthorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_services_author_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/author.service */ "./src/app/services/author.service.ts");
/* harmony import */ var src_app_models_author_create_author_view__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/models/author/create.author.view */ "./src/app/models/author/create.author.view.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");








var CreateItemAuthorComponent = /** @class */ (function () {
    function CreateItemAuthorComponent(data, formBuilder, authorService, toastr, dialogRef) {
        this.data = data;
        this.formBuilder = formBuilder;
        this.authorService = authorService;
        this.toastr = toastr;
        this.dialogRef = dialogRef;
    }
    CreateItemAuthorComponent.prototype.ngOnInit = function () {
        this.formGroup = this.formBuilder.group({
            name: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    CreateItemAuthorComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.formGroup.invalid) {
            return this.toastr.error("Enter all fields");
        }
        var product = new src_app_models_author_create_author_view__WEBPACK_IMPORTED_MODULE_5__["CreateAuthorView"]();
        product.name = this.formGroup.controls.name.value;
        this.authorService.create(product).subscribe(function (res) {
            _this.dialogRef.close();
        });
        this.toastr.success("Author Created");
    };
    Object.defineProperty(CreateItemAuthorComponent.prototype, "f", {
        get: function () {
            return this.formGroup.controls;
        },
        enumerable: true,
        configurable: true
    });
    CreateItemAuthorComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"],] }] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: src_app_services_author_service__WEBPACK_IMPORTED_MODULE_4__["AuthorService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"] }
    ]; };
    CreateItemAuthorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "create-item-author",
            template: __webpack_require__(/*! raw-loader!./create-item-author.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/create-item-author/create-item-author.component.html"),
            styles: [__webpack_require__(/*! ./create-item-author.component.css */ "./src/app/admin-panel/create-item-author/create-item-author.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"]))
    ], CreateItemAuthorComponent);
    return CreateItemAuthorComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/create-item-book/create-item-book.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/admin-panel/create-item-book/create-item-book.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-typography {\r\n    position: absolute;\r\n    background: none;\r\n    background: rgba(208, 252, 255, 0.88);\r\n    padding: 5px;\r\n    width: 25%;\r\n    width: 26%;\r\n    height: 308px;\r\n    margin-left: 35%;\r\n    border: 3px #cccccc solid;\r\n    border-radius: 24px;\r\n    padding: 0px 150px;\r\n    text-align: center;\r\n  }\r\n  .headline {\r\n    text-align: center;\r\n    color: rgb(128, 0, 191);\r\n    font-size: 45px;\r\n    font-weight: 1000;\r\n  }\r\n  .overlay {\r\n    content: \"\";\r\n    display: block;\r\n    position: fixed;\r\n    top: 0;\r\n    left: 0;\r\n    height: 100%;\r\n    width: 100%;\r\n    background: rgba(0, 0, 0, 0.5);\r\n    z-index: 100;\r\n    padding-top:10%;\r\n  }\r\n  .col-sm{\r\n    flex-basis: 2;\r\n  }\r\n  .credit-card{\r\n    margin-top: 25%;\r\n  }\r\n  .mat\r\n  .mat-button, .mat-flat-button, .mat-icon-button, .mat-stroked-button { border-radius: unset;\r\n    line-height: unset; }\r\n  .mat-ripple{\r\n      border-radius: unset;\r\n      line-height: unset; \r\n    }\r\n  .ng-select {\r\n      width: 91%;\r\n      display: -webkit-inline-box;\r\n      display: inline-flex;\r\n    }\r\n  .inp2{\r\n    margin-left: 5%;\r\n  }\r\n  .toast-bottom-right .toast-container\r\n  {background-color: red}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC9jcmVhdGUtaXRlbS1ib29rL2NyZWF0ZS1pdGVtLWJvb2suY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIscUNBQXFDO0lBQ3JDLFlBQVk7SUFDWixVQUFVO0lBQ1YsVUFBVTtJQUNWLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsa0JBQWtCO0VBQ3BCO0VBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsdUJBQXVCO0lBQ3ZCLGVBQWU7SUFDZixpQkFBaUI7RUFDbkI7RUFDQTtJQUNFLFdBQVc7SUFDWCxjQUFjO0lBQ2QsZUFBZTtJQUNmLE1BQU07SUFDTixPQUFPO0lBQ1AsWUFBWTtJQUNaLFdBQVc7SUFDWCw4QkFBOEI7SUFDOUIsWUFBWTtJQUNaLGVBQWU7RUFDakI7RUFDQTtJQUNFLGFBQWE7RUFDZjtFQUNBO0lBQ0UsZUFBZTtFQUNqQjtFQUNBO3lFQUN1RSxvQkFBb0I7SUFDekYsa0JBQWtCLEVBQUU7RUFDdEI7TUFDSSxvQkFBb0I7TUFDcEIsa0JBQWtCO0lBQ3BCO0VBRUE7TUFDRSxVQUFVO01BQ1YsMkJBQW9CO01BQXBCLG9CQUFvQjtJQUN0QjtFQUNGO0lBQ0UsZUFBZTtFQUNqQjtFQUNBO0dBQ0MscUJBQXFCIiwiZmlsZSI6ImFwcC9hZG1pbi1wYW5lbC9jcmVhdGUtaXRlbS1ib29rL2NyZWF0ZS1pdGVtLWJvb2suY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtdHlwb2dyYXBoeSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgyMDgsIDI1MiwgMjU1LCAwLjg4KTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIHdpZHRoOiAyNSU7XHJcbiAgICB3aWR0aDogMjYlO1xyXG4gICAgaGVpZ2h0OiAzMDhweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAzNSU7XHJcbiAgICBib3JkZXI6IDNweCAjY2NjY2NjIHNvbGlkO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcclxuICAgIHBhZGRpbmc6IDBweCAxNTBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgLmhlYWRsaW5lIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGNvbG9yOiByZ2IoMTI4LCAwLCAxOTEpO1xyXG4gICAgZm9udC1zaXplOiA0NXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDEwMDA7XHJcbiAgfVxyXG4gIC5vdmVybGF5IHtcclxuICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC41KTtcclxuICAgIHotaW5kZXg6IDEwMDtcclxuICAgIHBhZGRpbmctdG9wOjEwJTtcclxuICB9XHJcbiAgLmNvbC1zbXtcclxuICAgIGZsZXgtYmFzaXM6IDI7XHJcbiAgfVxyXG4gIC5jcmVkaXQtY2FyZHtcclxuICAgIG1hcmdpbi10b3A6IDI1JTtcclxuICB9XHJcbiAgLm1hdFxyXG4gIC5tYXQtYnV0dG9uLCAubWF0LWZsYXQtYnV0dG9uLCAubWF0LWljb24tYnV0dG9uLCAubWF0LXN0cm9rZWQtYnV0dG9uIHsgYm9yZGVyLXJhZGl1czogdW5zZXQ7XHJcbiAgICBsaW5lLWhlaWdodDogdW5zZXQ7IH1cclxuICAubWF0LXJpcHBsZXtcclxuICAgICAgYm9yZGVyLXJhZGl1czogdW5zZXQ7XHJcbiAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDsgXHJcbiAgICB9XHJcblxyXG4gICAgLm5nLXNlbGVjdCB7XHJcbiAgICAgIHdpZHRoOiA5MSU7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAgfVxyXG4gIC5pbnAye1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gIH1cclxuICAudG9hc3QtYm90dG9tLXJpZ2h0IC50b2FzdC1jb250YWluZXJcclxuICB7YmFja2dyb3VuZC1jb2xvcjogcmVkfSJdfQ== */"

/***/ }),

/***/ "./src/app/admin-panel/create-item-book/create-item-book.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/admin-panel/create-item-book/create-item-book.component.ts ***!
  \****************************************************************************/
/*! exports provided: CreateItemBookComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateItemBookComponent", function() { return CreateItemBookComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var src_app_services_book_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/book.service */ "./src/app/services/book.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_author_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/author.service */ "./src/app/services/author.service.ts");
/* harmony import */ var src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/models/author/get.author.list.view */ "./src/app/models/author/get.author.list.view.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var src_app_models_genre_get_genre_view__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/models/genre/get.genre.view */ "./src/app/models/genre/get.genre.view.ts");
/* harmony import */ var src_app_services_genre_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/genre.service */ "./src/app/services/genre.service.ts");
/* harmony import */ var src_app_models_book_ceate_book_view__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/models/book/ceate.book.view */ "./src/app/models/book/ceate.book.view.ts");











var CreateItemBookComponent = /** @class */ (function () {
    function CreateItemBookComponent(data, bookService, formBuilder, dialogRef, toastr, authorService, genreService) {
        this.data = data;
        this.bookService = bookService;
        this.formBuilder = formBuilder;
        this.dialogRef = dialogRef;
        this.toastr = toastr;
        this.authorService = authorService;
        this.genreService = genreService;
        this.createBookModel = new src_app_models_book_ceate_book_view__WEBPACK_IMPORTED_MODULE_10__["CreateBookView"]();
        this.authorIdList = new src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_6__["GetAuthorView"]();
        this.genreIdList = new src_app_models_genre_get_genre_view__WEBPACK_IMPORTED_MODULE_8__["GetAllGenreView"]();
    }
    CreateItemBookComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authorService.get().subscribe(function (data) {
            _this.authorIdList = data;
        });
        this.genreService.get().subscribe(function (data) {
            _this.genreIdList = data;
        });
        this.formGroup = this.formBuilder.group({
            title: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            price: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            authorIdList: [""],
            genreIdList: [""],
            description: [""],
            picture: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
        });
    };
    CreateItemBookComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.selectedFile = event.target.files[0];
        var uploadData = new FormData();
        uploadData.append("image", this.selectedFile);
        if (uploadData == null) {
            this.toastr.error("bla");
        }
        else {
            this.bookService.uploadImage(uploadData).subscribe(function (res) {
                _this.stringUrl = res;
            });
        }
    };
    CreateItemBookComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.formGroup.invalid) {
            return this.toastr.error("Enter all fields");
        }
        else {
            var product = new src_app_models_book_ceate_book_view__WEBPACK_IMPORTED_MODULE_10__["CreateBookView"]();
            product.title = this.formGroup.controls.title.value;
            product.price = this.formGroup.controls.price.value;
            product.description = this.formGroup.controls.description.value;
            product.authorIdList = this.formGroup.controls.authorIdList.value;
            product.genreIdList = this.formGroup.controls.genreIdList.value;
            product.picture = this.stringUrl;
            this.bookService.create(product).subscribe(function (res) {
                _this.dialogRef.close();
            });
            this.toastr.success("Succes", "sds");
        }
    };
    Object.defineProperty(CreateItemBookComponent.prototype, "f", {
        get: function () {
            return this.formGroup.controls;
        },
        enumerable: true,
        configurable: true
    });
    CreateItemBookComponent.prototype.getAuthor = function () {
        this.bookService.getAll();
    };
    CreateItemBookComponent.prototype.getGenre = function () {
        this.bookService.getAll();
    };
    CreateItemBookComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] },
        { type: src_app_services_book_service__WEBPACK_IMPORTED_MODULE_3__["BookService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"] },
        { type: src_app_services_author_service__WEBPACK_IMPORTED_MODULE_5__["AuthorService"] },
        { type: src_app_services_genre_service__WEBPACK_IMPORTED_MODULE_9__["GenreService"] }
    ]; };
    CreateItemBookComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "create-item-book",
            template: __webpack_require__(/*! raw-loader!./create-item-book.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/create-item-book/create-item-book.component.html"),
            styles: [__webpack_require__(/*! ./create-item-book.component.css */ "./src/app/admin-panel/create-item-book/create-item-book.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"]))
    ], CreateItemBookComponent);
    return CreateItemBookComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/create-item-genre/create-item-genre.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/admin-panel/create-item-genre/create-item-genre.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-typography {\r\n    position: absolute;\r\n    background: none;\r\n    background: rgba(208, 252, 255, 0.88);\r\n    padding: 5px;\r\n    width: 25%;\r\n    width: 26%;\r\n    height: 308px;\r\n    margin-left: 35%;\r\n    border: 3px #cccccc solid;\r\n    border-radius: 24px;\r\n    padding: 0px 150px;\r\n    text-align: center;\r\n  }\r\n  .headline {\r\n    text-align: center;\r\n    color: rgb(128, 0, 191);\r\n    font-size: 45px;\r\n    font-weight: 1000;\r\n  }\r\n  .overlay {\r\n    content: \"\";\r\n    display: block;\r\n    position: fixed;\r\n    top: 0;\r\n    left: 0;\r\n    height: 100%;\r\n    width: 100%;\r\n    background: rgba(0, 0, 0, 0.5);\r\n    z-index: 100;\r\n    padding-top:10%;\r\n  }\r\n  .col-sm{\r\n    flex-basis: 2;\r\n  }\r\n  .credit-card{\r\n    margin-top: 25%;\r\n  }\r\n  .mat\r\n  .mat-button, .mat-flat-button, .mat-icon-button, .mat-stroked-button { border-radius: unset;\r\n    line-height: unset; }\r\n  .mat-ripple{\r\n      border-radius: unset;\r\n      line-height: unset; \r\n    }\r\n  .ng-select {\r\n      width: 91%;\r\n      display: -webkit-inline-box;\r\n      display: inline-flex;\r\n    }\r\n  .inp2{\r\n    margin-left: 5%;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC9jcmVhdGUtaXRlbS1nZW5yZS9jcmVhdGUtaXRlbS1nZW5yZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixxQ0FBcUM7SUFDckMsWUFBWTtJQUNaLFVBQVU7SUFDVixVQUFVO0lBQ1YsYUFBYTtJQUNiLGdCQUFnQjtJQUNoQix5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixrQkFBa0I7RUFDcEI7RUFDQTtJQUNFLGtCQUFrQjtJQUNsQix1QkFBdUI7SUFDdkIsZUFBZTtJQUNmLGlCQUFpQjtFQUNuQjtFQUNBO0lBQ0UsV0FBVztJQUNYLGNBQWM7SUFDZCxlQUFlO0lBQ2YsTUFBTTtJQUNOLE9BQU87SUFDUCxZQUFZO0lBQ1osV0FBVztJQUNYLDhCQUE4QjtJQUM5QixZQUFZO0lBQ1osZUFBZTtFQUNqQjtFQUNBO0lBQ0UsYUFBYTtFQUNmO0VBQ0E7SUFDRSxlQUFlO0VBQ2pCO0VBQ0E7eUVBQ3VFLG9CQUFvQjtJQUN6RixrQkFBa0IsRUFBRTtFQUN0QjtNQUNJLG9CQUFvQjtNQUNwQixrQkFBa0I7SUFDcEI7RUFFQTtNQUNFLFVBQVU7TUFDViwyQkFBb0I7TUFBcEIsb0JBQW9CO0lBQ3RCO0VBQ0Y7SUFDRSxlQUFlO0VBQ2pCIiwiZmlsZSI6ImFwcC9hZG1pbi1wYW5lbC9jcmVhdGUtaXRlbS1nZW5yZS9jcmVhdGUtaXRlbS1nZW5yZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC10eXBvZ3JhcGh5IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDIwOCwgMjUyLCAyNTUsIDAuODgpO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgd2lkdGg6IDI1JTtcclxuICAgIHdpZHRoOiAyNiU7XHJcbiAgICBoZWlnaHQ6IDMwOHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDM1JTtcclxuICAgIGJvcmRlcjogM3B4ICNjY2NjY2Mgc29saWQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xyXG4gICAgcGFkZGluZzogMHB4IDE1MHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAuaGVhZGxpbmUge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgY29sb3I6IHJnYigxMjgsIDAsIDE5MSk7XHJcbiAgICBmb250LXNpemU6IDQ1cHg7XHJcbiAgICBmb250LXdlaWdodDogMTAwMDtcclxuICB9XHJcbiAgLm92ZXJsYXkge1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gICAgei1pbmRleDogMTAwO1xyXG4gICAgcGFkZGluZy10b3A6MTAlO1xyXG4gIH1cclxuICAuY29sLXNte1xyXG4gICAgZmxleC1iYXNpczogMjtcclxuICB9XHJcbiAgLmNyZWRpdC1jYXJke1xyXG4gICAgbWFyZ2luLXRvcDogMjUlO1xyXG4gIH1cclxuICAubWF0XHJcbiAgLm1hdC1idXR0b24sIC5tYXQtZmxhdC1idXR0b24sIC5tYXQtaWNvbi1idXR0b24sIC5tYXQtc3Ryb2tlZC1idXR0b24geyBib3JkZXItcmFkaXVzOiB1bnNldDtcclxuICAgIGxpbmUtaGVpZ2h0OiB1bnNldDsgfVxyXG4gIC5tYXQtcmlwcGxle1xyXG4gICAgICBib3JkZXItcmFkaXVzOiB1bnNldDtcclxuICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0OyBcclxuICAgIH1cclxuXHJcbiAgICAubmctc2VsZWN0IHtcclxuICAgICAgd2lkdGg6IDkxJTtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICB9XHJcbiAgLmlucDJ7XHJcbiAgICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/admin-panel/create-item-genre/create-item-genre.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/admin-panel/create-item-genre/create-item-genre.component.ts ***!
  \******************************************************************************/
/*! exports provided: CreateItemGenreComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateItemGenreComponent", function() { return CreateItemGenreComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_genre_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/genre.service */ "./src/app/services/genre.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var src_app_models_genre_create_genre_view__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/models/genre/create.genre.view */ "./src/app/models/genre/create.genre.view.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");







var CreateItemGenreComponent = /** @class */ (function () {
    function CreateItemGenreComponent(data, formBuilder, genreService, dialogRef, toastr) {
        this.data = data;
        this.formBuilder = formBuilder;
        this.genreService = genreService;
        this.dialogRef = dialogRef;
        this.toastr = toastr;
    }
    CreateItemGenreComponent.prototype.ngOnInit = function () {
        this.formGroup = this.formBuilder.group({
            name: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    CreateItemGenreComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.formGroup.invalid) {
            return this.toastr.error("Enter all fields");
        }
        var product = new src_app_models_genre_create_genre_view__WEBPACK_IMPORTED_MODULE_5__["CreateGenreView"]();
        product.Title = this.formGroup.controls.name.value;
        this.genreService.create(product).subscribe(function (res) {
            _this.dialogRef.close();
        });
        this.toastr.success("Magazine create");
    };
    Object.defineProperty(CreateItemGenreComponent.prototype, "f", {
        get: function () {
            return this.formGroup.controls;
        },
        enumerable: true,
        configurable: true
    });
    CreateItemGenreComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MAT_DIALOG_DATA"],] }] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: src_app_services_genre_service__WEBPACK_IMPORTED_MODULE_3__["GenreService"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"] }
    ]; };
    CreateItemGenreComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "create-item-genre",
            template: __webpack_require__(/*! raw-loader!./create-item-genre.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/create-item-genre/create-item-genre.component.html"),
            styles: [__webpack_require__(/*! ./create-item-genre.component.css */ "./src/app/admin-panel/create-item-genre/create-item-genre.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MAT_DIALOG_DATA"]))
    ], CreateItemGenreComponent);
    return CreateItemGenreComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/create-item-magazine/create-item-magazine.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/admin-panel/create-item-magazine/create-item-magazine.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-typography {\r\n    position: absolute;\r\n    background: none;\r\n    background: rgba(208, 252, 255, 0.88);\r\n    padding: 5px;\r\n    width: 26%;\r\n    height: 258px;\r\n    margin-left: 35%;\r\n    border: 3px #cccccc solid;\r\n    border-radius: 24px;\r\n    padding: 0px 150px;\r\n    text-align: center;\r\n  }\r\n  .headline {\r\n    text-align: center;\r\n    color: rgb(128, 0, 191);\r\n    font-size: 45px;\r\n    font-weight: 1000;\r\n  }\r\n  .overlay {\r\n    content: \"\";\r\n    display: block;\r\n    position: fixed;\r\n    top: 0;\r\n    left: 0;\r\n    height: 100%;\r\n    width: 100%;\r\n    background: rgba(0, 0, 0, 0.5);\r\n    z-index: 100;\r\n  }\r\n  .credit-card{\r\n    margin-top: 25%;\r\n  }\r\n  .overlay{\r\n    padding-top: 10%;\r\n  }\r\n  .inp2{\r\n    margin-left: 5%;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC9jcmVhdGUtaXRlbS1tYWdhemluZS9jcmVhdGUtaXRlbS1tYWdhemluZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixxQ0FBcUM7SUFDckMsWUFBWTtJQUNaLFVBQVU7SUFDVixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLHVCQUF1QjtJQUN2QixlQUFlO0lBQ2YsaUJBQWlCO0VBQ25CO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsY0FBYztJQUNkLGVBQWU7SUFDZixNQUFNO0lBQ04sT0FBTztJQUNQLFlBQVk7SUFDWixXQUFXO0lBQ1gsOEJBQThCO0lBQzlCLFlBQVk7RUFDZDtFQUNBO0lBQ0UsZUFBZTtFQUNqQjtFQUNBO0lBQ0UsZ0JBQWdCO0VBQ2xCO0VBQ0E7SUFDRSxlQUFlO0VBQ2pCIiwiZmlsZSI6ImFwcC9hZG1pbi1wYW5lbC9jcmVhdGUtaXRlbS1tYWdhemluZS9jcmVhdGUtaXRlbS1tYWdhemluZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC10eXBvZ3JhcGh5IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDIwOCwgMjUyLCAyNTUsIDAuODgpO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgd2lkdGg6IDI2JTtcclxuICAgIGhlaWdodDogMjU4cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMzUlO1xyXG4gICAgYm9yZGVyOiAzcHggI2NjY2NjYyBzb2xpZDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBwYWRkaW5nOiAwcHggMTUwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5oZWFkbGluZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBjb2xvcjogcmdiKDEyOCwgMCwgMTkxKTtcclxuICAgIGZvbnQtc2l6ZTogNDVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiAxMDAwO1xyXG4gIH1cclxuICAub3ZlcmxheSB7XHJcbiAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNSk7XHJcbiAgICB6LWluZGV4OiAxMDA7XHJcbiAgfVxyXG4gIC5jcmVkaXQtY2FyZHtcclxuICAgIG1hcmdpbi10b3A6IDI1JTtcclxuICB9XHJcbiAgLm92ZXJsYXl7XHJcbiAgICBwYWRkaW5nLXRvcDogMTAlO1xyXG4gIH1cclxuICAuaW5wMntcclxuICAgIG1hcmdpbi1sZWZ0OiA1JTtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/admin-panel/create-item-magazine/create-item-magazine.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/admin-panel/create-item-magazine/create-item-magazine.component.ts ***!
  \************************************************************************************/
/*! exports provided: CreateItemMagazineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateItemMagazineComponent", function() { return CreateItemMagazineComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/magazine.service */ "./src/app/services/magazine.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var src_app_models_magazine_create_magazine_view__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/models/magazine/create.magazine.view */ "./src/app/models/magazine/create.magazine.view.ts");







var CreateItemMagazineComponent = /** @class */ (function () {
    function CreateItemMagazineComponent(data, magazineService, formBuilder, dialogRef, toastr) {
        this.data = data;
        this.magazineService = magazineService;
        this.formBuilder = formBuilder;
        this.dialogRef = dialogRef;
        this.toastr = toastr;
    }
    CreateItemMagazineComponent.prototype.ngOnInit = function () {
        this.formGroup = this.formBuilder.group({
            title: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            price: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            description: [""],
            picture: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    CreateItemMagazineComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.selectedFile = event.target.files[0];
        var uploadData = new FormData();
        uploadData.append("image", this.selectedFile);
        this.magazineService.uploadImage(uploadData).subscribe(function (res) {
            _this.stringUrl = res;
        });
    };
    CreateItemMagazineComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.formGroup.invalid) {
            return this.toastr.error("Enter all fields");
        }
        var product = new src_app_models_magazine_create_magazine_view__WEBPACK_IMPORTED_MODULE_6__["CreateMagazineView"]();
        product.title = this.formGroup.controls.title.value;
        product.price = this.formGroup.controls.price.value;
        product.description = this.formGroup.controls.description.value;
        product.picture = this.stringUrl;
        this.magazineService.create(product).subscribe(function (res) {
            _this.dialogRef.close();
        });
        this.toastr.success("Magazine create");
    };
    Object.defineProperty(CreateItemMagazineComponent.prototype, "f", {
        get: function () {
            return this.formGroup.controls;
        },
        enumerable: true,
        configurable: true
    });
    CreateItemMagazineComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"],] }] },
        { type: src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_4__["MagazineService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"] }
    ]; };
    CreateItemMagazineComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "create-item-magazine",
            template: __webpack_require__(/*! raw-loader!./create-item-magazine.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/create-item-magazine/create-item-magazine.component.html"),
            styles: [__webpack_require__(/*! ./create-item-magazine.component.css */ "./src/app/admin-panel/create-item-magazine/create-item-magazine.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"]))
    ], CreateItemMagazineComponent);
    return CreateItemMagazineComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/create-user/create-user.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/admin-panel/create-user/create-user.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-typography {\r\n    position: absolute;\r\n    background: none;\r\n    background: rgba(208, 252, 255, 0.88);\r\n    padding: 5px;\r\n    width: 26%;\r\n    height: 258px;\r\n    margin-left: 35%;\r\n    border: 3px #cccccc solid;\r\n    border-radius: 24px;\r\n    padding: 0px 150px;\r\n    text-align: center;\r\n  }\r\n  .headline {\r\n    text-align: center;\r\n    color: rgb(128, 0, 191);\r\n    font-size: 45px;\r\n    font-weight: 1000;\r\n  }\r\n  .overlay {\r\n    content: \"\";\r\n    display: block;\r\n    position: fixed;\r\n    top: 0;\r\n    left: 0;\r\n    height: 100%;\r\n    width: 100%;\r\n    background: rgba(0, 0, 0, 0.5);\r\n    z-index: 100;\r\n  }\r\n  .credit-card{\r\n    margin-top: 25%;\r\n  }\r\n  .overlay{\r\n    padding-top: 10%;\r\n  }\r\n  .inp2{\r\n    margin-left: 5%;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC9jcmVhdGUtdXNlci9jcmVhdGUtdXNlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixxQ0FBcUM7SUFDckMsWUFBWTtJQUNaLFVBQVU7SUFDVixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLHVCQUF1QjtJQUN2QixlQUFlO0lBQ2YsaUJBQWlCO0VBQ25CO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsY0FBYztJQUNkLGVBQWU7SUFDZixNQUFNO0lBQ04sT0FBTztJQUNQLFlBQVk7SUFDWixXQUFXO0lBQ1gsOEJBQThCO0lBQzlCLFlBQVk7RUFDZDtFQUNBO0lBQ0UsZUFBZTtFQUNqQjtFQUNBO0lBQ0UsZ0JBQWdCO0VBQ2xCO0VBQ0E7SUFDRSxlQUFlO0VBQ2pCIiwiZmlsZSI6ImFwcC9hZG1pbi1wYW5lbC9jcmVhdGUtdXNlci9jcmVhdGUtdXNlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC10eXBvZ3JhcGh5IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDIwOCwgMjUyLCAyNTUsIDAuODgpO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgd2lkdGg6IDI2JTtcclxuICAgIGhlaWdodDogMjU4cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMzUlO1xyXG4gICAgYm9yZGVyOiAzcHggI2NjY2NjYyBzb2xpZDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBwYWRkaW5nOiAwcHggMTUwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5oZWFkbGluZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBjb2xvcjogcmdiKDEyOCwgMCwgMTkxKTtcclxuICAgIGZvbnQtc2l6ZTogNDVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiAxMDAwO1xyXG4gIH1cclxuICAub3ZlcmxheSB7XHJcbiAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNSk7XHJcbiAgICB6LWluZGV4OiAxMDA7XHJcbiAgfVxyXG4gIC5jcmVkaXQtY2FyZHtcclxuICAgIG1hcmdpbi10b3A6IDI1JTtcclxuICB9XHJcbiAgLm92ZXJsYXl7XHJcbiAgICBwYWRkaW5nLXRvcDogMTAlO1xyXG4gIH1cclxuICAuaW5wMntcclxuICAgIG1hcmdpbi1sZWZ0OiA1JTtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/admin-panel/create-user/create-user.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/admin-panel/create-user/create-user.component.ts ***!
  \******************************************************************/
/*! exports provided: CreateUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateUserComponent", function() { return CreateUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/authentication/user.service */ "./src/app/services/authentication/user.service.ts");
/* harmony import */ var src_app_models_user_cereate_users_view__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/models/user/cereate.users.view */ "./src/app/models/user/cereate.users.view.ts");







var CreateUserComponent = /** @class */ (function () {
    function CreateUserComponent(data, userService, formBuilder, dialogRef, toastr) {
        this.data = data;
        this.userService = userService;
        this.formBuilder = formBuilder;
        this.dialogRef = dialogRef;
        this.toastr = toastr;
    }
    CreateUserComponent.prototype.ngOnInit = function () {
        this.formGroup = this.formBuilder.group({
            email: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            role: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    };
    CreateUserComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.formGroup.invalid) {
            return this.toastr.error("Enter all fields");
        }
        var user = new src_app_models_user_cereate_users_view__WEBPACK_IMPORTED_MODULE_6__["CreateUserView"]();
        user.email = this.formGroup.controls.email.value;
        user.emailConfirmed = true;
        user.password = this.formGroup.controls.password.value;
        user.role = this.formGroup.controls.role.value;
        this.userService.CreateUser(user).subscribe(function (res) {
            _this.dialogRef.close();
        });
        this.toastr.success("User create");
    };
    Object.defineProperty(CreateUserComponent.prototype, "f", {
        get: function () {
            return this.formGroup.controls;
        },
        enumerable: true,
        configurable: true
    });
    CreateUserComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] },
        { type: src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] }
    ]; };
    CreateUserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "create-user",
            template: __webpack_require__(/*! raw-loader!./create-user.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/create-user/create-user.component.html"),
            styles: [__webpack_require__(/*! ./create-user.component.css */ "./src/app/admin-panel/create-user/create-user.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"]))
    ], CreateUserComponent);
    return CreateUserComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/genre-editing/genre-editing.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/admin-panel/genre-editing/genre-editing.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".enlarge{\r\n    width: 50%\r\n}\r\n.k-grid {\r\n    background-clip: unset;\r\n}\r\n.enlarge {\r\n    width: 50%;\r\n  }\r\n.btn {\r\n    color: #ffffff;\r\n    border-radius: 10rem;\r\n    background-color: #926dbe;\r\n  }\r\n.k-grid {\r\n    background-clip: unset;\r\n  }\r\nimg {\r\n    width: 50px;\r\n  }\r\n.div-zoom {\r\n    width: 85px;\r\n    height: 55px;\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC9nZW5yZS1lZGl0aW5nL2dlbnJlLWVkaXRpbmcuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJO0FBQ0o7QUFDQTtJQUNJLHNCQUFzQjtBQUMxQjtBQUNBO0lBQ0ksVUFBVTtFQUNaO0FBQ0E7SUFDRSxjQUFjO0lBQ2Qsb0JBQW9CO0lBQ3BCLHlCQUF5QjtFQUMzQjtBQUVBO0lBQ0Usc0JBQXNCO0VBQ3hCO0FBQ0E7SUFDRSxXQUFXO0VBQ2I7QUFDQTtJQUNFLFdBQVc7SUFDWCxZQUFZO0VBQ2QiLCJmaWxlIjoiYXBwL2FkbWluLXBhbmVsL2dlbnJlLWVkaXRpbmcvZ2VucmUtZWRpdGluZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVubGFyZ2V7XHJcbiAgICB3aWR0aDogNTAlXHJcbn1cclxuLmstZ3JpZCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNsaXA6IHVuc2V0O1xyXG59XHJcbi5lbmxhcmdlIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgfVxyXG4gIC5idG4ge1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHJlbTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM5MjZkYmU7XHJcbiAgfVxyXG4gIFxyXG4gIC5rLWdyaWQge1xyXG4gICAgYmFja2dyb3VuZC1jbGlwOiB1bnNldDtcclxuICB9XHJcbiAgaW1nIHtcclxuICAgIHdpZHRoOiA1MHB4O1xyXG4gIH1cclxuICAuZGl2LXpvb20ge1xyXG4gICAgd2lkdGg6IDg1cHg7XHJcbiAgICBoZWlnaHQ6IDU1cHg7XHJcbiAgfVxyXG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/admin-panel/genre-editing/genre-editing.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/admin-panel/genre-editing/genre-editing.component.ts ***!
  \**********************************************************************/
/*! exports provided: GenreEditingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenreEditingComponent", function() { return GenreEditingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_models_genre_get_genre_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/genre/get.genre.view */ "./src/app/models/genre/get.genre.view.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-pwa/local-storage */ "./node_modules/@ngx-pwa/local-storage/fesm5/ngx-pwa-local-storage.js");
/* harmony import */ var src_app_services_genre_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/genre.service */ "./src/app/services/genre.service.ts");
/* harmony import */ var _progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @progress/kendo-data-query */ "./node_modules/@progress/kendo-data-query/dist/es/main.js");
/* harmony import */ var _create_item_genre_create_item_genre_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../create-item-genre/create-item-genre.component */ "./src/app/admin-panel/create-item-genre/create-item-genre.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");











var GenreEditingComponent = /** @class */ (function () {
    function GenreEditingComponent(localStorage, dialog, toastr, genreService, router) {
        this.localStorage = localStorage;
        this.dialog = dialog;
        this.toastr = toastr;
        this.genreService = genreService;
        this.router = router;
        this.notifyParent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.genreList = new src_app_models_genre_get_genre_view__WEBPACK_IMPORTED_MODULE_3__["GetAllGenreView"]();
    }
    GenreEditingComponent_1 = GenreEditingComponent;
    GenreEditingComponent.prototype.ngOnInit = function () {
        this.loadData();
        this.notifyParent.emit(GenreEditingComponent_1);
    };
    GenreEditingComponent.prototype.loadData = function () {
        var _this = this;
        this.genreService.get().subscribe(function (data) {
            _this.genreList = data;
            _this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_8__["process"])(_this.genreList.genreList, _this.gridState);
        });
    };
    GenreEditingComponent.prototype.addHandler = function (_a) {
        var sender = _a.sender;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](0),
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
        });
        sender.addRow(this.formGroup);
    };
    GenreEditingComponent.prototype.editHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.id),
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.name)
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    GenreEditingComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    GenreEditingComponent.prototype.removeHandler = function (_a) {
        var _this = this;
        var dataItem = _a.dataItem;
        this.genreService.delete(dataItem.id).subscribe(function () { return _this.loadData(); });
        this.toastr.success("removed");
    };
    GenreEditingComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    GenreEditingComponent.prototype.Logout = function () {
        localStorage.removeItem("data");
        window.location.reload();
    };
    GenreEditingComponent.prototype.dataStateChange = function (state) {
        this.gridState = state;
        this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_8__["process"])(this.genreList.genreList, this.gridState);
    };
    GenreEditingComponent.prototype.cloasing = function () {
        this.router.navigate([{ outlets: { editing: null } }]);
    };
    GenreEditingComponent.prototype.openCreateGenreDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_create_item_genre_create_item_genre_component__WEBPACK_IMPORTED_MODULE_9__["CreateItemGenreComponent"]);
        dialogRef.afterClosed().subscribe(function (result) {
            _this.loadData();
        });
    };
    var GenreEditingComponent_1;
    GenreEditingComponent.ctorParameters = function () { return [
        { type: _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_6__["LocalStorage"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_10__["ToastrService"] },
        { type: src_app_services_genre_service__WEBPACK_IMPORTED_MODULE_7__["GenreService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], GenreEditingComponent.prototype, "notifyParent", void 0);
    GenreEditingComponent = GenreEditingComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "genre-editing",
            template: __webpack_require__(/*! raw-loader!./genre-editing.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/genre-editing/genre-editing.component.html"),
            styles: [__webpack_require__(/*! ./genre-editing.component.css */ "./src/app/admin-panel/genre-editing/genre-editing.component.css")]
        })
    ], GenreEditingComponent);
    return GenreEditingComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/magazine-editing/magazine-editing.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/admin-panel/magazine-editing/magazine-editing.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".enlarge {\r\n  width: 50%;\r\n}\r\n.btn {\r\n  color: #ffffff;\r\n  border-radius: 10rem;\r\n  background-color: #926dbe;\r\n}\r\n.k-grid {\r\n  background-clip: unset;\r\n}\r\nimg {\r\n  width: 50px;\r\n}\r\n.div-zoom {\r\n  width: 85px;\r\n  height: 55px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC9tYWdhemluZS1lZGl0aW5nL21hZ2F6aW5lLWVkaXRpbmcuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFVBQVU7QUFDWjtBQUNBO0VBQ0UsY0FBYztFQUNkLG9CQUFvQjtFQUNwQix5QkFBeUI7QUFDM0I7QUFFQTtFQUNFLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UsV0FBVztBQUNiO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsWUFBWTtBQUNkIiwiZmlsZSI6ImFwcC9hZG1pbi1wYW5lbC9tYWdhemluZS1lZGl0aW5nL21hZ2F6aW5lLWVkaXRpbmcuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lbmxhcmdlIHtcclxuICB3aWR0aDogNTAlO1xyXG59XHJcbi5idG4ge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcmVtO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM5MjZkYmU7XHJcbn1cclxuXHJcbi5rLWdyaWQge1xyXG4gIGJhY2tncm91bmQtY2xpcDogdW5zZXQ7XHJcbn1cclxuaW1nIHtcclxuICB3aWR0aDogNTBweDtcclxufVxyXG4uZGl2LXpvb20ge1xyXG4gIHdpZHRoOiA4NXB4O1xyXG4gIGhlaWdodDogNTVweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/admin-panel/magazine-editing/magazine-editing.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/admin-panel/magazine-editing/magazine-editing.component.ts ***!
  \****************************************************************************/
/*! exports provided: MagazineEditingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MagazineEditingComponent", function() { return MagazineEditingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/magazine.service */ "./src/app/services/magazine.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/authentication/user.service */ "./src/app/services/authentication/user.service.ts");
/* harmony import */ var _progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @progress/kendo-data-query */ "./node_modules/@progress/kendo-data-query/dist/es/main.js");
/* harmony import */ var _create_item_magazine_create_item_magazine_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../create-item-magazine/create-item-magazine.component */ "./src/app/admin-panel/create-item-magazine/create-item-magazine.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");









var MagazineEditingComponent = /** @class */ (function () {
    function MagazineEditingComponent(magazineService, dialog, userService, toastr) {
        this.magazineService = magazineService;
        this.dialog = dialog;
        this.userService = userService;
        this.toastr = toastr;
        this.isOpen = false;
        this.gridState = { sort: [], skip: 0, take: 10 };
    }
    MagazineEditingComponent.prototype.ngOnInit = function () {
        this.loadData();
        this.admin = this.userService.getIsAdmin();
    };
    MagazineEditingComponent.prototype.loadData = function () {
        var _this = this;
        this.magazineService.getAllMagazine().subscribe(function (data) {
            _this.magazineList = data;
            _this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_6__["process"])(_this.magazineList.magazineList, _this.gridState);
        });
    };
    MagazineEditingComponent.prototype.addHandler = function (_a) {
        var sender = _a.sender;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](0),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            price: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](0),
            picture: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("")
        });
        sender.addRow(this.formGroup);
    };
    MagazineEditingComponent.prototype.editHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.id),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.title, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            price: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.price),
            public: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.picture)
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    MagazineEditingComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    MagazineEditingComponent.prototype.saveHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, formGroup = _a.formGroup, isNew = _a.isNew;
        if (isNew) {
            var product = formGroup.value;
            this.magazineService.create(product).subscribe(function () { return _this.loadData(); });
            this.toastr.success("removed");
        }
        if (!isNew) {
            var product = formGroup.value;
            this.magazineService.update(product).subscribe(function () { return _this.loadData(); });
        }
        sender.closeRow(rowIndex);
    };
    MagazineEditingComponent.prototype.removeHandler = function (_a) {
        var _this = this;
        var dataItem = _a.dataItem;
        this.magazineService.delete(dataItem.id).subscribe(function () { return _this.loadData(); });
        this.toastr.success("removed");
    };
    MagazineEditingComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    MagazineEditingComponent.prototype.Logout = function () {
        localStorage.removeItem("data");
        window.location.reload();
    };
    MagazineEditingComponent.prototype.getAuthorList = function (authorList) {
        var authorString = "";
        for (var i = 0; i < authorList.authorList.length; i++) {
            authorString += authorList.authorList[i].name;
        }
        return authorString;
    };
    MagazineEditingComponent.prototype.dataStateChange = function (state) {
        this.gridState = state;
        this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_6__["process"])(this.magazineList.magazineList, this.gridState);
    };
    MagazineEditingComponent.prototype.openCreateMagazineDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_create_item_magazine_create_item_magazine_component__WEBPACK_IMPORTED_MODULE_7__["CreateItemMagazineComponent"]);
        {
        }
        dialogRef.afterClosed().subscribe(function (result) {
            _this.magazineService.getAllMagazine();
        });
    };
    MagazineEditingComponent.ctorParameters = function () { return [
        { type: src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_3__["MagazineService"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
        { type: src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"] }
    ]; };
    MagazineEditingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "magazine-editing",
            template: __webpack_require__(/*! raw-loader!./magazine-editing.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/magazine-editing/magazine-editing.component.html"),
            styles: [__webpack_require__(/*! ./magazine-editing.component.css */ "./src/app/admin-panel/magazine-editing/magazine-editing.component.css")]
        })
    ], MagazineEditingComponent);
    return MagazineEditingComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/order-editing/order-editing.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/admin-panel/order-editing/order-editing.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvYWRtaW4tcGFuZWwvb3JkZXItZWRpdGluZy9vcmRlci1lZGl0aW5nLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin-panel/order-editing/order-editing.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/admin-panel/order-editing/order-editing.component.ts ***!
  \**********************************************************************/
/*! exports provided: OrderEditingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderEditingComponent", function() { return OrderEditingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @progress/kendo-data-query */ "./node_modules/@progress/kendo-data-query/dist/es/main.js");
/* harmony import */ var _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-pwa/local-storage */ "./node_modules/@ngx-pwa/local-storage/fesm5/ngx-pwa-local-storage.js");
/* harmony import */ var src_app_services_order_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/order.service */ "./src/app/services/order.service.ts");
/* harmony import */ var src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/authentication/user.service */ "./src/app/services/authentication/user.service.ts");







var OrderEditingComponent = /** @class */ (function () {
    function OrderEditingComponent(localStorage, orderServicer, userService) {
        this.localStorage = localStorage;
        this.orderServicer = orderServicer;
        this.userService = userService;
        this.isOpen = false;
        this.isClose = true;
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
    }
    OrderEditingComponent.prototype.ngOnInit = function () {
        this.loadData();
        this.admin = this.userService.getIsAdmin();
    };
    OrderEditingComponent.prototype.loadData = function () {
        var _this = this;
        this.orderServicer.getAllOrders().subscribe(function (data) {
            _this.orders = data;
            _this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_3__["process"])(_this.orders.orders, _this.gridState);
        });
    };
    OrderEditingComponent.prototype.addHandler = function (_a) {
        var sender = _a.sender;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](0),
            userId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            amount: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            books: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            magazines: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("")
        });
        sender.addRow(this.formGroup);
    };
    OrderEditingComponent.prototype.editHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.id),
            userId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.userId),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.email),
            amount: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.amount),
            books: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.books),
            magazines: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.magazines)
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    OrderEditingComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    OrderEditingComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    OrderEditingComponent.prototype.Logout = function () {
        localStorage.removeItem("data");
        window.location.reload();
    };
    OrderEditingComponent.prototype.getBookList = function (order) {
        var text = "No Ordered";
        var bookString = "";
        for (var i = 0; i < order.books.length; i++) {
            bookString += order.books[i].title + "";
        }
        if (bookString === "") {
            return text;
        }
        return bookString;
    };
    OrderEditingComponent.prototype.getMagazineList = function (order) {
        var text = "No Ordered";
        var magazineString = "";
        for (var i = 0; i < order.magazines.length; i++) {
            magazineString += order.magazines[i].title + "";
        }
        if (magazineString === "") {
            return text;
        }
        else {
            return magazineString;
        }
    };
    OrderEditingComponent.prototype.dataStateChange = function (state) {
        this.gridState = state;
        this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_3__["process"])(this.orders.orders, this.gridState);
    };
    OrderEditingComponent.ctorParameters = function () { return [
        { type: _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_4__["LocalStorage"] },
        { type: src_app_services_order_service__WEBPACK_IMPORTED_MODULE_5__["OrderService"] },
        { type: src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] }
    ]; };
    OrderEditingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "order-editing",
            template: __webpack_require__(/*! raw-loader!./order-editing.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/order-editing/order-editing.component.html"),
            styles: [__webpack_require__(/*! ./order-editing.component.css */ "./src/app/admin-panel/order-editing/order-editing.component.css")]
        })
    ], OrderEditingComponent);
    return OrderEditingComponent;
}());



/***/ }),

/***/ "./src/app/admin-panel/user-editing/user-editing.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/admin-panel/user-editing/user-editing.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".enlarge {\r\n    width: 50%;\r\n  }\r\n  .btn {\r\n    color: #ffffff;\r\n    border-radius: 10rem;\r\n    background-color: #926dbe;\r\n  }\r\n  .k-grid {\r\n    background-clip: unset;\r\n  }\r\n  img {\r\n    width: 50px;\r\n  }\r\n  .div-zoom {\r\n    width: 85px;\r\n    height: 55px;\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hZG1pbi1wYW5lbC91c2VyLWVkaXRpbmcvdXNlci1lZGl0aW5nLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxVQUFVO0VBQ1o7RUFDQTtJQUNFLGNBQWM7SUFDZCxvQkFBb0I7SUFDcEIseUJBQXlCO0VBQzNCO0VBRUE7SUFDRSxzQkFBc0I7RUFDeEI7RUFDQTtJQUNFLFdBQVc7RUFDYjtFQUNBO0lBQ0UsV0FBVztJQUNYLFlBQVk7RUFDZCIsImZpbGUiOiJhcHAvYWRtaW4tcGFuZWwvdXNlci1lZGl0aW5nL3VzZXItZWRpdGluZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVubGFyZ2Uge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICB9XHJcbiAgLmJ0biB7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcmVtO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzkyNmRiZTtcclxuICB9XHJcbiAgXHJcbiAgLmstZ3JpZCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNsaXA6IHVuc2V0O1xyXG4gIH1cclxuICBpbWcge1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgfVxyXG4gIC5kaXYtem9vbSB7XHJcbiAgICB3aWR0aDogODVweDtcclxuICAgIGhlaWdodDogNTVweDtcclxuICB9XHJcbiAgIl19 */"

/***/ }),

/***/ "./src/app/admin-panel/user-editing/user-editing.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/admin-panel/user-editing/user-editing.component.ts ***!
  \********************************************************************/
/*! exports provided: UserEditingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserEditingComponent", function() { return UserEditingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @progress/kendo-data-query */ "./node_modules/@progress/kendo-data-query/dist/es/main.js");
/* harmony import */ var src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/authentication/user.service */ "./src/app/services/authentication/user.service.ts");
/* harmony import */ var src_app_models_user_get_all_user_view__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/models/user/get.all.user.view */ "./src/app/models/user/get.all.user.view.ts");
/* harmony import */ var _create_user_create_user_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../create-user/create-user.component */ "./src/app/admin-panel/create-user/create-user.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");









var UserEditingComponent = /** @class */ (function () {
    function UserEditingComponent(userService, dialog, toastr) {
        this.userService = userService;
        this.dialog = dialog;
        this.toastr = toastr;
        this.userList = new src_app_models_user_get_all_user_view__WEBPACK_IMPORTED_MODULE_5__["GetAllUserView"]();
        this.isOpen = false;
        this.isClose = true;
        this.gridState = {
            sort: [],
            skip: 0,
            take: 13
        };
    }
    UserEditingComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    UserEditingComponent.prototype.loadData = function () {
        var _this = this;
        this.userService.getAllUsersRole().subscribe(function (data) {
            _this.userList = data;
            _this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_3__["process"])(_this.userList.userList, _this.gridState);
        });
    };
    UserEditingComponent.prototype.addHandler = function (_a) {
        var sender = _a.sender;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            firsName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            userName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            phoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](""),
            role: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("")
        });
        sender.addRow(this.formGroup);
    };
    UserEditingComponent.prototype.editHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.id),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.email),
            firsName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.firsName),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.lastName),
            userName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.userName),
            phoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.phoneNumber),
            role: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dataItem.role)
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    UserEditingComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    UserEditingComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    UserEditingComponent.prototype.openCreateUserDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_create_user_create_user_component__WEBPACK_IMPORTED_MODULE_6__["CreateUserComponent"]);
        {
        }
        dialogRef.afterClosed().subscribe(function (result) {
            _this.loadData();
        });
    };
    UserEditingComponent.prototype.removeHandler = function (_a) {
        var _this = this;
        var dataItem = _a.dataItem;
        this.userService.DeleteUser(dataItem.id).subscribe(function () { return _this.loadData(); });
        this.toastr.success("removed");
    };
    UserEditingComponent.prototype.Logout = function () {
        localStorage.removeItem("data");
        window.location.reload();
    };
    UserEditingComponent.prototype.dataStateChange = function (state) {
        this.gridState = state;
        this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_3__["process"])(this.userList.userList, this.gridState);
    };
    UserEditingComponent.ctorParameters = function () { return [
        { type: src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialog"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"] }
    ]; };
    UserEditingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "user-editing",
            template: __webpack_require__(/*! raw-loader!./user-editing.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-panel/user-editing/user-editing.component.html"),
            styles: [__webpack_require__(/*! ./user-editing.component.css */ "./src/app/admin-panel/user-editing/user-editing.component.css")]
        })
    ], UserEditingComponent);
    return UserEditingComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [
    {
        path: "bookstoremodule",
        loadChildren: "./bookstore/bookstore.module#BookstoreModule"
    },
    {
        path: "authenticationmodule",
        loadChildren: "./authentication/authentication.module#AuthenticationModule"
    },
    { path: "menumodule", loadChildren: "./menu/menu.module#MenuModule" },
    {
        path: "adminpanelmodule",
        loadChildren: "./admin-panel/adminpanel.module#AdminPanelModule"
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvYXBwLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = "my-app";
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-root",
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: socialConfigs, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "socialConfigs", function() { return socialConfigs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select-ng-select.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/esm5/table.es5.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/fesm5/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _ng_select_ng_option_highlight__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-select/ng-option-highlight */ "./node_modules/@ng-select/ng-option-highlight/fesm5/ng-select-ng-option-highlight.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @progress/kendo-angular-dropdowns */ "./node_modules/@progress/kendo-angular-dropdowns/dist/fesm5/index.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/fesm5/angular-bootstrap-md.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/ngx-cookie-service.es5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/fesm5/index.js");
/* harmony import */ var _services_author_service__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./services/author.service */ "./src/app/services/author.service.ts");
/* harmony import */ var _services_authentication_user_service__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./services/authentication/user.service */ "./src/app/services/authentication/user.service.ts");
/* harmony import */ var _services_book_service__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./services/book.service */ "./src/app/services/book.service.ts");
/* harmony import */ var _services_authentication_login_service__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./services/authentication/login.service */ "./src/app/services/authentication/login.service.ts");
/* harmony import */ var _services_magazine_service__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./services/magazine.service */ "./src/app/services/magazine.service.ts");
/* harmony import */ var _services_order_service__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./services/order.service */ "./src/app/services/order.service.ts");
/* harmony import */ var _shared_helpers_jwt_intersepor__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./shared/helpers/jwt.intersepor */ "./src/app/shared/helpers/jwt.intersepor.ts");
/* harmony import */ var _services_genre_service__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./services/genre.service */ "./src/app/services/genre.service.ts");
/* harmony import */ var _authentication_authentication_module__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./authentication/authentication.module */ "./src/app/authentication/authentication.module.ts");
/* harmony import */ var _bookstore_bookstore_module__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./bookstore/bookstore.module */ "./src/app/bookstore/bookstore.module.ts");
/* harmony import */ var _admin_panel_adminpanel_module__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./admin-panel/adminpanel.module */ "./src/app/admin-panel/adminpanel.module.ts");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_40___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_40__);
/* harmony import */ var _services_authentication_social_login_service__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./services/authentication/social.login.service */ "./src/app/services/authentication/social.login.service.ts");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./services/data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var _services_counte_service__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./services/counte.service */ "./src/app/services/counte.service.ts");















































function socialConfigs() {
    var config = new angular_6_social_login__WEBPACK_IMPORTED_MODULE_40__["AuthServiceConfig"]([
        {
            id: angular_6_social_login__WEBPACK_IMPORTED_MODULE_40__["GoogleLoginProvider"].PROVIDER_ID,
            provider: new angular_6_social_login__WEBPACK_IMPORTED_MODULE_40__["GoogleLoginProvider"]("411577703476-2rprflggjnjdvbsunu1k3jt2d341evc4.apps.googleusercontent.com")
        }
    ]);
    return config;
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]],
            imports: [
                _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_13__["DropDownsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_16__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_18__["TooltipModule"].forRoot(),
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_19__["ModalModule"].forRoot(),
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_6__["GridModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"],
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_3__["NgSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCardModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrModule"].forRoot({
                    timeOut: 1000,
                    positionClass: "toast-bottom-right"
                }),
                _angular_material_core__WEBPACK_IMPORTED_MODULE_14__["MatNativeDateModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_15__["MatMenuModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_24__["NgbAlertModule"],
                _angular_cdk_table__WEBPACK_IMPORTED_MODULE_5__["CdkTableModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_24__["NgbModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_21__["MatCheckboxModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_20__["MatButtonModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _bookstore_bookstore_module__WEBPACK_IMPORTED_MODULE_38__["BookstoreModule"],
                _admin_panel_adminpanel_module__WEBPACK_IMPORTED_MODULE_39__["AdminPanelModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"],
                _ng_select_ng_option_highlight__WEBPACK_IMPORTED_MODULE_8__["NgOptionHighlightModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_17__["ButtonsModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_17__["CollapseModule"],
                angular_6_social_login__WEBPACK_IMPORTED_MODULE_40__["SocialLoginModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_17__["WavesModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSidenavModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_12__["NgMultiSelectDropDownModule"].forRoot(),
                _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_28__["PopupModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_27__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_25__["HttpClientModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_23__["CommonModule"],
                _authentication_authentication_module__WEBPACK_IMPORTED_MODULE_37__["AuthenticationModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_27__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot([
                    { path: "", redirectTo: "book", pathMatch: "full" },
                    { path: "**", redirectTo: "/" }
                ], { useHash: true }),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_26__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_26__["NoopAnimationsModule"]
            ],
            exports: [_angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTableModule"]],
            providers: [
                angular_6_social_login__WEBPACK_IMPORTED_MODULE_40__["AuthService"],
                { provide: _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatDialogRef"], useValue: {} },
                { provide: _angular_material__WEBPACK_IMPORTED_MODULE_7__["MAT_DIALOG_DATA"], useValue: [] },
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_25__["HTTP_INTERCEPTORS"],
                    useClass: _shared_helpers_jwt_intersepor__WEBPACK_IMPORTED_MODULE_35__["Interceptor"],
                    multi: true
                },
                {
                    provide: angular_6_social_login__WEBPACK_IMPORTED_MODULE_40__["AuthServiceConfig"],
                    useFactory: socialConfigs
                },
                _services_author_service__WEBPACK_IMPORTED_MODULE_29__["AuthorService"],
                _services_authentication_login_service__WEBPACK_IMPORTED_MODULE_32__["LoginService"],
                _services_counte_service__WEBPACK_IMPORTED_MODULE_43__["CounterService"],
                _services_authentication_user_service__WEBPACK_IMPORTED_MODULE_30__["UserService"],
                _services_book_service__WEBPACK_IMPORTED_MODULE_31__["BookService"],
                _services_magazine_service__WEBPACK_IMPORTED_MODULE_33__["MagazineService"],
                _services_data_service__WEBPACK_IMPORTED_MODULE_42__["DataService"],
                ngx_cookie_service__WEBPACK_IMPORTED_MODULE_22__["CookieService"],
                _services_authentication_social_login_service__WEBPACK_IMPORTED_MODULE_41__["SocialLoginService"],
                _services_genre_service__WEBPACK_IMPORTED_MODULE_36__["GenreService"],
                _services_order_service__WEBPACK_IMPORTED_MODULE_34__["OrderService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/authentication/authentication.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/authentication/authentication.module.ts ***!
  \*********************************************************/
/*! exports provided: AuthenticationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationModule", function() { return AuthenticationModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _registration_completion_registration_completion_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registration-completion/registration-completion.component */ "./src/app/authentication/registration-completion/registration-completion.component.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/authentication/registration/registration.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login/login.component */ "./src/app/authentication/login/login.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @progress/kendo-angular-dropdowns */ "./node_modules/@progress/kendo-angular-dropdowns/dist/fesm5/index.js");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/fesm5/angular-bootstrap-md.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/fesm5/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select-ng-select.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/esm5/table.es5.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _ng_select_ng_option_highlight__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ng-select/ng-option-highlight */ "./node_modules/@ng-select/ng-option-highlight/fesm5/ng-select-ng-option-highlight.js");
/* harmony import */ var _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @progress/kendo-angular-buttons */ "./node_modules/@progress/kendo-angular-buttons/dist/fesm5/index.js");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/fesm5/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
























var AuthenticationModule = /** @class */ (function () {
    function AuthenticationModule() {
    }
    AuthenticationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild([
                    { path: "register", component: _registration_registration_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationComponent"] },
                    { path: "login", component: _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"] },
                    {
                        path: "app-registration-completion",
                        component: _registration_completion_registration_completion_component__WEBPACK_IMPORTED_MODULE_3__["RegistrationCompletionComponent"]
                    }
                ]),
                _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_7__["DropDownsModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_8__["TooltipModule"].forRoot(),
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_8__["ModalModule"].forRoot(),
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_9__["GridModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatDialogModule"],
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatCardModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_12__["ToastrModule"].forRoot({
                    timeOut: 1000,
                    positionClass: "toast-bottom-right"
                }),
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatMenuModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbAlertModule"],
                _angular_cdk_table__WEBPACK_IMPORTED_MODULE_14__["CdkTableModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_15__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_16__["AppRoutingModule"],
                _ng_select_ng_option_highlight__WEBPACK_IMPORTED_MODULE_17__["NgOptionHighlightModule"],
                _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_18__["ButtonsModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_8__["CollapseModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_8__["WavesModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSidenavModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_19__["NgMultiSelectDropDownModule"].forRoot(),
                _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_20__["PopupModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_21__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_22__["HttpClientModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatToolbarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_21__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_23__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSelectModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_23__["NoopAnimationsModule"]
            ],
            declarations: [
                _registration_completion_registration_completion_component__WEBPACK_IMPORTED_MODULE_3__["RegistrationCompletionComponent"],
                _registration_registration_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"]
            ],
            providers: []
        })
    ], AuthenticationModule);
    return AuthenticationModule;
}());



/***/ }),

/***/ "./src/app/authentication/login/login.component.css":
/*!**********************************************************!*\
  !*** ./src/app/authentication/login/login.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".text-center {\r\n  border: 1px double black;\r\n  background: none;\r\n  background: rgba(135, 241, 255, 0.431);\r\n  padding: 30px;\r\n  width: 30%;\r\n  height: 400px;\r\n  margin-left: 35%;\r\n  -webkit-margin-before: 13%;\r\n          margin-block-start: 13%;\r\n  border: 3px #cccccc solid;\r\n  border-radius: 30px;\r\n}\r\n.image {\r\n  position: absolute;\r\n  overflow: auto;\r\n  top: 0;\r\n  left: 0;\r\n  height: 100%;\r\n  width: 100%;\r\n  background-image: url(https://images.clipartlogo.com/files/istock/previews/7631/76316673-stack-of-books.jpg);\r\n}\r\n.btn {\r\n  -webkit-margin-before: 3%;\r\n          margin-block-start: 3%;\r\n  padding: 15px 150px;\r\n  background: rgba(112, 206, 250, 0.5);\r\n  border: 0.5px solid #5582ffe7;\r\n}\r\n.alert {\r\n  background: rgba(72, 214, 233, 0.918);\r\n  position: absolute;\r\n}\r\n.my-div1 {\r\n  -webkit-margin-before: 6%;\r\n          margin-block-start: 6%;\r\n}\r\n.my-div2 {\r\n  -webkit-margin-before: 6%;\r\n          margin-block-start: 6%;\r\n}\r\n.alert-danger {\r\n  color: #4b1f23;\r\n  margin-top: 1%;\r\n  border-radius: 61rem;\r\n  background-color: #e47e88;\r\n  border-color: #f5c6cb;\r\n}\r\n.alert {\r\n  padding: 0 0;\r\n}\r\n.my-but {\r\n  background-color: #4367a640;\r\n  width: 15%;\r\n  height: 15%;\r\n  margin-top: 1%;\r\n}\r\n.img {\r\n  width: 50%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hdXRoZW50aWNhdGlvbi9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usd0JBQXdCO0VBQ3hCLGdCQUFnQjtFQUNoQixzQ0FBc0M7RUFDdEMsYUFBYTtFQUNiLFVBQVU7RUFDVixhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLDBCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIseUJBQXlCO0VBSXpCLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxNQUFNO0VBQ04sT0FBTztFQUNQLFlBQVk7RUFDWixXQUFXO0VBQ1gsNEdBQTRHO0FBQzlHO0FBQ0E7RUFDRSx5QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLG1CQUFtQjtFQUNuQixvQ0FBb0M7RUFDcEMsNkJBQTZCO0FBQy9CO0FBQ0E7RUFDRSxxQ0FBcUM7RUFDckMsa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSx5QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSx5QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBRUE7RUFDRSxjQUFjO0VBQ2QsY0FBYztFQUNkLG9CQUFvQjtFQUNwQix5QkFBeUI7RUFDekIscUJBQXFCO0FBQ3ZCO0FBQ0E7RUFDRSxZQUFZO0FBQ2Q7QUFDQTtFQUNFLDJCQUEyQjtFQUMzQixVQUFVO0VBQ1YsV0FBVztFQUNYLGNBQWM7QUFDaEI7QUFDQTtFQUNFLFVBQVU7QUFDWiIsImZpbGUiOiJhcHAvYXV0aGVudGljYXRpb24vbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi50ZXh0LWNlbnRlciB7XHJcbiAgYm9yZGVyOiAxcHggZG91YmxlIGJsYWNrO1xyXG4gIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgxMzUsIDI0MSwgMjU1LCAwLjQzMSk7XHJcbiAgcGFkZGluZzogMzBweDtcclxuICB3aWR0aDogMzAlO1xyXG4gIGhlaWdodDogNDAwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDM1JTtcclxuICBtYXJnaW4tYmxvY2stc3RhcnQ6IDEzJTtcclxuICBib3JkZXI6IDNweCAjY2NjY2NjIHNvbGlkO1xyXG4gIC1tb3otYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgLWtodG1sLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcclxufVxyXG4uaW1hZ2Uge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBvdmVyZmxvdzogYXV0bztcclxuICB0b3A6IDA7XHJcbiAgbGVmdDogMDtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vaW1hZ2VzLmNsaXBhcnRsb2dvLmNvbS9maWxlcy9pc3RvY2svcHJldmlld3MvNzYzMS83NjMxNjY3My1zdGFjay1vZi1ib29rcy5qcGcpO1xyXG59XHJcbi5idG4ge1xyXG4gIG1hcmdpbi1ibG9jay1zdGFydDogMyU7XHJcbiAgcGFkZGluZzogMTVweCAxNTBweDtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDExMiwgMjA2LCAyNTAsIDAuNSk7XHJcbiAgYm9yZGVyOiAwLjVweCBzb2xpZCAjNTU4MmZmZTc7XHJcbn1cclxuLmFsZXJ0IHtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDcyLCAyMTQsIDIzMywgMC45MTgpO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG4ubXktZGl2MSB7XHJcbiAgbWFyZ2luLWJsb2NrLXN0YXJ0OiA2JTtcclxufVxyXG4ubXktZGl2MiB7XHJcbiAgbWFyZ2luLWJsb2NrLXN0YXJ0OiA2JTtcclxufVxyXG5cclxuLmFsZXJ0LWRhbmdlciB7XHJcbiAgY29sb3I6ICM0YjFmMjM7XHJcbiAgbWFyZ2luLXRvcDogMSU7XHJcbiAgYm9yZGVyLXJhZGl1czogNjFyZW07XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U0N2U4ODtcclxuICBib3JkZXItY29sb3I6ICNmNWM2Y2I7XHJcbn1cclxuLmFsZXJ0IHtcclxuICBwYWRkaW5nOiAwIDA7XHJcbn1cclxuLm15LWJ1dCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQzNjdhNjQwO1xyXG4gIHdpZHRoOiAxNSU7XHJcbiAgaGVpZ2h0OiAxNSU7XHJcbiAgbWFyZ2luLXRvcDogMSU7XHJcbn1cclxuLmltZyB7XHJcbiAgd2lkdGg6IDUwJTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/authentication/login/login.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/authentication/login/login.component.ts ***!
  \*********************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_authentication_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/authentication/login.service */ "./src/app/services/authentication/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var src_app_services_authentication_social_login_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/authentication/social.login.service */ "./src/app/services/authentication/social.login.service.ts");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-6-social-login */ "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
/* harmony import */ var angular_6_social_login__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var src_app_models_authentication_token__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/models/authentication/token */ "./src/app/models/authentication/token.ts");
/* harmony import */ var src_app_models_authentication_social_login_view__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/models/authentication/social.login.view */ "./src/app/models/authentication/social.login.view.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");










var LoginComponent = /** @class */ (function () {
    function LoginComponent(loginService, activatedRoate, toastr, socialAuthService, socialLoginService, router) {
        this.loginService = loginService;
        this.activatedRoate = activatedRoate;
        this.toastr = toastr;
        this.socialAuthService = socialAuthService;
        this.socialLoginService = socialLoginService;
        this.router = router;
        this.submitted = false;
        this.credentials = { email: "", password: "" };
        this.token = new src_app_models_authentication_token__WEBPACK_IMPORTED_MODULE_7__["Token"]();
        this.socialUserData = new src_app_models_authentication_social_login_view__WEBPACK_IMPORTED_MODULE_8__["SocialLoginView"]();
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.activatedRoate.queryParams.subscribe(function (param) {
            _this.brandNew = param["brandNew"];
            _this.credentials.email = param["email"];
        });
    };
    LoginComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    LoginComponent.prototype.login = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        if (!valid) {
            return;
        }
        this.submitted = true;
        this.isRequesting = true;
        this.errors = "";
        this.loginService.login(value.email, value.password).subscribe(function (result) {
            if (result) {
                localStorage.setItem("data", JSON.stringify(result));
                _this.router.navigate(["/book"]);
            }
            else {
            }
        }, function (error) {
            if (error)
                _this.toastr.error(" Please enter the correct username and password ");
        });
    };
    LoginComponent.prototype.socialSignIn = function (socialPlatform) {
        var _this = this;
        var socialPlatformProvider;
        if (socialPlatform == "google") {
            socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_6__["GoogleLoginProvider"].PROVIDER_ID;
        }
        this.socialAuthService.signIn(socialPlatformProvider).then(function (userData) {
            _this.socialUserData.email = userData.email;
            _this.socialLoginService
                .SaveResponce(_this.socialUserData)
                .subscribe(function (res) {
                (_this.token.accessToken = res.accessToken),
                    (_this.token.refreshToken = res.refreshToken),
                    localStorage.setItem("data", _this.token.accessToken);
                localStorage.setItem("Refresh", _this.token.refreshToken);
                var test = jwt_decode__WEBPACK_IMPORTED_MODULE_4__(res.accessToken);
                console.log(test);
                if (_this.token.accessToken != null &&
                    _this.token.refreshToken != null) {
                    _this.router.navigate(["book"]);
                }
            });
            console.log("google" + " sign in data : ", userData);
        });
    };
    LoginComponent.ctorParameters = function () { return [
        { type: src_app_services_authentication_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"] },
        { type: angular_6_social_login__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
        { type: src_app_services_authentication_social_login_service__WEBPACK_IMPORTED_MODULE_5__["SocialLoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "login",
            template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/authentication/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/authentication/login/login.component.css")]
        })
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/authentication/registration-completion/registration-completion.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/authentication/registration-completion/registration-completion.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".image {\r\n    position: absolute;\r\n    overflow: auto;\r\n    top: 0;\r\n    left: 0;\r\n    height: 100%;\r\n    width: 100%;\r\n    background-image: url(https://images.clipartlogo.com/files/istock/previews/7631/76316673-stack-of-books.jpg);\r\n  }\r\n  .btn {\r\n    -webkit-margin-before: 5%;\r\n            margin-block-start: 5%;\r\n    padding: 15px 150px;\r\n    background: rgba(112, 206, 250, 0.5);\r\n    border: 0.5px solid #5582ffe7;\r\n    width: 22%;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hdXRoZW50aWNhdGlvbi9yZWdpc3RyYXRpb24tY29tcGxldGlvbi9yZWdpc3RyYXRpb24tY29tcGxldGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxNQUFNO0lBQ04sT0FBTztJQUNQLFlBQVk7SUFDWixXQUFXO0lBQ1gsNEdBQTRHO0VBQzlHO0VBQ0E7SUFDRSx5QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixvQ0FBb0M7SUFDcEMsNkJBQTZCO0lBQzdCLFVBQVU7RUFDWiIsImZpbGUiOiJhcHAvYXV0aGVudGljYXRpb24vcmVnaXN0cmF0aW9uLWNvbXBsZXRpb24vcmVnaXN0cmF0aW9uLWNvbXBsZXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbWFnZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBvdmVyZmxvdzogYXV0bztcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL2ltYWdlcy5jbGlwYXJ0bG9nby5jb20vZmlsZXMvaXN0b2NrL3ByZXZpZXdzLzc2MzEvNzYzMTY2NzMtc3RhY2stb2YtYm9va3MuanBnKTtcclxuICB9XHJcbiAgLmJ0biB7XHJcbiAgICBtYXJnaW4tYmxvY2stc3RhcnQ6IDUlO1xyXG4gICAgcGFkZGluZzogMTVweCAxNTBweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMTEyLCAyMDYsIDI1MCwgMC41KTtcclxuICAgIGJvcmRlcjogMC41cHggc29saWQgIzU1ODJmZmU3O1xyXG4gICAgd2lkdGg6IDIyJTtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/authentication/registration-completion/registration-completion.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/authentication/registration-completion/registration-completion.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: RegistrationCompletionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationCompletionComponent", function() { return RegistrationCompletionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_models_authentication_confirm_register_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/authentication/confirm.register.view */ "./src/app/models/authentication/confirm.register.view.ts");
/* harmony import */ var src_app_services_authentication_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/authentication/login.service */ "./src/app/services/authentication/login.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");






var RegistrationCompletionComponent = /** @class */ (function () {
    function RegistrationCompletionComponent(router, activateRoute, loginService, toastr) {
        this.router = router;
        this.activateRoute = activateRoute;
        this.loginService = loginService;
        this.toastr = toastr;
        this.confirmcred = new src_app_models_authentication_confirm_register_view__WEBPACK_IMPORTED_MODULE_3__["ConfirmRegisterView"]();
        this.confirmcred.email = this.activateRoute.snapshot.queryParams.email;
        this.confirmcred.code = this.activateRoute.snapshot.queryParams.token;
    }
    RegistrationCompletionComponent.prototype.ngOnInit = function () { };
    RegistrationCompletionComponent.prototype.confirm = function () {
        this.loginService.confirm(this.confirmcred).subscribe();
        this.toastr.success("Registration is completed!");
        this.router.navigate(["/login"]);
    };
    RegistrationCompletionComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: src_app_services_authentication_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"] }
    ]; };
    RegistrationCompletionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-registration-completion",
            template: __webpack_require__(/*! raw-loader!./registration-completion.component.html */ "./node_modules/raw-loader/index.js!./src/app/authentication/registration-completion/registration-completion.component.html"),
            styles: [__webpack_require__(/*! ./registration-completion.component.css */ "./src/app/authentication/registration-completion/registration-completion.component.css")]
        })
    ], RegistrationCompletionComponent);
    return RegistrationCompletionComponent;
}());



/***/ }),

/***/ "./src/app/authentication/registration/registration.component.css":
/*!************************************************************************!*\
  !*** ./src/app/authentication/registration/registration.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".col-md-12 {\r\n  margin-bottom: 5%;\r\n}\r\n.text-center {\r\n  background: none;\r\n  background: rgba(135, 241, 255, 0.431);\r\n  padding: 30px;\r\n  width: 30%;\r\n  height: 520px;\r\n  margin-left: 35%;\r\n  -webkit-margin-before: 7%;\r\n          margin-block-start: 7%;\r\n  border: 3px #cccccc solid;\r\n  border-radius: 70px;\r\n}\r\n.my-div {\r\n  float: left;\r\n  padding-left: 15%;\r\n  height: 135%;\r\n  width: 45%;\r\n}\r\n.my-div2 {\r\n  float: right;\r\n  padding-right: 15%;\r\n  height: 135%;\r\n  width: 45%;\r\n}\r\n.my-div3 {\r\n  float: left;\r\n  padding-left: 15%;\r\n  -webkit-margin-before: 4%;\r\n          margin-block-start: 4%;\r\n  width: 77%;\r\n}\r\n.my-div4 {\r\n  float: left;\r\n  padding-left: 15%;\r\n  -webkit-margin-before: 4%;\r\n          margin-block-start: 4%;\r\n  width: 77%;\r\n}\r\n.form-control {\r\n  width: 124%;\r\n}\r\n.form-control {\r\n  width: 124%;\r\n}\r\n.btn {\r\n  -webkit-margin-before: 5%;\r\n          margin-block-start: 5%;\r\n  padding: 15px 150px;\r\n  background: rgba(112, 206, 250, 0.5);\r\n  border: 0.5px solid #5582ffe7;\r\n  width: 82%;\r\n}\r\n.image {\r\n  position: absolute;\r\n  overflow: auto;\r\n  top: 0;\r\n  left: 0;\r\n  height: 100%;\r\n  width: 100%;\r\n  background-image: url(https://images.clipartlogo.com/files/istock/previews/7631/76316673-stack-of-books.jpg);\r\n}\r\n.my-div5 {\r\n  padding-left: 7%;\r\n  \r\n}\r\n.my-div6{\r\n    padding-left: 7%;\r\n}\r\n.my-form{\r\n  margin-right: 35px;\r\n}\r\n.alert-danger{\r\nposition: absolute;  color: #813c3ca1;\r\n  background-color: #ccccff;\r\n  border-color: #000000;\r\n  margin-left: 25px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9hdXRoZW50aWNhdGlvbi9yZWdpc3RyYXRpb24vcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixzQ0FBc0M7RUFDdEMsYUFBYTtFQUNiLFVBQVU7RUFDVixhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLHlCQUFzQjtVQUF0QixzQkFBc0I7RUFDdEIseUJBQXlCO0VBSXpCLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsV0FBVztFQUNYLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osVUFBVTtBQUNaO0FBQ0E7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixVQUFVO0FBQ1o7QUFDQTtFQUNFLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIseUJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0QixVQUFVO0FBQ1o7QUFDQTtFQUNFLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIseUJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0QixVQUFVO0FBQ1o7QUFDQTtFQUNFLFdBQVc7QUFDYjtBQUNBO0VBQ0UsV0FBVztBQUNiO0FBQ0E7RUFDRSx5QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLG1CQUFtQjtFQUNuQixvQ0FBb0M7RUFDcEMsNkJBQTZCO0VBQzdCLFVBQVU7QUFDWjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxNQUFNO0VBQ04sT0FBTztFQUNQLFlBQVk7RUFDWixXQUFXO0VBQ1gsNEdBQTRHO0FBQzlHO0FBQ0E7RUFDRSxnQkFBZ0I7O0FBRWxCO0FBQ0E7SUFDSSxnQkFBZ0I7QUFDcEI7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0FBQ0Esa0JBQWtCLEdBQUcsZ0JBQWdCO0VBQ25DLHlCQUF5QjtFQUN6QixxQkFBcUI7RUFDckIsaUJBQWlCO0FBQ25CIiwiZmlsZSI6ImFwcC9hdXRoZW50aWNhdGlvbi9yZWdpc3RyYXRpb24vcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29sLW1kLTEyIHtcclxuICBtYXJnaW4tYm90dG9tOiA1JTtcclxufVxyXG4udGV4dC1jZW50ZXIge1xyXG4gIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgxMzUsIDI0MSwgMjU1LCAwLjQzMSk7XHJcbiAgcGFkZGluZzogMzBweDtcclxuICB3aWR0aDogMzAlO1xyXG4gIGhlaWdodDogNTIwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDM1JTtcclxuICBtYXJnaW4tYmxvY2stc3RhcnQ6IDclO1xyXG4gIGJvcmRlcjogM3B4ICNjY2NjY2Mgc29saWQ7XHJcbiAgLW1vei1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAta2h0bWwtYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG59XHJcbi5teS1kaXYge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHBhZGRpbmctbGVmdDogMTUlO1xyXG4gIGhlaWdodDogMTM1JTtcclxuICB3aWR0aDogNDUlO1xyXG59XHJcbi5teS1kaXYyIHtcclxuICBmbG9hdDogcmlnaHQ7XHJcbiAgcGFkZGluZy1yaWdodDogMTUlO1xyXG4gIGhlaWdodDogMTM1JTtcclxuICB3aWR0aDogNDUlO1xyXG59XHJcbi5teS1kaXYzIHtcclxuICBmbG9hdDogbGVmdDtcclxuICBwYWRkaW5nLWxlZnQ6IDE1JTtcclxuICBtYXJnaW4tYmxvY2stc3RhcnQ6IDQlO1xyXG4gIHdpZHRoOiA3NyU7XHJcbn1cclxuLm15LWRpdjQge1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHBhZGRpbmctbGVmdDogMTUlO1xyXG4gIG1hcmdpbi1ibG9jay1zdGFydDogNCU7XHJcbiAgd2lkdGg6IDc3JTtcclxufVxyXG4uZm9ybS1jb250cm9sIHtcclxuICB3aWR0aDogMTI0JTtcclxufVxyXG4uZm9ybS1jb250cm9sIHtcclxuICB3aWR0aDogMTI0JTtcclxufVxyXG4uYnRuIHtcclxuICBtYXJnaW4tYmxvY2stc3RhcnQ6IDUlO1xyXG4gIHBhZGRpbmc6IDE1cHggMTUwcHg7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgxMTIsIDIwNiwgMjUwLCAwLjUpO1xyXG4gIGJvcmRlcjogMC41cHggc29saWQgIzU1ODJmZmU3O1xyXG4gIHdpZHRoOiA4MiU7XHJcbn1cclxuLmltYWdlIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL2ltYWdlcy5jbGlwYXJ0bG9nby5jb20vZmlsZXMvaXN0b2NrL3ByZXZpZXdzLzc2MzEvNzYzMTY2NzMtc3RhY2stb2YtYm9va3MuanBnKTtcclxufVxyXG4ubXktZGl2NSB7XHJcbiAgcGFkZGluZy1sZWZ0OiA3JTtcclxuICBcclxufVxyXG4ubXktZGl2NntcclxuICAgIHBhZGRpbmctbGVmdDogNyU7XHJcbn1cclxuLm15LWZvcm17XHJcbiAgbWFyZ2luLXJpZ2h0OiAzNXB4O1xyXG59XHJcbi5hbGVydC1kYW5nZXJ7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTsgIGNvbG9yOiAjODEzYzNjYTE7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2NjY2NmZjtcclxuICBib3JkZXItY29sb3I6ICMwMDAwMDA7XHJcbiAgbWFyZ2luLWxlZnQ6IDI1cHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/authentication/registration/registration.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/authentication/registration/registration.component.ts ***!
  \***********************************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_authentication_login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/authentication/login.service */ "./src/app/services/authentication/login.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");





var RegistrationComponent = /** @class */ (function () {
    function RegistrationComponent(router, loginService, toastr) {
        this.router = router;
        this.loginService = loginService;
        this.toastr = toastr;
        this.submitted = false;
    }
    RegistrationComponent.prototype.ngOnInit = function () { };
    RegistrationComponent.prototype.registerUser = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        if (!valid) {
            return;
        }
        this.submitted = true;
        this.isRequesting = true;
        console.log(this.submitted);
        console.log(this.isRequesting);
        this.user = value;
        this.loginService.register(this.user).subscribe(function (result) {
            console.log(result);
            _this.toastr.success("Successful registration ");
        }, function (errors) {
            console.log(errors);
        }, function () {
            return _this.router.navigate(["/login"], {
                queryParams: { brandNew: true, email: value.email }
            });
        });
    };
    RegistrationComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: src_app_services_authentication_login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] }
    ]; };
    RegistrationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "register",
            template: __webpack_require__(/*! raw-loader!./registration.component.html */ "./node_modules/raw-loader/index.js!./src/app/authentication/registration/registration.component.html"),
            styles: [__webpack_require__(/*! ./registration.component.css */ "./src/app/authentication/registration/registration.component.css")]
        })
    ], RegistrationComponent);
    return RegistrationComponent;
}());



/***/ }),

/***/ "./src/app/bookstore/author/author.component.css":
/*!*******************************************************!*\
  !*** ./src/app/bookstore/author/author.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".my-form{\r\n    border: 1px double rgb(189, 44, 44);\r\n    background: none;\r\n    background: rgba(135, 241, 255, 0.431);\r\n    padding: 30px;\r\n    width: 45%;\r\n    height: 320px;\r\n    margin-left: 15%;\r\n    -webkit-margin-before: 3%;\r\n            margin-block-start: 3%;\r\n    border: 3px #cccccc solid;\r\n    border-radius: 10px;\r\n}\r\n.content {\r\n    padding: 30px;\r\n    color: #787878;\r\n    background-color: #fcf7f8;\r\n    border: 1px solid rgba(0,0,0,.05);}\r\n.bg-light {\r\n        background-color: #5fa4b7!important\r\n      }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9ib29rc3RvcmUvYXV0aG9yL2F1dGhvci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksbUNBQW1DO0lBQ25DLGdCQUFnQjtJQUNoQixzQ0FBc0M7SUFDdEMsYUFBYTtJQUNiLFVBQVU7SUFDVixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLHlCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIseUJBQXlCO0lBSXpCLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksYUFBYTtJQUNiLGNBQWM7SUFDZCx5QkFBeUI7SUFDekIsaUNBQWlDLENBQUM7QUFDbEM7UUFDSTtNQUNGIiwiZmlsZSI6ImFwcC9ib29rc3RvcmUvYXV0aG9yL2F1dGhvci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15LWZvcm17XHJcbiAgICBib3JkZXI6IDFweCBkb3VibGUgcmdiKDE4OSwgNDQsIDQ0KTtcclxuICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDEzNSwgMjQxLCAyNTUsIDAuNDMxKTtcclxuICAgIHBhZGRpbmc6IDMwcHg7XHJcbiAgICB3aWR0aDogNDUlO1xyXG4gICAgaGVpZ2h0OiAzMjBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgICBtYXJnaW4tYmxvY2stc3RhcnQ6IDMlO1xyXG4gICAgYm9yZGVyOiAzcHggI2NjY2NjYyBzb2xpZDtcclxuICAgIC1tb3otYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIC1raHRtbC1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG4uY29udGVudCB7XHJcbiAgICBwYWRkaW5nOiAzMHB4O1xyXG4gICAgY29sb3I6ICM3ODc4Nzg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmNmN2Y4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLDAsMCwuMDUpO31cclxuICAgIC5iZy1saWdodCB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzVmYTRiNyFpbXBvcnRhbnRcclxuICAgICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/bookstore/author/author.component.ts":
/*!******************************************************!*\
  !*** ./src/app/bookstore/author/author.component.ts ***!
  \******************************************************/
/*! exports provided: AuthorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorComponent", function() { return AuthorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_models_author_create_author_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/author/create.author.view */ "./src/app/models/author/create.author.view.ts");
/* harmony import */ var src_app_services_author_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/author.service */ "./src/app/services/author.service.ts");
/* harmony import */ var src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/models/author/get.author.list.view */ "./src/app/models/author/get.author.list.view.ts");





var AuthorComponent = /** @class */ (function () {
    function AuthorComponent(authorService) {
        this.authorService = authorService;
        this.toggleText = "Add Author";
        this.author = new src_app_models_author_create_author_view__WEBPACK_IMPORTED_MODULE_2__["CreateAuthorView"]();
        this.show = false;
        this.authorList = new src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_4__["GetAuthorView"]();
    }
    AuthorComponent.prototype.ngOnInit = function () {
        this.getauthors();
    };
    AuthorComponent.prototype.getauthors = function () {
        var _this = this;
        this.authorService.get().subscribe(function (res) {
            _this.authorList = res;
        });
    };
    AuthorComponent.prototype.onToggle = function () {
        this.show = !this.show;
        this.toggleText = this.show ? "Hide" : "Add Author";
    };
    AuthorComponent.prototype.createAuthor = function () {
        var _this = this;
        this.authorService.create(this.author).subscribe(function (res) { return _this.getauthors(); });
    };
    AuthorComponent.ctorParameters = function () { return [
        { type: src_app_services_author_service__WEBPACK_IMPORTED_MODULE_3__["AuthorService"] }
    ]; };
    AuthorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "author",
            template: __webpack_require__(/*! raw-loader!./author.component.html */ "./node_modules/raw-loader/index.js!./src/app/bookstore/author/author.component.html"),
            styles: [__webpack_require__(/*! ./author.component.css */ "./src/app/bookstore/author/author.component.css")]
        })
    ], AuthorComponent);
    return AuthorComponent;
}());



/***/ }),

/***/ "./src/app/bookstore/book-description/book-description.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/bookstore/book-description/book-description.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-typography {\r\n  position: absolute;\r\n  background: none;\r\n  background: rgba(208, 252, 255, 0.88);\r\n  padding: 5px;\r\n  width: 72%;\r\n  height: 800px;\r\n  margin-left: 15%;\r\n  border: 3px #cccccc solid;\r\n  border-radius: 24px;\r\n  padding: 0px 125px;\r\n}\r\n.headline {\r\n  text-align: center;\r\n  color: rgb(128, 0, 191);\r\n  font-size: 45px;\r\n  font-weight: 1000;\r\n}\r\n.my-div {\r\n  width: 85%;\r\n  font-size: 20px;\r\n}\r\n.my-div2 {\r\n  padding: 25px;\r\n  color: rgb(0, 68, 255); \r\n  height: 80%;\r\n}\r\n.mid-cadr {\r\n  padding: 74px;\r\n  border-radius: 86px;\r\n}\r\n.overlay {\r\n  content: \"\";\r\n  display: block;\r\n  position: fixed;\r\n  top: 0;\r\n  left: 0;\r\n  height: 100%;\r\n  width: 100%;\r\n  background: rgba(0, 0, 0, 0.5);\r\n  z-index: 100;\r\n}\r\n.wrapper {\r\n  display: -webkit-box;\r\n  display: flex;\r\n}\r\n#left {\r\n  -webkit-box-flex: 0;\r\n          flex: 0 0 25%;\r\n}\r\n#right {\r\n  -webkit-box-flex: 4;\r\n          flex: 4;\r\n  margin-left: 10px;\r\n  width: 5%;\r\n  font-size: 13px;\r\n}\r\n.price {\r\n  margin-left: -35px;\r\n  padding: 35px;\r\n  font-weight: 1000;\r\n  font-size: 30px;\r\n  color: #f01d1d;\r\n}\r\n.mat-dialog-content {\r\n  max-height: 75vh;\r\n}\r\n.mat-dialog-actions {\r\n  padding: 5px 25px;\r\n}\r\n.enlarge{\r\n  width: 90%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9ib29rc3RvcmUvYm9vay1kZXNjcmlwdGlvbi9ib29rLWRlc2NyaXB0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLHFDQUFxQztFQUNyQyxZQUFZO0VBQ1osVUFBVTtFQUNWLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIseUJBQXlCO0VBSXpCLG1CQUFtQjtFQUNuQixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQix1QkFBdUI7RUFDdkIsZUFBZTtFQUNmLGlCQUFpQjtBQUNuQjtBQUNBO0VBQ0UsVUFBVTtFQUNWLGVBQWU7QUFDakI7QUFDQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsV0FBVztBQUNiO0FBRUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLGVBQWU7RUFDZixNQUFNO0VBQ04sT0FBTztFQUNQLFlBQVk7RUFDWixXQUFXO0VBQ1gsOEJBQThCO0VBQzlCLFlBQVk7QUFDZDtBQUVBO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0FBQ2Y7QUFFQTtFQUNFLG1CQUFhO1VBQWIsYUFBYTtBQUNmO0FBRUE7RUFDRSxtQkFBTztVQUFQLE9BQU87RUFDUCxpQkFBaUI7RUFDakIsU0FBUztFQUNULGVBQWU7QUFDakI7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGlCQUFpQjtBQUNuQjtBQUdBO0VBQ0UsVUFBVTtBQUNaIiwiZmlsZSI6ImFwcC9ib29rc3RvcmUvYm9vay1kZXNjcmlwdGlvbi9ib29rLWRlc2NyaXB0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF0LXR5cG9ncmFwaHkge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMjA4LCAyNTIsIDI1NSwgMC44OCk7XHJcbiAgcGFkZGluZzogNXB4O1xyXG4gIHdpZHRoOiA3MiU7XHJcbiAgaGVpZ2h0OiA4MDBweDtcclxuICBtYXJnaW4tbGVmdDogMTUlO1xyXG4gIGJvcmRlcjogM3B4ICNjY2NjY2Mgc29saWQ7XHJcbiAgLW1vei1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAta2h0bWwtYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAyNHB4O1xyXG4gIHBhZGRpbmc6IDBweCAxMjVweDtcclxufVxyXG4uaGVhZGxpbmUge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBjb2xvcjogcmdiKDEyOCwgMCwgMTkxKTtcclxuICBmb250LXNpemU6IDQ1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDEwMDA7XHJcbn1cclxuLm15LWRpdiB7XHJcbiAgd2lkdGg6IDg1JTtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuLm15LWRpdjIge1xyXG4gIHBhZGRpbmc6IDI1cHg7XHJcbiAgY29sb3I6IHJnYigwLCA2OCwgMjU1KTsgXHJcbiAgaGVpZ2h0OiA4MCU7XHJcbn1cclxuXHJcbi5taWQtY2FkciB7XHJcbiAgcGFkZGluZzogNzRweDtcclxuICBib3JkZXItcmFkaXVzOiA4NnB4O1xyXG59XHJcbi5vdmVybGF5IHtcclxuICBjb250ZW50OiBcIlwiO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB0b3A6IDA7XHJcbiAgbGVmdDogMDtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gIHotaW5kZXg6IDEwMDtcclxufVxyXG5cclxuLndyYXBwZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbiNsZWZ0IHtcclxuICBmbGV4OiAwIDAgMjUlO1xyXG59XHJcblxyXG4jcmlnaHQge1xyXG4gIGZsZXg6IDQ7XHJcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgd2lkdGg6IDUlO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG4ucHJpY2Uge1xyXG4gIG1hcmdpbi1sZWZ0OiAtMzVweDtcclxuICBwYWRkaW5nOiAzNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiAxMDAwO1xyXG4gIGZvbnQtc2l6ZTogMzBweDtcclxuICBjb2xvcjogI2YwMWQxZDtcclxufVxyXG4ubWF0LWRpYWxvZy1jb250ZW50IHtcclxuICBtYXgtaGVpZ2h0OiA3NXZoO1xyXG59XHJcbi5tYXQtZGlhbG9nLWFjdGlvbnMge1xyXG4gIHBhZGRpbmc6IDVweCAyNXB4O1xyXG59XHJcblxyXG5cclxuLmVubGFyZ2V7XHJcbiAgd2lkdGg6IDkwJTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/bookstore/book-description/book-description.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/bookstore/book-description/book-description.component.ts ***!
  \**************************************************************************/
/*! exports provided: BookDescriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookDescriptionComponent", function() { return BookDescriptionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_book_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/book.service */ "./src/app/services/book.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var src_app_services_author_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/author.service */ "./src/app/services/author.service.ts");
/* harmony import */ var src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/models/author/get.author.list.view */ "./src/app/models/author/get.author.list.view.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var src_app_models_book_get_book_list_view__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/models/book/get.book.list.view */ "./src/app/models/book/get.book.list.view.ts");
/* harmony import */ var src_app_models_book_get_book_view__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/models/book/get.book.view */ "./src/app/models/book/get.book.view.ts");
/* harmony import */ var src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/shared/order.item.view */ "./src/app/shared/order.item.view.ts");










var BookDescriptionComponent = /** @class */ (function () {
    function BookDescriptionComponent(bookService, authorService, toastr, data) {
        this.bookService = bookService;
        this.authorService = authorService;
        this.toastr = toastr;
        this.data = data;
        this.bookDescription = new src_app_models_book_get_book_view__WEBPACK_IMPORTED_MODULE_8__["GetBookView"]();
        this.books = new src_app_models_book_get_book_list_view__WEBPACK_IMPORTED_MODULE_7__["GetBookListView"]();
        this.authorList = new src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_5__["GetAuthorView"]();
    }
    BookDescriptionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getBooks();
        this.authorService.get().subscribe(function (data) {
            _this.authorList = data;
        });
    };
    BookDescriptionComponent.prototype.getBooks = function () {
        var _this = this;
        this.bookService.getAll().subscribe(function (databook) {
            _this.books = databook;
            _this.getBookById(_this.data);
            _this.bookDescription = _this.getBookById(_this.data.id);
        });
    };
    BookDescriptionComponent.prototype.getAuthorList = function (book) {
        var authorString = "";
        for (var i = 0; i < book.authorList.length; i++) {
            authorString += book.authorList[i].name + " ";
        }
        return authorString;
    };
    BookDescriptionComponent.prototype.getBookById = function (id) {
        for (var i = 0; i < this.books.books.length; i++) {
            if (this.books.books[i].id == id) {
                return this.books.books[i];
            }
        }
    };
    BookDescriptionComponent.prototype.onButtonClick = function (id) {
        var orderItem = new src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_9__["OrderItem"]();
        orderItem.id = id;
        orderItem.item = src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_9__["Item"].Book;
        var key = "item1";
        var existingOrder = JSON.parse(localStorage.getItem(key));
        if (existingOrder === null) {
            existingOrder = new Array();
        }
        existingOrder.push(orderItem);
        localStorage.setItem(key, JSON.stringify(existingOrder));
        this.toastr.success("Book Add To Order");
    };
    BookDescriptionComponent.ctorParameters = function () { return [
        { type: src_app_services_book_service__WEBPACK_IMPORTED_MODULE_2__["BookService"] },
        { type: src_app_services_author_service__WEBPACK_IMPORTED_MODULE_4__["AuthorService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"] },
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"],] }] }
    ]; };
    BookDescriptionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "book-description",
            template: __webpack_require__(/*! raw-loader!./book-description.component.html */ "./node_modules/raw-loader/index.js!./src/app/bookstore/book-description/book-description.component.html"),
            styles: [__webpack_require__(/*! ./book-description.component.css */ "./src/app/bookstore/book-description/book-description.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"]))
    ], BookDescriptionComponent);
    return BookDescriptionComponent;
}());



/***/ }),

/***/ "./src/app/bookstore/book/book.component.css":
/*!***************************************************!*\
  !*** ./src/app/bookstore/book/book.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".image {\r\n  position: absolute;\r\n  overflow: auto;\r\n  height: 100vh;\r\n  width: 100%;\r\n  background-image: url(https://images.clipartlogo.com/files/istock/previews/7631/76316673-stack-of-books.jpg);\r\n}\r\n.myForm {\r\n  position: relative;\r\n  height: 92%;\r\n}\r\n.k-widget {\r\n  border: none;\r\n}\r\n.myForm2 {\r\n  height: 12%;\r\n  background-color: #226f8d;\r\n}\r\n.btn-sm {\r\n  margin-right: 0.5%;\r\n  border-radius: 7rem;\r\n  color: #ffffff;\r\n}\r\n.btn-lg {\r\n  margin-left: 2.5%;\r\n  border-radius: 10rem;\r\n  color: #ffffff;\r\n}\r\n.btn {\r\n  background-color: #d5b0f86e;\r\n}\r\n.small-image {\r\n  background-image: url(http://sportkniga.com.ua/images/books228577dtg3.jpg);\r\n  height: 10vh;\r\n  width: 5%;\r\n}\r\n.k-button.k-primary,\r\n.k-slider .k-draghandle {\r\n  border-color: #747b1d;\r\n  color: #000000;\r\n  background-color: #d5b0f86e;\r\n}\r\n.sad {\r\n  border-color: #747b1d;\r\n  color: #000000;\r\n  background-color: #d5b0f86e;\r\n\r\n  background-image: none;\r\n  background-image: none;\r\n}\r\n.bg-light {\r\n  background-color: #5fa4b7 !important;\r\n}\r\n.but {\r\n  background-color: #d5b0f86e;\r\n  margin-right: 2%;\r\n  width: 35%;\r\n}\r\n.btn-info {\r\n  border-color: #b9bfc0;\r\n}\r\nimg {\r\n  width: 50px;\r\n}\r\n.enlarge:hover {\r\n  -webkit-transform: scale(3.5, 3.5);\r\n          transform: scale(3.5, 3.5);\r\n  -webkit-transform-origin: 0 0;\r\n          transform-origin: 0 0;\r\n  position: absolute;\r\n}\r\n.div-zoom {\r\n  width: 85px;\r\n  height: 55px;\r\n}\r\n.navbar {\r\n  margin-bottom: 0px;\r\n}\r\n.logPanel {\r\n  display: contents;\r\n  width: 100%;\r\n}\r\n.element.style {\r\n    height: 833px;\r\n}\r\n.k-grid{\r\n  background-color: #94e8ff69;\r\n  color: rgba(78, 0, 255, 0.9);\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9ib29rc3RvcmUvYm9vay9ib29rLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGFBQWE7RUFDYixXQUFXO0VBQ1gsNEdBQTRHO0FBQzlHO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztBQUNiO0FBQ0E7RUFDRSxZQUFZO0FBQ2Q7QUFDQTtFQUNFLFdBQVc7RUFDWCx5QkFBeUI7QUFDM0I7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsY0FBYztBQUNoQjtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSwyQkFBMkI7QUFDN0I7QUFDQTtFQUNFLDBFQUEwRTtFQUMxRSxZQUFZO0VBQ1osU0FBUztBQUNYO0FBQ0E7O0VBRUUscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7QUFDN0I7QUFDQTtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsMkJBQTJCOztFQUUzQixzQkFBc0I7RUFDdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSxvQ0FBb0M7QUFDdEM7QUFDQTtFQUNFLDJCQUEyQjtFQUMzQixnQkFBZ0I7RUFDaEIsVUFBVTtBQUNaO0FBQ0E7RUFDRSxxQkFBcUI7QUFDdkI7QUFFQTtFQUNFLFdBQVc7QUFDYjtBQUNBO0VBQ0Usa0NBQTBCO1VBQTFCLDBCQUEwQjtFQUMxQiw2QkFBcUI7VUFBckIscUJBQXFCO0VBQ3JCLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUNBO0VBQ0Usa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsV0FBVztBQUNiO0FBQ0E7SUFDSSxhQUFhO0FBQ2pCO0FBQ0E7RUFDRSwyQkFBMkI7RUFDM0IsNEJBQTRCO0FBQzlCIiwiZmlsZSI6ImFwcC9ib29rc3RvcmUvYm9vay9ib29rLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW1hZ2Uge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBvdmVyZmxvdzogYXV0bztcclxuICBoZWlnaHQ6IDEwMHZoO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL2ltYWdlcy5jbGlwYXJ0bG9nby5jb20vZmlsZXMvaXN0b2NrL3ByZXZpZXdzLzc2MzEvNzYzMTY2NzMtc3RhY2stb2YtYm9va3MuanBnKTtcclxufVxyXG4ubXlGb3JtIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgaGVpZ2h0OiA5MiU7XHJcbn1cclxuLmstd2lkZ2V0IHtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuLm15Rm9ybTIge1xyXG4gIGhlaWdodDogMTIlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyMjZmOGQ7XHJcbn1cclxuLmJ0bi1zbSB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAwLjUlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDdyZW07XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbn1cclxuLmJ0bi1sZyB7XHJcbiAgbWFyZ2luLWxlZnQ6IDIuNSU7XHJcbiAgYm9yZGVyLXJhZGl1czogMTByZW07XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbn1cclxuLmJ0biB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q1YjBmODZlO1xyXG59XHJcbi5zbWFsbC1pbWFnZSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHA6Ly9zcG9ydGtuaWdhLmNvbS51YS9pbWFnZXMvYm9va3MyMjg1NzdkdGczLmpwZyk7XHJcbiAgaGVpZ2h0OiAxMHZoO1xyXG4gIHdpZHRoOiA1JTtcclxufVxyXG4uay1idXR0b24uay1wcmltYXJ5LFxyXG4uay1zbGlkZXIgLmstZHJhZ2hhbmRsZSB7XHJcbiAgYm9yZGVyLWNvbG9yOiAjNzQ3YjFkO1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkNWIwZjg2ZTtcclxufVxyXG4uc2FkIHtcclxuICBib3JkZXItY29sb3I6ICM3NDdiMWQ7XHJcbiAgY29sb3I6ICMwMDAwMDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q1YjBmODZlO1xyXG5cclxuICBiYWNrZ3JvdW5kLWltYWdlOiBub25lO1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IG5vbmU7XHJcbn1cclxuLmJnLWxpZ2h0IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWZhNGI3ICFpbXBvcnRhbnQ7XHJcbn1cclxuLmJ1dCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q1YjBmODZlO1xyXG4gIG1hcmdpbi1yaWdodDogMiU7XHJcbiAgd2lkdGg6IDM1JTtcclxufVxyXG4uYnRuLWluZm8ge1xyXG4gIGJvcmRlci1jb2xvcjogI2I5YmZjMDtcclxufVxyXG5cclxuaW1nIHtcclxuICB3aWR0aDogNTBweDtcclxufVxyXG4uZW5sYXJnZTpob3ZlciB7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgzLjUsIDMuNSk7XHJcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCAwO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG4uZGl2LXpvb20ge1xyXG4gIHdpZHRoOiA4NXB4O1xyXG4gIGhlaWdodDogNTVweDtcclxufVxyXG4ubmF2YmFyIHtcclxuICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbn1cclxuLmxvZ1BhbmVsIHtcclxuICBkaXNwbGF5OiBjb250ZW50cztcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4uZWxlbWVudC5zdHlsZSB7XHJcbiAgICBoZWlnaHQ6IDgzM3B4O1xyXG59XHJcbi5rLWdyaWR7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzk0ZThmZjY5O1xyXG4gIGNvbG9yOiByZ2JhKDc4LCAwLCAyNTUsIDAuOSk7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/bookstore/book/book.component.ts":
/*!**************************************************!*\
  !*** ./src/app/bookstore/book/book.component.ts ***!
  \**************************************************/
/*! exports provided: BookComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookComponent", function() { return BookComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_book_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/book.service */ "./src/app/services/book.service.ts");
/* harmony import */ var src_app_services_author_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/author.service */ "./src/app/services/author.service.ts");
/* harmony import */ var src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/models/author/get.author.list.view */ "./src/app/models/author/get.author.list.view.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-pwa/local-storage */ "./node_modules/@ngx-pwa/local-storage/fesm5/ngx-pwa-local-storage.js");
/* harmony import */ var _progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @progress/kendo-data-query */ "./node_modules/@progress/kendo-data-query/dist/es/main.js");
/* harmony import */ var src_app_models_genre_get_genre_view__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/models/genre/get.genre.view */ "./src/app/models/genre/get.genre.view.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var src_app_models_book_get_book_list_view__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/models/book/get.book.list.view */ "./src/app/models/book/get.book.list.view.ts");
/* harmony import */ var _book_description_book_description_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../book-description/book-description.component */ "./src/app/bookstore/book-description/book-description.component.ts");
/* harmony import */ var src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/services/authentication/user.service */ "./src/app/services/authentication/user.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_menu_order_basket_order_basket_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! src/app/menu/order-basket/order-basket.component */ "./src/app/menu/order-basket/order-basket.component.ts");
/* harmony import */ var src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! src/app/shared/order.item.view */ "./src/app/shared/order.item.view.ts");
















var BookComponent = /** @class */ (function () {
    function BookComponent(localStorage, dialog, authorService, bookService, userService, toastr) {
        this.localStorage = localStorage;
        this.dialog = dialog;
        this.authorService = authorService;
        this.bookService = bookService;
        this.userService = userService;
        this.toastr = toastr;
        this.isAdminPageOpen = false;
        this.isOpen = false;
        this.isClose = true;
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.books = new src_app_models_book_get_book_list_view__WEBPACK_IMPORTED_MODULE_10__["GetBookListView"]();
        this.authorList = new src_app_models_author_get_author_list_view__WEBPACK_IMPORTED_MODULE_4__["GetAuthorView"]();
        this.genreList = new src_app_models_genre_get_genre_view__WEBPACK_IMPORTED_MODULE_8__["GetAllGenreView"]();
    }
    BookComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadData();
        this.authorService.get().subscribe(function (data) {
            _this.authorList = data;
        });
        this.admin = this.userService.getIsAdmin();
    };
    BookComponent.prototype.loadData = function () {
        var _this = this;
        this.bookService.getAll().subscribe(function (data) {
            _this.books = data;
            _this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_7__["process"])(_this.books.books, _this.gridState);
        });
    };
    BookComponent.prototype.addHandler = function (_a) {
        var sender = _a.sender;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            price: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0),
            picture: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            authorList: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](""),
            genreList: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("")
        });
        sender.addRow(this.formGroup);
    };
    BookComponent.prototype.editHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](dataItem.id),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](dataItem.title, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            price: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](dataItem.price),
            authorList: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](dataItem.authorList),
            genreList: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](dataItem.genreList)
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    BookComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    BookComponent.prototype.saveHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, formGroup = _a.formGroup, isNew = _a.isNew;
        if (isNew) {
            var product = formGroup.value;
            this.bookService.create(product).subscribe(function () { return _this.loadData(); });
        }
        if (!isNew) {
            var product = formGroup.value;
            this.bookService.update(product).subscribe(function () { return _this.loadData(); });
        }
        sender.closeRow(rowIndex);
    };
    BookComponent.prototype.removeHandler = function (_a) {
        var _this = this;
        var dataItem = _a.dataItem;
        this.bookService.delete(dataItem.id).subscribe(function () { return _this.loadData(); });
    };
    BookComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    BookComponent.prototype.Logout = function () {
        localStorage.removeItem("data");
        window.location.reload();
    };
    BookComponent.prototype.authorChange = function (value) {
        this.formGroup.get("author").setValue(value);
    };
    BookComponent.prototype.genreChange = function (value) {
        this.formGroup.get("genre").setValue(value);
    };
    BookComponent.prototype.getAuthorList = function (book) {
        var authorString = "";
        for (var i = 0; i < book.authorList.length; i++) {
            authorString += book.authorList[i].name + ",";
        }
        return authorString;
    };
    BookComponent.prototype.getGenreList = function (book) {
        var genreString = "";
        for (var i = 0; i < book.genreList.length; i++) {
            genreString += book.genreList[i].title + " ";
        }
        return genreString;
    };
    BookComponent.prototype.dataStateChange = function (state) {
        this.gridState = state;
        this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_7__["process"])(this.books.books, this.gridState);
    };
    BookComponent.prototype.onButtonClick = function (id, price) {
        var orderItem = new src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_15__["OrderItem"]();
        orderItem.id = id;
        orderItem.item = src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_15__["Item"].Book;
        orderItem.pirce = price;
        var key = "item1";
        var existingOrder = JSON.parse(localStorage.getItem(key));
        if (existingOrder === null) {
            existingOrder = new Array();
        }
        existingOrder.push(orderItem);
        localStorage.setItem(key, JSON.stringify(existingOrder));
        this.toastr.success("Book Add To Order");
    };
    BookComponent.prototype.openDialog = function () {
        var dialogRef = this.dialog.open(src_app_menu_order_basket_order_basket_component__WEBPACK_IMPORTED_MODULE_14__["OrderBasketComponent"]);
        {
        }
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Dialog result: " + result);
        });
    };
    BookComponent.prototype.openDialogCreate = function () {
        var dialogRef = this.dialog.open(src_app_menu_order_basket_order_basket_component__WEBPACK_IMPORTED_MODULE_14__["OrderBasketComponent"]);
        {
        }
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Dialog result: " + result);
        });
    };
    BookComponent.prototype.openDescription = function (id) {
        if (this.isOpen === false) {
            var dialogRef = this.dialog.open(_book_description_book_description_component__WEBPACK_IMPORTED_MODULE_11__["BookDescriptionComponent"], {
                data: { id: id }
            });
            {
            }
            dialogRef.afterClosed().subscribe(function (result) {
                console.log("Dialog result: " + result);
            });
        }
    };
    BookComponent.prototype.openAdminPage = function () {
        if (this.isAdminPageOpen == false) {
            this.isAdminPageOpen = true;
        }
    };
    BookComponent.ctorParameters = function () { return [
        { type: _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_6__["LocalStorage"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatDialog"] },
        { type: src_app_services_author_service__WEBPACK_IMPORTED_MODULE_3__["AuthorService"] },
        { type: src_app_services_book_service__WEBPACK_IMPORTED_MODULE_2__["BookService"] },
        { type: src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_12__["UserService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"] }
    ]; };
    BookComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"])({
            selector: "book",
            template: __webpack_require__(/*! raw-loader!./book.component.html */ "./node_modules/raw-loader/index.js!./src/app/bookstore/book/book.component.html"),
            styles: [__webpack_require__(/*! ./book.component.css */ "./src/app/bookstore/book/book.component.css")]
        })
    ], BookComponent);
    return BookComponent;
}());



/***/ }),

/***/ "./src/app/bookstore/bookstore.module.ts":
/*!***********************************************!*\
  !*** ./src/app/bookstore/bookstore.module.ts ***!
  \***********************************************/
/*! exports provided: BookstoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookstoreModule", function() { return BookstoreModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _author_author_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./author/author.component */ "./src/app/bookstore/author/author.component.ts");
/* harmony import */ var _book_book_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./book/book.component */ "./src/app/bookstore/book/book.component.ts");
/* harmony import */ var _book_description_book_description_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./book-description/book-description.component */ "./src/app/bookstore/book-description/book-description.component.ts");
/* harmony import */ var _magazine_magazine_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./magazine/magazine.component */ "./src/app/bookstore/magazine/magazine.component.ts");
/* harmony import */ var _magazine_description_magazine_description_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./magazine-description/magazine-description.component */ "./src/app/bookstore/magazine-description/magazine-description.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/fesm5/index.js");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/fesm5/angular-bootstrap-md.js");
/* harmony import */ var _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @progress/kendo-angular-dropdowns */ "./node_modules/@progress/kendo-angular-dropdowns/dist/fesm5/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select-ng-select.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/esm5/table.es5.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _ng_select_ng_option_highlight__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ng-select/ng-option-highlight */ "./node_modules/@ng-select/ng-option-highlight/fesm5/ng-select-ng-option-highlight.js");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/fesm5/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _authentication_authentication_module__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../authentication/authentication.module */ "./src/app/authentication/authentication.module.ts");
/* harmony import */ var _menu_menu_module__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../menu/menu.module */ "./src/app/menu/menu.module.ts");


























var BookstoreModule = /** @class */ (function () {
    function BookstoreModule() {
    }
    BookstoreModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forChild([
                    { path: "author", component: _author_author_component__WEBPACK_IMPORTED_MODULE_3__["AuthorComponent"] },
                    { path: "book", component: _book_book_component__WEBPACK_IMPORTED_MODULE_4__["BookComponent"] },
                    { path: "magazine", component: _magazine_magazine_component__WEBPACK_IMPORTED_MODULE_6__["MagazineComponent"] },
                    { path: "magazine-description", component: _magazine_description_magazine_description_component__WEBPACK_IMPORTED_MODULE_7__["MagazineDescriptionComponent"] },
                    { path: "book=description", component: _book_description_book_description_component__WEBPACK_IMPORTED_MODULE_5__["BookDescriptionComponent"] }
                ]),
                _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_11__["DropDownsModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"].forRoot(),
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_10__["ModalModule"].forRoot(),
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_9__["GridModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatDialogModule"],
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_13__["NgSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatCardModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_14__["ToastrModule"].forRoot({
                    timeOut: 1000,
                    positionClass: "toast-bottom-right"
                }),
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatMenuModule"],
                _angular_cdk_table__WEBPACK_IMPORTED_MODULE_15__["CdkTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_16__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_17__["AppRoutingModule"],
                _ng_select_ng_option_highlight__WEBPACK_IMPORTED_MODULE_18__["NgOptionHighlightModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_10__["ButtonsModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_10__["CollapseModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_10__["WavesModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatSidenavModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_19__["NgMultiSelectDropDownModule"].forRoot(),
                _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_20__["PopupModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_21__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_22__["HttpClientModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _menu_menu_module__WEBPACK_IMPORTED_MODULE_25__["MenuModule"],
                _authentication_authentication_module__WEBPACK_IMPORTED_MODULE_24__["AuthenticationModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatToolbarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_21__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_23__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatSelectModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_23__["NoopAnimationsModule"]
            ],
            declarations: [
                _author_author_component__WEBPACK_IMPORTED_MODULE_3__["AuthorComponent"],
                _book_book_component__WEBPACK_IMPORTED_MODULE_4__["BookComponent"],
                _book_description_book_description_component__WEBPACK_IMPORTED_MODULE_5__["BookDescriptionComponent"],
                _magazine_magazine_component__WEBPACK_IMPORTED_MODULE_6__["MagazineComponent"],
                _magazine_description_magazine_description_component__WEBPACK_IMPORTED_MODULE_7__["MagazineDescriptionComponent"]
            ]
        })
    ], BookstoreModule);
    return BookstoreModule;
}());



/***/ }),

/***/ "./src/app/bookstore/magazine-description/magazine-description.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/bookstore/magazine-description/magazine-description.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-typography {\r\n  position: absolute;\r\n  background: none;\r\n  background: rgba(208, 252, 255, 0.88);\r\n  padding: 5px;\r\n  width: 72%;\r\n  height: 800px;\r\n  margin-left: 15%;\r\n  border: 3px #cccccc solid;\r\n  border-radius: 24px;\r\n  padding: 0px 125px;\r\n}\r\n.headline {\r\n  text-align: center;\r\n  color: rgb(128, 0, 191);\r\n  font-size: 45px;\r\n  font-weight: 1000;\r\n}\r\n.my-div {\r\n  width: 85%;\r\n  font-size: 20px;\r\n}\r\n.my-div2 {\r\n  padding: 25px;\r\n  color: rgb(0, 68, 255); \r\n  height: 80%;\r\n}\r\n.mid-cadr {\r\n  padding: 74px;\r\n  border-radius: 86px;\r\n}\r\n.overlay {\r\n  content: \"\";\r\n  display: block;\r\n  position: fixed;\r\n  top: 0;\r\n  left: 0;\r\n  height: 100%;\r\n  width: 100%;\r\n  background: rgba(0, 0, 0, 0.5);\r\n  z-index: 100;\r\n}\r\n.wrapper {\r\n  display: -webkit-box;\r\n  display: flex;\r\n}\r\n#left {\r\n  -webkit-box-flex: 0;\r\n          flex: 0 0 25%;\r\n}\r\n#right {\r\n  -webkit-box-flex: 4;\r\n          flex: 4;\r\n  margin-left: 10px;\r\n  width: 5%;\r\n  font-size: 13px;\r\n}\r\n.price {\r\n  margin-left: -35px;\r\n  padding: 35px;\r\n  font-weight: 1000;\r\n  font-size: 30px;\r\n  color: #f01d1d;\r\n}\r\n.mat-dialog-content {\r\n  max-height: 75vh;\r\n}\r\n.mat-dialog-actions {\r\n  padding: 5px 25px;\r\n}\r\n.enlarge{\r\n  width: 90%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9ib29rc3RvcmUvbWFnYXppbmUtZGVzY3JpcHRpb24vbWFnYXppbmUtZGVzY3JpcHRpb24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIscUNBQXFDO0VBQ3JDLFlBQVk7RUFDWixVQUFVO0VBQ1YsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQix5QkFBeUI7RUFJekIsbUJBQW1CO0VBQ25CLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLHVCQUF1QjtFQUN2QixlQUFlO0VBQ2YsaUJBQWlCO0FBQ25CO0FBQ0E7RUFDRSxVQUFVO0VBQ1YsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QixXQUFXO0FBQ2I7QUFFQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7QUFDckI7QUFDQTtFQUNFLFdBQVc7RUFDWCxjQUFjO0VBQ2QsZUFBZTtFQUNmLE1BQU07RUFDTixPQUFPO0VBQ1AsWUFBWTtFQUNaLFdBQVc7RUFDWCw4QkFBOEI7RUFDOUIsWUFBWTtBQUNkO0FBRUE7RUFDRSxvQkFBYTtFQUFiLGFBQWE7QUFDZjtBQUVBO0VBQ0UsbUJBQWE7VUFBYixhQUFhO0FBQ2Y7QUFFQTtFQUNFLG1CQUFPO1VBQVAsT0FBTztFQUNQLGlCQUFpQjtFQUNqQixTQUFTO0VBQ1QsZUFBZTtBQUNqQjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGNBQWM7QUFDaEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsaUJBQWlCO0FBQ25CO0FBR0E7RUFDRSxVQUFVO0FBQ1oiLCJmaWxlIjoiYXBwL2Jvb2tzdG9yZS9tYWdhemluZS1kZXNjcmlwdGlvbi9tYWdhemluZS1kZXNjcmlwdGlvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC10eXBvZ3JhcGh5IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgYmFja2dyb3VuZDogbm9uZTtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDIwOCwgMjUyLCAyNTUsIDAuODgpO1xyXG4gIHBhZGRpbmc6IDVweDtcclxuICB3aWR0aDogNzIlO1xyXG4gIGhlaWdodDogODAwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICBib3JkZXI6IDNweCAjY2NjY2NjIHNvbGlkO1xyXG4gIC1tb3otYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgLWtodG1sLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcclxuICBwYWRkaW5nOiAwcHggMTI1cHg7XHJcbn1cclxuLmhlYWRsaW5lIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6IHJnYigxMjgsIDAsIDE5MSk7XHJcbiAgZm9udC1zaXplOiA0NXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiAxMDAwO1xyXG59XHJcbi5teS1kaXYge1xyXG4gIHdpZHRoOiA4NSU7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG59XHJcbi5teS1kaXYyIHtcclxuICBwYWRkaW5nOiAyNXB4O1xyXG4gIGNvbG9yOiByZ2IoMCwgNjgsIDI1NSk7IFxyXG4gIGhlaWdodDogODAlO1xyXG59XHJcblxyXG4ubWlkLWNhZHIge1xyXG4gIHBhZGRpbmc6IDc0cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogODZweDtcclxufVxyXG4ub3ZlcmxheSB7XHJcbiAgY29udGVudDogXCJcIjtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC41KTtcclxuICB6LWluZGV4OiAxMDA7XHJcbn1cclxuXHJcbi53cmFwcGVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4jbGVmdCB7XHJcbiAgZmxleDogMCAwIDI1JTtcclxufVxyXG5cclxuI3JpZ2h0IHtcclxuICBmbGV4OiA0O1xyXG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gIHdpZHRoOiA1JTtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuLnByaWNlIHtcclxuICBtYXJnaW4tbGVmdDogLTM1cHg7XHJcbiAgcGFkZGluZzogMzVweDtcclxuICBmb250LXdlaWdodDogMTAwMDtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbiAgY29sb3I6ICNmMDFkMWQ7XHJcbn1cclxuLm1hdC1kaWFsb2ctY29udGVudCB7XHJcbiAgbWF4LWhlaWdodDogNzV2aDtcclxufVxyXG4ubWF0LWRpYWxvZy1hY3Rpb25zIHtcclxuICBwYWRkaW5nOiA1cHggMjVweDtcclxufVxyXG5cclxuXHJcbi5lbmxhcmdle1xyXG4gIHdpZHRoOiA5MCU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/bookstore/magazine-description/magazine-description.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/bookstore/magazine-description/magazine-description.component.ts ***!
  \**********************************************************************************/
/*! exports provided: MagazineDescriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MagazineDescriptionComponent", function() { return MagazineDescriptionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/magazine.service */ "./src/app/services/magazine.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var src_app_models_magazine_get_magazine_list_view__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/models/magazine/get.magazine.list.view */ "./src/app/models/magazine/get.magazine.list.view.ts");
/* harmony import */ var src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/order.item.view */ "./src/app/shared/order.item.view.ts");







var MagazineDescriptionComponent = /** @class */ (function () {
    function MagazineDescriptionComponent(magazineService, toastr, data) {
        this.magazineService = magazineService;
        this.toastr = toastr;
        this.data = data;
        this.magazineList = new src_app_models_magazine_get_magazine_list_view__WEBPACK_IMPORTED_MODULE_5__["GetAllMagazineView"]();
        this.magazineDescription = new src_app_models_magazine_get_magazine_list_view__WEBPACK_IMPORTED_MODULE_5__["MagazineGetMagazineItemView"];
    }
    MagazineDescriptionComponent.prototype.ngOnInit = function () {
        this.getMagazines();
    };
    MagazineDescriptionComponent.prototype.getMagazines = function () {
        var _this = this;
        this.magazineService.getAllMagazine().subscribe(function (datamagazine) {
            _this.magazineList = datamagazine;
            _this.getMagazineById(_this.data);
            _this.magazineDescription = _this.getMagazineById(_this.data.id);
        });
    };
    MagazineDescriptionComponent.prototype.getMagazineById = function (id) {
        for (var i = 0; i < this.magazineList.magazineList.length; i++) {
            if (this.magazineList.magazineList[i].id == id) {
                return this.magazineList.magazineList[i];
            }
        }
    };
    MagazineDescriptionComponent.prototype.onButtonClick = function (id) {
        var orderItem = new src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_6__["OrderItem"]();
        orderItem.id = id;
        orderItem.item = src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_6__["Item"].Book;
        var key = "item1";
        var existingOrder = JSON.parse(localStorage.getItem(key));
        if (existingOrder === null) {
            existingOrder = new Array();
        }
        existingOrder.push(orderItem);
        localStorage.setItem(key, JSON.stringify(existingOrder));
        this.toastr.success("Book Add To Order");
    };
    MagazineDescriptionComponent.ctorParameters = function () { return [
        { type: src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_3__["MagazineService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] },
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
    ]; };
    MagazineDescriptionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "magazine-description",
            template: __webpack_require__(/*! raw-loader!./magazine-description.component.html */ "./node_modules/raw-loader/index.js!./src/app/bookstore/magazine-description/magazine-description.component.html"),
            styles: [__webpack_require__(/*! ./magazine-description.component.css */ "./src/app/bookstore/magazine-description/magazine-description.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"]))
    ], MagazineDescriptionComponent);
    return MagazineDescriptionComponent;
}());



/***/ }),

/***/ "./src/app/bookstore/magazine/magazine.component.css":
/*!***********************************************************!*\
  !*** ./src/app/bookstore/magazine/magazine.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.image{\r\n    position: absolute;\r\n    overflow: auto;\r\n    height: 100vh;\r\n    width: 100%;\r\n    background-image: url(https://images.clipartlogo.com/files/istock/previews/7631/76316673-stack-of-books.jpg);\r\n  }\r\n  .myForm{\r\n    position: relative;\r\n    height: 92%;\r\n   }\r\n  .k-widget{\r\n     border: none;\r\n  \r\n   }\r\n  .myForm2{\r\n    height: 12%;\r\n    background-color: #226f8d;\r\n  }\r\n  .btn-lg\r\n  {\r\n    margin-left: 2.5%;\r\n    border-radius: 6rem;\r\n    border-color: #cbcbcc;\r\n  }\r\n  .btn{\r\n    background-color: #d5b0f86e;\r\n  }\r\n  .k-button.k-primary, .k-slider .k-draghandle {\r\n    border-color: #747b1d;\r\n    color: #000000;\r\n    background-color: #d5b0f86e;\r\n  }\r\n  .sad {\r\n    border-color: #747b1d;\r\n    color: #000000;\r\n    background-color: #d5b0f86e;\r\n  \r\n    background-image: none;\r\n    background-image: none;\r\n  }\r\n  .bg-light {\r\n    background-color: #5fa4b7!important\r\n  }\r\n  .btn-sm\r\n{\r\n  margin-right: 0.5%;\r\n  border-radius: 7rem;\r\n  color: #ffffff;\r\n}\r\n  .btn-info {\r\n  border-color: #b9bfc0;\r\n}\r\n  img {\r\n  width: 50px;\r\n}\r\n  .enlarge:hover {\r\n\t-webkit-transform:scale(3.5,3.5);\r\n\t        transform:scale(3.5,3.5);\r\n  -webkit-transform-origin:0 0;\r\n          transform-origin:0 0;\r\n  position: absolute;\r\n\r\n}\r\n  .div-zoom{\r\n  width:85px; height:55px;\r\n}\r\n  .small-image{\r\n  background-image: url(http://sportkniga.com.ua/images/books228577dtg3.jpg);\r\n  height: 10vh;\r\n  width: 5%;\r\n}\r\n  .k-grid{\r\n  background-color: #94e8ff69;\r\n  color: rgba(78, 0, 255, 0.9);\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9ib29rc3RvcmUvbWFnYXppbmUvbWFnYXppbmUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGFBQWE7SUFDYixXQUFXO0lBQ1gsNEdBQTRHO0VBQzlHO0VBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsV0FBVztHQUNaO0VBQ0E7S0FDRSxZQUFZOztHQUVkO0VBQ0Q7SUFDRSxXQUFXO0lBQ1gseUJBQXlCO0VBQzNCO0VBQ0E7O0lBRUUsaUJBQWlCO0lBQ2pCLG1CQUFtQjtJQUNuQixxQkFBcUI7RUFDdkI7RUFDQTtJQUNFLDJCQUEyQjtFQUM3QjtFQUNBO0lBQ0UscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCwyQkFBMkI7RUFDN0I7RUFDQTtJQUNFLHFCQUFxQjtJQUNyQixjQUFjO0lBQ2QsMkJBQTJCOztJQUUzQixzQkFBc0I7SUFDdEIsc0JBQXNCO0VBQ3hCO0VBQ0E7SUFDRTtFQUNGO0VBQ0Y7O0VBRUUsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixjQUFjO0FBQ2hCO0VBQ0E7RUFDRSxxQkFBcUI7QUFDdkI7RUFFQTtFQUNFLFdBQVc7QUFDYjtFQUNBO0NBQ0MsZ0NBQXdCO1NBQXhCLHdCQUF3QjtFQUN2Qiw0QkFBb0I7VUFBcEIsb0JBQW9CO0VBQ3BCLGtCQUFrQjs7QUFFcEI7RUFDQTtFQUNFLFVBQVUsRUFBRSxXQUFXO0FBQ3pCO0VBQ0E7RUFDRSwwRUFBMEU7RUFDMUUsWUFBWTtFQUNaLFNBQVM7QUFDWDtFQUNBO0VBQ0UsMkJBQTJCO0VBQzNCLDRCQUE0QjtBQUM5QiIsImZpbGUiOiJhcHAvYm9va3N0b3JlL21hZ2F6aW5lL21hZ2F6aW5lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLmltYWdle1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoaHR0cHM6Ly9pbWFnZXMuY2xpcGFydGxvZ28uY29tL2ZpbGVzL2lzdG9jay9wcmV2aWV3cy83NjMxLzc2MzE2NjczLXN0YWNrLW9mLWJvb2tzLmpwZyk7XHJcbiAgfVxyXG4gIC5teUZvcm17XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBoZWlnaHQ6IDkyJTtcclxuICAgfVxyXG4gICAuay13aWRnZXR7XHJcbiAgICAgYm9yZGVyOiBub25lO1xyXG4gIFxyXG4gICB9XHJcbiAgLm15Rm9ybTJ7XHJcbiAgICBoZWlnaHQ6IDEyJTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyMjZmOGQ7XHJcbiAgfVxyXG4gIC5idG4tbGdcclxuICB7XHJcbiAgICBtYXJnaW4tbGVmdDogMi41JTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDZyZW07XHJcbiAgICBib3JkZXItY29sb3I6ICNjYmNiY2M7XHJcbiAgfVxyXG4gIC5idG57XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDViMGY4NmU7XHJcbiAgfVxyXG4gIC5rLWJ1dHRvbi5rLXByaW1hcnksIC5rLXNsaWRlciAuay1kcmFnaGFuZGxlIHtcclxuICAgIGJvcmRlci1jb2xvcjogIzc0N2IxZDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Q1YjBmODZlO1xyXG4gIH1cclxuICAuc2FkIHtcclxuICAgIGJvcmRlci1jb2xvcjogIzc0N2IxZDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Q1YjBmODZlO1xyXG4gIFxyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogbm9uZTtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IG5vbmU7XHJcbiAgfVxyXG4gIC5iZy1saWdodCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWZhNGI3IWltcG9ydGFudFxyXG4gIH1cclxuLmJ0bi1zbVxyXG57XHJcbiAgbWFyZ2luLXJpZ2h0OiAwLjUlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDdyZW07XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbn1cclxuLmJ0bi1pbmZvIHtcclxuICBib3JkZXItY29sb3I6ICNiOWJmYzA7XHJcbn1cclxuXHJcbmltZyB7XHJcbiAgd2lkdGg6IDUwcHg7XHJcbn1cclxuLmVubGFyZ2U6aG92ZXIge1xyXG5cdHRyYW5zZm9ybTpzY2FsZSgzLjUsMy41KTtcclxuICB0cmFuc2Zvcm0tb3JpZ2luOjAgMDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcblxyXG59XHJcbi5kaXYtem9vbXtcclxuICB3aWR0aDo4NXB4OyBoZWlnaHQ6NTVweDtcclxufVxyXG4uc21hbGwtaW1hZ2V7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHA6Ly9zcG9ydGtuaWdhLmNvbS51YS9pbWFnZXMvYm9va3MyMjg1NzdkdGczLmpwZyk7XHJcbiAgaGVpZ2h0OiAxMHZoO1xyXG4gIHdpZHRoOiA1JTtcclxufVxyXG4uay1ncmlke1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM5NGU4ZmY2OTtcclxuICBjb2xvcjogcmdiYSg3OCwgMCwgMjU1LCAwLjkpO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/bookstore/magazine/magazine.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/bookstore/magazine/magazine.component.ts ***!
  \**********************************************************/
/*! exports provided: MagazineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MagazineComponent", function() { return MagazineComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/magazine.service */ "./src/app/services/magazine.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @progress/kendo-data-query */ "./node_modules/@progress/kendo-data-query/dist/es/main.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _menu_order_basket_order_basket_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../menu/order-basket/order-basket.component */ "./src/app/menu/order-basket/order-basket.component.ts");
/* harmony import */ var src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/authentication/user.service */ "./src/app/services/authentication/user.service.ts");
/* harmony import */ var _magazine_description_magazine_description_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../magazine-description/magazine-description.component */ "./src/app/bookstore/magazine-description/magazine-description.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/shared/order.item.view */ "./src/app/shared/order.item.view.ts");











var MagazineComponent = /** @class */ (function () {
    function MagazineComponent(magazineService, dialog, userService, toastr) {
        this.magazineService = magazineService;
        this.dialog = dialog;
        this.userService = userService;
        this.toastr = toastr;
        this.isOpen = false;
        this.gridState = { sort: [], skip: 0, take: 10 };
    }
    MagazineComponent.prototype.ngOnInit = function () {
        this.loadData();
        this.admin = this.userService.getIsAdmin();
    };
    MagazineComponent.prototype.loadData = function () {
        var _this = this;
        this.magazineService.getAllMagazine().subscribe(function (data) {
            _this.magazineList = data;
            _this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__["process"])(_this.magazineList.magazineList, _this.gridState);
        });
    };
    MagazineComponent.prototype.addHandler = function (_a) {
        var sender = _a.sender;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            price: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0),
            picture: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("")
        });
        sender.addRow(this.formGroup);
    };
    MagazineComponent.prototype.editHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](dataItem.id),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](dataItem.title, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            price: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](dataItem.price),
            public: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](dataItem.picture)
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    MagazineComponent.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    MagazineComponent.prototype.saveHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, formGroup = _a.formGroup, isNew = _a.isNew;
        if (isNew) {
            var product = formGroup.value;
            this.magazineService.create(product).subscribe(function () { return _this.loadData(); });
        }
        if (!isNew) {
            var product = formGroup.value;
            this.magazineService.update(product).subscribe(function () { return _this.loadData(); });
        }
        sender.closeRow(rowIndex);
    };
    MagazineComponent.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    MagazineComponent.prototype.Logout = function () {
        localStorage.removeItem("data");
        window.location.reload();
    };
    MagazineComponent.prototype.authorChange = function (value) {
        this.formGroup.get("author").setValue(value);
    };
    MagazineComponent.prototype.getAuthorList = function (authorList) {
        var authorString = "";
        for (var i = 0; i < authorList.authorList.length; i++) {
            authorString += authorList.authorList[i].name;
        }
        return authorString;
    };
    MagazineComponent.prototype.dataStateChange = function (state) {
        this.gridState = state;
        this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__["process"])(this.magazineList.magazineList, this.gridState);
    };
    MagazineComponent.prototype.onButtonClick = function (id, price) {
        var orderItem = new src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_10__["OrderItem"]();
        orderItem.id = id;
        orderItem.item = src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_10__["Item"].Magazine;
        orderItem.pirce = price;
        var key = "item1";
        var existingOrder = JSON.parse(localStorage.getItem(key));
        if (existingOrder === null) {
            existingOrder = new Array();
        }
        existingOrder.push(orderItem);
        localStorage.setItem(key, JSON.stringify(existingOrder));
        this.toastr.success("Added to Basket");
    };
    MagazineComponent.prototype.openDialog = function () {
        var dialogRef = this.dialog.open(_menu_order_basket_order_basket_component__WEBPACK_IMPORTED_MODULE_6__["OrderBasketComponent"]);
        {
        }
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Dialog result: " + result);
        });
    };
    MagazineComponent.prototype.openDescription = function (id) {
        if (this.isOpen === false) {
            var dialogRef = this.dialog.open(_magazine_description_magazine_description_component__WEBPACK_IMPORTED_MODULE_8__["MagazineDescriptionComponent"], {
                data: { id: id }
            });
            {
            }
            dialogRef.afterClosed().subscribe(function (result) {
                console.log("Dialog result: " + result);
            });
        }
    };
    MagazineComponent.ctorParameters = function () { return [
        { type: src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_2__["MagazineService"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"] },
        { type: src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"] }
    ]; };
    MagazineComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "magazine",
            template: __webpack_require__(/*! raw-loader!./magazine.component.html */ "./node_modules/raw-loader/index.js!./src/app/bookstore/magazine/magazine.component.html"),
            styles: [__webpack_require__(/*! ./magazine.component.css */ "./src/app/bookstore/magazine/magazine.component.css")]
        })
    ], MagazineComponent);
    return MagazineComponent;
}());



/***/ }),

/***/ "./src/app/menu/mainpage/mainpage.component.css":
/*!******************************************************!*\
  !*** ./src/app/menu/mainpage/mainpage.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-sm {\r\n  margin-right: 0.5%;\r\n  border-radius: 7rem;\r\n  color: #ffffff;\r\n}\r\n.btn-lg {\r\n  margin-left: 2.5%;\r\n  border-radius: 10rem;\r\n  color: #ffffff;\r\n}\r\n.btn {\r\n  background-color: #89cebae3;\r\n}\r\n.small-image {\r\n  background-image: url(http://sportkniga.com.ua/images/books228577dtg3.jpg);\r\n  height: 10vh;\r\n  width: 5%;\r\n}\r\n.sad {\r\n  border-color: #747b1d;\r\n  color: #000000;\r\n  background-color: #d5b0f86e;\r\n\r\n  background-image: none;\r\n  background-image: none;\r\n}\r\n.z-index {\r\n  top: 10px;\r\n  left: 20px;\r\n  bottom: 10px;\r\n  position: relative;\r\n  z-index: 1000;\r\n  border: 2px #ff6a84 solid;\r\n  font-weight: 600;\r\n  border-radius: 41%;\r\n  color: #0011ff;\r\n}\r\n.glyphicon {\r\n  right: 30%;\r\n}\r\n.bg-light {\r\n  background-color: #48c0d1f2 !important;\r\n}\r\n.but {\r\n  background-color: #d5b0f86e;\r\n  margin-right: 2%;\r\n  width: 35%;\r\n}\r\n.btn-info {\r\n  border-color: #b9bfc0;\r\n}\r\n.navbar {\r\n  margin-bottom: 0px;\r\n}\r\n.logPanel {\r\n  display: contents;\r\n  width: 100%;\r\n}\r\n.navbar-brand {\r\n  border-radius: 13%;\r\n  color: rgba(54, 0, 255, 0.9);\r\n}\r\n.element.style {\r\n  height: 733px;\r\n}\r\n.btn-outline-success {\r\n  color: #000000;\r\n}\r\n.btn.btn-sm {\r\n  font-size: 2rem;\r\n}\r\n.userMail {\r\n  margin-right: 2%;\r\n}\r\n.txt {\r\n  font-size: 20px;\r\n  margin-top: 10px;\r\n  margin-bottom: 5px;\r\n  color: rgb(22, 131, 255);\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9tZW51L21haW5wYWdlL21haW5wYWdlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGNBQWM7QUFDaEI7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsY0FBYztBQUNoQjtBQUNBO0VBQ0UsMkJBQTJCO0FBQzdCO0FBQ0E7RUFDRSwwRUFBMEU7RUFDMUUsWUFBWTtFQUNaLFNBQVM7QUFDWDtBQUVBO0VBQ0UscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCwyQkFBMkI7O0VBRTNCLHNCQUFzQjtFQUN0QixzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLFNBQVM7RUFDVCxVQUFVO0VBQ1YsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IseUJBQXlCO0VBQ3pCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsY0FBYztBQUNoQjtBQUNBO0VBQ0UsVUFBVTtBQUNaO0FBRUE7RUFDRSxzQ0FBc0M7QUFDeEM7QUFDQTtFQUNFLDJCQUEyQjtFQUMzQixnQkFBZ0I7RUFDaEIsVUFBVTtBQUNaO0FBQ0E7RUFDRSxxQkFBcUI7QUFDdkI7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLFdBQVc7QUFDYjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLDRCQUE0QjtBQUM5QjtBQUNBO0VBQ0UsYUFBYTtBQUNmO0FBQ0E7RUFDRSxjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxlQUFlO0FBQ2pCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLHdCQUF3QjtBQUMxQiIsImZpbGUiOiJhcHAvbWVudS9tYWlucGFnZS9tYWlucGFnZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0bi1zbSB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAwLjUlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDdyZW07XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbn1cclxuLmJ0bi1sZyB7XHJcbiAgbWFyZ2luLWxlZnQ6IDIuNSU7XHJcbiAgYm9yZGVyLXJhZGl1czogMTByZW07XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbn1cclxuLmJ0biB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzg5Y2ViYWUzO1xyXG59XHJcbi5zbWFsbC1pbWFnZSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHA6Ly9zcG9ydGtuaWdhLmNvbS51YS9pbWFnZXMvYm9va3MyMjg1NzdkdGczLmpwZyk7XHJcbiAgaGVpZ2h0OiAxMHZoO1xyXG4gIHdpZHRoOiA1JTtcclxufVxyXG5cclxuLnNhZCB7XHJcbiAgYm9yZGVyLWNvbG9yOiAjNzQ3YjFkO1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkNWIwZjg2ZTtcclxuXHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbm9uZTtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiBub25lO1xyXG59XHJcbi56LWluZGV4IHtcclxuICB0b3A6IDEwcHg7XHJcbiAgbGVmdDogMjBweDtcclxuICBib3R0b206IDEwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHotaW5kZXg6IDEwMDA7XHJcbiAgYm9yZGVyOiAycHggI2ZmNmE4NCBzb2xpZDtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGJvcmRlci1yYWRpdXM6IDQxJTtcclxuICBjb2xvcjogIzAwMTFmZjtcclxufVxyXG4uZ2x5cGhpY29uIHtcclxuICByaWdodDogMzAlO1xyXG59XHJcblxyXG4uYmctbGlnaHQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM0OGMwZDFmMiAhaW1wb3J0YW50O1xyXG59XHJcbi5idXQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkNWIwZjg2ZTtcclxuICBtYXJnaW4tcmlnaHQ6IDIlO1xyXG4gIHdpZHRoOiAzNSU7XHJcbn1cclxuLmJ0bi1pbmZvIHtcclxuICBib3JkZXItY29sb3I6ICNiOWJmYzA7XHJcbn1cclxuXHJcbi5uYXZiYXIge1xyXG4gIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG4ubG9nUGFuZWwge1xyXG4gIGRpc3BsYXk6IGNvbnRlbnRzO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5uYXZiYXItYnJhbmQge1xyXG4gIGJvcmRlci1yYWRpdXM6IDEzJTtcclxuICBjb2xvcjogcmdiYSg1NCwgMCwgMjU1LCAwLjkpO1xyXG59XHJcbi5lbGVtZW50LnN0eWxlIHtcclxuICBoZWlnaHQ6IDczM3B4O1xyXG59XHJcbi5idG4tb3V0bGluZS1zdWNjZXNzIHtcclxuICBjb2xvcjogIzAwMDAwMDtcclxufVxyXG4uYnRuLmJ0bi1zbSB7XHJcbiAgZm9udC1zaXplOiAycmVtO1xyXG59XHJcbi51c2VyTWFpbCB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAyJTtcclxufVxyXG4udHh0IHtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgY29sb3I6IHJnYigyMiwgMTMxLCAyNTUpO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/menu/mainpage/mainpage.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/menu/mainpage/mainpage.component.ts ***!
  \*****************************************************/
/*! exports provided: MainpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainpageComponent", function() { return MainpageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/authentication/user.service */ "./src/app/services/authentication/user.service.ts");
/* harmony import */ var src_app_models_authentication_refresh_token_view__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/models/authentication/refresh.token.view */ "./src/app/models/authentication/refresh.token.view.ts");
/* harmony import */ var _order_basket_order_basket_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../order-basket/order-basket.component */ "./src/app/menu/order-basket/order-basket.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_counte_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/counte.service */ "./src/app/services/counte.service.ts");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");










var MainpageComponent = /** @class */ (function () {
    function MainpageComponent(dialog, userService, dataService, router, counterService) {
        this.dialog = dialog;
        this.userService = userService;
        this.dataService = dataService;
        this.router = router;
        this.counterService = counterService;
        this.admin = false;
        this.model = new src_app_models_authentication_refresh_token_view__WEBPACK_IMPORTED_MODULE_5__["RefreshTokenView"]();
    }
    MainpageComponent.prototype.ngOnInit = function () {
        this.admin = this.userService.getIsAdmin();
        this.getAll();
        this.callCounterService();
        this.getAmount();
    };
    MainpageComponent.prototype.getAmount = function () {
        this.counterService.itemCounter();
        this.orderCounter = this.counterService.getAmount();
    };
    MainpageComponent.prototype.openDialog = function () {
        var dialogRef = this.dialog.open(_order_basket_order_basket_component__WEBPACK_IMPORTED_MODULE_6__["OrderBasketComponent"]);
        {
        }
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Dialog result: " + result);
        });
    };
    MainpageComponent.prototype.callCounterService = function () {
        this.counterService.itemCounter();
    };
    MainpageComponent.prototype.Logout = function () {
        localStorage.removeItem("data");
        localStorage.removeItem("item1");
        localStorage.removeItem("Refresh");
        window.location.reload();
    };
    MainpageComponent.prototype.getAll = function () {
        var _this = this;
        this.userService.getUsers().subscribe(function (res) {
            _this.userList = res;
        });
        this.showItem();
    };
    MainpageComponent.prototype.showItem = function () {
        var token = jwt_decode__WEBPACK_IMPORTED_MODULE_3__(localStorage.getItem("data"));
        this.model.Email = token["sub"];
        return this.model.Email;
    };
    MainpageComponent.prototype.openAdminPage = function () {
        this.router.navigate(["/app-admin-page"]);
    };
    MainpageComponent.ctorParameters = function () { return [
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
        { type: src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
        { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_9__["DataService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
        { type: src_app_services_counte_service__WEBPACK_IMPORTED_MODULE_8__["CounterService"] }
    ]; };
    MainpageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-mainpage",
            template: __webpack_require__(/*! raw-loader!./mainpage.component.html */ "./node_modules/raw-loader/index.js!./src/app/menu/mainpage/mainpage.component.html"),
            styles: [__webpack_require__(/*! ./mainpage.component.css */ "./src/app/menu/mainpage/mainpage.component.css")]
        })
    ], MainpageComponent);
    return MainpageComponent;
}());



/***/ }),

/***/ "./src/app/menu/menu.module.ts":
/*!*************************************!*\
  !*** ./src/app/menu/menu.module.ts ***!
  \*************************************/
/*! exports provided: MenuModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuModule", function() { return MenuModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _mainpage_mainpage_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mainpage/mainpage.component */ "./src/app/menu/mainpage/mainpage.component.ts");
/* harmony import */ var _order_basket_order_basket_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./order-basket/order-basket.component */ "./src/app/menu/order-basket/order-basket.component.ts");
/* harmony import */ var _shop_description_shop_description_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shop-description/shop-description.component */ "./src/app/menu/shop-description/shop-description.component.ts");
/* harmony import */ var _payment_payment_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./payment/payment.component */ "./src/app/menu/payment/payment.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/fesm5/angular-bootstrap-md.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/esm5/table.es5.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _ng_select_ng_option_highlight__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ng-select/ng-option-highlight */ "./node_modules/@ng-select/ng-option-highlight/fesm5/ng-select-ng-option-highlight.js");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/fesm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _authentication_authentication_module__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../authentication/authentication.module */ "./src/app/authentication/authentication.module.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");



























var MenuModule = /** @class */ (function () {
    function MenuModule() {
    }
    MenuModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_15__["RouterModule"].forChild([
                    { path: "app-mainpage", component: _mainpage_mainpage_component__WEBPACK_IMPORTED_MODULE_3__["MainpageComponent"] },
                    { path: "order", component: _order_basket_order_basket_component__WEBPACK_IMPORTED_MODULE_4__["OrderBasketComponent"] },
                    { path: "app-shop-description", component: _shop_description_shop_description_component__WEBPACK_IMPORTED_MODULE_5__["ShopDescriptionComponent"] },
                    { path: "payment", component: _payment_payment_component__WEBPACK_IMPORTED_MODULE_6__["PaymentComponent"] }
                ]),
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatMenuModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__["NgbAlertModule"],
                _angular_cdk_table__WEBPACK_IMPORTED_MODULE_17__["CdkTableModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_14__["NgbModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatIconModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_8__["BsDropdownModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_13__["MatCheckboxModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatTableModule"],
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__["ModalModule"],
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_10__["TooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatCardModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_18__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_19__["AppRoutingModule"],
                _ng_select_ng_option_highlight__WEBPACK_IMPORTED_MODULE_20__["NgOptionHighlightModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_9__["ButtonsModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_9__["CollapseModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_9__["WavesModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatSidenavModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_21__["NgMultiSelectDropDownModule"].forRoot(),
                _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_22__["PopupModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_23__["HttpClientModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _authentication_authentication_module__WEBPACK_IMPORTED_MODULE_24__["AuthenticationModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatToolbarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_25__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatSelectModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_25__["NoopAnimationsModule"]
            ],
            declarations: [
                _mainpage_mainpage_component__WEBPACK_IMPORTED_MODULE_3__["MainpageComponent"],
                _order_basket_order_basket_component__WEBPACK_IMPORTED_MODULE_4__["OrderBasketComponent"],
                _shop_description_shop_description_component__WEBPACK_IMPORTED_MODULE_5__["ShopDescriptionComponent"],
                _payment_payment_component__WEBPACK_IMPORTED_MODULE_6__["PaymentComponent"]
            ],
            exports: [_mainpage_mainpage_component__WEBPACK_IMPORTED_MODULE_3__["MainpageComponent"]]
        })
    ], MenuModule);
    return MenuModule;
}());



/***/ }),

/***/ "./src/app/menu/order-basket/order-basket.component.css":
/*!**************************************************************!*\
  !*** ./src/app/menu/order-basket/order-basket.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-typography {\r\n  background: none;\r\n  background: rgba(135, 241, 255, 0.808);\r\n  padding: 5px;\r\n  width: 74%;\r\n  margin-left: 14%;\r\n  border: 3px #cccccc solid;\r\n  border-radius: 20px;\r\n  max-height: 120vh;\r\n  padding: 50px 110px;\r\n}\r\n.my-div {\r\n  border: 4px double rgb(138, 208, 236);\r\n  background: rgb(132, 240, 240);\r\n  padding: 20px;\r\n  width: 320px;\r\n  word-wrap: break-word;\r\n  display: inline-block;\r\n  padding: 30px 25px;\r\n  font-size: 14px;\r\n  line-height: 2.428571;\r\n}\r\n.my-div2 {\r\n  border: 4px double rgb(138, 208, 236);\r\n  background: rgb(132, 240, 240);\r\n  padding: 20px;\r\n  width: 290px;\r\n  word-wrap: break-word;\r\n  display: inline-block;\r\n  padding: 30px 25px;\r\n}\r\n.overlay {\r\n  content: \"\";\r\n  display: block;\r\n  position: fixed;\r\n  top: 0;\r\n  left: 0;\r\n  height: 100%;\r\n  width: 100%;\r\n  background: rgba(0, 0, 0, 0.5);\r\n  z-index: 100;\r\n}\r\n.total {\r\n  font-weight: bold;\r\n  font-size: 23px;\r\n  color: #f01d1d;\r\n  padding: 15px;\r\n  margin-left: -12px;\r\n}\r\n.btn-lg {\r\n  font-size: 12px;\r\n  background-color: #b9d6d8;\r\n  color: #333;\r\n  border-color: #b9d6d8;\r\n}\r\n.title {\r\n  font-family: \"Times New Roman\", Times, serif;\r\n  font-size: 280%;\r\n  margin: auto;\r\n  text-align: center;\r\n  color: rebeccapurple;\r\n}\r\n.enlarge {\r\n  width: 20%;\r\n}\r\n.wrapper {\r\n  display: -webkit-box;\r\n  display: flex;\r\n}\r\n.left {\r\n  width: 20%;\r\n}\r\n#right {\r\n  -webkit-box-flex: 2;\r\n          flex: 2;\r\n  margin-left: 20px;\r\n  width: 10%;\r\n  font-size: 17px;\r\n}\r\n.my-div3 {\r\n  font-weight: 500;\r\n  font-size: 20px;\r\n  color: #f01d1d;\r\n}\r\n.my-div1 {\r\n  font-weight: 500;\r\n  font-size: 20px;\r\n  color: #f01d1d;\r\n  color: rgb(128, 0, 191);\r\n}\r\n.warning {\r\n  font-family: \"Times New Roman\", Times, serif;\r\n  font-size: 250%;\r\n  margin: auto;\r\n  text-align: center;\r\n  color: #da212186;\r\n}\r\n.btn-sm {\r\n  font-size: 2.2rem;\r\n}\r\n.mat-dialog-conten {\r\n  max-height: 50vh;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9tZW51L29yZGVyLWJhc2tldC9vcmRlci1iYXNrZXQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFnQjtFQUNoQixzQ0FBc0M7RUFDdEMsWUFBWTtFQUNaLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIseUJBQXlCO0VBSXpCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSxxQ0FBcUM7RUFDckMsOEJBQThCO0VBQzlCLGFBQWE7RUFDYixZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLHFCQUFxQjtBQUN2QjtBQUNBO0VBQ0UscUNBQXFDO0VBQ3JDLDhCQUE4QjtFQUM5QixhQUFhO0VBQ2IsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLGVBQWU7RUFDZixNQUFNO0VBQ04sT0FBTztFQUNQLFlBQVk7RUFDWixXQUFXO0VBQ1gsOEJBQThCO0VBQzlCLFlBQVk7QUFDZDtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixjQUFjO0VBQ2QsYUFBYTtFQUNiLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixXQUFXO0VBQ1gscUJBQXFCO0FBQ3ZCO0FBQ0E7RUFDRSw0Q0FBNEM7RUFDNUMsZUFBZTtFQUNmLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsb0JBQW9CO0FBQ3RCO0FBQ0E7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLG9CQUFhO0VBQWIsYUFBYTtBQUNmO0FBQ0E7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLG1CQUFPO1VBQVAsT0FBTztFQUNQLGlCQUFpQjtFQUNqQixVQUFVO0VBQ1YsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGNBQWM7RUFDZCx1QkFBdUI7QUFDekI7QUFDQTtFQUNFLDRDQUE0QztFQUM1QyxlQUFlO0VBQ2YsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGlCQUFpQjtBQUNuQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCIiwiZmlsZSI6ImFwcC9tZW51L29yZGVyLWJhc2tldC9vcmRlci1iYXNrZXQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtdHlwb2dyYXBoeSB7XHJcbiAgYmFja2dyb3VuZDogbm9uZTtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDEzNSwgMjQxLCAyNTUsIDAuODA4KTtcclxuICBwYWRkaW5nOiA1cHg7XHJcbiAgd2lkdGg6IDc0JTtcclxuICBtYXJnaW4tbGVmdDogMTQlO1xyXG4gIGJvcmRlcjogM3B4ICNjY2NjY2Mgc29saWQ7XHJcbiAgLW1vei1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAta2h0bWwtYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIG1heC1oZWlnaHQ6IDEyMHZoO1xyXG4gIHBhZGRpbmc6IDUwcHggMTEwcHg7XHJcbn1cclxuLm15LWRpdiB7XHJcbiAgYm9yZGVyOiA0cHggZG91YmxlIHJnYigxMzgsIDIwOCwgMjM2KTtcclxuICBiYWNrZ3JvdW5kOiByZ2IoMTMyLCAyNDAsIDI0MCk7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICB3aWR0aDogMzIwcHg7XHJcbiAgd29yZC13cmFwOiBicmVhay13b3JkO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBwYWRkaW5nOiAzMHB4IDI1cHg7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAyLjQyODU3MTtcclxufVxyXG4ubXktZGl2MiB7XHJcbiAgYm9yZGVyOiA0cHggZG91YmxlIHJnYigxMzgsIDIwOCwgMjM2KTtcclxuICBiYWNrZ3JvdW5kOiByZ2IoMTMyLCAyNDAsIDI0MCk7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICB3aWR0aDogMjkwcHg7XHJcbiAgd29yZC13cmFwOiBicmVhay13b3JkO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBwYWRkaW5nOiAzMHB4IDI1cHg7XHJcbn1cclxuLm92ZXJsYXkge1xyXG4gIGNvbnRlbnQ6IFwiXCI7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHRvcDogMDtcclxuICBsZWZ0OiAwO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNSk7XHJcbiAgei1pbmRleDogMTAwO1xyXG59XHJcbi50b3RhbCB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAyM3B4O1xyXG4gIGNvbG9yOiAjZjAxZDFkO1xyXG4gIHBhZGRpbmc6IDE1cHg7XHJcbiAgbWFyZ2luLWxlZnQ6IC0xMnB4O1xyXG59XHJcbi5idG4tbGcge1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjlkNmQ4O1xyXG4gIGNvbG9yOiAjMzMzO1xyXG4gIGJvcmRlci1jb2xvcjogI2I5ZDZkODtcclxufVxyXG4udGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlRpbWVzIE5ldyBSb21hblwiLCBUaW1lcywgc2VyaWY7XHJcbiAgZm9udC1zaXplOiAyODAlO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6IHJlYmVjY2FwdXJwbGU7XHJcbn1cclxuLmVubGFyZ2Uge1xyXG4gIHdpZHRoOiAyMCU7XHJcbn1cclxuLndyYXBwZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuLmxlZnQge1xyXG4gIHdpZHRoOiAyMCU7XHJcbn1cclxuI3JpZ2h0IHtcclxuICBmbGV4OiAyO1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gIHdpZHRoOiAxMCU7XHJcbiAgZm9udC1zaXplOiAxN3B4O1xyXG59XHJcbi5teS1kaXYzIHtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjb2xvcjogI2YwMWQxZDtcclxufVxyXG4ubXktZGl2MSB7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbiAgY29sb3I6ICNmMDFkMWQ7XHJcbiAgY29sb3I6IHJnYigxMjgsIDAsIDE5MSk7XHJcbn1cclxuLndhcm5pbmcge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlRpbWVzIE5ldyBSb21hblwiLCBUaW1lcywgc2VyaWY7XHJcbiAgZm9udC1zaXplOiAyNTAlO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6ICNkYTIxMjE4NjtcclxufVxyXG4uYnRuLXNtIHtcclxuICBmb250LXNpemU6IDIuMnJlbTtcclxufVxyXG4ubWF0LWRpYWxvZy1jb250ZW4ge1xyXG4gIG1heC1oZWlnaHQ6IDUwdmg7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/menu/order-basket/order-basket.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/menu/order-basket/order-basket.component.ts ***!
  \*************************************************************/
/*! exports provided: OrderBasketComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderBasketComponent", function() { return OrderBasketComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-pwa/local-storage */ "./node_modules/@ngx-pwa/local-storage/fesm5/ngx-pwa-local-storage.js");
/* harmony import */ var src_app_services_book_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/book.service */ "./src/app/services/book.service.ts");
/* harmony import */ var src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/magazine.service */ "./src/app/services/magazine.service.ts");
/* harmony import */ var src_app_models_magazine_get_magazine_list_view__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/models/magazine/get.magazine.list.view */ "./src/app/models/magazine/get.magazine.list.view.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var src_app_models_book_get_book_list_view__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/models/book/get.book.list.view */ "./src/app/models/book/get.book.list.view.ts");
/* harmony import */ var src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/order.item.view */ "./src/app/shared/order.item.view.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");











var OrderBasketComponent = /** @class */ (function () {
    function OrderBasketComponent(bookService, localStorage, dataService, magazineService, toastr, router) {
        this.bookService = bookService;
        this.localStorage = localStorage;
        this.dataService = dataService;
        this.magazineService = magazineService;
        this.toastr = toastr;
        this.router = router;
        this.basketIsEmpty = false;
        this.books = new src_app_models_book_get_book_list_view__WEBPACK_IMPORTED_MODULE_7__["GetBookListView"]();
        this.booklistOrder = new Array();
        this.magazineList = new src_app_models_magazine_get_magazine_list_view__WEBPACK_IMPORTED_MODULE_5__["GetAllMagazineView"]();
        this.magazineListOrder = new Array();
        this.orderTotalPrice = 0;
    }
    OrderBasketComponent.prototype.ngOnInit = function () {
        this.loadata();
        this.orderTotalPrice;
    };
    OrderBasketComponent.prototype.loadata = function () {
        var _this = this;
        this.bookService.getAll().subscribe(function (data) {
            _this.books = data;
            _this.showItem();
        });
        this.magazineService.getAllMagazine().subscribe(function (data) {
            _this.magazineList = data;
            _this.showItemMagazine();
        });
    };
    OrderBasketComponent.prototype.showItem = function () {
        var existingOrder = JSON.parse(localStorage.getItem("item1"));
        if (existingOrder == null) {
            this.basketIsEmpty = true;
        }
        else {
            for (var i = 0; i < existingOrder.length; i++) {
                if (existingOrder[i].item == src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_8__["Item"].Book) {
                    var res = this.getBookById(existingOrder[i].id);
                    this.booklistOrder.push(res);
                    this.orderTotalPrice = this.orderTotalPrice + res.price;
                }
            }
        }
    };
    OrderBasketComponent.prototype.showItemMagazine = function () {
        var existingOrder = JSON.parse(localStorage.getItem("item1"));
        if (existingOrder == null) {
            this.basketIsEmpty = true;
        }
        else {
            for (var i = 0; i < existingOrder.length; i++) {
                if (existingOrder[i].item == src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_8__["Item"].Magazine) {
                    var ress = this.getMagazineById(existingOrder[i].id);
                    this.magazineListOrder.push(ress);
                    this.orderTotalPrice = this.orderTotalPrice + ress.price;
                }
            }
        }
    };
    OrderBasketComponent.prototype.getBookById = function (id) {
        var a = this.books;
        for (var i = 0; i < this.books.books.length; i++) {
            if (this.books.books[i].id == id) {
                return this.books.books[i];
            }
        }
    };
    OrderBasketComponent.prototype.getMagazineById = function (id) {
        for (var i = 0; i < this.magazineList.magazineList.length; i++) {
            if (this.magazineList.magazineList[i].id == id) {
                return this.magazineList.magazineList[i];
            }
        }
    };
    OrderBasketComponent.prototype.deleteItem = function () {
        var key = "item1";
        localStorage.removeItem(key);
        window.location.reload();
        this.toastr.success("removed");
    };
    OrderBasketComponent.prototype.openPayment = function () {
        this.router.navigate(["/payment"]);
        this.dataService.changeAmount(this.orderTotalPrice);
    };
    OrderBasketComponent.ctorParameters = function () { return [
        { type: src_app_services_book_service__WEBPACK_IMPORTED_MODULE_3__["BookService"] },
        { type: _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_2__["LocalStorage"] },
        { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_10__["DataService"] },
        { type: src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_4__["MagazineService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"] }
    ]; };
    OrderBasketComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "order",
            template: __webpack_require__(/*! raw-loader!./order-basket.component.html */ "./node_modules/raw-loader/index.js!./src/app/menu/order-basket/order-basket.component.html"),
            styles: [__webpack_require__(/*! ./order-basket.component.css */ "./src/app/menu/order-basket/order-basket.component.css")]
        })
    ], OrderBasketComponent);
    return OrderBasketComponent;
}());



/***/ }),

/***/ "./src/app/menu/payment/payment.component.css":
/*!****************************************************!*\
  !*** ./src/app/menu/payment/payment.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.credit-card {\r\n  width: 360px;\r\n  height: 400px;\r\n  margin: 60px auto 0;\r\n  border: 1px solid rgba(158, 119, 119, 0.425);\r\n  border-radius: 6px;\r\n  background-color: rgba(145, 241, 250, 0.794);\r\n  box-shadow: 1px 2px 3px 0 rgba(0,0,0,.10);\r\n}\r\n.form-header {\r\n  height: 60px;\r\n  padding: 20px 30px 0;\r\n  border-bottom: 1px solid #e1e8ee;\r\n}\r\n.form-body {\r\n  height: 340px;\r\n  padding: 30px 30px 20px;\r\n  line-height: 2;\r\n}\r\n.title {\r\n  font-size: 18px;\r\n  margin: 0;\r\n  color: #5e6977;\r\n}\r\n.card-number,\r\n.cvv-input input,\r\n.month select,\r\n.year select {\r\n    font-size: 14px;\r\n    font-weight: 100;\r\n    line-height: 14px;\r\n}\r\n.card-number,\r\n.month select,\r\n.year select {\r\n    font-size: 14px;\r\n    font-weight: 100;\r\n    line-height: 14px;  \r\n}\r\n.card-number,\r\n.cvv-details,\r\n.cvv-input input,\r\n.month select,\r\n.year select {\r\n    opacity: 1;\r\n    color: #507485;\r\n    padding-left: 5px;\r\n    letter-spacing: 5px;\r\n}\r\n.card-number {\r\n  width: 100%;\r\n  margin-bottom: 20px;\r\n  padding-left: 5px;\r\n  border: 2px solid #3775ac;\r\n  border-radius: 6px;\r\n}\r\n.image{\r\n  position: absolute;\r\n  overflow: auto;\r\n  height: 100vh;\r\n  width: 100%;\r\n  background-image: url(https://images.clipartlogo.com/files/istock/previews/7631/76316673-stack-of-books.jpg);\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9tZW51L3BheW1lbnQvcGF5bWVudC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQTtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDRDQUE0QztFQUM1QyxrQkFBa0I7RUFDbEIsNENBQTRDO0VBQzVDLHlDQUF5QztBQUMzQztBQUNBO0VBQ0UsWUFBWTtFQUNaLG9CQUFvQjtFQUNwQixnQ0FBZ0M7QUFDbEM7QUFFQTtFQUNFLGFBQWE7RUFDYix1QkFBdUI7RUFDdkIsY0FBYztBQUNoQjtBQUNBO0VBQ0UsZUFBZTtFQUNmLFNBQVM7RUFDVCxjQUFjO0FBQ2hCO0FBQ0E7Ozs7SUFJSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtBQUNyQjtBQUVBOzs7SUFHSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtBQUNyQjtBQUVBOzs7OztJQUtJLFVBQVU7SUFDVixjQUFjO0lBQ2QsaUJBQWlCO0lBQ2pCLG1CQUFtQjtBQUN2QjtBQUNBO0VBQ0UsV0FBVztFQUNYLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxhQUFhO0VBQ2IsV0FBVztFQUNYLDRHQUE0RztBQUM5RyIsImZpbGUiOiJhcHAvbWVudS9wYXltZW50L3BheW1lbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4uY3JlZGl0LWNhcmQge1xyXG4gIHdpZHRoOiAzNjBweDtcclxuICBoZWlnaHQ6IDQwMHB4O1xyXG4gIG1hcmdpbjogNjBweCBhdXRvIDA7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgxNTgsIDExOSwgMTE5LCAwLjQyNSk7XHJcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMTQ1LCAyNDEsIDI1MCwgMC43OTQpO1xyXG4gIGJveC1zaGFkb3c6IDFweCAycHggM3B4IDAgcmdiYSgwLDAsMCwuMTApO1xyXG59XHJcbi5mb3JtLWhlYWRlciB7XHJcbiAgaGVpZ2h0OiA2MHB4O1xyXG4gIHBhZGRpbmc6IDIwcHggMzBweCAwO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZTFlOGVlO1xyXG59XHJcblxyXG4uZm9ybS1ib2R5IHtcclxuICBoZWlnaHQ6IDM0MHB4O1xyXG4gIHBhZGRpbmc6IDMwcHggMzBweCAyMHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAyO1xyXG59XHJcbi50aXRsZSB7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIG1hcmdpbjogMDtcclxuICBjb2xvcjogIzVlNjk3NztcclxufVxyXG4uY2FyZC1udW1iZXIsXHJcbi5jdnYtaW5wdXQgaW5wdXQsXHJcbi5tb250aCBzZWxlY3QsXHJcbi55ZWFyIHNlbGVjdCB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBmb250LXdlaWdodDogMTAwO1xyXG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XHJcbn1cclxuIFxyXG4uY2FyZC1udW1iZXIsXHJcbi5tb250aCBzZWxlY3QsXHJcbi55ZWFyIHNlbGVjdCB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBmb250LXdlaWdodDogMTAwO1xyXG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7ICBcclxufVxyXG4gXHJcbi5jYXJkLW51bWJlcixcclxuLmN2di1kZXRhaWxzLFxyXG4uY3Z2LWlucHV0IGlucHV0LFxyXG4ubW9udGggc2VsZWN0LFxyXG4ueWVhciBzZWxlY3Qge1xyXG4gICAgb3BhY2l0eTogMTtcclxuICAgIGNvbG9yOiAjNTA3NDg1O1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogNXB4O1xyXG59XHJcbi5jYXJkLW51bWJlciB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICBib3JkZXI6IDJweCBzb2xpZCAjMzc3NWFjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDZweDtcclxufVxyXG5cclxuLmltYWdle1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBvdmVyZmxvdzogYXV0bztcclxuICBoZWlnaHQ6IDEwMHZoO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChodHRwczovL2ltYWdlcy5jbGlwYXJ0bG9nby5jb20vZmlsZXMvaXN0b2NrL3ByZXZpZXdzLzc2MzEvNzYzMTY2NzMtc3RhY2stb2YtYm9va3MuanBnKTtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/menu/payment/payment.component.ts":
/*!***************************************************!*\
  !*** ./src/app/menu/payment/payment.component.ts ***!
  \***************************************************/
/*! exports provided: PaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentComponent", function() { return PaymentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/order.service */ "./src/app/services/order.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_models_order_create_order_view__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/models/order/create.order.view */ "./src/app/models/order/create.order.view.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/magazine.service */ "./src/app/services/magazine.service.ts");
/* harmony import */ var _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngx-pwa/local-storage */ "./node_modules/@ngx-pwa/local-storage/fesm5/ngx-pwa-local-storage.js");
/* harmony import */ var src_app_services_book_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/book.service */ "./src/app/services/book.service.ts");
/* harmony import */ var src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/shared/order.item.view */ "./src/app/shared/order.item.view.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");














var PaymentComponent = /** @class */ (function () {
    function PaymentComponent(orderService, formBuilder, bookService, localStorage, magazineService, toastr, router, dataService) {
        this.orderService = orderService;
        this.formBuilder = formBuilder;
        this.bookService = bookService;
        this.localStorage = localStorage;
        this.magazineService = magazineService;
        this.toastr = toastr;
        this.router = router;
        this.dataService = dataService;
        this.amountSource = new rxjs__WEBPACK_IMPORTED_MODULE_12__["BehaviorSubject"](0);
    }
    PaymentComponent.prototype.ngOnInit = function () {
        this.formGroup = this.formBuilder.group({
            CardCvc: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            CardNumber: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            CardExpMonth: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            CardExpYear: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
        });
        this.orderTotalPrice = this.dataService.getAmount();
    };
    Object.defineProperty(PaymentComponent.prototype, "f", {
        get: function () {
            return this.formGroup.controls;
        },
        enumerable: true,
        configurable: true
    });
    PaymentComponent.prototype.onSubmit = function () {
        if (this.formGroup.invalid) {
            return this.toastr.error("Enter all fields");
        }
        var product = new src_app_models_order_create_order_view__WEBPACK_IMPORTED_MODULE_5__["CreateOrderView"]();
        product.CardCvc = this.formGroup.controls.CardCvc.value;
        product.CardNumber = this.formGroup.controls.CardNumber.value;
        product.CardExpMonth = this.formGroup.controls.CardExpMonth.value;
        product.CardExpYear = this.formGroup.controls.CardExpYear.value;
        this.orderService.charge(this.getItem(product)).subscribe();
        this.toastr.success("Succes Payment");
        this.router.navigate(["/book"]);
    };
    PaymentComponent.prototype.getItem = function (product) {
        var email = jwt_decode__WEBPACK_IMPORTED_MODULE_2__(localStorage.getItem("data"));
        product.email = email["sub"];
        var key = "item1";
        var existingOrder = JSON.parse(localStorage.getItem(key));
        for (var i = 0; i < existingOrder.length; i++) {
            if (existingOrder[i].item == src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_11__["Item"].Book) {
                product.bookIdList.push(existingOrder[i].id);
            }
            if (existingOrder[i].item == src_app_shared_order_item_view__WEBPACK_IMPORTED_MODULE_11__["Item"].Magazine) {
                product.magazineIdList.push(existingOrder[i].id);
            }
        }
        return product;
    };
    PaymentComponent.prototype.onClick = function (event) {
        var value = event.target.value;
        document.getElementById("spaseInput");
        var newValue = "";
        value = value.replace(/\s/g, "");
        for (var i = 0; i < value.length; i++) {
            if (i % 4 == 0 && i > 0)
                newValue = newValue.concat(" ");
            newValue = newValue.concat(value[i]);
        }
        event.target.value = newValue;
    };
    PaymentComponent.ctorParameters = function () { return [
        { type: src_app_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
        { type: src_app_services_book_service__WEBPACK_IMPORTED_MODULE_10__["BookService"] },
        { type: _ngx_pwa_local_storage__WEBPACK_IMPORTED_MODULE_9__["LocalStorage"] },
        { type: src_app_services_magazine_service__WEBPACK_IMPORTED_MODULE_8__["MagazineService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
        { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_13__["DataService"] }
    ]; };
    PaymentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "payment",
            template: __webpack_require__(/*! raw-loader!./payment.component.html */ "./node_modules/raw-loader/index.js!./src/app/menu/payment/payment.component.html"),
            styles: [__webpack_require__(/*! ./payment.component.css */ "./src/app/menu/payment/payment.component.css")]
        })
    ], PaymentComponent);
    return PaymentComponent;
}());



/***/ }),

/***/ "./src/app/menu/shop-description/shop-description.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/menu/shop-description/shop-description.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".image {\r\n    position: absolute;\r\n    overflow: auto;\r\n    height: 100vh;\r\n    width: 100%;\r\n    background-image: url(https://images.clipartlogo.com/files/istock/previews/7631/76316673-stack-of-books.jpg);\r\n  }\r\n  .myForm {\r\n    height: 179%;\r\n    background-color: rgb(172, 231, 255);\r\n    position: inherit;\r\n    text-align: center;\r\n  }\r\n  .head{\r\n      text-align: center;\r\n      font-family:  fantasy;\r\n      font-weight: 1900;\r\n      \r\n    }\r\n  .div1{\r\n  color: #ff1313;\r\n  width: 55%;\r\n  margin-top: 3%;\r\n  margin-left: 23%;\r\n  font-size: large;\r\n}\r\n  .div2 {\r\n  margin-top: 16%;\r\n  color: #8a2be2ba;\r\n  font-size: x-large;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9tZW51L3Nob3AtZGVzY3JpcHRpb24vc2hvcC1kZXNjcmlwdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxhQUFhO0lBQ2IsV0FBVztJQUNYLDRHQUE0RztFQUM5RztFQUNBO0lBQ0UsWUFBWTtJQUNaLG9DQUFvQztJQUNwQyxpQkFBaUI7SUFDakIsa0JBQWtCO0VBQ3BCO0VBQ0E7TUFDSSxrQkFBa0I7TUFDbEIscUJBQXFCO01BQ3JCLGlCQUFpQjs7SUFFbkI7RUFDSjtFQUNFLGNBQWM7RUFDZCxVQUFVO0VBQ1YsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixnQkFBZ0I7QUFDbEI7RUFDQTtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0FBQ3BCIiwiZmlsZSI6ImFwcC9tZW51L3Nob3AtZGVzY3JpcHRpb24vc2hvcC1kZXNjcmlwdGlvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltYWdlIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vaW1hZ2VzLmNsaXBhcnRsb2dvLmNvbS9maWxlcy9pc3RvY2svcHJldmlld3MvNzYzMS83NjMxNjY3My1zdGFjay1vZi1ib29rcy5qcGcpO1xyXG4gIH1cclxuICAubXlGb3JtIHtcclxuICAgIGhlaWdodDogMTc5JTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigxNzIsIDIzMSwgMjU1KTtcclxuICAgIHBvc2l0aW9uOiBpbmhlcml0O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAuaGVhZHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBmb250LWZhbWlseTogIGZhbnRhc3k7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiAxOTAwO1xyXG4gICAgICBcclxuICAgIH1cclxuLmRpdjF7XHJcbiAgY29sb3I6ICNmZjEzMTM7XHJcbiAgd2lkdGg6IDU1JTtcclxuICBtYXJnaW4tdG9wOiAzJTtcclxuICBtYXJnaW4tbGVmdDogMjMlO1xyXG4gIGZvbnQtc2l6ZTogbGFyZ2U7XHJcbn1cclxuLmRpdjIge1xyXG4gIG1hcmdpbi10b3A6IDE2JTtcclxuICBjb2xvcjogIzhhMmJlMmJhO1xyXG4gIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/menu/shop-description/shop-description.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/menu/shop-description/shop-description.component.ts ***!
  \*********************************************************************/
/*! exports provided: ShopDescriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopDescriptionComponent", function() { return ShopDescriptionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ShopDescriptionComponent = /** @class */ (function () {
    function ShopDescriptionComponent() {
    }
    ShopDescriptionComponent.prototype.ngOnInit = function () { };
    ShopDescriptionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-shop-description",
            template: __webpack_require__(/*! raw-loader!./shop-description.component.html */ "./node_modules/raw-loader/index.js!./src/app/menu/shop-description/shop-description.component.html"),
            styles: [__webpack_require__(/*! ./shop-description.component.css */ "./src/app/menu/shop-description/shop-description.component.css")]
        })
    ], ShopDescriptionComponent);
    return ShopDescriptionComponent;
}());



/***/ }),

/***/ "./src/app/models/authentication/confirm.register.view.ts":
/*!****************************************************************!*\
  !*** ./src/app/models/authentication/confirm.register.view.ts ***!
  \****************************************************************/
/*! exports provided: ConfirmRegisterView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmRegisterView", function() { return ConfirmRegisterView; });
var ConfirmRegisterView = /** @class */ (function () {
    function ConfirmRegisterView() {
    }
    return ConfirmRegisterView;
}());



/***/ }),

/***/ "./src/app/models/authentication/refresh.token.view.ts":
/*!*************************************************************!*\
  !*** ./src/app/models/authentication/refresh.token.view.ts ***!
  \*************************************************************/
/*! exports provided: RefreshTokenView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RefreshTokenView", function() { return RefreshTokenView; });
var RefreshTokenView = /** @class */ (function () {
    function RefreshTokenView() {
    }
    return RefreshTokenView;
}());



/***/ }),

/***/ "./src/app/models/authentication/social.login.view.ts":
/*!************************************************************!*\
  !*** ./src/app/models/authentication/social.login.view.ts ***!
  \************************************************************/
/*! exports provided: SocialLoginView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocialLoginView", function() { return SocialLoginView; });
var SocialLoginView = /** @class */ (function () {
    function SocialLoginView() {
    }
    return SocialLoginView;
}());



/***/ }),

/***/ "./src/app/models/authentication/token.ts":
/*!************************************************!*\
  !*** ./src/app/models/authentication/token.ts ***!
  \************************************************/
/*! exports provided: Token */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Token", function() { return Token; });
var Token = /** @class */ (function () {
    function Token() {
    }
    return Token;
}());



/***/ }),

/***/ "./src/app/models/authentication/user.ts":
/*!***********************************************!*\
  !*** ./src/app/models/authentication/user.ts ***!
  \***********************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/models/author/create.author.view.ts":
/*!*****************************************************!*\
  !*** ./src/app/models/author/create.author.view.ts ***!
  \*****************************************************/
/*! exports provided: CreateAuthorView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateAuthorView", function() { return CreateAuthorView; });
var CreateAuthorView = /** @class */ (function () {
    function CreateAuthorView() {
    }
    return CreateAuthorView;
}());



/***/ }),

/***/ "./src/app/models/author/get.author.list.view.ts":
/*!*******************************************************!*\
  !*** ./src/app/models/author/get.author.list.view.ts ***!
  \*******************************************************/
/*! exports provided: GetAuthorView, AuthorGetAuthorItemView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAuthorView", function() { return GetAuthorView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorGetAuthorItemView", function() { return AuthorGetAuthorItemView; });
var GetAuthorView = /** @class */ (function () {
    function GetAuthorView() {
        this.authorList = new Array();
    }
    return GetAuthorView;
}());

var AuthorGetAuthorItemView = /** @class */ (function () {
    function AuthorGetAuthorItemView() {
    }
    return AuthorGetAuthorItemView;
}());



/***/ }),

/***/ "./src/app/models/book/ceate.book.view.ts":
/*!************************************************!*\
  !*** ./src/app/models/book/ceate.book.view.ts ***!
  \************************************************/
/*! exports provided: CreateBookView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateBookView", function() { return CreateBookView; });
var CreateBookView = /** @class */ (function () {
    function CreateBookView() {
        this.authorIdList = [];
        this.genreIdList = [];
    }
    return CreateBookView;
}());



/***/ }),

/***/ "./src/app/models/book/get.book.list.view.ts":
/*!***************************************************!*\
  !*** ./src/app/models/book/get.book.list.view.ts ***!
  \***************************************************/
/*! exports provided: GetBookListView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetBookListView", function() { return GetBookListView; });
var GetBookListView = /** @class */ (function () {
    function GetBookListView() {
    }
    return GetBookListView;
}());



/***/ }),

/***/ "./src/app/models/book/get.book.view.ts":
/*!**********************************************!*\
  !*** ./src/app/models/book/get.book.view.ts ***!
  \**********************************************/
/*! exports provided: GetBookView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetBookView", function() { return GetBookView; });
var GetBookView = /** @class */ (function () {
    function GetBookView() {
    }
    return GetBookView;
}());



/***/ }),

/***/ "./src/app/models/genre/create.genre.view.ts":
/*!***************************************************!*\
  !*** ./src/app/models/genre/create.genre.view.ts ***!
  \***************************************************/
/*! exports provided: CreateGenreView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateGenreView", function() { return CreateGenreView; });
var CreateGenreView = /** @class */ (function () {
    function CreateGenreView() {
    }
    return CreateGenreView;
}());



/***/ }),

/***/ "./src/app/models/genre/get.genre.view.ts":
/*!************************************************!*\
  !*** ./src/app/models/genre/get.genre.view.ts ***!
  \************************************************/
/*! exports provided: GetAllGenreView, GenreGetGenreItemVeiw */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllGenreView", function() { return GetAllGenreView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenreGetGenreItemVeiw", function() { return GenreGetGenreItemVeiw; });
var GetAllGenreView = /** @class */ (function () {
    function GetAllGenreView() {
        this.genreList = new Array();
    }
    return GetAllGenreView;
}());

var GenreGetGenreItemVeiw = /** @class */ (function () {
    function GenreGetGenreItemVeiw() {
    }
    return GenreGetGenreItemVeiw;
}());



/***/ }),

/***/ "./src/app/models/magazine/create.magazine.view.ts":
/*!*********************************************************!*\
  !*** ./src/app/models/magazine/create.magazine.view.ts ***!
  \*********************************************************/
/*! exports provided: CreateMagazineView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateMagazineView", function() { return CreateMagazineView; });
var CreateMagazineView = /** @class */ (function () {
    function CreateMagazineView() {
    }
    return CreateMagazineView;
}());



/***/ }),

/***/ "./src/app/models/magazine/get.magazine.list.view.ts":
/*!***********************************************************!*\
  !*** ./src/app/models/magazine/get.magazine.list.view.ts ***!
  \***********************************************************/
/*! exports provided: GetAllMagazineView, MagazineGetMagazineItemView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllMagazineView", function() { return GetAllMagazineView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MagazineGetMagazineItemView", function() { return MagazineGetMagazineItemView; });
var GetAllMagazineView = /** @class */ (function () {
    function GetAllMagazineView() {
        this.magazineList = new Array();
    }
    return GetAllMagazineView;
}());

var MagazineGetMagazineItemView = /** @class */ (function () {
    function MagazineGetMagazineItemView() {
    }
    return MagazineGetMagazineItemView;
}());



/***/ }),

/***/ "./src/app/models/order/create.order.view.ts":
/*!***************************************************!*\
  !*** ./src/app/models/order/create.order.view.ts ***!
  \***************************************************/
/*! exports provided: CreateOrderView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateOrderView", function() { return CreateOrderView; });
var CreateOrderView = /** @class */ (function () {
    function CreateOrderView() {
        (this.bookIdList = new Array()),
            (this.magazineIdList = new Array());
    }
    return CreateOrderView;
}());



/***/ }),

/***/ "./src/app/models/user/cereate.users.view.ts":
/*!***************************************************!*\
  !*** ./src/app/models/user/cereate.users.view.ts ***!
  \***************************************************/
/*! exports provided: CreateUserView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateUserView", function() { return CreateUserView; });
var CreateUserView = /** @class */ (function () {
    function CreateUserView() {
        this.emailConfirmed = true;
    }
    return CreateUserView;
}());



/***/ }),

/***/ "./src/app/models/user/get.all.user.view.ts":
/*!**************************************************!*\
  !*** ./src/app/models/user/get.all.user.view.ts ***!
  \**************************************************/
/*! exports provided: GetAllUserView, UserGetAllUserItemView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetAllUserView", function() { return GetAllUserView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserGetAllUserItemView", function() { return UserGetAllUserItemView; });
var GetAllUserView = /** @class */ (function () {
    function GetAllUserView() {
        this.userList = new Array();
    }
    return GetAllUserView;
}());

var UserGetAllUserItemView = /** @class */ (function () {
    function UserGetAllUserItemView() {
    }
    return UserGetAllUserItemView;
}());



/***/ }),

/***/ "./src/app/services/authentication/login.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/authentication/login.service.ts ***!
  \**********************************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_models_authentication_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/models/authentication/user */ "./src/app/models/authentication/user.ts");
/* harmony import */ var rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/internal/operators/map */ "./node_modules/rxjs/internal/operators/map.js");
/* harmony import */ var rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");






var LoginService = /** @class */ (function () {
    function LoginService(http) {
        this.http = http;
    }
    LoginService.prototype.register = function (user) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].apiUrl + "/api/account/register", user);
    };
    LoginService.prototype.login = function (userName, password) {
        var user = new src_app_models_authentication_user__WEBPACK_IMPORTED_MODULE_3__["User"]();
        user.email = userName;
        user.password = password;
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].apiUrl + "/api/account/login", user);
    };
    LoginService.prototype.logout = function () {
        localStorage.removeItem("data");
        window.location.reload();
    };
    LoginService.prototype.refreshToken = function (model) {
        return this.http
            .post(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].apiUrl + "/api/Account/RefreshTokens", model)
            .pipe(Object(rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_4__["map"])(function (user) {
            return user;
        }));
    };
    LoginService.prototype.confirm = function (confirmcred) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].apiUrl + "/api/Account/ConfirmEmail", confirmcred);
    };
    LoginService.prototype.getAuthToken = function () {
        var currentUser = JSON.parse(localStorage.getItem("data"));
        if (currentUser != null) {
            return currentUser.accessToken;
        }
    };
    LoginService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/services/authentication/social.login.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/services/authentication/social.login.service.ts ***!
  \*****************************************************************/
/*! exports provided: SocialLoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocialLoginService", function() { return SocialLoginService; });
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");


var SocialLoginService = /** @class */ (function () {
    function SocialLoginService(http) {
        this.http = http;
    }
    SocialLoginService.prototype.SaveResponce = function (user) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiUrl + "/api/account/SocialLogin", user);
    };
    SocialLoginService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    return SocialLoginService;
}());



/***/ }),

/***/ "./src/app/services/authentication/user.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/authentication/user.service.ts ***!
  \*********************************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.service */ "./src/app/services/authentication/login.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");







var UserService = /** @class */ (function () {
    function UserService(loginService, http, router) {
        this.loginService = loginService;
        this.http = http;
        this.router = router;
        this.isLoggedIn = false;
        this.isAdmin = false;
        this.isLoggedIn = !!localStorage.getItem("data");
        this.getIsAdminFromLocalStorage();
    }
    UserService.prototype.logout = function () {
        this.loginService.logout();
        this.isLoggedIn = false;
    };
    UserService.prototype.getIsLoggedIn = function () {
        return this.isLoggedIn;
    };
    UserService.prototype.getIsAdmin = function () {
        return this.isAdmin;
    };
    UserService.prototype.changeRole = function (role) {
        this.isAdmin = role;
    };
    UserService.prototype.getUserRole = function () {
        var token = jwt_decode__WEBPACK_IMPORTED_MODULE_4__(localStorage.getItem("data"));
        var role = token["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
        return role;
    };
    UserService.prototype.getIsAdminFromLocalStorage = function () {
        var userData = localStorage.getItem("data");
        if (userData === null) {
            return this.router.navigate(["/login"]);
        }
        var decodedToken = jwt_decode__WEBPACK_IMPORTED_MODULE_4__(userData);
        if (decodedToken) {
            var role = decodedToken["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
            if (role === "admin") {
                this.changeRole(true);
            }
        }
    };
    UserService.prototype.getAllUsersRole = function () {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiUrl + "/api/user/getusersrole");
    };
    UserService.prototype.CreateUser = function (user) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiUrl + "/api/user/createuser", user);
    };
    UserService.prototype.DeleteUser = function (id) {
        return this.http.delete("/api/user/deleteuser" + id);
    };
    UserService.prototype.getUsers = function () {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiUrl + "/api/user/getusers");
    };
    UserService.ctorParameters = function () { return [
        { type: _login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/services/author.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/author.service.ts ***!
  \********************************************/
/*! exports provided: AuthorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorService", function() { return AuthorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");




var AuthorService = /** @class */ (function () {
    function AuthorService(http) {
        this.http = http;
    }
    AuthorService.prototype.get = function () {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl + "/api/author/getall");
    };
    AuthorService.prototype.create = function (item) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl + "/api/author/create", item);
    };
    AuthorService.prototype.update = function (item) {
        return this.http.put(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl + "/api/author/update", item);
    };
    AuthorService.prototype.delete = function (id) {
        return this.http.delete(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl + "/api/author/delete/" + id);
    };
    AuthorService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    AuthorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], AuthorService);
    return AuthorService;
}());



/***/ }),

/***/ "./src/app/services/book.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/book.service.ts ***!
  \******************************************/
/*! exports provided: BookService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookService", function() { return BookService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");


var BookService = /** @class */ (function () {
    function BookService(http) {
        this.http = http;
    }
    BookService.prototype.getAll = function () {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/book/getall");
    };
    BookService.prototype.getBookByid = function (id) {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/book/GetById" + id);
    };
    BookService.prototype.create = function (item) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/book/create", item);
    };
    BookService.prototype.delete = function (id) {
        return this.http.delete(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/book/detele/" + id);
    };
    BookService.prototype.update = function (item) {
        return this.http.put(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/book/update", item);
    };
    BookService.prototype.uploadImage = function (data) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/book/UploadImage", data);
    };
    BookService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }
    ]; };
    return BookService;
}());



/***/ }),

/***/ "./src/app/services/counte.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/counte.service.ts ***!
  \********************************************/
/*! exports provided: CounterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CounterService", function() { return CounterService; });
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");


var CounterService = /** @class */ (function () {
    function CounterService(dataService) {
        this.dataService = dataService;
        this.orderCounter = 0;
        this.basketIsEmpty = false;
        this.amountSource = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](0);
    }
    CounterService.prototype.itemCounter = function () {
        this.orderCounter = 0;
        var existingOrder = JSON.parse(localStorage.getItem("item1"));
        if (existingOrder == null) {
            this.basketIsEmpty = true;
        }
        else {
            for (var i = 0; i < existingOrder.length; i++) {
                if (existingOrder[i] != null) {
                    this.orderCounter = this.orderCounter + 1;
                    this.changeAmount(this.orderCounter);
                }
            }
        }
    };
    CounterService.prototype.changeAmount = function (orderCounter) {
        this.amountSource.next(orderCounter);
    };
    CounterService.prototype.getAmount = function () {
        return this.amountSource.getValue();
    };
    CounterService.ctorParameters = function () { return [
        { type: _data_service__WEBPACK_IMPORTED_MODULE_0__["DataService"] }
    ]; };
    return CounterService;
}());



/***/ }),

/***/ "./src/app/services/data.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/data.service.ts ***!
  \******************************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");

var DataService = /** @class */ (function () {
    function DataService() {
        this.amountSource = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"](0);
    }
    DataService.prototype.changeAmount = function (orderTotalPrice) {
        this.amountSource.next(orderTotalPrice);
    };
    DataService.prototype.getAmount = function () {
        return this.amountSource.getValue();
    };
    return DataService;
}());



/***/ }),

/***/ "./src/app/services/genre.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/genre.service.ts ***!
  \*******************************************/
/*! exports provided: GenreService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenreService", function() { return GenreService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");


var GenreService = /** @class */ (function () {
    function GenreService(http) {
        this.http = http;
    }
    GenreService.prototype.get = function () {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/genre/getall");
    };
    GenreService.prototype.create = function (item) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/genre/create", item);
    };
    GenreService.prototype.delete = function (id) {
        return this.http.delete(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/genre/delete/" + id);
    };
    GenreService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }
    ]; };
    return GenreService;
}());



/***/ }),

/***/ "./src/app/services/magazine.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/magazine.service.ts ***!
  \**********************************************/
/*! exports provided: MagazineService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MagazineService", function() { return MagazineService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");


var MagazineService = /** @class */ (function () {
    function MagazineService(http) {
        this.http = http;
    }
    MagazineService.prototype.getAllMagazine = function () {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/magazine/getall");
    };
    MagazineService.prototype.create = function (item) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/magazine/create", item);
    };
    MagazineService.prototype.update = function (item) {
        return this.http.put(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/magazine/update", item);
    };
    MagazineService.prototype.delete = function (id) {
        return this.http.delete(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/magazine/delete/" + id);
    };
    MagazineService.prototype.uploadImage = function (data) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/magazine/UploadImage", data);
    };
    MagazineService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }
    ]; };
    return MagazineService;
}());



/***/ }),

/***/ "./src/app/services/order.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/order.service.ts ***!
  \*******************************************/
/*! exports provided: OrderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderService", function() { return OrderService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");


var OrderService = /** @class */ (function () {
    function OrderService(http) {
        this.http = http;
    }
    OrderService.prototype.charge = function (item) {
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/order/charge", item);
    };
    OrderService.prototype.getAllOrders = function () {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + "/api/order/getallorders");
    };
    OrderService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }
    ]; };
    return OrderService;
}());



/***/ }),

/***/ "./src/app/shared/helpers/auth-guard.ts":
/*!**********************************************!*\
  !*** ./src/app/shared/helpers/auth-guard.ts ***!
  \**********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/authentication/user.service */ "./src/app/services/authentication/user.service.ts");




var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, userService) {
        this.router = router;
        this.userService = userService;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        var userRole = this.userService.getUserRole();
        if (userRole) {
            if (userRole !== "admin") {
                this.router.navigate(["/"]);
                return false;
            }
            return true;
        }
        this.router.navigate(["/login"], { queryParams: { returnUrl: state.url } });
        return false;
    };
    AuthGuard.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: src_app_services_authentication_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] }
    ]; };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: "root" })
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/shared/helpers/jwt.intersepor.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/helpers/jwt.intersepor.ts ***!
  \**************************************************/
/*! exports provided: tokenGetter, Interceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tokenGetter", function() { return tokenGetter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Interceptor", function() { return Interceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services_authentication_login_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/authentication/login.service */ "./src/app/services/authentication/login.service.ts");
/* harmony import */ var _models_authentication_refresh_token_view__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../models/authentication/refresh.token.view */ "./src/app/models/authentication/refresh.token.view.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








function tokenGetter() {
    var currentUser = JSON.parse(localStorage.getItem("data"));
    if (currentUser == null)
        return false;
    return currentUser.token;
}
var Interceptor = /** @class */ (function () {
    function Interceptor(loginService, router) {
        this.loginService = loginService;
        this.router = router;
        this.model = new _models_authentication_refresh_token_view__WEBPACK_IMPORTED_MODULE_6__["RefreshTokenView"]();
        this.tokenSubject = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](null);
    }
    Interceptor.prototype.settingHeader = function (req, token) {
        var newreq = req.clone({
            setHeaders: { Authorization: "Bearer " + token }
        });
        return newreq;
    };
    Interceptor.prototype.refreshToken = function (model) {
        this.loginService.refreshToken(model).subscribe(function (res) {
            localStorage.setItem("data", res.accessToken);
            localStorage.setItem("Refresh", res.refreshToken);
        });
    };
    Interceptor.prototype.intercept = function (req, next) {
        var _this = this;
        var accToken = localStorage.getItem("data");
        req = this.settingHeader(req, accToken);
        return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            if (error.status === 401) {
                window.location.reload();
                var token = jwt_decode__WEBPACK_IMPORTED_MODULE_3__(localStorage.getItem("data"));
                _this.model.Email = token["sub"];
                _this.model.Role =
                    token["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
                _this.refreshToken(_this.model);
                return _this.loginService.refreshToken(_this.model).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (res) {
                    if (res) {
                        _this.tokenSubject.next(res.accessToken);
                        return next.handle(_this.settingHeader(req, res.accessToken));
                    }
                }));
            }
        }));
    };
    Interceptor.ctorParameters = function () { return [
        { type: _services_authentication_login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] }
    ]; };
    Interceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], Interceptor);
    return Interceptor;
}());



/***/ }),

/***/ "./src/app/shared/order.item.view.ts":
/*!*******************************************!*\
  !*** ./src/app/shared/order.item.view.ts ***!
  \*******************************************/
/*! exports provided: OrderItem, Item */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderItem", function() { return OrderItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return Item; });
var OrderItem = /** @class */ (function () {
    function OrderItem() {
    }
    return OrderItem;
}());

var Item;
(function (Item) {
    Item[Item["Book"] = 0] = "Book";
    Item[Item["Magazine"] = 1] = "Magazine";
})(Item || (Item = {}));


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiUrl: 'http://localhost:56227'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Anuitex-112\source\repos\BookStoreNew\BookStore.Web\ClientApp\my-app\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map