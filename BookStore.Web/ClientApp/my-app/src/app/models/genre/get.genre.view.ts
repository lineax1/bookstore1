export class GetAllGenreView {
  constructor() {
    this.genreList = new Array<GenreGetGenreItemVeiw>();
  }
  
  public genreList: Array<GenreGetGenreItemVeiw>;
}
export class GenreGetGenreItemVeiw {
  public title: string;
  public id: number;
}
