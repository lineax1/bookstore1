export class CreateMagazineView{
   public id: number;
   public title: string;
   public price: number; 
   public description: string;
   public picture: string;
}