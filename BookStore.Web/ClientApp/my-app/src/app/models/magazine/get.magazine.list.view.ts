export class GetAllMagazineView {
  constructor() {
    this.magazineList = new Array<MagazineGetMagazineItemView>();
  }
  public magazineList: Array<MagazineGetMagazineItemView>;
}
export class MagazineGetMagazineItemView {
  public id: number;
  public title: string;
  public price: number;
  public picture: string;
  public description: string;
}
