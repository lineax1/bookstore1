export class UpdateMagazineView{
   public id:number;
   public title: string;
   public price: number;
   public Picture: string;
   public description: string;
}