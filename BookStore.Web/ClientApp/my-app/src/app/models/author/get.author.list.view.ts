export class GetAuthorView {
  constructor() {
    this.authorList = new Array<AuthorGetAuthorItemView>();
  }
  
  public authorList: Array<AuthorGetAuthorItemView>;
}
export class AuthorGetAuthorItemView {
  public id: number;
  public name: string;
}
