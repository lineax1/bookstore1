export class CreateOrderView {
  public email: string;
  public userId: string;
  public bookIdList?: Array<number>;
  public magazineIdList?: Array<number>;
  public CardCvc: number;
  public CardNumber: number;
  public CardExpMonth: number;
  public CardExpYear: number;

  constructor() {
    (this.bookIdList = new Array<number>()),
      (this.magazineIdList = new Array<number>());
  }
}
