export class GetAllOrderView {
  constructor() {
    this.orders = new Array<OrderGetAllOrderItemView>();
  }
  public orders: Array<OrderGetAllOrderItemView>;
}
export class OrderGetAllOrderItemView {
  public id: number;
  public userId: string;
  public email: string;
  public amount?: number;
  public books?: Array<GetOrdersBookItemView>;
  public magazines?: Array<GetOrdersMagazineItemBookView>;
}
export class GetAllOrderBookView {
  constructor() {
    this.books = new Array<GetAllOrderBookView>();
  }
  public books: Array<GetAllOrderBookView>;
}

export class GetAllOrderMagazineView {
  constructor() {
    this.magazines = new Array<GetOrdersMagazineItemBookView>();
  }
  public magazines: Array<GetOrdersMagazineItemBookView>;
}

export class GetOrdersBookItemView {
  public id: number;
  public title: string;
  public price: number;
}

export class GetOrdersMagazineItemBookView {
  public id: number;
  public title: string;
  public price: number;
}
