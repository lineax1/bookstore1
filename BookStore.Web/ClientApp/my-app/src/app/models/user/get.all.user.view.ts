export class GetAllUserView {
  constructor() {
    this.userList = new Array<UserGetAllUserItemView>();
  }
  public userList: Array<UserGetAllUserItemView>;
}
export class UserGetAllUserItemView {
  public id : string;
  public email?: string;
  public lastName?: string;
  public firstName?: string;
  public phoneNumber?: string;
  public userName: string;
  public role: string;
}
