export class CreateUserView {
  public email: string;
  public password: string;
  public role: string;
  public emailConfirmed: boolean = true;
}
