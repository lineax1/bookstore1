import { AuthorGetAuthorItemView } from "../author/get.author.list.view";
import { GenreGetGenreItemVeiw } from "../genre/get.genre.view";

export class GetBookView {
  public id: number;
  public title: string;
  public price: number;
  public picture: string;
  public authorList?: AuthorGetAuthorItemView;
  public genreList?: GenreGetGenreItemVeiw;
  public description: string;
}
