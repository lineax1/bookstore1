export class UpdateBookView {
  public id: number;
  public title: string;
  public price: number;
  public picture: string;
  public description: string;
}
