import { AuthorGetAuthorItemView } from "../author/get.author.list.view";

export class GetBookById {
  public id: number;
  public title: string;
  public price: number;
  public picture: string;
  public authorList?: AuthorGetAuthorItemView;
  public description: string;
}
