import { GetBookView } from "./get.book.view";

export class GetBookListView {
  books: Array<GetBookView>;
}
