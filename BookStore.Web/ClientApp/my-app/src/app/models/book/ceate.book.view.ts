export class CreateBookView {
  public id?: number;
  public title: string;
  public price: number;
  public description: string;
  public authorIdList?: Array<number>;
  public genreIdList?: Array<number>;
  public picture: string;
  constructor() {
    this.authorIdList = [];
    this.genreIdList = [];
  }
}
