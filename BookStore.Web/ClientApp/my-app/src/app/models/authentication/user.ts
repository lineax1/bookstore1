export class User
{
    public email: string;
    public password: string;
    public lastName?: string;
    public firstName?: string;
    public phoneNumber?: string;
}