import { OrderItem } from "../shared/order.item.view";
import { DataService } from "./data.service";
import { BehaviorSubject, Observable, Subject } from "rxjs";

export class CounterService {
  public static OrderItemSubject = new Subject<OrderItem>();

  public orderCounter: number = 0;
  public basketIsEmpty: boolean = false;
  public amountSource = new BehaviorSubject<number>(0);

  public existingOrderArray = Array<OrderItem>();
  constructor(public dataService: DataService) {
    CounterService.OrderItemSubject.subscribe(resp => {
      this.existingOrderArray.push(resp);
    });
  }

  public itemCounter() {
    this.orderCounter = 0;
    let existingOrder: Array<OrderItem> = JSON.parse(
      localStorage.getItem("item1")
    );

    if (existingOrder == null) {
      this.basketIsEmpty = true;
    } else {
      for (var i = 0; i < existingOrder.length; i++) {
        if (existingOrder[i] != null) {
          this.orderCounter = this.orderCounter + 1;
          this.changeAmount(this.orderCounter);
        }
      }
    }
  }

  public changeAmount(orderCounter: number) {
    this.amountSource.next(orderCounter);
  }

  public getAmount(): number {
    return this.amountSource.getValue();
  }
}
