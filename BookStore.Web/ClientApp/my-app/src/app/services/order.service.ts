import { HttpClient } from "@angular/common/http";
import { CreateOrderView } from "../models/order/create.order.view";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { GetAllOrderView } from "../models/order/get.all.order.view";

export class OrderService {
  constructor(private http: HttpClient) {}
  
  public charge(item: CreateOrderView): Observable<CreateOrderView> {
    return this.http.post<CreateOrderView>(
      environment.apiUrl + "/api/order/charge",
      item
    );
  }

  public getAllOrders(): Observable<GetAllOrderView> {
    return this.http.get<GetAllOrderView>(
      environment.apiUrl + "/api/order/getallorders"
    );
  }
}
