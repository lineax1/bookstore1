import { Injectable } from "@angular/core";
import { LoginService } from "./login.service";
import { HttpClient } from "@angular/common/http";
import * as jwt_decode from "jwt-decode";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { GetAllUserView } from "src/app/models/user/get.all.user.view";
import { environment } from "src/environments/environment";
import { CreateUserView } from "src/app/models/user/cereate.users.view";

@Injectable()
export class UserService {
  private isLoggedIn: boolean = false;
  private isAdmin: boolean = false;

  constructor(
    private loginService: LoginService,
    private http: HttpClient,
    private router: Router
  ) {
    this.isLoggedIn = !!localStorage.getItem("data");
    this.getIsAdminFromLocalStorage();
  }

  public logout() {
    this.loginService.logout();
    this.isLoggedIn = false;
  }

  public getIsLoggedIn() {
    return this.isLoggedIn;
  }

  public getIsAdmin() {
    return this.isAdmin;
  }

  public changeRole(role: boolean) {
    this.isAdmin = role;
  }

  public getUserRole(): string {
    const token = jwt_decode(localStorage.getItem("data"));
    const role =
      token["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
    return role;
  }

  public getIsAdminFromLocalStorage() {
    let userData = localStorage.getItem("data");
    if (userData === null) {
      return this.router.navigate(["/login"]);
    }
    let decodedToken = jwt_decode(userData);
    if (decodedToken) {
      let role =
        decodedToken[
          "http://schemas.microsoft.com/ws/2008/06/identity/claims/role"
        ];
      if (role === "admin") {
        this.changeRole(true);
      }
    }
  }

  public getAllUsersRole(): Observable<GetAllUserView> {
    return this.http.get<GetAllUserView>(
      environment.apiUrl + "/api/user/getusersrole"
    );
  }

  public CreateUser(user: CreateUserView): Observable<CreateUserView> {
    return this.http.post<CreateUserView>(
      environment.apiUrl + "/api/user/createuser",
      user
    );
  }

  public DeleteUser(id: string): Observable<any> {
    return this.http.delete("/api/user/deleteuser" + id);
  }

  public getUsers(): Observable<GetAllUserView> {
    return this.http.get<GetAllUserView>(
      environment.apiUrl + "/api/user/getusers"
    );
  }
}
