import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { SocialLoginView } from "src/app/models/authentication/social.login.view";
import { Observable } from "rxjs";

export class SocialLoginService {
  constructor(private http: HttpClient) {}

  public SaveResponce(user: SocialLoginView): Observable<any> {
    return this.http.post(
      environment.apiUrl + "/api/account/SocialLogin",
      user
    );
  }
}
