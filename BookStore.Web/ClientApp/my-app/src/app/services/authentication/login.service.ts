import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserRegistration } from "src/app/models/authentication/user.registration";
import { User } from "src/app/models/authentication/user";
import { map } from "rxjs/internal/operators/map";
import { RefreshTokenView } from "src/app/models/authentication/refresh.token.view";
import { Token } from "src/app/models/authentication/token";
import { environment } from "src/environments/environment";
import { ConfirmRegisterView } from "src/app/models/authentication/confirm.register.view";

@Injectable()
export class LoginService {
  constructor(private http: HttpClient) {}

  public register(user: UserRegistration): Observable<any> {
    return this.http.post(environment.apiUrl + "/api/account/register", user);
  }

  public login(userName: string, password: string): Observable<any> {
    let user = new User();
    user.email = userName;
    user.password = password;
    return this.http.post(environment.apiUrl + "/api/account/login", user);
  }

  public logout() {
    localStorage.removeItem("data");
    window.location.reload();
  }

  refreshToken(model: RefreshTokenView): Observable<Token> {
    return this.http
      .post<Token>(environment.apiUrl + "/api/Account/RefreshTokens", model)
      .pipe(
        map(user => {
          return user;
        })
      );
  }

  public confirm(confirmcred: ConfirmRegisterView): Observable<any> {
    return this.http.post<ConfirmRegisterView>(
      environment.apiUrl + "/api/Account/ConfirmEmail",
      confirmcred
    );
  }

  getAuthToken(): string {
    let currentUser = JSON.parse(localStorage.getItem("data"));
    if (currentUser != null) {
      return currentUser.accessToken;
    }
  }
}
