import { BehaviorSubject } from "rxjs";

export class DataService {
  public amountSource = new BehaviorSubject<number>(0);

  constructor() {}

  public changeAmount(orderTotalPrice: number) {
    this.amountSource.next(orderTotalPrice);
  }

  public getAmount(): number {
    return this.amountSource.getValue();
  }
}
