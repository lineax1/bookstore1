import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { CreateMagazineView } from "../models/magazine/create.magazine.view";
import { GetAllMagazineView } from "../models/magazine/get.magazine.list.view";
import { UpdateMagazineView } from "../models/magazine/update.magazine.view";
import { DeleteMagazineView } from "../models/magazine/delete.magazine.view";

export class MagazineService {
  constructor(private http: HttpClient) {}

  public getAllMagazine(): Observable<GetAllMagazineView> {
    return this.http.get<GetAllMagazineView>(
      environment.apiUrl + "/api/magazine/getall"
    );
  }

  public create(item: CreateMagazineView): Observable<CreateMagazineView> {
    return this.http.post<CreateMagazineView>(
      environment.apiUrl + "/api/magazine/create",
      item
    );
  }

  public update(item: UpdateMagazineView): Observable<UpdateMagazineView> {
    return this.http.put<UpdateMagazineView>(
      environment.apiUrl + "/api/magazine/update",
      item
    );
  }

  public delete(id?: number): Observable<DeleteMagazineView> {
    return this.http.delete<DeleteMagazineView>(
      environment.apiUrl + "/api/magazine/delete/" + id
    );
  }

  public uploadImage(data: FormData): Observable<any> {
    return this.http.post(
      environment.apiUrl + "/api/magazine/UploadImage",
      data
    );
  }
}
