import { HttpClient } from "@angular/common/http";

import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { GetBookById } from "../models/book/get.book.by.id";
import { CreateBookView } from "../models/book/ceate.book.view";
import { GetBookListView } from "../models/book/get.book.list.view";
import { DeleteBookView } from "../models/book/delete.book.component";
import { UpdateBookView } from "../models/book/update.book.view";

export class BookService {
  constructor(private http: HttpClient) {}

  public getAll(): Observable<GetBookListView> {
    return this.http.get<GetBookListView>(
      environment.apiUrl + "/api/book/getall"
    );
  }

  public getBookByid(id?: number): Observable<GetBookById> {
    return this.http.get<GetBookById>(
      environment.apiUrl + "/api/book/GetById" + id
    );
  }

  public create(item: CreateBookView): Observable<CreateBookView> {
    return this.http.post<CreateBookView>(
      environment.apiUrl + "/api/book/create",
      item
    );
  }

  public delete(id?: number): Observable<DeleteBookView> {
    return this.http.delete<DeleteBookView>(
      environment.apiUrl + "/api/book/detele/" + id
    );
  }

  public update(item: UpdateBookView): Observable<UpdateBookView> {
    return this.http.put<UpdateBookView>(
      environment.apiUrl + "/api/book/update",
      item
    );
  }

  public uploadImage(data: FormData): Observable<any> {
    return this.http.post(environment.apiUrl + "/api/book/UploadImage", data);
  }
}
