import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { GetAllGenreView } from "../models/genre/get.genre.view";
import { CreateGenreView } from "../models/genre/create.genre.view";
import { DeleteGenreView } from "../models/genre/delete.genre.view";
import { environment } from "src/environments/environment";

export class GenreService {
  constructor(private http: HttpClient) {}

  public get(): Observable<GetAllGenreView> {
    return this.http.get<GetAllGenreView>(
      environment.apiUrl + "/api/genre/getall"
    );
  }

  public create(item: CreateGenreView): Observable<CreateGenreView> {
    return this.http.post<CreateGenreView>(
      environment.apiUrl + "/api/genre/create",
      item
    );
  }

  public delete(id?: number): Observable<DeleteGenreView> {
    return this.http.delete<DeleteGenreView>(
      environment.apiUrl + "/api/genre/delete/" + id
    );
  }
}
