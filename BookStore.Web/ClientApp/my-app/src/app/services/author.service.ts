import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { GetAuthorView } from "../models/author/get.author.list.view";
import { CreateAuthorView } from "../models/author/create.author.view";
import { Observable } from "rxjs";
import { UpdateAuthorView } from "../models/author/update.author.view";
import { DeleteAuthorView } from "../models/author/delete.author.view";
import { environment } from "src/environments/environment";
@Injectable()
export class AuthorService {
  constructor(private http: HttpClient) {}

  public get(): Observable<GetAuthorView> {
    return this.http.get<GetAuthorView>(
      environment.apiUrl + "/api/author/getall"
    );
  }

  public create(item: CreateAuthorView): Observable<CreateAuthorView> {
    return this.http.post<CreateAuthorView>(
      environment.apiUrl + "/api/author/create",
      item
    );
  }

  public update(item: UpdateAuthorView): Observable<UpdateAuthorView> {
    return this.http.put<UpdateAuthorView>(
      environment.apiUrl + "/api/author/update",
      item
    );
  }

  public delete(id?: number): Observable<DeleteAuthorView> {
    return this.http.delete<DeleteAuthorView>(
      environment.apiUrl + "/api/author/delete/" + id
    );
  }
}
