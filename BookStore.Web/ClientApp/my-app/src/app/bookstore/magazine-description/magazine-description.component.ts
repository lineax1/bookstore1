import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MagazineService } from "src/app/services/magazine.service";
import { ToastrService } from "ngx-toastr";
import {
  MagazineGetMagazineItemView,
  GetAllMagazineView
} from "src/app/models/magazine/get.magazine.list.view";
import { OrderItem, Item } from "src/app/shared/order.item.view";

@Component({
  selector: "magazine-description",
  templateUrl: "./magazine-description.component.html",
  styleUrls: ["./magazine-description.component.css"]
})
export class MagazineDescriptionComponent implements OnInit {
  public id: number;
  public magazineDescription: MagazineGetMagazineItemView;
  public magazineList: GetAllMagazineView;

  constructor(
    public magazineService: MagazineService,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.magazineList = new GetAllMagazineView();
    this.magazineDescription = new MagazineGetMagazineItemView();
  }
  ngOnInit() {
    this.getMagazines();
  }
  public getMagazines() {
    this.magazineService.getAllMagazine().subscribe(datamagazine => {
      this.magazineList = datamagazine;
      this.getMagazineById(this.data);
      this.magazineDescription = this.getMagazineById(this.data.id);
    });
  }
  public getMagazineById(id: number) {
    for (var i = 0; i < this.magazineList.magazineList.length; i++) {
      if (this.magazineList.magazineList[i].id == id) {
        return this.magazineList.magazineList[i];
      }
    }
  }
  public onButtonClick(id: number) {
    let orderItem = new OrderItem();
    orderItem.id = id;
    orderItem.item = Item.Book;
    let key = "item1";
    let existingOrder: Array<OrderItem> = JSON.parse(localStorage.getItem(key));
    if (existingOrder === null) {
      existingOrder = new Array<OrderItem>();
    }
    existingOrder.push(orderItem);
    localStorage.setItem(key, JSON.stringify(existingOrder));
    this.toastr.success("Book Add To Order");
  }
}
