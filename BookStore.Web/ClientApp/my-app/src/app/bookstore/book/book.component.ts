import { Observable } from "rxjs";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { BookService } from "src/app/services/book.service";
import { AuthorService } from "src/app/services/author.service";
import {
  GetAuthorView,
  AuthorGetAuthorItemView
} from "src/app/models/author/get.author.list.view";
import { Component, OnInit } from "@angular/core";
import { LocalStorage } from "@ngx-pwa/local-storage";
import {
  GridDataResult,
  DataStateChangeEvent
} from "@progress/kendo-angular-grid";
import { State, process } from "@progress/kendo-data-query";
import {
  GenreGetGenreItemVeiw,
  GetAllGenreView
} from "src/app/models/genre/get.genre.view";
import { ToastrService } from "ngx-toastr";
import { GetBookListView } from "src/app/models/book/get.book.list.view";
import { UpdateBookView } from "src/app/models/book/update.book.view";
import { CreateBookView } from "src/app/models/book/ceate.book.view";
import { BookDescriptionComponent } from "../book-description/book-description.component";
import { UserService } from "src/app/services/authentication/user.service";
import { MatDialog } from "@angular/material";
import { OrderBasketComponent } from "src/app/menu/order-basket/order-basket.component";
import { OrderItem, Item } from "src/app/shared/order.item.view";
import { CounterService } from "src/app/services/counte.service";

@Component({
  selector: "book",
  templateUrl: "./book.component.html",
  styleUrls: ["./book.component.css"]
})
export class BookComponent implements OnInit {
  public formGroup: FormGroup;
  public author: AuthorGetAuthorItemView;
  private editedRowIndex: number;
  public genre: GenreGetGenreItemVeiw;
  public genreList: GetAllGenreView;
  public books: GetBookListView;
  public id: number;
  public isAdminPageOpen = false;
  public isOpen: boolean = false;
  public isClose: boolean = true;
  public authorList: GetAuthorView;
  public admin: boolean;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };
  public gridData: GridDataResult;
  public length: Observable<number>;

  constructor(
    public localStorage: LocalStorage,
    private dialog: MatDialog,
    private authorService: AuthorService,
    private bookService: BookService,
    private userService: UserService,
    private toastr: ToastrService
  ) {
    this.books = new GetBookListView();
    this.authorList = new GetAuthorView();
    this.genreList = new GetAllGenreView();
  }

  ngOnInit(): void {
    this.loadData();
    this.authorService.get().subscribe(data => {
      this.authorList = data;
    });
    this.admin = this.userService.getIsAdmin();
  }

  public loadData() {
    this.bookService.getAll().subscribe(data => {
      this.books = data;
      this.gridData = process(this.books.books, this.gridState);
    });
  }

  public addHandler({ sender }) {
    this.closeEditor(sender);
    this.formGroup = new FormGroup({
      id: new FormControl(0),
      title: new FormControl("", Validators.required),
      price: new FormControl(0),
      picture: new FormControl(""),
      description: new FormControl(""),
      authorList: new FormControl(""),
      genreList: new FormControl("")
    });
    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      id: new FormControl(dataItem.id),
      title: new FormControl(dataItem.title, Validators.required),
      price: new FormControl(dataItem.price),
      authorList: new FormControl(dataItem.authorList),
      genreList: new FormControl(dataItem.genreList)
    });
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({ sender, rowIndex, formGroup, isNew }) {
    if (isNew) {
      const product: CreateBookView = formGroup.value;
      this.bookService.create(product).subscribe(() => this.loadData());
    }
    if (!isNew) {
      const product: UpdateBookView = formGroup.value;
      this.bookService.update(product).subscribe(() => this.loadData());
    }
    sender.closeRow(rowIndex);
  }

  public removeHandler({ dataItem }) {
    this.bookService.delete(dataItem.id).subscribe(() => this.loadData());
  }

  private closeEditor(
    grid: { closeRow: (arg0: number) => void },
    rowIndex = this.editedRowIndex
  ) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public Logout() {
    localStorage.removeItem("data");
    window.location.reload();
  }

  public authorChange(value: any) {
    this.formGroup.get("author").setValue(value);
  }

  public genreChange(value: any) {
    this.formGroup.get("genre").setValue(value);
  }

  public getAuthorList(book: { authorList: { name: string }[] }) {
    let authorString = "";
    for (let i = 0; i < book.authorList.length; i++) {
      authorString += book.authorList[i].name + ",";
    }
    return authorString;
  }

  public getGenreList(book: { genreList: { title: string }[] }) {
    let genreString = "";
    for (let i = 0; i < book.genreList.length; i++) {
      genreString += book.genreList[i].title + " ";
    }
    return genreString;
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridState = state;
    this.gridData = process(this.books.books, this.gridState);
  }

  public onButtonClick(id: number, price: number) {
    let orderItem = new OrderItem();
    orderItem.id = id;
    orderItem.item = Item.Book;
    orderItem.pirce = price;
    let key = "item1";
    let existingOrder: Array<OrderItem> = JSON.parse(localStorage.getItem(key));
    if (existingOrder === null) {
      existingOrder = new Array<OrderItem>();
    }
    existingOrder.push(orderItem);
    localStorage.setItem(key, JSON.stringify(existingOrder));
    CounterService.OrderItemSubject.next(orderItem);
    this.toastr.success("Book Add To Order");
  }

  openDialog() {
    const dialogRef = this.dialog.open(OrderBasketComponent);
    {
    }
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openDialogCreate() {
    const dialogRef = this.dialog.open(OrderBasketComponent);
    {
    }
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openDescription(id: number) {
    if (this.isOpen === false) {
      const dialogRef = this.dialog.open(BookDescriptionComponent, {
        data: { id }
      });
      {
      }
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
      });
    }
  }

  openAdminPage() {
    if (this.isAdminPageOpen == false) {
      this.isAdminPageOpen = true;
    }
  }
}
