import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthorComponent } from "./author/author.component";
import { BookComponent } from "./book/book.component";
import { BookDescriptionComponent } from "./book-description/book-description.component";
import { MagazineComponent } from "./magazine/magazine.component";
import { MagazineDescriptionComponent } from "./magazine-description/magazine-description.component";
import { RouterModule } from "@angular/router";
import { GridModule } from "@progress/kendo-angular-grid";
import {
  ModalModule,
  TooltipModule,
  ButtonsModule,
  CollapseModule,
  WavesModule
} from "angular-bootstrap-md";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import {
  MatDialogModule,
  MatTableModule,
  MatCardModule,
  MatNativeDateModule,
  MatMenuModule,
  MatIconModule,
  MatCheckboxModule,
  MatButtonModule,
  MatSidenavModule,
  MatListModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatSelectModule
} from "@angular/material";
import { NgSelectModule } from "@ng-select/ng-select";
import { ToastrModule } from "ngx-toastr";
import { CdkTableModule } from "@angular/cdk/table";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "../app-routing.module";
import { NgOptionHighlightModule } from "@ng-select/ng-option-highlight";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { PopupModule } from "@progress/kendo-angular-popup";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import {
  BrowserAnimationsModule,
  NoopAnimationsModule
} from "@angular/platform-browser/animations";
import { AuthenticationModule } from "../authentication/authentication.module";
import { MenuModule } from "../menu/menu.module";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: "author", component: AuthorComponent },
      { path: "book", component: BookComponent },
      { path: "magazine", component: MagazineComponent },
      { path: "magazine-description", component: MagazineDescriptionComponent },
      { path: "book=description", component: BookDescriptionComponent }
    ]),
    DropDownsModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    GridModule,
    MatDialogModule,
    NgSelectModule,
    MatTableModule,
    MatCardModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: "toast-bottom-right"
    }),
    MatNativeDateModule,
    MatMenuModule,
    CdkTableModule,
    MatIconModule,
    MatCheckboxModule,
    MatButtonModule,
    BrowserModule,
    AppRoutingModule,
    NgOptionHighlightModule,
    ButtonsModule,
    CollapseModule,
    WavesModule,
    MatSidenavModule,
    NgMultiSelectDropDownModule.forRoot(),
    PopupModule,
    MatListModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    MenuModule,
    AuthenticationModule,
    MatToolbarModule,
    FormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    NoopAnimationsModule
  ],
  declarations: [
    AuthorComponent,
    BookComponent,
    BookDescriptionComponent,
    MagazineComponent,
    MagazineDescriptionComponent
  ]
})
export class BookstoreModule {}
