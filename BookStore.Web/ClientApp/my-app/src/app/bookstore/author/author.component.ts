import { Component, OnInit } from "@angular/core";
import { CreateAuthorView } from "src/app/models/author/create.author.view";
import { AuthorService } from "src/app/services/author.service";
import { GetAuthorView } from "src/app/models/author/get.author.list.view";
@Component({
  selector: "author",
  templateUrl: "./author.component.html",
  styleUrls: ["./author.component.css"]
})
export class AuthorComponent implements OnInit {
  public toggleText: string = "Add Author";
  public name: string;
  public author: CreateAuthorView = new CreateAuthorView();

  public show: boolean = false;
  public authorList: GetAuthorView;

  constructor(public authorService: AuthorService) {
    this.authorList = new GetAuthorView();
  }

  ngOnInit() {
    this.getauthors();
  }

  public getauthors() {
    this.authorService.get().subscribe(res => {
      this.authorList = res;
    });
  }

  public onToggle(): void {
    this.show = !this.show;
    this.toggleText = this.show ? "Hide" : "Add Author";
  }

  public createAuthor() {
    this.authorService.create(this.author).subscribe(res => this.getauthors());
  }
}
