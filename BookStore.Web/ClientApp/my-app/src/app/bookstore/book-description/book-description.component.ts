import { Component, OnInit, Inject } from "@angular/core";
import { BookService } from "src/app/services/book.service";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AuthorService } from "src/app/services/author.service";
import { GetAuthorView } from "src/app/models/author/get.author.list.view";
import { ToastrService } from "ngx-toastr";
import { GetBookListView } from "src/app/models/book/get.book.list.view";
import { GetBookView } from "src/app/models/book/get.book.view";
import { OrderItem, Item } from "src/app/shared/order.item.view";

@Component({
  selector: "book-description",
  templateUrl: "./book-description.component.html",
  styleUrls: ["./book-description.component.css"]
})
export class BookDescriptionComponent implements OnInit {
  public books: GetBookListView;
  public id: number;
  public bookDescription: GetBookView;
  public authorList: GetAuthorView;

  constructor(
    public bookService: BookService,
    public authorService: AuthorService,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.bookDescription = new GetBookView();
    this.books = new GetBookListView();
    this.authorList = new GetAuthorView();
  }

  ngOnInit() {
    this.getBooks();
    this.authorService.get().subscribe(data => {
      this.authorList = data;
    });
  }

  public getBooks() {
    this.bookService.getAll().subscribe(databook => {
      this.books = databook;
      this.getBookById(this.data);
      this.bookDescription = this.getBookById(this.data.id);
    });
  }

  getAuthorList(book: { authorList: { name: string }[] }) {
    let authorString = "";
    for (let i = 0; i < book.authorList.length; i++) {
      authorString += book.authorList[i].name + " ";
    }
    return authorString;
  }

  public getBookById(id: number) {
    for (var i = 0; i < this.books.books.length; i++) {
      if (this.books.books[i].id == id) {
        return this.books.books[i];
      }
    }
  }

  public onButtonClick(id: number) {
    let orderItem = new OrderItem();
    orderItem.id = id;
    orderItem.item = Item.Book;
    let key = "item1";
    let existingOrder: Array<OrderItem> = JSON.parse(localStorage.getItem(key));
    if (existingOrder === null) {
      existingOrder = new Array<OrderItem>();
    }
    existingOrder.push(orderItem);
    localStorage.setItem(key, JSON.stringify(existingOrder));
    this.toastr.success("Book Add To Order");
  }
}
