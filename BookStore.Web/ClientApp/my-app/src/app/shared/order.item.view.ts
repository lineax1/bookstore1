export class OrderItem {
  public id: number;
  public item: Item;
  public pirce: number;
}

export enum Item {
  Book = 0,
  Magazine = 1
}
