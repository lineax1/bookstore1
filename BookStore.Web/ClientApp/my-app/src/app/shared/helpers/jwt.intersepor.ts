import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from "@angular/common/http";
import { catchError, switchMap } from "rxjs/operators";
import * as jwt_decode from "jwt-decode";
import { BehaviorSubject, Observable } from "rxjs";
import { LoginService } from "../../services/authentication/login.service";
import { RefreshTokenView } from "../../models/authentication/refresh.token.view";
import { Router } from "@angular/router";

export function tokenGetter() {
  let currentUser = JSON.parse(localStorage.getItem("data"));
  if (currentUser == null) return false;
  return currentUser.token;
}

@Injectable()
export class Interceptor implements HttpInterceptor {
  model: RefreshTokenView = new RefreshTokenView();
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private loginService: LoginService, private router: Router) {}

  settingHeader(req: HttpRequest<any>, token: string): HttpRequest<any> {
    let newreq = req.clone({
      setHeaders: { Authorization: "Bearer " + token }
    });
    return newreq;
  }

  refreshToken(model: RefreshTokenView) {
    this.loginService.refreshToken(model).subscribe(res => {
      localStorage.setItem("data", res.accessToken);
      localStorage.setItem("Refresh", res.refreshToken);
    });
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let accToken = localStorage.getItem("data");
    req = this.settingHeader(req, accToken);

    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          window.location.reload();
          var token = jwt_decode(localStorage.getItem("data"));
          this.model.Email = token["sub"];
          this.model.Role =
            token[
              "http://schemas.microsoft.com/ws/2008/06/identity/claims/role"
            ];
          this.refreshToken(this.model);

          return this.loginService.refreshToken(this.model).pipe(
            switchMap(res => {
              if (res) {
                this.tokenSubject.next(res.accessToken);

                return next.handle(this.settingHeader(req, res.accessToken));
              }
            })
          );
        }
      })
    );
  }
}
