import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { LoginService } from "src/app/services/authentication/login.service";
import { ToastrService } from "ngx-toastr";
import { UserRegistration } from "src/app/models/authentication/user.registration";

@Component({
  selector: "register",
  templateUrl: "./registration.component.html",
  styleUrls: ["./registration.component.css"]
})
export class RegistrationComponent implements OnInit {
  public isRequesting: boolean;
  public submitted: boolean = false;
  public user: UserRegistration;

  constructor(
    public router: Router,
    public loginService: LoginService,
    private toastr: ToastrService
  ) {}
  ngOnInit() {}

  public registerUser({ value, valid }) {
    if (!valid) {
      return;
    }

    this.submitted = true;
    this.isRequesting = true;
    console.log(this.submitted);
    console.log(this.isRequesting);
    this.user = value;

    this.loginService.register(this.user).subscribe(
      result => {
        console.log(result);
        this.toastr.success("Successful registration ");
      },
      errors => {
        console.log(errors);
      },

      () =>
        this.router.navigate(["/login"], {
          queryParams: { brandNew: true, email: value.email }
        })
    );
  }
}
