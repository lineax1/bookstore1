import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RegistrationCompletionComponent } from "./registration-completion/registration-completion.component";
import { RegistrationComponent } from "./registration/registration.component";
import { LoginComponent } from "./login/login.component";
import { RouterModule } from "@angular/router";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import {
  TooltipModule,
  ModalModule,
  CollapseModule,
  WavesModule
} from "angular-bootstrap-md";
import { GridModule } from "@progress/kendo-angular-grid";
import {
  MatDialogModule,
  MatTableModule,
  MatCardModule,
  MatNativeDateModule,
  MatMenuModule,
  MatIconModule,
  MatCheckboxModule,
  MatButtonModule,
  MatSidenavModule,
  MatListModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatSelectModule
} from "@angular/material";
import { NgSelectModule } from "@ng-select/ng-select";
import { ToastrModule } from "ngx-toastr";
import { NgbAlertModule, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CdkTableModule } from "@angular/cdk/table";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "../app-routing.module";
import { NgOptionHighlightModule } from "@ng-select/ng-option-highlight";
import { ButtonsModule } from "@progress/kendo-angular-buttons";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { PopupModule } from "@progress/kendo-angular-popup";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import {
  BrowserAnimationsModule,
  NoopAnimationsModule
} from "@angular/platform-browser/animations";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: "register", component: RegistrationComponent },
      { path: "login", component: LoginComponent },
      {
        path: "app-registration-completion",
        component: RegistrationCompletionComponent
      }
    ]),
    DropDownsModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    GridModule,
    MatDialogModule,
    NgSelectModule,
    MatTableModule,
    MatCardModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: "toast-bottom-right"
    }),
    MatNativeDateModule,
    MatMenuModule,
    NgbAlertModule,
    CdkTableModule,
    NgbModule,
    MatIconModule,
    MatCheckboxModule,
    MatButtonModule,
    BrowserModule,
    AppRoutingModule,
    NgOptionHighlightModule,
    ButtonsModule,
    CollapseModule,
    WavesModule,
    MatSidenavModule,
    NgMultiSelectDropDownModule.forRoot(),
    PopupModule,
    MatListModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    MatToolbarModule,
    FormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    NoopAnimationsModule
  ],
  declarations: [
    RegistrationCompletionComponent,
    RegistrationComponent,
    LoginComponent
  ],
  providers: []
})
export class AuthenticationModule {}
