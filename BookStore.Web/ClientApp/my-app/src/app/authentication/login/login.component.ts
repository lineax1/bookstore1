import { Component, OnInit } from "@angular/core";
import { LoginService } from "src/app/services/authentication/login.service";
import { Subscription } from "rxjs";
import { User } from "src/app/models/authentication/user";
import { Router, ActivatedRoute } from "@angular/router";
import * as jwt_decode from "jwt-decode";
import { SocialLoginService } from "src/app/services/authentication/social.login.service";
import { AuthService, GoogleLoginProvider } from "angular-6-social-login";
import { Token } from "src/app/models/authentication/token";
import { SocialLoginView } from "src/app/models/authentication/social.login.view";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  public subscription: Subscription;
  public errors: string;
  public brandNew: boolean;
  public isRequesting: boolean;
  public submitted: boolean = false;
  public credentials: User = { email: "", password: "" };
  public token: Token = new Token();
  public socialUserData: SocialLoginView = new SocialLoginView();

  constructor(
    private loginService: LoginService,
    private activatedRoate: ActivatedRoute,
    private toastr: ToastrService,
    private socialAuthService: AuthService,
    private socialLoginService: SocialLoginService,
    private router: Router
  ) {}

  ngOnInit() {
    this.subscription = this.activatedRoate.queryParams.subscribe(
      (param: any) => {
        this.brandNew = param["brandNew"];
        this.credentials.email = param["email"];
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  public login({ value, valid }) {
    if (!valid) {
      return;
    }
    this.submitted = true;
    this.isRequesting = true;
    this.errors = "";
    this.loginService.login(value.email, value.password).subscribe(
      result => {
        if (result) {
          localStorage.setItem("data", JSON.stringify(result));
          this.router.navigate(["/book"]);
        } else {
        }
      },
      error => {
        if (error)
          this.toastr.error(" Please enter the correct username and password ");
      }
    );
  }

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;

    if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(userData => {
      this.socialUserData.email = userData.email;

      this.socialLoginService
        .SaveResponce(this.socialUserData)
        .subscribe((res: Token) => {
          (this.token.accessToken = res.accessToken),
            (this.token.refreshToken = res.refreshToken),
            localStorage.setItem("data", this.token.accessToken);
          localStorage.setItem("Refresh", this.token.refreshToken);
          const test = jwt_decode(res.accessToken);
          console.log(test);

          if (
            this.token.accessToken != null &&
            this.token.refreshToken != null
          ) {
            this.router.navigate(["book"]);
          }
        });

      console.log("google" + " sign in data : ", userData);
    });
  }
}
