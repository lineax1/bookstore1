import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmRegisterView } from "src/app/models/authentication/confirm.register.view";
import { LoginService } from "src/app/services/authentication/login.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-registration-completion",
  templateUrl: "./registration-completion.component.html",
  styleUrls: ["./registration-completion.component.css"]
})
export class RegistrationCompletionComponent implements OnInit {
  public confirmcred: ConfirmRegisterView = new ConfirmRegisterView();

  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private loginService: LoginService,
    private toastr: ToastrService
  ) {
    this.confirmcred.email = this.activateRoute.snapshot.queryParams.email;
    this.confirmcred.code = this.activateRoute.snapshot.queryParams.token;
  }
  ngOnInit() {}

  public confirm() {
    this.loginService.confirm(this.confirmcred).subscribe();
    this.toastr.success("Registration is completed!");
    this.router.navigate(["/login"]);
  }
}
