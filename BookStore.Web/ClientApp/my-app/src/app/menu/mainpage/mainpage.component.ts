import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import * as jwt_decode from "jwt-decode";
import { UserService } from "src/app/services/authentication/user.service";
import { GetAllUserView } from "src/app/models/user/get.all.user.view";
import { RefreshTokenView } from "src/app/models/authentication/refresh.token.view";
import { OrderBasketComponent } from "../order-basket/order-basket.component";
import { Router } from "@angular/router";
import { CounterService } from "src/app/services/counte.service";
import { DataService } from "src/app/services/data.service";

@Component({
  selector: "app-mainpage",
  templateUrl: "./mainpage.component.html",
  styleUrls: ["./mainpage.component.css"]
})
export class MainpageComponent implements OnInit {
  public admin: boolean = false;
  public orderCounter: number;
  model: RefreshTokenView = new RefreshTokenView();
  public userList: GetAllUserView;
  searchText: string;

  constructor(
    private dialog: MatDialog,
    private userService: UserService,
    public dataService: DataService,
    private router: Router,
    public counterService: CounterService
  ) {}

  ngOnInit() {
    this.admin = this.userService.getIsAdmin();
    this.getAll();
    this.callCounterService();
    this.getAmount();
  }

  public getAmount() {
    this.counterService.itemCounter();
    this.orderCounter = this.counterService.getAmount();
  }

  openDialog() {
    const dialogRef = this.dialog.open(OrderBasketComponent);
    {
    }
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  public callCounterService() {
    this.counterService.itemCounter();
  }

  public Logout() {
    localStorage.removeItem("data");
    localStorage.removeItem("item1");
    localStorage.removeItem("Refresh");
    window.location.reload();
  }

  public getAll() {
    this.userService.getUsers().subscribe(res => {
      this.userList = res;
    });
    this.showItem();
  }

  public showItem() {
    var token = jwt_decode(localStorage.getItem("data"));
    this.model.Email = token["sub"];
    return this.model.Email;
  }

  public openAdminPage() {
    this.router.navigate(["/app-admin-page"]);
  }
}
