import { Component, OnInit } from "@angular/core";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { BookService } from "src/app/services/book.service";
import { MagazineService } from "src/app/services/magazine.service";
import {
  GetAllMagazineView,
  MagazineGetMagazineItemView
} from "src/app/models/magazine/get.magazine.list.view";
import { ToastrService } from "ngx-toastr";
import { GetBookView } from "src/app/models/book/get.book.view";
import { GetBookListView } from "src/app/models/book/get.book.list.view";
import { OrderItem, Item } from "src/app/shared/order.item.view";
import { Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";

@Component({
  selector: "order",
  templateUrl: "./order-basket.component.html",
  styleUrls: ["./order-basket.component.css"]
})
export class OrderBasketComponent implements OnInit {
  public books: GetBookListView;
  public itemsMagazine: Array<OrderItem>;
  public booklistOrder: Array<GetBookView>;
  public magazineList: GetAllMagazineView;
  public magazineListOrder: Array<MagazineGetMagazineItemView>;
  public bookTotalPrice: number;
  public orderCounter: number;
  public magazineTotalPrice: number;
  public basketIsEmpty: boolean = false;
  public orderTotalPrice: number;

  constructor(
    public bookService: BookService,
    public localStorage: LocalStorage,
    private dataService: DataService,
    public magazineService: MagazineService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.books = new GetBookListView();
    this.booklistOrder = new Array<GetBookView>();
    this.magazineList = new GetAllMagazineView();
    this.magazineListOrder = new Array<MagazineGetMagazineItemView>();
    this.orderTotalPrice = 0;
  }

  ngOnInit(): void {
    this.loadata();
    this.orderTotalPrice;
  }

  public loadata() {
    this.bookService.getAll().subscribe(data => {
      this.books = data;
      this.showItem();
    });

    this.magazineService.getAllMagazine().subscribe(data => {
      this.magazineList = data;
      this.showItemMagazine();
    });
  }

  public showItem() {
    let existingOrder: Array<OrderItem> = JSON.parse(
      localStorage.getItem("item1")
    );

    if (existingOrder == null) {
      this.basketIsEmpty = true;
    } else {
      for (var i = 0; i < existingOrder.length; i++) {
        if (existingOrder[i].item == Item.Book) {
          let res = this.getBookById(existingOrder[i].id);
          this.booklistOrder.push(res);
          this.orderTotalPrice = this.orderTotalPrice + res.price;
        }
      }
    }
  }

  public showItemMagazine() {
    let existingOrder: Array<OrderItem> = JSON.parse(
      localStorage.getItem("item1")
    );

    if (existingOrder == null) {
      this.basketIsEmpty = true;
    } else {
      for (var i = 0; i < existingOrder.length; i++) {
        if (existingOrder[i].item == Item.Magazine) {
          let ress = this.getMagazineById(existingOrder[i].id);
          this.magazineListOrder.push(ress);
          this.orderTotalPrice = this.orderTotalPrice + ress.price;
        }
      }
    }
  }

  public getBookById(id: number) {
    var a = this.books;
    for (var i = 0; i < this.books.books.length; i++) {
      if (this.books.books[i].id == id) {
        return this.books.books[i];
      }
    }
  }

  public getMagazineById(id: number) {
    for (var i = 0; i < this.magazineList.magazineList.length; i++) {
      if (this.magazineList.magazineList[i].id == id) {
        return this.magazineList.magazineList[i];
      }
    }
  }

  public deleteItem() {
    let key = "item1";
    localStorage.removeItem(key);
    window.location.reload();
    this.toastr.success("removed");
  }

  public openPayment() {
    this.router.navigate(["/payment"]);
    this.dataService.changeAmount(this.orderTotalPrice);
  }
}
