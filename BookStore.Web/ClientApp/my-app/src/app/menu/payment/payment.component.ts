import { Component, OnInit } from "@angular/core";
import * as jwt_decode from "jwt-decode";
import { OrderService } from "src/app/services/order.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CreateOrderView } from "src/app/models/order/create.order.view";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { MagazineService } from "src/app/services/magazine.service";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { BookService } from "src/app/services/book.service";
import { OrderItem, Item } from "src/app/shared/order.item.view";
import { BehaviorSubject } from "rxjs";
import { DataService } from "src/app/services/data.service";

@Component({
  selector: "payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.css"]
})
export class PaymentComponent implements OnInit {
  public formGroup: FormGroup;
  public orderTotalPrice: number;
  public amountSource = new BehaviorSubject<number>(0);
  constructor(
    private orderService: OrderService,
    private formBuilder: FormBuilder,
    public bookService: BookService,
    public localStorage: LocalStorage,
    public magazineService: MagazineService,
    private toastr: ToastrService,
    private router: Router,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      CardCvc: ["", Validators.required],
      CardNumber: ["", Validators.required],
      CardExpMonth: ["", Validators.required],
      CardExpYear: ["", Validators.required]
    });
    this.orderTotalPrice = this.dataService.getAmount();
  }
  get f() {
    return this.formGroup.controls;
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      return this.toastr.error("Enter all fields");
    }

    const product: CreateOrderView = new CreateOrderView();
    product.CardCvc = this.formGroup.controls.CardCvc.value;
    product.CardNumber = this.formGroup.controls.CardNumber.value;
    product.CardExpMonth = this.formGroup.controls.CardExpMonth.value;
    product.CardExpYear = this.formGroup.controls.CardExpYear.value;
    this.orderService.charge(this.getItem(product)).subscribe();
    this.toastr.success("Succes Payment");
    this.router.navigate(["/book"]);
  }

  public getItem(product) {
    var email = jwt_decode(localStorage.getItem("data"));
    product.email = email["sub"];
    let key = "item1";
    let existingOrder: Array<OrderItem> = JSON.parse(localStorage.getItem(key));

    for (var i = 0; i < existingOrder.length; i++) {
      if (existingOrder[i].item == Item.Book) {
        product.bookIdList.push(existingOrder[i].id);
      }
      if (existingOrder[i].item == Item.Magazine) {
        product.magazineIdList.push(existingOrder[i].id);
      }
    }
    return product;
  }

  public onClick(event) {
    let value = event.target.value;
    document.getElementById("spaseInput");
    let newValue = "";

    value = value.replace(/\s/g, "");

    for (var i = 0; i < value.length; i++) {
      if (i % 4 == 0 && i > 0) newValue = newValue.concat(" ");
      newValue = newValue.concat(value[i]);
    }
    event.target.value = newValue;
  }
}
