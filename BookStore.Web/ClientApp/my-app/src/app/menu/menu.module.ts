import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MainpageComponent } from "./mainpage/mainpage.component";
import { OrderBasketComponent } from "./order-basket/order-basket.component";
import { ShopDescriptionComponent } from "./shop-description/shop-description.component";
import { PaymentComponent } from "./payment/payment.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import {
  ButtonsModule,
  WavesModule,
  CollapseModule
} from "angular-bootstrap-md";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ModalModule } from "ngx-bootstrap/modal";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgbAlertModule } from "@ng-bootstrap/ng-bootstrap";
import { RouterModule } from "@angular/router";
import {
  MatFormFieldModule,
  MatSelectModule,
  MatTableModule,
  MatIconModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatDialogModule,
  MatCardModule,
  MatNativeDateModule,
  MatMenuModule
} from "@angular/material";
import { CdkTableModule } from "@angular/cdk/table";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "../app-routing.module";
import { NgOptionHighlightModule } from "@ng-select/ng-option-highlight";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { PopupModule } from "@progress/kendo-angular-popup";
import { HttpClientModule } from "@angular/common/http";
import { AuthenticationModule } from "../authentication/authentication.module";
import {
  BrowserAnimationsModule,
  NoopAnimationsModule
} from "@angular/platform-browser/animations";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: "app-mainpage", component: MainpageComponent },
      { path: "order", component: OrderBasketComponent },
      { path: "app-shop-description", component: ShopDescriptionComponent },
      { path: "payment", component: PaymentComponent }
    ]),
    MatNativeDateModule,
    MatMenuModule,
    NgbAlertModule,
    CdkTableModule,
    NgbModule,
    MatIconModule,
    BsDropdownModule,
    MatCheckboxModule,
    MatButtonModule,
    MatTableModule,
    ModalModule,
    TooltipModule,
    MatDialogModule,
    MatCardModule,
    BrowserModule,
    AppRoutingModule,
    NgOptionHighlightModule,
    ButtonsModule,
    CollapseModule,
    WavesModule,
    MatSidenavModule,
    NgMultiSelectDropDownModule.forRoot(),
    PopupModule,
    MatListModule,

    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    AuthenticationModule,
    MatToolbarModule,
    FormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    NoopAnimationsModule
  ],
  declarations: [
    MainpageComponent,
    OrderBasketComponent,
    ShopDescriptionComponent,
    PaymentComponent
  ],
  exports: [MainpageComponent]
})
export class MenuModule {}
