import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "bookstoremodule",
    loadChildren: "./bookstore/bookstore.module#BookstoreModule"
  },
  {
    path: "authenticationmodule",
    loadChildren: "./authentication/authentication.module#AuthenticationModule"
  },
  { path: "menumodule", loadChildren: "./menu/menu.module#MenuModule" },
  {
    path: "adminpanelmodule",
    loadChildren: "./admin-panel/adminpanel.module#AdminPanelModule"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
