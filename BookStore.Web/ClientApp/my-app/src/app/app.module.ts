import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgSelectModule } from "@ng-select/ng-select";
import { RouterModule } from "@angular/router";
import { CdkTableModule } from "@angular/cdk/table";
import { GridModule } from "@progress/kendo-angular-grid";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { NgOptionHighlightModule } from "@ng-select/ng-option-highlight";
import { ToastrModule } from "ngx-toastr";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import { MatNativeDateModule } from "@angular/material/core";
import { MatMenuModule } from "@angular/material/menu";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import {
  ButtonsModule,
  WavesModule,
  CollapseModule
} from "angular-bootstrap-md";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ModalModule } from "ngx-bootstrap/modal";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { CookieService } from "ngx-cookie-service";
import { CommonModule } from "@angular/common";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgbAlertModule } from "@ng-bootstrap/ng-bootstrap";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PopupModule } from "@progress/kendo-angular-popup";
import { AuthorService } from "./services/author.service";
import { UserService } from "./services/authentication/user.service";
import { BookService } from "./services/book.service";
import { LoginService } from "./services/authentication/login.service";

import {
  MatFormFieldModule,
  MatSelectModule,
  MatTableModule,
  MatIconModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatDialogModule,
  MatCardModule
} from "@angular/material";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { MagazineService } from "./services/magazine.service";
import { OrderService } from "./services/order.service";
import { Interceptor } from "./shared/helpers/jwt.intersepor";
import { GenreService } from "./services/genre.service";
import { AuthenticationModule } from "./authentication/authentication.module";
import { BookstoreModule } from "./bookstore/bookstore.module";
import { AdminPanelModule } from "./admin-panel/adminpanel.module";
import {
  AuthServiceConfig,
  SocialLoginModule,
  GoogleLoginProvider,
  AuthService
} from "angular-6-social-login";
import { SocialLoginService } from "./services/authentication/social.login.service";
import { DataService } from "./services/data.service";
import { CounterService } from "./services/counte.service";

export function socialConfigs() {
  const config = new AuthServiceConfig([
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(
        "411577703476-2rprflggjnjdvbsunu1k3jt2d341evc4.apps.googleusercontent.com"
      )
    }
  ]);
  return config;
}
@NgModule({
  declarations: [AppComponent],
  imports: [
    DropDownsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    GridModule,
    MatDialogModule,
    NgSelectModule,
    MatTableModule,
    MatCardModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: "toast-bottom-right"
    }),
    MatNativeDateModule,
    MatMenuModule,
    NgbAlertModule,
    CdkTableModule,
    NgbModule,
    MatIconModule,
    MatCheckboxModule,
    MatButtonModule,
    BrowserModule,
    BookstoreModule,
    AdminPanelModule,
    AppRoutingModule,
    NgOptionHighlightModule,
    ButtonsModule,
    CollapseModule,
    SocialLoginModule,
    WavesModule,
    MatSidenavModule,
    NgMultiSelectDropDownModule.forRoot(),
    PopupModule,
    MatListModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    AuthenticationModule,
    MatToolbarModule,
    FormsModule,
    RouterModule.forRoot(
      [
        { path: "", redirectTo: "book", pathMatch: "full" },
        { path: "**", redirectTo: "/" }
      ],
      { useHash: true }
    ),
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    NoopAnimationsModule
  ],

  exports: [MatTableModule],
  providers: [
    AuthService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
    {
      provide: AuthServiceConfig,
      useFactory: socialConfigs
    },
    AuthorService,
    LoginService,
    CounterService,
    UserService,
    BookService,
    MagazineService,
    DataService,
    CookieService,
    SocialLoginService,
    GenreService,
    OrderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
