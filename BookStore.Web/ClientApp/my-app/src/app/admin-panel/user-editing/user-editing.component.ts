import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { State, process } from "@progress/kendo-data-query";
import { UserService } from "src/app/services/authentication/user.service";
import {
  GridDataResult,
  DataStateChangeEvent
} from "@progress/kendo-angular-grid";
import { Observable } from "rxjs";
import { GetAllUserView } from "src/app/models/user/get.all.user.view";
import { CreateUserComponent } from "../create-user/create-user.component";
import { MatDialog } from "@angular/material/dialog";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "user-editing",
  templateUrl: "./user-editing.component.html",
  styleUrls: ["./user-editing.component.css"]
})
export class UserEditingComponent implements OnInit {
  public formGroup: FormGroup;
  public editedRowIndex: number;
  public userList: GetAllUserView = new GetAllUserView();
  public id: number;
  public isOpen: boolean = false;
  public isClose: boolean = true;
  public admin: boolean;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 13
  };
  public gridData: GridDataResult;
  public lenght: Observable<number>;

  constructor(
    public userService: UserService,
    private dialog: MatDialog,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.loadData();
  }

  private loadData() {
    this.userService.getAllUsersRole().subscribe(data => {
      this.userList = data;
      this.gridData = process(this.userList.userList, this.gridState);
    });
  }

  public addHandler({ sender }) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      id: new FormControl(""),
      email: new FormControl(""),
      firsName: new FormControl(""),
      lastName: new FormControl(""),
      userName: new FormControl(""),
      phoneNumber: new FormControl(""),
      role: new FormControl("")
    });
    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      id: new FormControl(dataItem.id),
      email: new FormControl(dataItem.email),
      firsName: new FormControl(dataItem.firsName),
      lastName: new FormControl(dataItem.lastName),
      userName: new FormControl(dataItem.userName),
      phoneNumber: new FormControl(dataItem.phoneNumber),
      role: new FormControl(dataItem.role)
    });

    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  private closeEditor(
    grid: { closeRow: (agr0: number) => void },
    rowIndex = this.editedRowIndex
  ) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  openCreateUserDialog() {
    const dialogRef = this.dialog.open(CreateUserComponent);
    {
    }
    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }

  public removeHandler({ dataItem }) {
    this.userService.DeleteUser(dataItem.id).subscribe(() => this.loadData());
    this.toastr.success("removed");
  }

  public Logout() {
    localStorage.removeItem("data");
    window.location.reload();
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridState = state;
    this.gridData = process(this.userList.userList, this.gridState);
  }
}
