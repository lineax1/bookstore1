import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import {
  AuthorGetAuthorItemView,
  GetAuthorView
} from "src/app/models/author/get.author.list.view";
import { State, process } from "@progress/kendo-data-query";
import {
  GridDataResult,
  DataStateChangeEvent
} from "@progress/kendo-angular-grid";
import { Observable } from "rxjs/internal/Observable";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { AuthorService } from "src/app/services/author.service";
import { MatDialog } from "@angular/material/dialog";
import { BookService } from "src/app/services/book.service";
import { UserService } from "src/app/services/authentication/user.service";

import { CreateItemBookComponent } from "../create-item-book/create-item-book.component";
import {
  GetAllGenreView,
  GenreGetGenreItemVeiw
} from "src/app/models/genre/get.genre.view";
import { GenreService } from "src/app/services/genre.service";
import { ToastrService } from "ngx-toastr";
import { GetBookListView } from "src/app/models/book/get.book.list.view";
import { UpdateBookView } from "src/app/models/book/update.book.view";
import { CreateBookView } from "src/app/models/book/ceate.book.view";

@Component({
  selector: "book-editing",
  templateUrl: "./book-editing.component.html",
  styleUrls: ["./book-editing.component.css"]
})
export class BookEditingComponent implements OnInit {
  public formGroup: FormGroup;
  public author: AuthorGetAuthorItemView;
  public genre: GenreGetGenreItemVeiw;
  private editedRowIndex: number;
  public books: GetBookListView;
  public id: number;
  public isOpen: boolean = false;
  public isClose: boolean = true;
  public authorIdList: GetAuthorView;
  public genreIdList: GetAllGenreView;
  public admin: boolean;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };
  public gridData: GridDataResult;
  public length: Observable<number>;

  constructor(
    private genreService: GenreService,
    public localStorage: LocalStorage,
    private dialog: MatDialog,
    private authorService: AuthorService,
    private bookService: BookService,
    private toastr: ToastrService,
    private userService: UserService
  ) {
    this.books = new GetBookListView();
    this.authorIdList = new GetAuthorView();
    this.genreIdList = new GetAllGenreView();
  }

  ngOnInit(): void {
    this.loadData();
    this.authorService.get().subscribe(data => {
      this.authorIdList = data;
    });

    this.genreService.get().subscribe(data => {
      this.genreIdList = data;
    });
    this.admin = this.userService.getIsAdmin();
  }

  private loadData() {
    this.bookService.getAll().subscribe(data => {
      this.books = data;
      this.gridData = process(this.books.books, this.gridState);
    });
  }

  public addHandler({ sender }) {
    this.closeEditor(sender);
    this.formGroup = new FormGroup({
      id: new FormControl(0),
      title: new FormControl("", Validators.required),
      price: new FormControl(0),
      picture: new FormControl(""),
      authorIdList: new FormControl(""),
      genreIdList: new FormControl("")
    });
    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      id: new FormControl(dataItem.id),
      title: new FormControl(dataItem.title, Validators.required),
      price: new FormControl(dataItem.price),
      picture: new FormControl(dataItem.picture),
      authorIdList: new FormControl([]),
      genreIdList: new FormControl([])
    });

    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({ sender, rowIndex, formGroup, isNew }) {
    if (isNew) {
      const product: CreateBookView = formGroup.value;
      this.bookService.create(product).subscribe(() => this.loadData());
    }
    if (!isNew) {
      const product: UpdateBookView = formGroup.value;
      this.bookService.update(product).subscribe(() => this.loadData());
    }
    sender.closeRow(rowIndex);
  }

  public removeHandler({ dataItem }) {
    this.bookService.delete(dataItem.id).subscribe(() => this.loadData());
    this.toastr.success("removed");
  }

  private closeEditor(
    grid: { closeRow: (arg0: number) => void },
    rowIndex = this.editedRowIndex
  ) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public Logout() {
    localStorage.removeItem("data");
    window.location.reload();
  }

  public authorChange(value: any) {
    let ValuesId = new Array<number>();
    for (let i = 0; i < value.length; i++) {
      let res = value[i].id;
      ValuesId.push(res);
    }
    this.formGroup.get("authorIdList").setValue(ValuesId);
  }

  public genreChange(value: any) {
    let idValues = new Array<number>();
    for (let i = 0; i < value.length; i++) {
      let res = value[i].id;
      idValues.push(res);
    }
    this.formGroup.get("genreIdList").setValue(idValues);
  }

  public getAuthorList(book: { authorList: { name: string }[] }) {
    let authorString = "";
    for (let i = 0; i < book.authorList.length; i++) {
      authorString += book.authorList[i].name + " ";
    }
    return authorString;
  }

  public getGenreList(book: { genreList: { title: string }[] }) {
    let genreString = "";
    for (let i = 0; i < book.genreList.length; i++) {
      genreString += book.genreList[i].title + "";
    }
    return genreString;
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridState = state;
    this.gridData = process(this.books.books, this.gridState);
  }

  openCreateBookDialog() {
    const dialogRef = this.dialog.open(CreateItemBookComponent);
    {
    }
    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }
}
