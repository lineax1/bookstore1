import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { BookService } from "src/app/services/book.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthorService } from "src/app/services/author.service";
import { GetAuthorView } from "src/app/models/author/get.author.list.view";
import { ToastrService } from "ngx-toastr";
import { GetAllGenreView } from "src/app/models/genre/get.genre.view";
import { GenreService } from "src/app/services/genre.service";
import { CreateBookView } from "src/app/models/book/ceate.book.view";

@Component({
  selector: "create-item-book",
  templateUrl: "./create-item-book.component.html",
  styleUrls: ["./create-item-book.component.css"]
})
export class CreateItemBookComponent implements OnInit {
  public formGroup: FormGroup;
  public createBookModel: CreateBookView = new CreateBookView();
  public selectedFile: File;
  public stringUrl: string;
  public authorIdList: GetAuthorView;
  public genreIdList: GetAllGenreView;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public bookService: BookService,
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateItemBookComponent>,
    private toastr: ToastrService,
    private authorService: AuthorService,
    private genreService: GenreService
  ) {
    this.authorIdList = new GetAuthorView();
    this.genreIdList = new GetAllGenreView();
  }

  ngOnInit() {
    this.authorService.get().subscribe(data => {
      this.authorIdList = data;
    });
    this.genreService.get().subscribe(data => {
      this.genreIdList = data;
    });
    this.formGroup = this.formBuilder.group({
      title: ["", Validators.required],
      price: ["", Validators.required],
      authorIdList: [""],
      genreIdList: [""],
      description: [""],
      picture: ["", Validators.required]
    });
  }

  public onFileChanged(event): void {
    this.selectedFile = event.target.files[0];
    const uploadData = new FormData();
    uploadData.append("image", this.selectedFile);
    if (uploadData == null) {
      this.toastr.error("bla");
    } else {
      this.bookService.uploadImage(uploadData).subscribe(res => {
        this.stringUrl = res;
      });
    }
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      return this.toastr.error("Enter all fields");
    } else {
      const product: CreateBookView = new CreateBookView();
      product.title = this.formGroup.controls.title.value;
      product.price = this.formGroup.controls.price.value;
      product.description = this.formGroup.controls.description.value;
      product.authorIdList = this.formGroup.controls.authorIdList.value;
      product.genreIdList = this.formGroup.controls.genreIdList.value;
      product.picture = this.stringUrl;

      this.bookService.create(product).subscribe(res => {
        this.dialogRef.close();
      });
      this.toastr.success("Succes", "sds");
    }
  }

  get f() {
    return this.formGroup.controls;
  }

  public getAuthor() {
    this.bookService.getAll();
  }

  public getGenre() {
    this.bookService.getAll();
  }
}
