import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthorEditingComponent } from "./author-editing/author-editing.component";
import { BookEditingComponent } from "./book-editing/book-editing.component";
import { MagazineEditingComponent } from "./magazine-editing/magazine-editing.component";
import { GenreEditingComponent } from "./genre-editing/genre-editing.component";
import { CreateItemAuthorComponent } from "./create-item-author/create-item-author.component";
import { CreateItemBookComponent } from "./create-item-book/create-item-book.component";
import { CreateItemGenreComponent } from "./create-item-genre/create-item-genre.component";
import { CreateItemMagazineComponent } from "./create-item-magazine/create-item-magazine.component";
import { RouterModule } from "@angular/router";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import {
  TooltipModule,
  ModalModule,
  CollapseModule,
  WavesModule,
  ButtonsModule
} from "angular-bootstrap-md";
import { GridModule } from "@progress/kendo-angular-grid";
import {
  MatDialogModule,
  MatTableModule,
  MatCardModule,
  MatNativeDateModule,
  MatMenuModule,
  MatIconModule,
  MatCheckboxModule,
  MatButtonModule,
  MatSidenavModule,
  MatListModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatSelectModule
} from "@angular/material";
import { NgSelectModule } from "@ng-select/ng-select";
import { ToastrModule } from "ngx-toastr";
import { NgbAlertModule, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CdkTableModule } from "@angular/cdk/table";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "../app-routing.module";
import { NgOptionHighlightModule } from "@ng-select/ng-option-highlight";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { PopupModule } from "@progress/kendo-angular-popup";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { AuthenticationModule } from "../authentication/authentication.module";
import {
  BrowserAnimationsModule,
  NoopAnimationsModule
} from "@angular/platform-browser/animations";
import { AdminPageComponent } from "./admin-page/admin-page.component";
import { MenuModule } from "../menu/menu.module";
import { AuthGuard } from "../shared/helpers/auth-guard";
import { OrderEditingComponent } from "./order-editing/order-editing.component";
import { UserEditingComponent } from "./user-editing/user-editing.component";
import { CreateUserComponent } from "./create-user/create-user.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: "app-admin-page",
        component: AdminPageComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: "author-editing",
            component: AuthorEditingComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          },
          {
            path: "book-editing",
            component: BookEditingComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          },
          {
            path: "magazine-editing",
            component: MagazineEditingComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          },
          {
            path: "genre-editing",
            component: GenreEditingComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          },
          {
            path: "order-editing",
            component: OrderEditingComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          },
          {
            path: "create-item-author",
            component: CreateItemAuthorComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          },
          {
            path: "create-item-book",
            component: CreateItemBookComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          },
          {
            path: "user-editing",
            component: UserEditingComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          },
          {
            path: "create-item-genre",
            component: CreateItemGenreComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          },
          {
            path: "create-item-magazine",
            component: CreateItemMagazineComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          },
          {
            path: "create-user",
            component: CreateUserComponent,
            canActivate: [AuthGuard],
            outlet: "editing"
          }
        ]
      }
    ]),
    DropDownsModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    GridModule,
    MatDialogModule,
    NgSelectModule,
    MatTableModule,
    MatCardModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: "toast-bottom-right"
    }),
    MatNativeDateModule,
    MatMenuModule,
    NgbAlertModule,
    CdkTableModule,
    NgbModule,
    MatIconModule,
    MatCheckboxModule,
    MatButtonModule,
    ButtonsModule,
    BrowserModule,
    AppRoutingModule,
    NgOptionHighlightModule,
    ButtonsModule,
    CollapseModule,
    MenuModule,
    WavesModule,
    MatSidenavModule,
    NgMultiSelectDropDownModule.forRoot(),
    PopupModule,
    MatListModule,

    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    AuthenticationModule,
    MatToolbarModule,
    FormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    NoopAnimationsModule
  ],
  declarations: [
    AdminPageComponent,
    AuthorEditingComponent,
    BookEditingComponent,
    MagazineEditingComponent,
    UserEditingComponent,
    CreateUserComponent,
    GenreEditingComponent,
    OrderEditingComponent,
    CreateItemAuthorComponent,
    CreateItemBookComponent,
    CreateItemGenreComponent,
    CreateItemMagazineComponent
  ]
})
export class AdminPanelModule {}
