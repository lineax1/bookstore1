import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GenreService } from "src/app/services/genre.service";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { CreateGenreView } from "src/app/models/genre/create.genre.view";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "create-item-genre",
  templateUrl: "./create-item-genre.component.html",
  styleUrls: ["./create-item-genre.component.css"]
})
export class CreateItemGenreComponent implements OnInit {
  public formGroup: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public formBuilder: FormBuilder,
    private genreService: GenreService,
    public dialogRef: MatDialogRef<CreateItemGenreComponent>,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      name: ["", Validators.required]
    });
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      return this.toastr.error("Enter all fields");
    }

    const product: CreateGenreView = new CreateGenreView();
    product.Title = this.formGroup.controls.name.value;
    this.genreService.create(product).subscribe(res => {
      this.dialogRef.close();
    });
    this.toastr.success("Magazine create");
  }

  get f() {
    return this.formGroup.controls;
  }
}
