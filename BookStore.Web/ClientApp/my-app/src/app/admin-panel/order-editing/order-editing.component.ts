import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import {
  GridDataResult,
  DataStateChangeEvent
} from "@progress/kendo-angular-grid";
import { Observable } from "rxjs";
import { State, process } from "@progress/kendo-data-query";
import { LocalStorage } from "@ngx-pwa/local-storage";
import {
  GetAllOrderView,
  GetAllOrderBookView,
  GetAllOrderMagazineView
} from "src/app/models/order/get.all.order.view";
import { OrderService } from "src/app/services/order.service";
import { UserService } from "src/app/services/authentication/user.service";

@Component({
  selector: "order-editing",
  templateUrl: "./order-editing.component.html",
  styleUrls: ["./order-editing.component.css"]
})
export class OrderEditingComponent implements OnInit {
  public formGroup: FormGroup;
  public editedRowIndex: number;
  public id: number;
  public orders: GetAllOrderView;
  public isOpen: boolean = false;
  public isClose: boolean = true;
  public books: GetAllOrderBookView;
  public magazines: GetAllOrderMagazineView;
  public admin: boolean;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };
  public gridData: GridDataResult;
  public length: Observable<number>;

  constructor(
    public localStorage: LocalStorage,
    public orderServicer: OrderService,
    public userService: UserService
  ) {}

  ngOnInit() {
    this.loadData();
    this.admin = this.userService.getIsAdmin();
  }

  private loadData() {
    this.orderServicer.getAllOrders().subscribe(data => {
      this.orders = data;
      this.gridData = process(this.orders.orders, this.gridState);
    });
  }

  public addHandler({ sender }) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      id: new FormControl(0),
      userId: new FormControl(""),
      email: new FormControl(""),
      amount: new FormControl(""),
      books: new FormControl(""),
      magazines: new FormControl("")
    });

    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      id: new FormControl(dataItem.id),
      userId: new FormControl(dataItem.userId),
      email: new FormControl(dataItem.email),
      amount: new FormControl(dataItem.amount),
      books: new FormControl(dataItem.books),
      magazines: new FormControl(dataItem.magazines)
    });

    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  private closeEditor(
    grid: { closeRow: (arg0: number) => void },
    rowIndex = this.editedRowIndex
  ) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public Logout() {
    localStorage.removeItem("data");
    window.location.reload();
  }

  public getBookList(order: { books: { title: string }[] }) {
    let text = "No Ordered";
    let bookString = "";
    for (let i = 0; i < order.books.length; i++) {
      bookString += order.books[i].title + "";
    }
    if (bookString === "") {
      return text;
    }
    return bookString;
  }

  public getMagazineList(order: { magazines: { title: string }[] }) {
    let text = "No Ordered";
    let magazineString = "";
    for (let i = 0; i < order.magazines.length; i++) {
      magazineString += order.magazines[i].title + "";
    }
    if (magazineString === "") {
      return text;
    } else {
      return magazineString;
    }
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridState = state;
    this.gridData = process(this.orders.orders, this.gridState);
  }
}
