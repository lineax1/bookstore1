import { Component, OnInit, Output, Input } from "@angular/core";
import { AuthorService } from 'src/app/services/author.service';

@Component({
  selector: "app-admin-page",
  templateUrl: "./admin-page.component.html",
  styleUrls: ["./admin-page.component.css"]
})

export class AdminPageComponent implements OnInit {
  constructor( public authService: AuthorService) {}

  ngOnInit() {}

}
