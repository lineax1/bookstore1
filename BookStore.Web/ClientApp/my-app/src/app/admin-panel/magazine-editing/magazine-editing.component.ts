import { Component, OnInit } from "@angular/core";
import {
  DataStateChangeEvent,
  GridDataResult
} from "@progress/kendo-angular-grid";
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { MagazineService } from "src/app/services/magazine.service";
import { MatDialog } from "@angular/material";
import { UserService } from "src/app/services/authentication/user.service";
import { State, process } from "@progress/kendo-data-query";
import { CreateItemMagazineComponent } from "../create-item-magazine/create-item-magazine.component";
import { ToastrService } from "ngx-toastr";
import { GetAllMagazineView } from "src/app/models/magazine/get.magazine.list.view";
import { CreateMagazineView } from "src/app/models/magazine/create.magazine.view";
import { UpdateMagazineView } from "src/app/models/magazine/update.magazine.view";

@Component({
  selector: "magazine-editing",
  templateUrl: "./magazine-editing.component.html",
  styleUrls: ["./magazine-editing.component.css"]
})
export class MagazineEditingComponent implements OnInit {
  private editedRowIndex: number;
  public isOpen: boolean = false;
  public formGroup: FormGroup;
  public magazineList: GetAllMagazineView;
  public gridState: State = { sort: [], skip: 0, take: 10 };
  public gridData: GridDataResult;
  public admin: boolean;

  constructor(
    public magazineService: MagazineService,
    private dialog: MatDialog,
    public userService: UserService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.loadData();
    this.admin = this.userService.getIsAdmin();
  }

  public loadData() {
    this.magazineService.getAllMagazine().subscribe(data => {
      this.magazineList = data;
      this.gridData = process(this.magazineList.magazineList, this.gridState);
    });
  }

  public addHandler({ sender }) {
    this.closeEditor(sender);
    this.formGroup = new FormGroup({
      id: new FormControl(0),
      title: new FormControl("", Validators.required),
      price: new FormControl(0),
      picture: new FormControl("")
    });
    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      id: new FormControl(dataItem.id),
      title: new FormControl(dataItem.title, Validators.required),
      price: new FormControl(dataItem.price),
      public: new FormControl(dataItem.picture)
    });

    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({ sender, rowIndex, formGroup, isNew }) {
    if (isNew) {
      const product: CreateMagazineView = formGroup.value;
      this.magazineService.create(product).subscribe(() => this.loadData());
      this.toastr.success("removed");
    }
    if (!isNew) {
      const product: UpdateMagazineView = formGroup.value;
      this.magazineService.update(product).subscribe(() => this.loadData());
    }
    sender.closeRow(rowIndex);
  }

  public removeHandler({ dataItem }) {
    this.magazineService.delete(dataItem.id).subscribe(() => this.loadData());
    this.toastr.success("removed");
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public Logout() {
    localStorage.removeItem("data");
    window.location.reload();
  }

  public getAuthorList(authorList) {
    let authorString = "";
    for (let i = 0; i < authorList.authorList.length; i++) {
      authorString += authorList.authorList[i].name;
    }
    return authorString;
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridState = state;
    this.gridData = process(this.magazineList.magazineList, this.gridState);
  }

  openCreateMagazineDialog() {
    const dialogRef = this.dialog.open(CreateItemMagazineComponent);
    {
    }
    dialogRef.afterClosed().subscribe(result => {
      this.magazineService.getAllMagazine();
    });
  }
}
