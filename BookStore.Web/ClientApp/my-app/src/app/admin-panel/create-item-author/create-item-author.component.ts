import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { AuthorService } from "src/app/services/author.service";
import { CreateAuthorView } from "src/app/models/author/create.author.view";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "create-item-author",
  templateUrl: "./create-item-author.component.html",
  styleUrls: ["./create-item-author.component.css"]
})
export class CreateItemAuthorComponent implements OnInit {
  public formGroup: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public formBuilder: FormBuilder,
    private authorService: AuthorService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<CreateItemAuthorComponent>
  ) {}

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      name: ["", Validators.required]
    });
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      return this.toastr.error("Enter all fields");
    }
    const product: CreateAuthorView = new CreateAuthorView();
    product.name = this.formGroup.controls.name.value;

    this.authorService.create(product).subscribe(res => {
      this.dialogRef.close();
    });
    this.toastr.success("Author Created");
  }

  get f() {
    return this.formGroup.controls;
  }
}
