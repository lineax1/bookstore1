import { Component, OnInit, Input, Output } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { GetAuthorView } from "src/app/models/author/get.author.list.view";
import {
  GridDataResult,
  DataStateChangeEvent
} from "@progress/kendo-angular-grid";
import { State, process } from "@progress/kendo-data-query";
import { Observable } from "rxjs";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { MatDialog, MatDialogRef } from "@angular/material";
import { AuthorService } from "src/app/services/author.service";
import { Router } from "@angular/router";
import { CreateAuthorView } from "src/app/models/author/create.author.view";
import { UpdateAuthorView } from "src/app/models/author/update.author.view";
import { EventEmitter } from "@angular/core";
import { CreateItemAuthorComponent } from "../create-item-author/create-item-author.component";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "author-editing",
  templateUrl: "./author-editing.component.html",
  styleUrls: ["./author-editing.component.css"]
})

export class AuthorEditingComponent implements OnInit {
  @Output() notifyParent: EventEmitter<any> = new EventEmitter();
  public formGroup: FormGroup;
  public authorList: GetAuthorView;
  public editedRowIndex: number;
  public id: number;
  public admin: boolean;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };
  public gridData: GridDataResult;
  public lenght: Observable<number>;
  
  constructor(
    public localStorage: LocalStorage,
    private dialog: MatDialog,
    private authorService: AuthorService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<AuthorEditingComponent>,
    private router: Router
  ) {
    this.authorList = new GetAuthorView();
  }

  ngOnInit(): void {
    this.loadData();
    this.notifyParent.emit(AuthorEditingComponent);
  }

  private loadData() {
    this.authorService.get().subscribe(data => {
      this.authorList = data;
      this.gridData = process(this.authorList.authorList, this.gridState);
    });
  }

  public addHandler({ sender }) {
    this.closeEditor(sender);
    this.formGroup = new FormGroup({
      id: new FormControl(0),
      name: new FormControl("", Validators.required)
    });
    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      id: new FormControl(dataItem.id),
      name: new FormControl(dataItem.name)
    });
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
    this.toastr.success("Edited");
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({ sender, rowIndex, formGroup, isNew }) {
    if (isNew) {
      const product: CreateAuthorView = formGroup.value;
      this.authorService.create(product).subscribe(() => this.loadData());
    }
    if (!isNew) {
      const product: UpdateAuthorView = formGroup.value;
      this.authorService.update(product).subscribe(() => this.loadData());
    }
    sender.closeRow(rowIndex);
  }
  
  public removeHandler({ dataItem }) {
    this.authorService.delete(dataItem.id).subscribe(() => this.loadData());
    this.toastr.success("Deleted");
  }

  public closeEditor(
    grid: { closeRow: (arg0: number) => void },
    rowIndex = this.editedRowIndex
  ) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public Logout() {
    localStorage.removeItem("data");
    window.location.reload();
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridState = state;
    this.gridData = process(this.authorList.authorList, this.gridState);
  }
  
  public cloasing() {
    this.router.navigate([{ outlets: { editing: null } }]);
  }

  openCreateAuthorDialog(): void {
    const dialogRef = this.dialog.open(CreateItemAuthorComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }
}
