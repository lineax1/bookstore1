/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GenreEditingComponent } from './genre-editing.component';

describe('GenreEditingComponent', () => {
  let component: GenreEditingComponent;
  let fixture: ComponentFixture<GenreEditingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenreEditingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenreEditingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
