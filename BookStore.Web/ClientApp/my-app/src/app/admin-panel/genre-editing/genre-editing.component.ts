import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { GetAllGenreView } from "src/app/models/genre/get.genre.view";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import {
  GridDataResult,
  DataStateChangeEvent
} from "@progress/kendo-angular-grid";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { GenreService } from "src/app/services/genre.service";
import { State, process } from "@progress/kendo-data-query";
import { CreateItemGenreComponent } from "../create-item-genre/create-item-genre.component";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "genre-editing",
  templateUrl: "./genre-editing.component.html",
  styleUrls: ["./genre-editing.component.css"]
})
export class GenreEditingComponent implements OnInit {
  @Output() notifyParent: EventEmitter<any> = new EventEmitter();

  public formGroup: FormGroup;
  public genreList: GetAllGenreView;
  public editedRowIndex: number;
  public id: number;
  public admin: boolean;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };
  public gridData: GridDataResult;
  public lenght: Observable<number>;

  constructor(
    public localStorage: LocalStorage,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private genreService: GenreService,
    private router: Router
  ) {
    this.genreList = new GetAllGenreView();
  }

  ngOnInit(): void {
    this.loadData();
    this.notifyParent.emit(GenreEditingComponent);
  }

  private loadData() {
    this.genreService.get().subscribe(data => {
      this.genreList = data;
      this.gridData = process(this.genreList.genreList, this.gridState);
    });
  }

  public addHandler({ sender }) {
    this.closeEditor(sender);
    this.formGroup = new FormGroup({
      id: new FormControl(0),
      name: new FormControl("", Validators.required)
    });
    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      id: new FormControl(dataItem.id),
      name: new FormControl(dataItem.name)
    });
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  public removeHandler({ dataItem }) {
    this.genreService.delete(dataItem.id).subscribe(() => this.loadData());
    this.toastr.success("removed");
  }

  public closeEditor(
    grid: { closeRow: (arg0: number) => void },
    rowIndex = this.editedRowIndex
  ) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public Logout() {
    localStorage.removeItem("data");
    window.location.reload();
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridState = state;
    this.gridData = process(this.genreList.genreList, this.gridState);
  }

  public cloasing() {
    this.router.navigate([{ outlets: { editing: null } }]);
  }

  openCreateGenreDialog(): void {
    const dialogRef = this.dialog.open(CreateItemGenreComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }
}
