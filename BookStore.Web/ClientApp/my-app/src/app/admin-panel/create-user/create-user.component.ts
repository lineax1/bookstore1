import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CreateItemMagazineComponent } from "../create-item-magazine/create-item-magazine.component";
import { ToastrService } from "ngx-toastr";
import { UserService } from "src/app/services/authentication/user.service";
import { CreateUserView } from "src/app/models/user/cereate.users.view";

@Component({
  selector: "create-user",
  templateUrl: "./create-user.component.html",
  styleUrls: ["./create-user.component.css"]
})
export class CreateUserComponent implements OnInit {
  public formGroup: FormGroup;
  public stringUrl: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public userService: UserService,
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateItemMagazineComponent>,
    private toastr: ToastrService
  ) {}
  
  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
      role: ["", Validators.required]
    });
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      return this.toastr.error("Enter all fields");
    }
    const user: CreateUserView = new CreateUserView();
    user.email = this.formGroup.controls.email.value;
    user.emailConfirmed = true;
    user.password = this.formGroup.controls.password.value;
    user.role = this.formGroup.controls.role.value;
    this.userService.CreateUser(user).subscribe(res => {
      this.dialogRef.close();
    });
    this.toastr.success("User create");
  }

  get f() {
    return this.formGroup.controls;
  }
}
