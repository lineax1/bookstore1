import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from "@angular/material";
import { MagazineService } from "src/app/services/magazine.service";
import { ToastrService } from "ngx-toastr";
import { CreateMagazineView } from "src/app/models/magazine/create.magazine.view";

@Component({
  selector: "create-item-magazine",
  templateUrl: "./create-item-magazine.component.html",
  styleUrls: ["./create-item-magazine.component.css"]
})
export class CreateItemMagazineComponent implements OnInit {
  public formGroup: FormGroup;
  public stringUrl: string;
  public selectedFile: File;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public magazineService: MagazineService,
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateItemMagazineComponent>,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      title: ["", Validators.required],
      price: ["", Validators.required],
      description: [""],
      picture: ["", Validators.required]
    });
  }

  public onFileChanged(event): void {
    this.selectedFile = event.target.files[0];
    const uploadData = new FormData();
    uploadData.append("image", this.selectedFile);
    this.magazineService.uploadImage(uploadData).subscribe(res => {
      this.stringUrl = res;
    });
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      return this.toastr.error("Enter all fields");
    }
    const product: CreateMagazineView = new CreateMagazineView();
    product.title = this.formGroup.controls.title.value;
    product.price = this.formGroup.controls.price.value;
    product.description = this.formGroup.controls.description.value;
    product.picture = this.stringUrl;

    this.magazineService.create(product).subscribe(res => {
      this.dialogRef.close();
    });
    this.toastr.success("Magazine create");
  }

  get f() {
    return this.formGroup.controls;
  }
}
