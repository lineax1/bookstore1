﻿using System.Threading.Tasks;
using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.GenreViews;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GenreController : ControllerBase
    {
        private readonly IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> GetAll()
        {
            GetAllGenreView result = await _genreService.GetAll();
            return Ok(result);
        }

        [HttpPost, Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Create(CreateGenreView createGenreView)
        {
            await _genreService.Create(createGenreView);
            return Ok();
        }

        [HttpDelete("{id}"), Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            await _genreService.Delete(id);
            return Ok();
        }
    }
}