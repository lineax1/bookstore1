﻿using System.Threading.Tasks;
using BookStore.BusinessLogicLayer.Helpers.Interfaces;
using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.BookViews;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BookController : Controller
    {
        private readonly IBookService _bookService;
        private readonly IUploadFile _uploadFile;

        public BookController(IBookService bookService, IUploadFile uploadFile)
        {
            _bookService = bookService;
            _uploadFile = uploadFile;
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> GetAll()
        {
            GetAllBookView result = await _bookService.GetAll();
            return Ok(result);
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> GetById(int id)
        {
            GetByIdBookView result = await _bookService.GetById(id);
            return Ok(result);
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> GetByTitle(string title)
        {
            GetByTitleBookView result = await _bookService.GetByTitle(title);
            return Ok(result);
        }

        [HttpPost, Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Create(CreateBookView createBook)
        {
            await _bookService.Create(createBook);
            return Ok();
        }

        [HttpPut, Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Update(UpdateBookView updateBook)
        {
            await _bookService.Update(updateBook);
            return Ok();
        }

        [HttpDelete("{id}"), Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Detele(int id)
        {
            await _bookService.Delete(id);
            return Ok();
        }

        [HttpPost, Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> UploadImage()
        {
            var file = Request.Form.Files[0];
            string url = await _uploadFile.UploadImage(file);
            return Json(url);
        }
    }
}