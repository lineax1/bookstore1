﻿using System.Threading.Tasks;
using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.AccountViews;
using BookStore.BusinessLogicLayer.Views.TokenViews;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.API.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterAccountView model)
        {
            await _accountService.Register(model);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmEmail(ConfirmRegisterView confirmRegisterView)
        {
            var res = await _accountService.ConfirmEmail(confirmRegisterView);
            return Ok(res);
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginAccountView model)
        {
            TokenView token = await _accountService.Login(model);
            return Ok(token);
        }

        [HttpPost]
        public async Task<IActionResult> SocialLogin(SocialLoginView socialLoginView)
        {
            TokenView token = await _accountService.SocialLogin(socialLoginView);
            return Ok(token);
        }

        [HttpPost]
        public IActionResult RefreshTokens([FromBody]RefreshTokenView model)
        {
            TokenView result = _accountService.RefreshToken(model);
            return Ok(result);
        }
    }
}