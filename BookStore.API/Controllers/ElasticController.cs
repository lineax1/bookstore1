﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.DataAccessLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nest;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.API.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class ElasticController : ControllerBase
    {

        private readonly IElasticService _service;
        private readonly ElasticClient _client;

        public ElasticController(IElasticService clientProvider)
        {
            _client = clientProvider.GetClient();
            _service = clientProvider;
        }

        [HttpGet]
        public async Task<IReadOnlyCollection<Author>> SearchAuthor(string type)
        {
            ISearchResponse<Author> searchResponse = await _client.SearchAsync<Author>(s => s
                    .From(0)
                    .Size(10)
                    .Query(q => q
                        .Match(m => m.Field(f => f.Name)
                        .Query(type)
                        ))
                  );

            IReadOnlyCollection<Author> people = searchResponse.Documents;
            return people;
        }

        [HttpGet]
        public async Task<IReadOnlyCollection<Book>> SearchBook(string type)
        {
            ISearchResponse<Book> searchResponse = await _client.SearchAsync<Book>(s => s
                    .From(0)
                    .Size(10)
                    .Query(q => q
                        .Match(m => m.Field(f => f.Title)
                        .Query(type)
                        ))
                  );

            IReadOnlyCollection<Book> people = searchResponse.Documents;
            return people;
        }

        [HttpGet]
        public async Task<IActionResult> IndexMany()
        {
            BulkResponse response = await _service.IndexMany();
            return Ok(response);
        }
    }
}
