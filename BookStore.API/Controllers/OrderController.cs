﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.OrdersViews;
using BookStore.BusinessLogicLayer.Views.PaymentViews;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OrderController : Controller
    {
        private readonly IMainOrderService _mainOrderService;

        public OrderController(IMainOrderService mainOrderService)
        {
            _mainOrderService = mainOrderService;
        }

        [HttpPost, Authorize]
        public async Task<IActionResult> Charge([FromBody]PaymentView paymentView)
        {
            await _mainOrderService.Charge(paymentView);
            return Ok();
        }

        [HttpGet, Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> GetAllOrders()
        {
            GetAllOrdersView result = await _mainOrderService.GetAllOrders();
            return Ok(result);
        }
    }
}