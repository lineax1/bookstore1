﻿using System.Threading.Tasks;
using BookStore.BusinessLogicLayer.Helpers.Interfaces;
using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.MagazineViews;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MagazineController : Controller
    {
        private readonly IMagazineService _magazineService;
        private readonly IUploadFile _uploadFile;

        public MagazineController(IMagazineService magazineService, IUploadFile uploadFile)
        {
            _magazineService = magazineService;
            _uploadFile = uploadFile;
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> GetById(int id)
        {
            GetByIdMagazineView getById = await _magazineService.GetById(id);
            return Ok(getById);
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> GetByTitle(string title)
        {
            GetByTitleMagazineView getByTitle = await _magazineService.GetByTitle(title);
            return Ok(getByTitle);
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> GetAll()
        {
            GetAllMagazineView getMagazine = await _magazineService.GetAll();
            return Ok(getMagazine);
        }

        [HttpPost, Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Create([FromBody]CreateMagazineView createMagazine)
        {
            await _magazineService.Create(createMagazine);
            return Ok();
        }

        [HttpPut, Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Update(UpdateMagazineView updateMagazine)
        {
            await _magazineService.Update(updateMagazine);
            return Ok();
        }

        [HttpDelete("{id}"), Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            await _magazineService.Delete(id);
            return Ok();
        }

        [HttpPost, Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> UploadImage()
        {
            var file = Request.Form.Files[0];
            string url = await _uploadFile.UploadImage(file);
            return Json(url);
        }
    }
}