﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.GenreViews;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;
        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
        }

        public async Task<GetAllGenreView> GetAll()
        {
            var genre = (await _genreRepository.GetAll());
            var genreres = genre.Select(x => new GenreGetAllGenreItemView
            {
                Id = x.Id,
                Title = x.Title
            }).ToList();
            var result = new GetAllGenreView() { GenreList = genreres };
            return result;
        }

        public async Task Create(CreateGenreView CreateGenreView)
        {
            await _genreRepository.Create(new Genre { Title = CreateGenreView.Title });
        }

        public async Task Delete(int id)
        {
            Genre genre = await _genreRepository.GetById(id);
            if (genre is null)
            {
                throw new Exception("Item not found");
            }
            await _genreRepository.Delete(genre);
        }
    }
}
