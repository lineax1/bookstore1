﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.OrdersViews;
using BookStore.BusinessLogicLayer.Views.PaymentViews;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public class MainOrderService : IMainOrderService
    {
        private readonly IMainOrderRepository _mainOrderRepository;
        private readonly IMagazineOrderRepository _magazineOrderRepository;
        private readonly IBookOrderRepository _bookOrderRepository;
        private readonly IBookRepository _bookRepository;
        private readonly IMagazineRepository _magazineRepository;
        private readonly UserManager<User> _userManager;

        public MainOrderService(IMainOrderRepository mainOrderRepository, UserManager<User> userManager, IMagazineRepository magazineRepository, IBookRepository bookRepository, IMagazineOrderRepository magazineOrderRepository, IBookOrderRepository bookOrderRepository)
        {
            _mainOrderRepository = mainOrderRepository;
            _magazineOrderRepository = magazineOrderRepository;
            _bookOrderRepository = bookOrderRepository;
            _bookRepository = bookRepository;
            _userManager = userManager;
            _magazineRepository = magazineRepository;
        }

        private async Task Create(PaymentView paymentView)
        {
            User user = await _userManager.FindByEmailAsync(paymentView.Email);
            int mainOrderId = await _mainOrderRepository.Create(new MainOrder() { UserId = user.Id, Amount = paymentView.Amount });
            List<MagazineOrder> magazineOrders = (paymentView.MagazineIdList.Select(x => new MagazineOrder()
            {
                MainOrderId = mainOrderId,
                MagazineId = x
            })).ToList();
            List<BookOrder> bookOrders = (paymentView.BookIdList.Select(x => new BookOrder()
            {
                MainOrderId = mainOrderId,
                BookId = x,
            })).ToList();
            await _magazineOrderRepository.CreateRange(magazineOrders);
            await _bookOrderRepository.CreateRange(bookOrders);
        }

        private async Task<decimal> CalculationOfTheAmounts(ICollection<int> BookId, ICollection<int> MagazineId)
        {
            PaymentView paymentView = new PaymentView();
            IEnumerable<Book> findBooks = await _bookRepository.GetAllItemsById(BookId.ToList());
            IEnumerable<Magazine> findMagazines = await _magazineRepository.GetAllItemsById(MagazineId.ToList());

            decimal bookSum = findBooks.Select(p => p.Price).Sum();
            decimal magazineSum = findMagazines.Select(p => p.Price).Sum();

            return paymentView.Amount = bookSum + magazineSum;
        }

        public async Task Charge([FromBody]PaymentView paymentView)
        {
            paymentView.Amount = await CalculationOfTheAmounts(paymentView.BookIdList.ToList(), paymentView.MagazineIdList.ToList());
            var options = new TokenCreateOptions
            {
                Card = new CreditCardOptions
                {
                    Number = paymentView.CardNumber,
                    ExpYear = paymentView.CardExpYear,
                    ExpMonth = paymentView.CardExpMonth,
                    Cvc = paymentView.CardCvc
                }
            };
            var service = new TokenService();
            Token stripeToken = service.Create(options);

            var charges = new ChargeService();
            Charge charge = charges.Create(new ChargeCreateOptions
            {
                Amount = Convert.ToInt64(paymentView.Amount),
                Description = "Test Payne",
                Currency = "usd",
                Source = stripeToken.Id
            });
            if (charge.Status == "succeeded")
            {
                await Create(paymentView);
            }
            throw new Exception();
        }

        public async Task<GetAllOrdersView> GetAllOrders()
        {
            IEnumerable<MainOrder> orders = await _mainOrderRepository.GetAll();
            IEnumerable<Book> books = await _bookRepository.GetAll();
            IEnumerable<Magazine> magazines = await _magazineRepository.GetAll();
            IEnumerable<BookOrder> bookOrders = await _bookOrderRepository.GetAll();
            IEnumerable<MagazineOrder> magazineOrders = await _magazineOrderRepository.GetAll();

            var Order = orders.Select(mainOrder => new OrderGetAllOrderItemView()
            {
                Id = mainOrder.Id,
                UserId = mainOrder.UserId,
                Amount = mainOrder.Amount,
                Email = _userManager.FindByIdAsync(mainOrder.UserId).Result.Email,
                Books = MapRange(books.Where(book => bookOrders.Where(x => x.MainOrderId == mainOrder.Id).Select(c => c.BookId).Contains(book.Id))),
                Magazines = MapRange(magazines.Where(magazine => magazineOrders.Where(x => x.MainOrderId == mainOrder.Id).Select(c => c.MagazineId).Contains(magazine.Id)))
            }).ToList();
            var result = new GetAllOrdersView() { Orders = Order };
            return result;
        }

        private List<GetAllBookViewItem> MapRange(IEnumerable<Book> books)
        {
            return books.Select(bookItem => new GetAllBookViewItem()
            {
                Id = bookItem.Id,
                Title = bookItem.Title,
                Price = bookItem.Price,
            }).ToList();
        }

        private List<GetAllMagazineViewItem> MapRange(IEnumerable<Magazine> magazines)
        {
            return magazines.Select(magazineItem => new GetAllMagazineViewItem()
            {
                Id = magazineItem.Id,
                Title = magazineItem.Title,
                Price = magazineItem.Price,
            }).ToList();
        }
    }
}
