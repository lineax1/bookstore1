﻿using BookStore.BusinessLogicLayer.Helpers.Interfaces;
using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.AccountViews;
using BookStore.BusinessLogicLayer.Views.TokenViews;
using BookStore.DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public class AccountService : IAccountService
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;

        public AccountService(IConfiguration configuration, UserManager<User> userManager, SignInManager<User> signInManager, IEmailService emailService)
        {
            _configuration = configuration;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailService = emailService;
        }

        public string GenerateJwtToken(IEnumerable<Claim> claims)
        {
            int expireMinute;
            int.TryParse(_configuration["ExpireTimeJwtToken"], out expireMinute);
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JwtKey_Security"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expire = DateTime.Now;

            var token = new JwtSecurityToken(
               _configuration["JwtIssuer"],
               _configuration["JwtAudience"],
               claims: claims,
               expires: expire.Add(TimeSpan.FromMinutes(expireMinute)),
               signingCredentials: creds
                );
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);
            return encodedJwt;
        }

        public async Task<string> Register(RegisterAccountView model)
        {
            string baseUrl = _configuration["Redirect"];
            var user = new User { Email = model.Email, UserName = model.Email, LastName = model.LastName, FirstName = model.FirstName, PhoneNumber = model.PhoneNumber };

            IdentityResult result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                throw new Exception("Erorr,Try Again");
            }
            await _userManager.AddToRoleAsync(user, "user");
            string _code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            string url = baseUrl + $"?email={model.Email}&token={_code}";
            await _emailService.SendEmailAsync(model.Email, "Confirm you account",
                $"Подтвердите регистрацию перейдя по ссылке: " +
                $"<a href ='{url}'>link,");
            return url;
        }

        public async Task<bool> ConfirmEmail(ConfirmRegisterView confirmRegisterView)
        {
            bool confirm = false;
            string code = confirmRegisterView.Code.Replace(" ", "+");
            if (string.IsNullOrEmpty(confirmRegisterView.Email) || string.IsNullOrEmpty(code))
            {
                throw new Exception();
            }
            User user = await _userManager.FindByEmailAsync(confirmRegisterView.Email);
            if (user == null)
            {
                throw new Exception();
            }
            IdentityResult result = await _userManager.ConfirmEmailAsync(user, code);
            if (!result.Succeeded)
            {
                throw new Exception();
            }
            return confirm;
        }

        public string GenerateJwtRefreshToken()
        {
            int expireHours;
            int.TryParse(_configuration["ExpireTimeJwtRefreshToken"], out expireHours);

            var now = DateTime.UtcNow;
            var creds = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey_Security"])),
                    SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
               _configuration["JwtIssuer"],
               _configuration["JwtAudience"],
               expires: now.Add(TimeSpan.FromHours(expireHours)),
               signingCredentials: creds
                );
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);
            return encodedJwt;
        }

        public TokenView RefreshToken(RefreshTokenView model)
        {
            User user = _userManager.Users.SingleOrDefault(r => r.Email == model.Email);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub,model.Email),
                new Claim(JwtRegisteredClaimNames.Jti,  Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Role, model.Role),
            };

            var token = new TokenView
            {
                AccessToken = GenerateJwtToken(claims),
                RefreshToken = GenerateJwtRefreshToken(),
            };
            return token;
        }

        public async Task<TokenView> Login(LoginAccountView model)
        {
            SignInResult result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

            if (!result.Succeeded)
            {
                throw new Exception("Invalid Data");
            }
            User user = _userManager.Users.SingleOrDefault(r => r.Email == model.Email);
            string Role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub,model.Email),
                new Claim(JwtRegisteredClaimNames.Jti,  Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Role, Role)
           };
            var token = new TokenView
            {
                AccessToken = GenerateJwtToken(claims),
                RefreshToken = GenerateJwtRefreshToken(),
            };
            return token;
        }

        public async Task<TokenView> SocialLogin(SocialLoginView socialLoginView)
        {
            if (string.IsNullOrEmpty(socialLoginView.Email))
            {
                throw new ArgumentException("Model is empty");
            }

            User user = await _userManager.FindByNameAsync(socialLoginView.Email);

            if (user != null)
            {
                await _signInManager.SignInAsync(user, true, null);
                return CreateTokenSociallLogin(user.Email);
            }
            var newUser = new User { Email = socialLoginView.Email, UserName = socialLoginView.Email };
            await _userManager.CreateAsync(newUser);
            await _userManager.AddToRoleAsync(newUser, "user");
            newUser.EmailConfirmed = true;
            await _signInManager.SignInAsync(newUser, true, null);
            return CreateTokenSociallLogin(newUser.Email);
        }

        private TokenView CreateTokenSociallLogin(string email)
        {
            var claims = new[]
            {
            new Claim(ClaimsIdentity.DefaultNameClaimType, email),
            new Claim(ClaimsIdentity.DefaultRoleClaimType, "user")
            };
            var token = new TokenView
            {
                AccessToken = GenerateJwtToken(claims),
                RefreshToken = GenerateJwtRefreshToken(),
            };
            return token;
        }
    }
}
