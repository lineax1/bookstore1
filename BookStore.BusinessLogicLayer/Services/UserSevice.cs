﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.UserViews;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly UserManager<User> _userManager;
        public UserService(IUserRepository userRepository, UserManager<User> userManager)
        {
            _userRepository = userRepository;
            _userManager = userManager;
        }

        public async Task CreateUser(CreateUserView createUser)
        {
            var user = new User
            {
                Email = createUser.Email,
                UserName = createUser.Email,
                EmailConfirmed = createUser.EmailConfirmed
            };
            IdentityResult result = await _userManager.CreateAsync(user, createUser.Password);
            if (!result.Succeeded)
            {
                throw new Exception("User not added");
            }
            await _userManager.AddToRoleAsync(user, createUser.Role);
        }

        public async Task DeleteUser(string Id)
        {
            User user = await _userManager.FindByIdAsync(Id);
            if(user is null)
            {
                throw new Exception("User is Null");
            }
            await _userRepository.Delete(Id);
        }

        public async Task ChangeUserRole(ChangeRoleUserView changeRoleUser)
        {
            User user = await _userManager.FindByIdAsync(changeRoleUser.Id);
            if (user != null)
            {
                string currentRole = (await _userManager.GetRolesAsync(user)).FirstOrDefault();
                await _userManager.RemoveFromRoleAsync(user, currentRole);
                await _userManager.AddToRoleAsync(user, changeRoleUser.Role);
                await _userManager.UpdateAsync(user);
            }
            throw new Exception("User Not Found");
        }

        public async Task<GetAllUsersView> GetUsers()
        {
            IEnumerable<User> users = (await _userRepository.GetAllUsers());
            var usersres = users.Select(user => new UserGetAllUserItemView
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            }).ToList();
            var result = new GetAllUsersView() { UserList = usersres };
            result.UserList = usersres;
            return result;
        }

        public async Task<GetAllUsersView> GetAllUsersWithRole()
        {
            IEnumerable<AspNetUserRole> userRoles = await _userRepository.GetUserWithRole();
            IEnumerable<User> user = await _userRepository.GetAllUsers();
            var result = new GetAllUsersView();
            result.UserList.AddRange(user.Select(us => MapUser(us, userRoles.ToList())));
            return result;
        }

        private UserGetAllUserItemView MapUser(User user, List<AspNetUserRole> userRoles)
        {
            var result = new UserGetAllUserItemView
            {
                Id = user.Id,
                Email = user.Email,
                UserName = user.UserName,
                FirstName = user.FirstName,
                PhoneNumber = user.PhoneNumber,
                LastName = user.LastName,
                Role = userRoles.FirstOrDefault(userRole => userRole.UserId == user.Id).Role.Select(role => role.Name).ToList()
            };
            return result;
        }
    }
}
