﻿using BookStore.BusinessLogicLayer.Views.AccountViews;
using BookStore.BusinessLogicLayer.Views.TokenViews;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services.Interfaces
{
    public interface IAccountService
    {
        string GenerateJwtToken(IEnumerable<Claim> claims);
        string GenerateJwtRefreshToken();
        TokenView RefreshToken(RefreshTokenView model);
        Task<string> Register(RegisterAccountView model);
        Task<TokenView> Login(LoginAccountView model);
        Task<bool> ConfirmEmail(ConfirmRegisterView confirmRegisterView);
        Task<TokenView> SocialLogin(SocialLoginView socialLoginView);
    }
}
