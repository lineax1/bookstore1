﻿using BookStore.BusinessLogicLayer.Views.GenreViews;
using BookStore.DataAccessLayer.Entities;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services.Interfaces
{
    public interface IGenreService
    {
        Task<GetAllGenreView> GetAll();
        Task Create(CreateGenreView CreateGenreView);
        Task Delete(int id);
    }
}
