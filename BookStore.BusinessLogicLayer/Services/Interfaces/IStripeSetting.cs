﻿namespace BookStore.BusinessLogicLayer.Services.Interfaces
{
    public interface IStripeSetting
    {
         string SecretKey { get; set; }
         string PublishableKey { get; set; }
    }
}
