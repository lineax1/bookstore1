﻿using BookStore.BusinessLogicLayer.Views.UserViews;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services.Interfaces
{
    public interface IUserService
    {
        Task CreateUser(CreateUserView createUser);
        Task DeleteUser(string id);
        Task ChangeUserRole(ChangeRoleUserView changeRoleUser);
        Task<GetAllUsersView> GetAllUsersWithRole();
        Task<GetAllUsersView> GetUsers();
    }
}
