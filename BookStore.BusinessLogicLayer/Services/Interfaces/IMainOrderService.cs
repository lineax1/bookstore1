﻿using BookStore.BusinessLogicLayer.Views.OrdersViews;
using BookStore.BusinessLogicLayer.Views.PaymentViews;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services.Interfaces
{
    public interface IMainOrderService
    {
        Task Charge([FromBody]PaymentView paymentView);
        Task<GetAllOrdersView> GetAllOrders();
    }
}
