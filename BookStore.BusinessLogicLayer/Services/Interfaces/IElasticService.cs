﻿using Nest;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services.Interfaces
{
    public interface IElasticService
    {
        ElasticClient GetClient();
        Task<BulkResponse> IndexMany();
    }
}
