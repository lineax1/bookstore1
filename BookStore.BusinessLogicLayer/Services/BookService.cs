﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.BookViews;
using System.Threading.Tasks;
using System.Linq;
using BookStore.DataAccessLayer.Entities;
using System.Collections.Generic;
using System;
using BookStore.DataAccessLayer.Interfaces;

namespace BookStore.BusinessLogicLayer.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookrepository;
        private readonly IBookAuthorRepository _bookAuthorRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly IGenreRepository _genreRepository;
        private readonly IBookGenreRepository _bookGenreRepository;

        public BookService(IBookRepository bookRepository, IBookAuthorRepository bookAuthor, IAuthorRepository authorRepository, IGenreRepository genreRepository, IBookGenreRepository bookGenreRepository)
        {
            _bookrepository = bookRepository;
            _bookAuthorRepository = bookAuthor;
            _authorRepository = authorRepository;
            _genreRepository = genreRepository;
            _bookGenreRepository = bookGenreRepository;
        }

        public async Task<GetByIdBookView> GetById(int id)
        {
            Book book = await _bookrepository.GetById(id);
            var result = new GetByIdBookView
            {
                Id = book.Id,
                Title = book.Title,
                Price = book.Price
            };
            return result;
        }

        public async Task<GetAllBookView> GetAll()
        {
            IEnumerable<Book> books = await _bookrepository.GetAll();
            IEnumerable<Author> authors = await _authorRepository.GetAll();
            IEnumerable<Genre> genres = await _genreRepository.GetAll();
            IEnumerable<BookAuthor> bookAuthors = await _bookAuthorRepository.GetAll();
            IEnumerable<BookGenre> bookGenres = await _bookGenreRepository.GetAll();

            var Book = books.Select(book => new BookGetAllBookItemView()
            {
                Id = book.Id,
                Description = book.Description,
                Picture = book.Picture,
                Price = book.Price,
                Title = book.Title,
                AuthorList = MapRange(authors.Where(author => bookAuthors.Where(x => x.BookId == book.Id).Select(c => c.AuthorId).Contains(author.Id))),
                GenreList = MapRange(genres.Where(genre => bookGenres.Where(x => x.BookId == book.Id).Select(x => x.GenreId).Contains(genre.Id))),
            }).ToList();
            var result = new GetAllBookView() { Books = Book };
            return result;
        }

        public async Task<GetByTitleBookView> GetByTitle(string title)
        {
            Book book = await _bookrepository.GetByTitle(title);
            var result = new GetByTitleBookView
            {
                Id = book.Id,
                Title = book.Title,
                Price = book.Price
            };
            return result;
        }

        public async Task Create(CreateBookView createBook)
        {
            var book = new Book()
            {
                Title = createBook.Title,
                Price = createBook.Price,
                Description = createBook.Description,
                Picture = createBook.Picture
            };
            int bookId = await _bookrepository.Create(book);

            List<BookAuthor> authorlist = (createBook.authorIdList.Select(x => new BookAuthor()
            {
                BookId = bookId,
                AuthorId = x
            })).ToList();
            await _bookAuthorRepository.CreateRange(authorlist);

            IEnumerable<BookGenre> genrelist = (createBook.genreIdList.Select(x => new BookGenre()
            {
                BookId = bookId,
                GenreId = x
            }));
            await _bookGenreRepository.CreateRange(genrelist);
        }

        public async Task Update(UpdateBookView updateBook)
        {
            Book book = await _bookrepository.GetById(updateBook.Id);
            var bookUpdate = new Book()
            {
                Title = updateBook.Title,
                Price = updateBook.Price,
                Id = updateBook.Id,
                Picture = updateBook.Picture,
                Description = updateBook.Description
            };
            await _bookrepository.Update(bookUpdate);
            await _bookAuthorRepository.DeleteRangeByBookId(book.Id);
            await _bookGenreRepository.DeleteRangeByBookId(book.Id);

            List<BookAuthor> authorlist = (updateBook.authorIdList.Select(author => new BookAuthor()
            {
                BookId = updateBook.Id,
                AuthorId = author
            })).ToList();
            await _bookAuthorRepository.CreateRange(authorlist);

            List<BookGenre> genrelist = (updateBook.genreIdList.Select(genre => new BookGenre()
            {
                BookId = updateBook.Id,
                GenreId = genre
            })).ToList();
            await _bookGenreRepository.CreateRange(genrelist);
        }

        public async Task Delete(int id)
        {
            Book book = await _bookrepository.GetById(id);
            if (book is null)
            {
                throw new Exception("Item is Null");
            }
            await _bookrepository.Delete(book);
        }

        private List<GetAllAuthorViewItem> MapRange(IEnumerable<Author> authors)
        {
            return authors.Select(author => new GetAllAuthorViewItem()
            {
                Id = author.Id,
                Name = author.Name
            }).ToList();
        }

        private List<GetAllGenreItemView> MapRange(IEnumerable<Genre> genres)
        {
            return genres.Select(genre => new GetAllGenreItemView()
            {
                Id = genre.Id,
                Title = genre.Title
            }).ToList();
        }
    }
}
