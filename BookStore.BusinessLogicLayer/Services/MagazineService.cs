﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.MagazineViews;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public class MagazineService : IMagazineService
    {
        private readonly IMagazineRepository _magazineRepository;

        public MagazineService(IMagazineRepository magazineRepository)
        {
            _magazineRepository = magazineRepository;
        }

        public async Task<GetByIdMagazineView> GetById(int id)
        {
            Magazine magazine = await _magazineRepository.GetById(id);
            var result = new GetByIdMagazineView
            {
                Id = magazine.Id,
                Title = magazine.Title,
                Price = magazine.Price,
                Picture = magazine.Picture,
                Description = magazine.Description
            };
            return result;
        }

        public async Task<GetByTitleMagazineView> GetByTitle(string title)
        {
            Magazine magazine = await _magazineRepository.GetByTitle(title);
            var result = new GetByTitleMagazineView
            {
                Id = magazine.Id,
                Title = magazine.Title,
                Price = magazine.Price,
                Description = magazine.Description
            };
            return result;
        }

        public async Task<GetAllMagazineView> GetAll()
        {
            var magazines = (await _magazineRepository.GetAll());
            List<MagazineGetAllMagazineViewItem> magazinesres = magazines.Select(magazine => new MagazineGetAllMagazineViewItem
            {
                Id = magazine.Id,
                Title = magazine.Title,
                Price = magazine.Price,
                Description = magazine.Description,
                Picture = magazine.Picture
            }).ToList();
            var result = new GetAllMagazineView
            {
                MagazineList = magazinesres
            };
            return result;
        }

        public async Task Delete(int id)
        {
            Magazine magazine = await _magazineRepository.GetById(id);
            if (magazine is null)
            {
                throw new Exception("Item not found");
            }
            await _magazineRepository.Delete(magazine);
        }

        public async Task Create(CreateMagazineView createMagazine)
        {
            var magazine = new Magazine
            {
                Title = createMagazine.Title,
                Price = createMagazine.Price,
                Description = createMagazine.Description,
                Picture = createMagazine.Picture
            };
            await _magazineRepository.Create(magazine);
        }

        public async Task Update(UpdateMagazineView updateMagazine)
        {
            Magazine getMagazine = await _magazineRepository.GetById(updateMagazine.Id);
            if (getMagazine is null)
            {
                var newMagazine = new Magazine
                {
                    Title = updateMagazine.Title,
                    Price = updateMagazine.Price,
                    Description = updateMagazine.Description,
                    Picture = updateMagazine.Picture
                };
                await _magazineRepository.Create(newMagazine);
            }
            var magazine = new Magazine
            {
                Id = updateMagazine.Id,
                Title = updateMagazine.Title,
                Price = updateMagazine.Price,
                Description = updateMagazine.Description,
                Picture = updateMagazine.Picture
            };
            await _magazineRepository.Update(magazine);
        }
    }
}
