﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.AuthorViews;
using BookStore.BusinessLogicLayer.Views.BookViews;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IBookRepository _bookRepository;

        public AuthorService(IAuthorRepository authorRepository, IBookRepository bookRepository)
        {
            _authorRepository = authorRepository;
            _bookRepository = bookRepository;
        }

        public async Task<GetByIdAuthorView> GetById(int id)
        {
            Author author = await _authorRepository.GetById(id);
            IEnumerable<Book> bookList = (await _bookRepository.GetBooksByAuthorId(id));
            var bookListres = bookList.Select(book => new BookGetAllBookItemView
            {
                Title = book.Title,
                Id = book.Id,
                Price = book.Price
            }).ToList();
            var result = new GetByIdAuthorView
            {
                Id = author.Id,
                Name = author.Name,
                books = bookListres
            };
            return result;
        }

        public async Task<GetByNameAuthorView> GetByName(string name)
        {
            Author author = await _authorRepository.GetByName(name);
            var result = new GetByNameAuthorView
            {
                Id = author.Id,
                Name = author.Name
            };
            return result;
        }

        public async Task<GetAllAuthorView> GetAll()
        {
            IEnumerable<Author> authors = (await _authorRepository.GetAll());
            List<AuthorGetAllAuthorViewsItem> authorView = authors.Select(author => new AuthorGetAllAuthorViewsItem
            {
                Name = author.Name,
                Id = author.Id
            }).ToList();
            var result = new GetAllAuthorView() { AuthorList = authorView };
            result.AuthorList = authorView;
            return result;
        }

        public async Task Create(CreateAuthorView createAuthor)
        {
            await _authorRepository.Create(new Author() { Name = createAuthor.Name });
        }

        public async Task Update(UpdateAuthorView authorViews)
        {
            Author getAuthor = await _authorRepository.GetById(authorViews.Id);
            if (getAuthor is null)
            {
                await _authorRepository.Create(new Author() { Name = authorViews.Name });
            }
            await _authorRepository.Update(new Author
            {
                Name = authorViews.Name,
                Id = authorViews.Id
            });
        }

        public async Task Delete(int id)
        {
            Author author = await _authorRepository.GetById(id);
            if (author is null)
            {
                throw new Exception("Item is Null");
            }
            await _authorRepository.Delete(author);
        }
    }
}
