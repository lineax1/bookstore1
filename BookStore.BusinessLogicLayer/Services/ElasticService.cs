﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using Microsoft.Extensions.Configuration;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public class ElasticService : IElasticService
    {
        private ElasticClient _client;
        private readonly IConfiguration _configuration;
        private readonly IBookAuthorRepository _bookAuthorRepository;

        public ElasticService(IConfiguration configuration, IBookAuthorRepository bookAuthorRepository)
        {
            _configuration = configuration;
            _bookAuthorRepository = bookAuthorRepository;
        }

        public async Task<BulkResponse> IndexMany()
        {
            IEnumerable<BookAuthor> booksAuthor = (await _bookAuthorRepository.GetAll());

            BulkResponse indexManyResponse = await _client.IndexManyAsync(booksAuthor);
            if (!indexManyResponse.IsValid)
            {
                var ex = indexManyResponse.OriginalException;
                var debEx = indexManyResponse.DebugInformation;
            }
            return indexManyResponse;
        }

        public ElasticClient GetClient()
        {
            if (_client != null)
            {
                return _client;
            }
            InitClient();
            return _client;
        }

        private void InitClient()
        {
            var node = new Uri(_configuration["EsUrl"]);
            using (ConnectionSettings connectionSettings = new ConnectionSettings(node))
            {
                _client = new ElasticClient(connectionSettings.DefaultIndex("demo"));
            }
        }
    }
}
