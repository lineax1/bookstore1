﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Views.PaymentViews
{
    public class PaymentView
    {
        public string CardNumber { get; set; }
        public long CardExpYear { get; set; }
        public long CardExpMonth { get; set; }
        public string CardCvc { get; set; }
        public decimal Amount { get; set; }
        public string Email { get; set; }

        public IEnumerable<int> BookIdList = new List<int>();
        public IEnumerable<int> MagazineIdList = new List<int>();
    }
}
