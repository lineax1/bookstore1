﻿namespace BookStore.BusinessLogicLayer.Views.AccountViews
{
    public class ConfirmRegisterView
    {
        public string Email { get; set; }
        public string Code { get; set; }
    }
}
