﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Views.GenreViews
{
    public class GetAllGenreView
    {
        public List<GenreGetAllGenreItemView> GenreList { get; set; }

        public GetAllGenreView()
        {
            GenreList = new List<GenreGetAllGenreItemView>();
        }
    }

    public class GenreGetAllGenreItemView
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
