﻿namespace BookStore.BusinessLogicLayer.Views.MagazineViews
{
    public class GetByIdMagazineView
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Picture { get; set; }
        public string Description { get; set; }
    }
}
