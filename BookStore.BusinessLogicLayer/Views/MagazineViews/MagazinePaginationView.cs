﻿namespace BookStore.BusinessLogicLayer.Views.MagazineViews
{
    public class MagazinePaginationView
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int Offset { get; set; }
        public int Next { get; set; }
    }
}
