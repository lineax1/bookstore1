﻿namespace BookStore.BusinessLogicLayer.Views.MagazineViews
{
    public class CreateMagazineView
    {
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Picture { get; set; }
        public string Description { get; set; }
    }
}
