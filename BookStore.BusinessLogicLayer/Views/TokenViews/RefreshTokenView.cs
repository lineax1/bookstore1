﻿namespace BookStore.BusinessLogicLayer.Views.TokenViews
{
    public class RefreshTokenView
    {
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
