﻿namespace BookStore.BusinessLogicLayer.Views.TokenViews
{
    public class TokenView
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string Role { get; set; }
    }
}
