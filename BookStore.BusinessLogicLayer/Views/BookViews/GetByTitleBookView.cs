﻿using BookStore.BusinessLogicLayer.Views.AuthorViews;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Views.BookViews
{
    public class GetByTitleBookView
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }

        public List<AuthorGetAllAuthorViewsItem> authorList { get; set; }
    }
}
