﻿using BookStore.BusinessLogicLayer.Views.AuthorViews;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Views.BookViews
{
    public class GetByIdBookView
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }

        public List<AuthorGetAllAuthorViewsItem> AuthorList { get; set; }
    }
}
