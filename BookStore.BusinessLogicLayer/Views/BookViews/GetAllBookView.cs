﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Views.BookViews
{
    public class GetAllBookView
    {
        public List<BookGetAllBookItemView> Books { get; set; }
        public GetAllBookView()
        {
            Books = new List<BookGetAllBookItemView>();
        }
    }

    public class BookGetAllBookItemView
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public IEnumerable<GetAllAuthorViewItem> AuthorList { get; set; }
        public IEnumerable<GetAllGenreItemView> GenreList { get; set; }
    }

    public class GetAllAuthorViewItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class GetAllGenreItemView
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
