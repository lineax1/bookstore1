﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Views.BookViews
{
    public class UpdateBookView
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }

        public IEnumerable<int> genreIdList = new List<int>();
        public IEnumerable<int> authorIdList = new List<int>();

    }
}
