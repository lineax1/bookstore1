﻿using BookStore.BusinessLogicLayer.Views.BookViews;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Views.AuthorViews
{
    public class GetByIdAuthorView
    {
        public string Name { get; set; }
        public int Id { get; set; }

        public List<BookGetAllBookItemView> books;
    }
}
