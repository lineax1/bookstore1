﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Views.AuthorViews
{
    public class GetAllAuthorView
    {
        public IEnumerable<AuthorGetAllAuthorViewsItem> AuthorList { get; set; }

        public GetAllAuthorView()
        {
            AuthorList = new List<AuthorGetAllAuthorViewsItem>();
        }
    }

    public class AuthorGetAllAuthorViewsItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

