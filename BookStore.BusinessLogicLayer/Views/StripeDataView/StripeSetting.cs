﻿using BookStore.BusinessLogicLayer.Services.Interfaces;

namespace BookStore.BusinessLogicLayer.Views.StripeDataView
{
    public class StripeSetting : IStripeSetting
    {
        public string SecretKey { get; set; }
        public string PublishableKey { get; set; }
    }
}
