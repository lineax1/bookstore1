﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Views.OrdersViews
{
    public class GetAllOrdersView
    {
        public List<OrderGetAllOrderItemView> Orders { get; set; }

        public GetAllOrdersView()
        {
            Orders = new List<OrderGetAllOrderItemView>();
        }
    }

    public class OrderGetAllOrderItemView
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }

        public string Email { get; set; }
        public string UserId { get; set; }

        public IEnumerable<GetAllBookViewItem> Books { get; set; }
        public IEnumerable<GetAllMagazineViewItem> Magazines { get; set; }
    }

    public class GetAllBookViewItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
    }

    public class GetAllMagazineViewItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
    }
}
