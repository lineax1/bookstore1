﻿namespace BookStore.BusinessLogicLayer.Views.UserViews
{
    public class CreateUserView
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool EmailConfirmed { get; set; }
    }
}
