﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Views.UserViews
{
    public class GetAllUsersView
    {
        public List<UserGetAllUserItemView> UserList { get; set; }

        public GetAllUsersView()
        {
            UserList = new List<UserGetAllUserItemView>();
        }
    }

    public class UserGetAllUserItemView
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public List<string> Role { get; set; }

        public UserGetAllUserItemView()
        {
            Role = new List<string>();
        }
    }
}
