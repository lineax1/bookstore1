﻿namespace BookStore.BusinessLogicLayer.Views.UserViews
{
   public class ChangeRoleUserView
    {
        public string Id { get; set; }
        public string Role { get; set; }
    }
}
