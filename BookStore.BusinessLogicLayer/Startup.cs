﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using BookStore.BusinessLogicLayer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using BookStore.BusinessLogicLayer.Helpers;
using BookStore.BusinessLogicLayer.Helpers.Interfaces;

namespace BookStore.BusinessLogicLayer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public static void ConfigureServices(IServiceCollection services, string ConnectionString, IConfiguration Configuration)
        {
            services.AddAuthorization(options =>
            {
                options.DefaultPolicy =
                     new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .Build();
            });
            services.Configure<IdentityOptions>(options =>
            {
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(Convert.ToInt32(Configuration["TimeSpan"]));
                options.Lockout.MaxFailedAccessAttempts = Convert.ToInt32(Configuration["AccessAttemp"]);
                options.Lockout.AllowedForNewUsers = true;
            });
            services.AddScoped<IAuthorService, AuthorService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IMagazineService, MagazineService>();
            services.AddScoped<IBookService, BookService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IMainOrderService, MainOrderService>();
            services.AddScoped<IGenreService, GenreService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUploadFile, UploadFile>();
            services.AddScoped<IElasticService, ElasticService>();

            DataAccessLayer.Startup.ConfigureServices(services, ConnectionString);

        }
    }
}
