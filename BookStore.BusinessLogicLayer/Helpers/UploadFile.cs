﻿using BookStore.BusinessLogicLayer.Helpers.Interfaces;
using Dropbox.Api;
using Dropbox.Api.Files;
using Dropbox.Api.Sharing;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Helpers
{
    public class UploadFile : IUploadFile
    {
        private readonly IConfiguration _configuration;

        public UploadFile(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<string> UploadImage(IFormFile file)
         {
            int imgSize = Convert.ToInt32(_configuration["ImageSize"]);
            string tokenDrBox = (_configuration["DropBoxToken"]);
            using (var dbx = new DropboxClient(tokenDrBox))
            {
                using (var image = Image.FromStream(file.OpenReadStream()))
                {
                    if (imgSize <= file.Length)
                    {
                        throw new Exception("NotFormat");
                    }
                }
                string folder = _configuration["FolderPath"];

                using (var memoryStrean = new MemoryStream())
                {
                    var Upload = await dbx.Files.UploadAsync(folder + "/" + file.FileName, WriteMode.Overwrite.Instance, body: file.OpenReadStream());
                    var linksResult = await dbx.Sharing.ListSharedLinksAsync(folder + "/" + file.FileName);
                    if (linksResult.Links.Count == 0)
                    {
                        Task<SharedLinkMetadata> tx = dbx.Sharing.CreateSharedLinkWithSettingsAsync(folder + "/" + file.FileName);
                        tx.Wait();
                        string url = linksResult.Links.First().Url.Replace(_configuration["DropBoxUrl"], _configuration["DropBoxApi"]);
                        return url;
                    }
                    else
                    {
                        string url = linksResult.Links.First().Url.Replace(_configuration["DropBoxUrl"], _configuration["DropBoxApi"]);
                        return url;
                    }
                }
            }
        }
    }
}
