﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Helpers.Interfaces
{
    public interface IUploadFile
    {
        Task<string> UploadImage(IFormFile file);
    }
}
