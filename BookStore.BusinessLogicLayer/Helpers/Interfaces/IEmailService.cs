﻿using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Helpers.Interfaces
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
