﻿using BookStore.DataAccessLayer.Config;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Dapper;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.Dapper
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        public BookRepository(IOptions<ConnectionStrings> connectionConfig) : base(connectionConfig, "Books")
        {
        }

        public async Task<Book> GetByTitle(string title)
        {
            return await Connection.QuerySingleAsync<Book>($"SELECT * FROM Books WHERE Title = @Title", new { Title = title });
        }

        public async Task<IEnumerable<Book>> GetBooksByAuthorId(int authorid)
        {
            return await Connection.QueryAsync<Book>($"SELECT * FROM Books INNER JOIN BookAuthors ON Books.Id = BookAuthors.BookId WHERE BookAuthors.AuthorId = @authorid");
        }
    }
}
