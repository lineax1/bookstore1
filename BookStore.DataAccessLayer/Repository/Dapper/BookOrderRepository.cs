﻿using BookStore.DataAccessLayer.Config;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Microsoft.Extensions.Options;

namespace BookStore.DataAccessLayer.Repository.Dapper
{
    public class BookOrderRepository : GenericRepository<BookOrder>, IBookOrderRepository
    {
        public BookOrderRepository(IOptions<ConnectionStrings> connectionConfig) : base(connectionConfig, "BookOrders")
        {
        }
    }
}
