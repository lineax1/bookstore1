﻿using BookStore.DataAccessLayer.Config;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Microsoft.Extensions.Options;

namespace BookStore.DataAccessLayer.Repository.Dapper
{
    public class MagazineOrderRepository : GenericRepository<MagazineOrder>, IMagazineOrderRepository
    {

        public MagazineOrderRepository(IOptions<ConnectionStrings> connectionConfig) : base(connectionConfig, "MagazineOrders")
        {
        }
    }
}

