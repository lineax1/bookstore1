﻿using BookStore.DataAccessLayer.Config;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Dapper;

namespace BookStore.DataAccessLayer.Repository.Dapper
{
    public class BookAuthorRepository : GenericRepository<BookAuthor>, IBookAuthorRepository
    {
        public BookAuthorRepository(IOptions<ConnectionStrings> connectionConfig) : base(connectionConfig, "BookAuthors")
        {
        }

        public async Task DeleteRangeByBookId(int bookId)
        {
            var sqlQuery = $"DELETE FROM BookAuthors WHERE BookId = @BookId";
            await Connection.ExecuteAsync(sqlQuery, new { BookId = bookId });
        }
    }
}
