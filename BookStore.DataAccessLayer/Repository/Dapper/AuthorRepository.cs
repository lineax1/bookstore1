﻿using BookStore.DataAccessLayer.Config;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Dapper;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.Dapper
{
    public class AuthorRepository : GenericRepository<Author>, IAuthorRepository
    {

        public AuthorRepository(IOptions<ConnectionStrings> connectionConfig) : base(connectionConfig, "Authors")
        {
        }

        public async Task<Author> GetByName(string name)
        {
            var sql = $"SELECT * FROM Authors WHERE Name = @Name";

            var result = await Connection.QuerySingleAsync<Author>(sql, new { Name = name });
            return result;
        }
    }
}
