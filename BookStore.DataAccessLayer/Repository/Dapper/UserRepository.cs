﻿using BookStore.DataAccessLayer.Config;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Models;
using Dapper;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.Dapper
{
    public class UserRepository : IUserRepository
    {

        private readonly string _connectionStrings;

        protected IDbConnection Connection
        {
            get { return new SqlConnection(_connectionStrings); }
        }

        public UserRepository(IOptions<ConnectionStrings> connectionConfigs)
        {
            var connection = connectionConfigs.Value;
            _connectionStrings = connection.DefaultConnection;
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            return (await Connection.QueryAsync<User>($"SELECT * FROM AspNetUsers")).ToList();
        }

        public async Task<IEnumerable<AspNetUserRole>> GetUserWithRole()
        {
            var orderDictionary = new Dictionary<string, AspNetUserRole>();
            string sql = @"SELECT * FROM AspNetUserRoles AS USERROLE  JOIN AspNetRoles AS ROLE ON USERROLE.RoleId = ROLE.Id";

            var result = await Connection.QueryAsync<AspNetUserRole, AspNetRole, AspNetUserRole>
            (sql, (usrole, role) =>
            {
                if (!orderDictionary.TryGetValue(usrole.UserId, out AspNetUserRole userRole))
                {
                    orderDictionary.Add(usrole.RoleId, userRole = usrole);
                }
                if (userRole.Role == null)
                {
                    userRole.Role = new List<AspNetRole>();
                }
                userRole.Role.Add(role);

                return userRole;
            });
            return result;
        }

        public async Task Delete(string Id)
        {
            var sqlQuery = $"DELETE FROM AspNetUsers WHERE Id = @Id";
            await Connection.ExecuteAsync(sqlQuery, new { Id });
        }
    }
}
