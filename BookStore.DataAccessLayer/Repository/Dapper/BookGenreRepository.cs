﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using BookStore.DataAccessLayer.Interfaces;
using Microsoft.Extensions.Options;
using BookStore.DataAccessLayer.Config;
using System.Threading.Tasks;
using Dapper;

namespace BookStore.DataAccessLayer.Repository.Dapper
{
    public class BookGenreRepository : GenericRepository<BookGenre>, IBookGenreRepository
    {
        public BookGenreRepository(IOptions<ConnectionStrings> connectionConfig) : base(connectionConfig, "BookGenres")
        {
        }

        public async Task DeleteRangeByBookId(int bookId)
        {
            var sqlQuery = $"DELETE FROM BookGenres WHERE BookId = @bookId";
            await Connection.ExecuteAsync(sqlQuery, new { BookId = bookId });
        }
    }
}
