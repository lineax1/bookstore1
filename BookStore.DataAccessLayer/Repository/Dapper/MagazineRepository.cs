﻿using BookStore.DataAccessLayer.Config;
using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Dapper;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.Dapper
{

    public class MagazineRepository : GenericRepository<Magazine>, IMagazineRepository
    {

        public MagazineRepository(IOptions<ConnectionStrings> connectionConfig) : base(connectionConfig, "Magazines")
        {
        }

        public async Task<Magazine> GetByTitle(string Title)
        {
            return await Connection.QuerySingleAsync<Magazine>($"SELECT * FROM Magazines WHERE Title = @Title", new { title = Title });
        }
    }
}
