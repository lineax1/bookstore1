﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;

namespace BookStore.DataAccessLayer.Repository.EF
{
    public class GenreRepositoryEF:GenericRepositoryEF<Genre>, IGenreRepository
    {
        private readonly ApplicationContext _context;

        public GenreRepositoryEF(ApplicationContext context) : base(context)
        {
            _context = context;
        }
    }
}
