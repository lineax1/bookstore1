﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;

namespace BookStore.DataAccessLayer.Repository.EF
{
    public class MagazineOrderRepositoryEF : GenericRepositoryEF<MagazineOrder>, IMagazineOrderRepository
    {
        private readonly ApplicationContext _context;
        private readonly DbSet<MagazineOrder> _dbSet;
        
        public MagazineOrderRepositoryEF(ApplicationContext context): base(context)
        {
            _context = context;
            _dbSet = context.Set<MagazineOrder>();
        }
    }
}
