﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.EF
{
    public class AuthorRepositoryEF : GenericRepositoryEF<Author>, IAuthorRepository
    {

        private readonly ApplicationContext _context;

        public AuthorRepositoryEF(ApplicationContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Author> GetByName(string name)
        {
            return await _context.Authors.Where(author => author.Name == name).FirstOrDefaultAsync();
        }
    }
}

