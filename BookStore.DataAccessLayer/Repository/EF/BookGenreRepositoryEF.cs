﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.EF
{
    public class BookGenreRepositoryEF: GenericRepositoryEF<BookGenre>, IBookGenreRepository
    {
        private readonly ApplicationContext _context;
        private readonly DbSet<BookGenre> _dbSet;

        public BookGenreRepositoryEF(ApplicationContext context): base(context)
        {
            _context = context;
            _dbSet = context.Set<BookGenre>();
        }

        public async Task DeleteRangeByBookId(int genreId)
        {
            BookGenre item = await _dbSet.FindAsync(genreId);
            if (item != null)
            {
                _dbSet.Attach(item);
                _dbSet.Remove(item);
            }
            await _context.SaveChangesAsync();
        }
    }
}
