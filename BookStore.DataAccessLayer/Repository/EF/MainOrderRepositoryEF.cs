﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.EF
{
    public class MainOrderRepositoryEF : GenericRepositoryEF<MainOrder>, IMainOrderRepository
    {

        private readonly ApplicationContext _context;

        public MainOrderRepositoryEF(ApplicationContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<MainOrder>> GetByUserId(string id)
        {
            return  _context.MainOrders.Where(order => order.UserId == id);
        }
    }
}
