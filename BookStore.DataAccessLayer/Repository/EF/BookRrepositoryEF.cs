﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.EF
{
    public class BookRrepositoryEF : GenericRepositoryEF<Book>, IBookRepository
    {
        private readonly ApplicationContext _context;

        public BookRrepositoryEF(ApplicationContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Book>> GetBooksByAuthorId(int authorid)
        {
            return _context.Books.Where(book => book.Id == authorid);
        }

        public async Task<Book> GetByTitle(string title)
        {
            return await _context.Books.Where(book => book.Title == title).FirstOrDefaultAsync();
        }
    }
}
