﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;

namespace BookStore.DataAccessLayer.Repository.EF
{
    public class BookOrderRepositoryEF: GenericRepositoryEF<BookOrder>, IBookOrderRepository
    {
        private readonly ApplicationContext _context;
        private readonly DbSet<BookOrder> _dbSet;

        public BookOrderRepositoryEF(ApplicationContext context): base(context)
        {
            _context = context;
            _dbSet = context.Set<BookOrder>();
        }
    }
}
