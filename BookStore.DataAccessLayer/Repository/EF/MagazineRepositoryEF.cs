﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.EF
{
    public class MagazineRepositoryEF : GenericRepositoryEF<Magazine>, IMagazineRepository
    {
        private readonly ApplicationContext _context;

        public MagazineRepositoryEF(ApplicationContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Magazine> GetByTitle(string title)
        {
            return await _context.Magazines.Where(magazine => magazine.Title == title).FirstOrDefaultAsync();
        }
    }
}
