﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.EF
{
    public class BookAuthorRepositoryEF : GenericRepositoryEF<BookAuthor>, IBookAuthorRepository
    {

        private readonly ApplicationContext _context;
        private readonly DbSet<BookAuthor> _dbSet;

        public BookAuthorRepositoryEF(ApplicationContext context) : base(context)
        {
            _context = context;
            _dbSet = context.Set<BookAuthor>();
        }

        public async Task DeleteRangeByBookId(int bookid)
        {
            BookAuthor item = await _dbSet.FindAsync(bookid);
            if (item != null)
            {
                _dbSet.Attach(item);
                _dbSet.Remove(item);
            }
            await _context.SaveChangesAsync();
        }
    }
}

