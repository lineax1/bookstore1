﻿using BookStore.DataAccessLayer.Config;
using BookStore.DataAccessLayer.GenericRepository;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.GenericRepository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly string _tableName;
        protected readonly string _connectionString;

        protected IDbConnection Connection
        {
            get { return new SqlConnection(_connectionString); }
        }

        public GenericRepository(IOptions<ConnectionStrings> connectionConfig, string tableName)
        {
            var connection = connectionConfig.Value;
            _connectionString = connection.DefaultConnection;
            _tableName = tableName;
        }

        public async Task<TEntity> GetById(int id)
        {
            return await Connection.GetAsync<TEntity>(id);
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            return await Connection.GetAllAsync<TEntity>();
        }

        public virtual async Task Update(TEntity entity)
        {
            var columns = GetColumns();
            var stringOfColumns = string.Join(", ", columns.Select(e => $"{e} = @{e}"));
            var query = $"UPDATE {_tableName} SET {stringOfColumns} WHERE Id = @Id";

            await Connection.ExecuteAsync(query, entity);
        }

        public virtual async Task UpdateRange(IEnumerable<TEntity> entities)
        {
            var columns = GetColumns();
            var stringOfColumns = string.Join(", ", columns.Select(e => $"{e} = @{e}"));
            var query = $"update {_tableName} set {stringOfColumns} where Id = @Id";

            await Connection.ExecuteAsync(query, entities);
        }

        public async Task<IEnumerable<TEntity>> GetAllItemsById(List<int> ids)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var sql = $"SELECT * FROM {typeof(TEntity).Name}s WHERE Id IN @Ids";
                return await db.QueryAsync<TEntity>(sql, new { Ids = ids.ToArray() });
            }
        }

        public async Task<int> Create(TEntity item)
        {
            var sql = string.Empty;
            var columns = GetColumns();
            var stringOfColumns = string.Join(", ", columns);
            var stringOfParameters = string.Join(", ", columns.Select(e => "@" + e));
            sql += $"INSERT INTO {_tableName} ({stringOfColumns}) VALUES ({stringOfParameters});";
            return await Connection.InsertAsync(item);
        }

        public async Task CreateRange(IEnumerable<TEntity> entity)
        {
            var sql = string.Empty;
            var columns = GetColumns();
            var stringOfColumns = string.Join(", ", columns);
            var stringOfParameters = string.Join(", ", columns.Select(e => "@" + e));
            sql += $"insert into {_tableName} ({stringOfColumns}) values ({stringOfParameters});";
            await Connection.ExecuteAsync(sql, entity);
        }

        public async Task Delete(TEntity entity)
        {
            await Connection.DeleteAsync(entity);
        }

        protected IEnumerable<string> GetColumns()
        {
            var a = typeof(TEntity).GetProperties();
            var b = a.Where(e => !Attribute.IsDefined(e, typeof(ForeignKeyAttribute)) && !Attribute.IsDefined(e, typeof(NotMappedAttribute)) && !Attribute.IsDefined(e, typeof(DatabaseGeneratedAttribute))
            && !Attribute.IsDefined(e, typeof(ComputedAttribute))).ToList();
            var c = b.Select(e => e.Name);
            return c;
        }
    }
}
