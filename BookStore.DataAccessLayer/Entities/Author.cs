﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccessLayer.Entities
{
    public class Author : BaseEntity
    {
        public string Name { get; set; }

        [Write(false)]
        [NotMapped]
        public IEnumerable<BookAuthor> BookAuthors { get; set; }
    }
}
