﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccessLayer.Entities
{
    public class Book : BaseEntity
    {
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }

        [NotMapped]
        [Write(false)]
        public IEnumerable<BookAuthor> BookAuthors { get; set; }
        [NotMapped]
        [Write(false)]
        public IEnumerable<BookOrder> BookOrders { get; set; }
        [NotMapped]
        [Write(false)]
        public IEnumerable<BookGenre> BookGenres { get; set; }
    }
}
