﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Dapper.Contrib.Extensions;

namespace BookStore.DataAccessLayer.Entities
{
    public class Genre : BaseEntity
    {
        public string Title { get; set; }
        [NotMapped]
        [Write(false)]
        public IEnumerable<BookGenre> BookGenres { get; set; }
    }
}
