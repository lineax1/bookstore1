﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccessLayer.Entities
{
    public class MainOrder : BaseEntity
    {
        public string UserId { get; set; }
        public decimal Amount { get; set; }

        [NotMapped]
        [Write(false)]
        public IEnumerable<BookOrder> BookOrders { get; set; }

        [NotMapped]
        [Write(false)]
        public IEnumerable<MagazineOrder> MagazineOrders { get; set; }

        [NotMapped]
        [Write(false)]
        public virtual User User { get; set; }
    }
}
