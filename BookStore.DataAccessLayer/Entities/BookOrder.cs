﻿using Dapper.Contrib.Extensions;

namespace BookStore.DataAccessLayer.Entities
{
    public class BookOrder: BaseEntity
    {
        public int BookId { get; set; }
        public int MainOrderId { get; set; }
        [Write(false)]
        public virtual Book Book { get; set; }
        [Write(false)]
        public virtual MainOrder MainOrder { get; set; }
    }
}
