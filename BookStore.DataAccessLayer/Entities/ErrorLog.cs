﻿using BookStore.DataAccessLayer.Entities;
namespace BookStore.DataAccessLayer.Entities
{
    public class ErrorLog : BaseEntity
    {
        public string MessageStatus {get;set;}
        public string StackTrace { get; set; }
    }
}
