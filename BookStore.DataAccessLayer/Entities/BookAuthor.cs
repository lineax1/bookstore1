﻿using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccessLayer.Entities
{
    public class BookAuthor: BaseEntity
    {
        public int BookId { get; set; } 
        public int AuthorId { get; set; }

        [NotMapped]
        [Write(false)]
        public virtual Book Book { get; set; }
        [NotMapped]
        [Write(false)]
        public virtual Author Author { get; set; }
    }
}
