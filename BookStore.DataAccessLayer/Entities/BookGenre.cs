﻿using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccessLayer.Entities
{
    public class BookGenre: BaseEntity
    {
        public int BookId { get; set; }
        public int GenreId { get; set; }  
        [NotMapped]
        [Write(false)]
        public Genre Genre { get; set; }
        [NotMapped]
        [Write(false)]
        public Book Book { get; set; }
    }
}
