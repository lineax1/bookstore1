﻿using Dapper.Contrib.Extensions;

namespace BookStore.DataAccessLayer.Entities
{
    public class MagazineOrder: BaseEntity
    {
        public int MagazineId { get; set; }
        public int MainOrderId { get; set; }
        [Write(false)]
        public virtual Magazine Magazine { get; set; }
        [Write(false)]
        public virtual MainOrder MainOrder { get; set; }
    }
}
