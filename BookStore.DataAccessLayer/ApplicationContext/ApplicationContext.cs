﻿using BookStore.DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BookStore.DataAccessLayer
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Magazine> Magazines { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<BookAuthor> BookAuthors { get; set; }
        public DbSet<BookOrder> BookOrders { get; set; }
        public DbSet<BookGenre> BookGenres { get; set; }
        public DbSet<MagazineOrder> MagazineOrders { get; set; }
        public DbSet<MainOrder> MainOrders { get; set; }
        public DbSet<ErrorLog> ErrorLogs { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
           : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Book>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<Book>()
                .Property(x => x.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<Author>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<Author>()
                .Property(x => x.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<Magazine>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<Magazine>()
                .Property(x => x.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<Genre>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<Genre>()
                .Property(x => x.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<User>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<User>()
                .Property(x => x.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<MainOrder>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<MainOrder>()
                .Property(x => x.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<MagazineOrder>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<MagazineOrder>()
                .Property(x => x.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<BookAuthor>()
              .HasKey(x => x.Id);
            modelBuilder.Entity<BookAuthor>()
                .Property(x => x.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<BookOrder>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<BookOrder>()
                .Property(x => x.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<BookOrder>()
                .HasOne(x => x.Book)
                .WithMany(x => x.BookOrders);
            modelBuilder.Entity<BookOrder>()
                .HasOne(x => x.MainOrder)
                .WithMany(x => x.BookOrders);

            modelBuilder.Entity<BookAuthor>()
                .HasOne(x => x.Book)
                .WithMany(m => m.BookAuthors);
            modelBuilder.Entity<BookAuthor>()
                .HasOne(x => x.Author)
                .WithMany(m => m.BookAuthors);
            modelBuilder.Entity<BookAuthor>();

            modelBuilder.Entity<MagazineOrder>()
                .HasOne(x => x.Magazine)
                .WithMany(x => x.MagazineOrders);
            modelBuilder.Entity<MagazineOrder>()
                .HasOne(x => x.MainOrder)
                .WithMany(x => x.MagazineOrders);

            modelBuilder.Entity<BookGenre>()
                .HasOne(x => x.Book)
                .WithMany(x => x.BookGenres);
            modelBuilder.Entity<BookGenre>()
                .HasOne(x => x.Genre)
                .WithMany(x => x.BookGenres);

            modelBuilder.Entity<MainOrder>()
                .HasOne(x => x.User)
                .WithMany(x => x.MainOrders);
            modelBuilder.Entity<User>()
                .HasMany(x => x.MainOrders)
                .WithOne(x => x.User);
        }
    }
}

