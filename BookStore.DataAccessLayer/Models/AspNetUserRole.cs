﻿using System.Collections.Generic;

namespace BookStore.DataAccessLayer.Models
{
    public class AspNetUserRole
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public ICollection<AspNetRole> Role { get; set; }
    }

    public class AspNetRole
    {
        public string Name { get; set; }
        public string Role { get; set; }
    }
}

