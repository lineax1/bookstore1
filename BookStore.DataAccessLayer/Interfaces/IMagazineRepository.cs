﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.GenericRepository;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Interfaces
{
    public interface IMagazineRepository : IGenericRepository<Magazine>
    {
        Task<Magazine> GetByTitle(string Title);
    }
}
