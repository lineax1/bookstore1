﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.GenericRepository;

namespace BookStore.DataAccessLayer.Interfaces
{
    public interface IBookOrderRepository: IGenericRepository<BookOrder>
    {
    }
}
