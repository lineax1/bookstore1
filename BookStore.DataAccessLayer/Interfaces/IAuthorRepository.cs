﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.GenericRepository;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Interfaces
{
    public interface IAuthorRepository : IGenericRepository<Author>
    {
        Task<Author> GetByName(string name);
    }
}

