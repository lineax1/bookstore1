﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Interfaces
{
    public interface IUserRepository
    {
        Task<IEnumerable<AspNetUserRole>> GetUserWithRole();
        Task<IEnumerable<User>> GetAllUsers();
        Task Delete(string id);
    }
}
