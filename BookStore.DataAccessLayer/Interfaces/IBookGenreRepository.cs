﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.GenericRepository;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Interfaces
{
    public interface IBookGenreRepository: IGenericRepository<BookGenre>
    {
        Task DeleteRangeByBookId(int genreId);
    }
}
