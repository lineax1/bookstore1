﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.GenericRepository;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Interfaces
{
    public interface IBookAuthorRepository : IGenericRepository<BookAuthor>
    {
        Task DeleteRangeByBookId(int bookId);
    }
}

