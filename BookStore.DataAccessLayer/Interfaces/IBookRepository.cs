﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.GenericRepository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Interfaces
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        Task<Book> GetByTitle(string Title);
        Task<IEnumerable<Book>> GetBooksByAuthorId(int authorid);
    }
}