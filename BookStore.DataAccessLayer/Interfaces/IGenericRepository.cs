﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.GenericRepository
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<TEntity> GetById(int Id);
        Task<IEnumerable<TEntity>> GetAll();
        Task<int> Create(TEntity entity);
        Task CreateRange(IEnumerable<TEntity> entities);
        Task Delete(TEntity id);
        Task<IEnumerable<TEntity>> GetAllItemsById(List<int> ids);
        Task UpdateRange(IEnumerable<TEntity> entities);
        Task Update(TEntity entity);
    }
}
