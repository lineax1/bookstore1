﻿using BookStore.DataAccessLayer.Entities;
using BookStore.DataAccessLayer.Interfaces;
using BookStore.DataAccessLayer.Repository.Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Data;
using System.Data.SqlClient;

namespace BookStore.DataAccessLayer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public static void ConfigureServices(IServiceCollection services, string ConnectionString)
        {
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(ConnectionString));
            services.AddTransient<IDbConnection>(db => new SqlConnection(ConnectionString));
            services.AddIdentity<User, IdentityRole>().AddEntityFrameworkStores<ApplicationContext>().AddDefaultTokenProviders();
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<IMagazineRepository, MagazineRepository>();
            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<IBookAuthorRepository, BookAuthorRepository>();
            services.AddScoped<IBookOrderRepository, BookOrderRepository>();
            services.AddScoped<IMagazineOrderRepository, MagazineOrderRepository>();
            services.AddScoped<IMainOrderRepository, MainOrderRepository>();
            services.AddScoped<IBookGenreRepository, BookGenreRepository>();
            services.AddScoped<IGenreRepository, GenreRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            var serviceProvider = services.BuildServiceProvider();
            AdminCreate.AdminCreate.DbInitializer(serviceProvider.GetService<UserManager<User>>(), serviceProvider.GetService<RoleManager<IdentityRole>>(), serviceProvider.GetService<IConfiguration>());
        }
    }
}
