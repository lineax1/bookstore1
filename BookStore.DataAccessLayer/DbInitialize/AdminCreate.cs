﻿using BookStore.DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace BookStore.DataAccessLayer.AdminCreate
{
    public static class AdminCreate
    {

        public static async void DbInitializer(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            User currentUser = await userManager.FindByNameAsync(configuration["AdminEmail"]);
            if (currentUser != null)
            {
                return;
            }
            await roleManager.CreateAsync(new IdentityRole("admin"));
            await roleManager.CreateAsync(new IdentityRole("user"));
            if (userManager.Users.Any())
            {
                return;
            }
            var user = new User()
            {
                UserName = configuration["AdminEmail"],
                Email = configuration["AdminEmail"],
            };
            string password = configuration["AdminPassword"];
            await userManager.CreateAsync(user, password);
            await userManager.AddToRoleAsync(user, "admin");
        }
    }
}
