﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStore.DataAccessLayer.Migrations
{
    public partial class RefreshDataBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MainOrders_AspNetUsers_UserId1",
                table: "MainOrders");

            migrationBuilder.DropIndex(
                name: "IX_MainOrders_UserId1",
                table: "MainOrders");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "MainOrders");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "MainOrders",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "Amount",
                table: "MainOrders",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_MainOrders_UserId",
                table: "MainOrders",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_MainOrders_AspNetUsers_UserId",
                table: "MainOrders",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MainOrders_AspNetUsers_UserId",
                table: "MainOrders");

            migrationBuilder.DropIndex(
                name: "IX_MainOrders_UserId",
                table: "MainOrders");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "MainOrders",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Amount",
                table: "MainOrders",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<string>(
                name: "UserId1",
                table: "MainOrders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MainOrders_UserId1",
                table: "MainOrders",
                column: "UserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_MainOrders_AspNetUsers_UserId1",
                table: "MainOrders",
                column: "UserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
