﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStore.DataAccessLayer.Migrations
{
    public partial class addPicture : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Picture",
                table: "Magazines",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Picture",
                table: "Books",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Picture",
                table: "Magazines");

            migrationBuilder.DropColumn(
                name: "Picture",
                table: "Books");
        }
    }
}
