﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStore.DataAccessLayer.Migrations
{
    public partial class ord : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalPrice",
                table: "MainOrders");

            migrationBuilder.RenameColumn(
                name: "DataOrder",
                table: "MainOrders",
                newName: "CreationDate");

            migrationBuilder.RenameColumn(
                name: "MainOrderId",
                table: "MainOrders",
                newName: "Id");

            migrationBuilder.AddColumn<int>(
                name: "Amount",
                table: "MainOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "UserId1",
                table: "MainOrders",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BookOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    BookId = table.Column<int>(nullable: false),
                    MainOrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BookOrders_Books_BookId",
                        column: x => x.BookId,
                        principalTable: "Books",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BookOrders_MainOrders_MainOrderId",
                        column: x => x.MainOrderId,
                        principalTable: "MainOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MagazineOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    MagazineId = table.Column<int>(nullable: false),
                    MainOrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MagazineOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MagazineOrders_Magazines_MagazineId",
                        column: x => x.MagazineId,
                        principalTable: "Magazines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MagazineOrders_MainOrders_MainOrderId",
                        column: x => x.MainOrderId,
                        principalTable: "MainOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MainOrders_UserId1",
                table: "MainOrders",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_BookOrders_BookId",
                table: "BookOrders",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_BookOrders_MainOrderId",
                table: "BookOrders",
                column: "MainOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_MagazineOrders_MagazineId",
                table: "MagazineOrders",
                column: "MagazineId");

            migrationBuilder.CreateIndex(
                name: "IX_MagazineOrders_MainOrderId",
                table: "MagazineOrders",
                column: "MainOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_MainOrders_AspNetUsers_UserId1",
                table: "MainOrders",
                column: "UserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MainOrders_AspNetUsers_UserId1",
                table: "MainOrders");

            migrationBuilder.DropTable(
                name: "BookOrders");

            migrationBuilder.DropTable(
                name: "MagazineOrders");

            migrationBuilder.DropIndex(
                name: "IX_MainOrders_UserId1",
                table: "MainOrders");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "MainOrders");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "MainOrders");

            migrationBuilder.RenameColumn(
                name: "CreationDate",
                table: "MainOrders",
                newName: "DataOrder");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "MainOrders",
                newName: "MainOrderId");

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPrice",
                table: "MainOrders",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
